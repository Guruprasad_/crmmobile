import android.content.Context
import android.os.Environment
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.ResponseModel.AccountAttachmentResModel
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.AccountNotesListitemBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.FileOutputStream
import java.util.UUID

class AccountNotesAdapter(val context: Context, private var datalist: List<AccountAttachmentResModel>): RecyclerView.Adapter<AccountNotesAdapter.ViewHolder>() {
    class ViewHolder(val binding: AccountNotesListitemBinding, val context: Context, private val datalist: List<AccountAttachmentResModel>
    ): RecyclerView.ViewHolder(binding.root) {
        init {
            binding.file.setOnClickListener {
                val position = adapterPosition
                if (position != RecyclerView.NO_POSITION){
                    val note = datalist[position]
                    val noteId = note.id
                    notePreview(noteId)
                }
            }
        }

        private fun notePreview(noteId: Int) {
            val jwtToken = getToken()
            if (jwtToken != null) {
                val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
                val call = service.notePreview("Bearer $jwtToken", noteId)
                call.enqueue(object : Callback<ResponseBody> {
                    override fun onResponse(
                        call: Call<ResponseBody>,
                        response: Response<ResponseBody>
                    ) {
                        if (response.isSuccessful){
                            val file = response.body()
                            if (file != null){
                                downloadAttachment(file)
                            }
                        }
                        else{
                            Constants.error(context, "Unsuccessful response: ${response.code()}")
                        }
                    }

                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        Constants.error(context, "Response is Unsuccessful: ${t.message}")
                    }

                })

            } else {
                Constants.error(context, "Token is null. Please log in again.")
            }
        }

        private fun downloadAttachment(file: ResponseBody) {

            CoroutineScope(Dispatchers.IO).launch {
                try {
                    val fileName = datalist[position].fileName
                    val fileNameOnDevice = UUID.randomUUID().toString() + fileName
                    val downloadsDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
                    val fileOnDevice = File(downloadsDir, fileNameOnDevice)

                    // Write the file content to the device's storage
                    FileOutputStream(fileOnDevice).use { output ->
                        output.write(file.bytes())
                    }

                    showToast("Note downloaded successfully")
                } catch (e: Exception) {
                    e.printStackTrace()
                    showToast("Failed to download Note")
                }
            }
        }

        private suspend fun showToast(message: String) {
            withContext(Dispatchers.Main) {
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
            }
        }

        private fun getToken(): String? {
            return context.getSharedPreferences("Token", Context.MODE_PRIVATE)
                .getString("jwtToken", null)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = AccountNotesListitemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding, context, datalist)
    }

    override fun getItemCount(): Int {
        return datalist.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.binding.createdDate.text = datalist[position].createdDate
        holder.binding.file.text = datalist[position].fileName
    }
}
