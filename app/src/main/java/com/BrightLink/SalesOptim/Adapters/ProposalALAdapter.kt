package com.BrightLink.SalesOptim.Adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.BrightLink.SalesOptim.Activities.AssignLead.Popups.ProposalALCustomer
import com.BrightLink.SalesOptim.Activities.AssignLead.Popups.ProposalALOwn
import com.BrightLink.SalesOptim.Activities.AssignLead.Popups.ProposalALProduct
import com.BrightLink.SalesOptim.Activities.AssignLead.Popups.ProposalALTandC

class ProposalALAdapter(fm: FragmentManager, lifecycle: Lifecycle) : FragmentStateAdapter(fm, lifecycle) {
    override fun getItemCount(): Int {
        return 4
    }

    override fun createFragment(position: Int): Fragment {
        return when(position){
            0 -> ProposalALOwn()
            1 -> ProposalALCustomer()
            2 -> ProposalALProduct()
            3 -> ProposalALTandC()
            else -> throw IllegalStateException("Invalid position")
        }
    }
}