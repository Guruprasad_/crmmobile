package com.BrightLink.SalesOptim.Adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.viewpager2.adapter.FragmentStateAdapter
import  androidx.lifecycle.Lifecycle
import com.BrightLink.SalesOptim.Activities.quotation.CustomerFragment
import com.BrightLink.SalesOptim.Activities.quotation.OwnFragment
import com.BrightLink.SalesOptim.Activities.quotation.ProductFragment
import com.BrightLink.SalesOptim.Activities.quotation.TermsAndConditionsFragment

class QuotationAdapter(fm: FragmentManager, lifecycle: Lifecycle) : FragmentStateAdapter(fm, lifecycle) {

    override fun getItemCount(): Int {
        return 4
    }

    override fun createFragment(position: Int): Fragment {
        return when(position){
            0 -> OwnFragment()
            1 -> CustomerFragment()
            2 -> ProductFragment()
            3 -> TermsAndConditionsFragment()
            else -> throw IllegalStateException("Invalid position")
        }
    }
}