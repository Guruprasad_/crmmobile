package com.BrightLink.SalesOptim.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.RecyclerView
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.ResponseModel.ShowContactModelItem
import com.BrightLink.SalesOptim.databinding.GetcontactBinding
import java.util.Locale

class ContactAdapter(private val context: Context, private val dataList: List<ShowContactModelItem>) : RecyclerView.Adapter<ContactAdapter.ViewHolder>() {

    private var filteredList: List<ShowContactModelItem> = dataList

    private lateinit var mListener: onItemClickListener
    //
    interface onItemClickListener{
        fun onItemClick(position: Int)
    }
    fun setOnItemClickListener(listener: onItemClickListener){
        mListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = GetcontactBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding,mListener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentItem = filteredList[position]


        with(holder.binding) {
            contactName.text = currentItem.fullName
            accountName.text = currentItem.accountName
            emailContact.text = currentItem.email
            phoneContact.text = currentItem.phone
            DepartmentContact.text = currentItem.department


            // Animation
            layout.startAnimation(
                AnimationUtils.loadAnimation(
                    holder.itemView.context,
                    R.anim.recycler_view_anime_2
                )
            )
        }

        holder.itemView.setOnClickListener {
            mListener.onItemClick(position)
        }
    }


    fun filter(text: String) {
        val searchText = text.lowercase(Locale.getDefault())
        filteredList = if (searchText.isEmpty()) {
            dataList
        } else {
            dataList.filter { item ->
                item.accountName!!.lowercase(Locale.getDefault()).contains(searchText) ||
                        item.phone!!.lowercase(Locale.getDefault()).contains(searchText) ||
                        item.fullName!!.lowercase(Locale.getDefault()).contains(searchText)

            }
        }
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return filteredList.size
    }

    inner class ViewHolder(val binding: GetcontactBinding, listener: onItemClickListener) : RecyclerView.ViewHolder(binding.root){
        init {
            itemView.setOnClickListener {
                listener.onItemClick(adapterPosition)
            }
        }
    }
}
