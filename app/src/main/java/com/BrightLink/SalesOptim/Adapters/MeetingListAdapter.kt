package com.BrightLink.SalesOptim.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.RecyclerView
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.ResponseModel.MeetingListModelItem
import com.BrightLink.SalesOptim.databinding.HeadingsMeetingListBinding

class MeetingListAdapter(val context : Context , private var dataList: List<MeetingListModelItem>) : RecyclerView.Adapter<MeetingListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = HeadingsMeetingListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.binding.title.text = dataList[position].title
        holder.binding.from.text = dataList[position].meetingFrom
        holder.binding.to.text  = dataList[position].meetingTo
        holder.binding.contactName.text=dataList[position].contactName
        holder.binding.meetingType.text = dataList[position].locationType
        holder.binding.meetingLink.text = dataList[position].meetingLinkOrLocation
        holder.binding.layout.startAnimation(
            AnimationUtils.loadAnimation(
                holder.itemView.context,
                R.anim.recycler_view_anime_2
            )
        )

    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    inner class ViewHolder( val binding: HeadingsMeetingListBinding) : RecyclerView.ViewHolder(binding.root) {
    }
}
