package com.BrightLink.SalesOptim.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.ColorStateList
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.core.content.ContextCompat
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.ResponseModel.ShowLeadsModelItem
import com.BrightLink.SalesOptim.databinding.LeadTableBinding
import java.util.Locale


class LeadAdapter(val context : Context,
                  private var dataList: List<ShowLeadsModelItem>) :
    RecyclerView.Adapter<LeadAdapter.ViewHolder>() {
    inner class ViewHolder( val binding: LeadTableBinding) : RecyclerView.ViewHolder(binding.root) {

    }

    private val sharedPreferences = context.getSharedPreferences("LeadPreferences", Context.MODE_PRIVATE)

    private var filteredList: List<ShowLeadsModelItem> = dataList


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = LeadTableBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val currentItem = filteredList[position]

        holder.binding.customer.text = currentItem.customerName
        holder.binding.revenue.text = currentItem.totalRevenue.toString()
        holder.binding.leadSource.text = currentItem.leadSource
        holder.binding.leadType.text = currentItem.leadType
        val status = currentItem.leadStatus
        statusColor(status, holder)
        holder.binding.leadStatus.text = status


        holder.binding.layout.startAnimation(
            AnimationUtils.loadAnimation(
                holder.itemView.context,
                R.anim.recycler_view_anime_2
            )
        )

        holder.binding.layout.setOnClickListener {
            val bundle = Bundle()
//            bundle.putInt("leadId",dataList.get(position).leadId)
            bundle.putString("phone",dataList.get(position).mobileNumber)
            bundle.putString("email",dataList.get(position).email)
            bundle.putString("revenue",dataList.get(position).leadRevenue)
            bundle.putString("leadStatus",dataList.get(position).leadStatus)

            Navigation.findNavController(it).navigate(R.id.assignLead,bundle)

            val leadId = dataList.get(position).leadId
            val leadStatus = dataList.get(position).leadStatus

            sharedPreferences.edit().putInt("leadId", leadId).apply()
            sharedPreferences.edit().putString("leadStatus", leadStatus).apply()
        }

    }

    @SuppressLint("ResourceAsColor")
    private fun statusColor(status: String, holder: ViewHolder) {
        if (status == "New Lead")
        {
            changeTextViewBackgroundTint(context,R.color.newLead,holder)
        }
        if (status == "Contacted")
        {
            changeTextViewBackgroundTint(context,R.color.contacted,holder)
        }
        if (status == "Email Sent")
        {
            changeTextViewBackgroundTint(context,R.color.emailSent,holder)
        }
        if (status == "Not Contacted"){
            changeTextViewBackgroundTint(context,R.color.notContacted,holder)
        }
        if (status == "Qualified"){
            changeTextViewBackgroundTint(context,R.color.qualified,holder)
        }
        if (status == "Not Qualified")
        {
            changeTextViewBackgroundTint(context,R.color.notQualified,holder)
        }
        if (status == "Proposal Sent")
        {
            changeTextViewBackgroundTint(context,R.color.proposalSend,holder)
        }
        if (status == "Lead Won")
        {
            changeTextViewBackgroundTint(context,R.color.leadWon,holder)
        }
        if (status == "Lead Lost")
        {
            changeTextViewBackgroundTint(context,R.color.leadLost,holder)
        }


    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    fun filter(text: String) {
        val searchText = text.lowercase(Locale.getDefault())
        filteredList = if (searchText.isEmpty()) {
            dataList
        } else {
            dataList.filter { item ->
                item.customerName.lowercase(Locale.getDefault()).contains(searchText) ||
                        item.leadStatus.lowercase(Locale.getDefault()).contains(searchText)

            }
        }
        notifyDataSetChanged()
    }


    fun changeTextViewBackgroundTint(context: Context, colorResId: Int , holder: ViewHolder) {
        val color = ContextCompat.getColor(context, colorResId)
        val colorStateList = ColorStateList.valueOf(color)
        holder.binding.leadStatus.backgroundTintList = colorStateList
    }
}
