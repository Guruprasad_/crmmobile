package com.BrightLink.SalesOptim.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.BrightLink.SalesOptim.ResponseModel.TaskListModelItem
import com.BrightLink.SalesOptim.databinding.ReminderTableBinding

class ReminderAdapter (val context: Context, private var dataList: List<TaskListModelItem>?):
    RecyclerView.Adapter<ReminderAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =ReminderTableBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.customerName.text = dataList!![position].customerName
        holder.binding.subject.text = dataList!![position].subject
        holder.binding.status.text = dataList!![position].status
        holder.binding.priority.text = dataList!![position].priority
        holder.binding.duedate.text = dataList!![position].duedate
    }

    override fun getItemCount(): Int {
        return dataList!!.size
    }

    inner class ViewHolder( val binding: ReminderTableBinding) : RecyclerView.ViewHolder(binding.root) {
    }
}