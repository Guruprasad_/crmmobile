package com.BrightLink.SalesOptim.Adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.BrightLink.SalesOptim.Activities.SalesOrderInvoice.BillingFragment
import com.BrightLink.SalesOptim.Activities.SalesOrderInvoice.OwnFragment
import com.BrightLink.SalesOptim.Activities.SalesOrderInvoice.ProductFragment
import com.BrightLink.SalesOptim.Activities.SalesOrderInvoice.ShippingFragment
import com.BrightLink.SalesOptim.Activities.SalesOrderInvoice.TandCFragment

class SalesOrderInvoiceAdapter(fm: FragmentManager, lifecycle: Lifecycle) : FragmentStateAdapter(fm, lifecycle) {

    override fun getItemCount(): Int {
        return 5
    }

    override fun createFragment(position: Int): Fragment {
        return when(position){
            0 -> OwnFragment()
            1 -> BillingFragment()
            2 -> ShippingFragment()
            3 -> ProductFragment()
            4 -> TandCFragment()
            else -> throw IllegalStateException("Invalid position")
        }
    }
}