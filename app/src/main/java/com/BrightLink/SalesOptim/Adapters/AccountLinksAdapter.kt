import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.BrightLink.SalesOptim.ResponseModel.AccountAttachmentResModel
import com.BrightLink.SalesOptim.databinding.AccountLinksListitemBinding

class AccountLinksAdapter(val context: Context, private var datalist: List<AccountAttachmentResModel>, private val linkClickListener: LinkClickListener ): RecyclerView.Adapter<AccountLinksAdapter.ViewHolder>() {

    interface LinkClickListener {
        fun onLinkClick(url: String)
    }

    class ViewHolder(val binding: AccountLinksListitemBinding, val context: Context, private var datalist: List<AccountAttachmentResModel>, private val linkClickListener: LinkClickListener) : RecyclerView.ViewHolder(binding.root), View.OnClickListener {
        init {
            binding.url.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val url = datalist[position].url
            val formattedUrl = if (!url.startsWith("http://") && !url.startsWith("https://")) {
                "http://$url" // Add http:// prefix if missing
            } else {
                url
            }

            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(formattedUrl))
            context.startActivity(intent)
        }
    }




    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = AccountLinksListitemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding, context, datalist, linkClickListener)
    }

    override fun getItemCount(): Int {
        return datalist.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.label.text = datalist[position].label
        holder.binding.createdDate.text = datalist[position].createdDate
        holder.binding.url.text= datalist[position].url
    }
}


