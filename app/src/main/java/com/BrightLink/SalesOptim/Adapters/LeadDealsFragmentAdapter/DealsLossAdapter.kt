package com.BrightLink.SalesOptim.Adapters.LeadDealsFragmentAdapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.RecyclerView
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.ResponseModel.LeadDeal.DealModel
import com.BrightLink.SalesOptim.databinding.DealsTableBinding


class DealsLossAdapter(val context: Context, private var dataList: DealModel?): RecyclerView.Adapter<DealsLossAdapter.ViewHolder>()
{
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DealsLossAdapter.ViewHolder {
        val binding = DealsTableBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: DealsLossAdapter.ViewHolder, position: Int) {

        holder.binding.customerName.text = dataList!!.dealsLossList.get(position).customerName
        holder.binding.customerEmail.text = dataList!!.dealsLossList.get(position).email
        holder.binding.customerRequirement.text = dataList!!.dealsLossList.get(position).requirement
        holder.binding.amount.text = dataList!!.dealsLossList.get(position).leadRevenue
        holder.binding.createdDate.text = dataList!!.dealsLossList.get(position).createdDate
        holder.binding.layout.startAnimation(
            AnimationUtils.loadAnimation(
                holder.itemView.context,
                R.anim.recycler_view_anime_2
            )
        )

    }
    override fun getItemCount(): Int {
        return dataList!!.dealsLossList.size
    }

    inner class ViewHolder( val binding: DealsTableBinding): RecyclerView.ViewHolder(binding.root)

}