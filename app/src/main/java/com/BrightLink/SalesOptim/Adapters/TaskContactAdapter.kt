package com.BrightLink.SalesOptim.Adapters

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.BrightLink.SalesOptim.ResponseModel.TaskContactResponseModelItem
import com.BrightLink.SalesOptim.databinding.GetTaskAssignLeadBinding

open class TaskContactAdapter(private val context : Context, private val dataList : List<TaskContactResponseModelItem> , private val phone : String , val token : String) : RecyclerView.Adapter<TaskContactAdapter.onViewHolder>() {

    class onViewHolder(val binding: GetTaskAssignLeadBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): onViewHolder {
        val view =
            GetTaskAssignLeadBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return onViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: onViewHolder, position: Int) {
        with(holder.binding) {
            name.text = dataList.get(position).customerName
            date.text = dataList.get(position).createdDate
            type.text = dataList.get(position).taskType
            val contactId = dataList.get(position).contactId
            val leadId = dataList.get(position).leadId

            val sharedPreferences = context.getSharedPreferences("LeadPreferences", Context.MODE_PRIVATE)
            val leadStatus = sharedPreferences.getString("leadStatus","")
            if (leadStatus == "Lead Won" || leadStatus == "Lead Lost"){
                call.visibility = View.INVISIBLE
            }

            call.setOnClickListener{

//                val call = ApiUtilities.getinstance().create(ApiInterface::class.java)
//                call.getChangeTaskStatus("Bearer $token",contactId,leadId).enqueue(object : Callback<Void>!!{
//
//                })

                val callIntent = Intent(Intent.ACTION_CALL)
                callIntent.data = Uri.parse("tel:$phone")
                context.startActivity(callIntent)
            }
        }
    }
}