package com.BrightLink.SalesOptim.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.BrightLink.SalesOptim.Constants.SharedPreferecneManager
import com.BrightLink.SalesOptim.Model.ProductModel
import com.BrightLink.SalesOptim.databinding.ProductItemLayoutBinding

class ProductAdapter(private val items : MutableList<ProductModel>) :
    RecyclerView.Adapter<ProductAdapter.onViewHolder>() {

    class onViewHolder(val binding : ProductItemLayoutBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): onViewHolder {
        val view = ProductItemLayoutBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return onViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: onViewHolder, position: Int) {
        holder.binding.srno.text = (position + 1).toString()
        holder.binding.productDes.text = items.get(position).productName.toString()
        holder.binding.qty.text = items.get(position).quantity.toString()
        holder.binding.price.text = items.get(position).unitPrice.toString()

        val unitPrice = items[position].unitPrice!!.toInt()
        val quantity = items[position].quantity!!.toInt()
        val total = unitPrice * quantity
        holder.binding.totalPrice.text = total.toString()
    }

    fun removeLastItem(context : Context) {
        if (items.isNotEmpty()) {
            val lastItem = items.removeAt(items.size - 1)
            notifyDataSetChanged()
            SharedPreferecneManager.removeProduct(context, lastItem)
        }
    }


}