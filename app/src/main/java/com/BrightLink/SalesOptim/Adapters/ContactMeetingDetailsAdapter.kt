package com.BrightLink.SalesOptim.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.BrightLink.SalesOptim.ResponseModel.ContactMeetingDetailsModelItem
import com.BrightLink.SalesOptim.databinding.ContactMeetingDetailsBinding

class ContactMeetingDetailsAdapter (val context: Context, private var datalist: List<ContactMeetingDetailsModelItem>) : RecyclerView.Adapter<ContactMeetingDetailsAdapter.ViewHolder>() {
    class ViewHolder(val binding : ContactMeetingDetailsBinding) : RecyclerView.ViewHolder(binding.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ContactMeetingDetailsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return datalist.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.title.text = datalist[position].title
        holder.binding.from.text = datalist[position].meetingFrom
        holder.binding.to.text = datalist[position].meetingTo
        holder.binding.contactName.text = datalist[position].contactName
        holder.binding.clientEmail.text = datalist[position].clientEmail
        holder.binding.meetingType.text = datalist[position].locationType
        holder.binding.link.text = datalist[position].meetingLinkOrLocation
    }
}