package com.BrightLink.SalesOptim.Adapters

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.BrightLink.SalesOptim.Activities.ui.notifications.NotificationsFragment.SharedPreferencesHelper
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.ResponseModel.ShowLeadsModelItem
import com.BrightLink.SalesOptim.databinding.NotificationTableBinding


class NotificationAdapter(
    private val context: Context,
    private var dataList: List<ShowLeadsModelItem>
) : RecyclerView.Adapter<NotificationAdapter.ViewHolder>() {
    private val sharedPreferences = context.getSharedPreferences("LeadPreferences", Context.MODE_PRIVATE)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = NotificationTableBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        dataList?.let { leads ->
            val leadItem = leads[position]

            holder.binding.apply {
//                leadId.text = leadItem.leadId.toString()
                customerName.text = leadItem.customerName
                phone.text = leadItem.mobileNumber
                leadStatus.text = leadItem.leadStatus

                action.setOnClickListener {
                    val savedLeadId = SharedPreferencesHelper.getLeadId(context)



                    if (leadItem.leadId == savedLeadId) {
                        val bundle = bundleOf(
                            "leadId" to leadItem.leadId,
                            "phone" to leadItem.mobileNumber,
                            "email" to leadItem.email,
                            "revenue" to leadItem.leadRevenue
                        )

                        it.findNavController().navigate(R.id.assignLead, bundle)
                    } else {
                        // Show a message or do nothing if leadId doesn't match
                        Constants.error(context, "Invalid lead ID")
                    }
                }
            }
            holder.binding.action.setOnClickListener {
                val bundle = Bundle()
//            bundle.putInt("leadId",dataList.get(position).leadId)
                bundle.putString("phone",dataList.get(position).mobileNumber)
                bundle.putString("email",dataList.get(position).email)
                bundle.putString("revenue",dataList.get(position).leadRevenue)
                bundle.putString("leadStatus",dataList.get(position).leadStatus)

                Navigation.findNavController(it).navigate(R.id.assignLead,bundle)

                val leadId = dataList.get(position).leadId

                sharedPreferences.edit().putInt("leadId", leadId).apply()
            }
        }
    }

    override fun getItemCount(): Int {
        return dataList?.size ?: 0
    }

    inner class ViewHolder(val binding: NotificationTableBinding) :
        RecyclerView.ViewHolder(binding.root)
}
