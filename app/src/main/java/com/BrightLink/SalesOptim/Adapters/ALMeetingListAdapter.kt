package com.BrightLink.SalesOptim.Adapters

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.BrightLink.SalesOptim.ResponseModel.ALMeetingModelItem
import com.BrightLink.SalesOptim.databinding.GetMeetingAssignLeadBinding

class ALMeetingListAdapter(val context: Context, private var datalist: List<ALMeetingModelItem>, private val linkClickListener: LinkClickListener): RecyclerView.Adapter<ALMeetingListAdapter.ViewHolder>() {

    interface LinkClickListener {
        fun onLinkClick(url: String)
    }


    class ViewHolder(val binding: GetMeetingAssignLeadBinding, val context: Context, private var datalist: List<ALMeetingModelItem>, private val linkClickListener: LinkClickListener) : RecyclerView.ViewHolder(binding.root), View.OnClickListener {
        init {
            binding.link.setOnClickListener(this)
        }

        override fun onClick(v: View){
            val url = datalist[position].meetingLinkOrLocation
            val formattedUrl = if (!url.startsWith("http://") && !url.startsWith("https://")) {
                "http://$url" // Add http:// prefix if missing
            } else {
                url
            }

            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(formattedUrl))
            context.startActivity(intent)
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = GetMeetingAssignLeadBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding,context,datalist,linkClickListener)
    }

    override fun getItemCount(): Int {
        return  datalist.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val meetingFrom = datalist[position].meetingFrom
        val meetingTo = datalist[position].meetingTo

        val dateTimeString = "$meetingFrom -\n$meetingTo"

        holder.binding.title.text = datalist[position].title
        holder.binding.dateAndTime.text = dateTimeString
        holder.binding.meetingType.text = datalist[position].locationType
        holder.binding.link.text = datalist[position].meetingLinkOrLocation
    }
}
