package com.BrightLink.SalesOptim.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.BrightLink.SalesOptim.Model.ProductModel
import com.BrightLink.SalesOptim.R


class GetProductAdapter(private val context: Context , val items : List<ProductModel>) : RecyclerView.Adapter<GetProductAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.product_item_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.bind(item , position)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val productDescription: TextView = itemView.findViewById(R.id.productDes)
        private val srNo: TextView = itemView.findViewById(R.id.srno)
        private val qty: TextView = itemView.findViewById(R.id.qty)
        private val price: TextView = itemView.findViewById(R.id.price)
        private val Total: TextView = itemView.findViewById(R.id.totalPrice)

        fun bind(item: ProductModel , position: Int) {
            productDescription.text = item.productName
            srNo.text = (position+1).toString()
            qty.text = item.quantity
            price.text = item.unitPrice
            val quantity = item.quantity!!.toInt()
            val price = item.unitPrice!!.toInt()
            val total = price * quantity
            Total.text = total.toString()
        }
    }

}