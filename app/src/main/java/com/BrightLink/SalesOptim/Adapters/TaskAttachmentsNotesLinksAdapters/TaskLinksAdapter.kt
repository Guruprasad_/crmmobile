package com.BrightLink.SalesOptim.Adapters.TaskAttachmentsNotesLinksAdapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.BrightLink.SalesOptim.databinding.GetLinksBinding
import android.content.Intent
import android.net.Uri
import com.BrightLink.SalesOptim.ResponseModel.NAL_ResModels.TaskLinksResModelItem

class TaskLinksAdapter(val context:Context, private var dataList:List<TaskLinksResModelItem>,private val linkClickListener: LinkClickListener): RecyclerView.Adapter<TaskLinksAdapter.ViewHolder>() {
    interface LinkClickListener {
        fun onLinkClick(url: String)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskLinksAdapter.ViewHolder {
        val binding = GetLinksBinding.inflate(LayoutInflater.from(parent.context),parent, false)
        return ViewHolder(binding,context, dataList,linkClickListener)
    }

    override fun onBindViewHolder(holder: TaskLinksAdapter.ViewHolder, position: Int) {
        holder.binding.linkCreatedBy.text = dataList!![position].createdBy
        holder.binding.linkCreatedDate.text = dataList!![position].createdDate
        holder.binding.linkLabel.text = dataList!![position].label
        holder.binding.linkUrl.text = dataList!![position].url
        holder.binding.linkDescription.text = dataList!![position].linkDescription
//        val link = dataList.links[position]
//        holder.binding.linkUrl.setText(link)
    }

    override fun getItemCount(): Int {
        return dataList!!.size ?:0
    }

    class ViewHolder(val binding: GetLinksBinding, private var context: Context, private var dataList:List<TaskLinksResModelItem>, private val linkClickListener: LinkClickListener) : RecyclerView.ViewHolder(binding.root), View.OnClickListener {

        init {
            binding.linkUrl.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val url = dataList!![position].url
            val formattedUrl = if (!url.startsWith("http://") && !url.startsWith("https://")) {
                "http://$url" // Add http:// prefix if missing
            } else {
                url
            }

            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(formattedUrl))
            context.startActivity(intent)
        }
    }


}