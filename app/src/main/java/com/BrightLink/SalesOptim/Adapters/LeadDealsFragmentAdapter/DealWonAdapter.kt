package com.BrightLink.SalesOptim.Adapters.LeadDealsFragmentAdapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.RecyclerView
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.ResponseModel.LeadDeal.DealModel
import com.BrightLink.SalesOptim.databinding.DealsTableBinding


class DealWonAdapter(val context: Context, private var dataList: DealModel?): RecyclerView.Adapter<DealWonAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DealWonAdapter.ViewHolder {
        val  binding = DealsTableBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: DealWonAdapter.ViewHolder, position: Int) {

        holder.binding.customerName.text = dataList!!.dealsWonList.get(position).customerName
        holder.binding.customerEmail.text = dataList!!.dealsWonList.get(position).email
        holder.binding.customerRequirement.text = dataList!!.dealsWonList.get(position).requirement
        holder.binding.amount.text = dataList!!.dealsWonList.get(position).leadRevenue
        holder.binding.createdDate.text = dataList!!.dealsWonList.get(position).createdDate
        holder.binding.layout.startAnimation(
            AnimationUtils.loadAnimation(
                holder.itemView.context,
                R.anim.recycler_view_anime_2
            )
        )
    }

    override fun getItemCount(): Int {
        return dataList!!.dealsWonList.size
    }

    class ViewHolder(val binding: DealsTableBinding) : RecyclerView.ViewHolder(binding.root)

}