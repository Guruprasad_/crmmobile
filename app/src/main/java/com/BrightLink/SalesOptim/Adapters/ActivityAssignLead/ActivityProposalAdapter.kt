package com.BrightLink.SalesOptim.Adapters.ActivityAssignLead

import android.annotation.SuppressLint
import android.content.Context
import android.os.Environment
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.databinding.GetProposalAssignLeadBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.File
import java.io.FileOutputStream
import android.widget.Toast
import com.BrightLink.SalesOptim.ResponseModel.ActivityStatus.ProposalStatusItem
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.withContext
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.UUID

class ActivityProposalAdapter (val context: Context, private var data: List<ProposalStatusItem> ): RecyclerView.Adapter<ActivityProposalAdapter.ViewHolder>() {

    inner class ViewHolder (val binding: GetProposalAssignLeadBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(proposalItem: ProposalStatusItem) {
            binding.proposalTitle.text = proposalItem.title
            binding.proposalDesc.text = proposalItem.description
            binding.proposalQuotation.text = proposalItem.status

            binding.proposalInvoice.setOnClickListener {
                val position = adapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val attachment = proposalItem.proposalId
                    showInvoice(attachment)
                }
            }
        }

        private fun showInvoice(proposalId: Int) {
            val jwtToken = getToken()
            if (jwtToken != null) {
                val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
                val call = service.getProposalInvoice("Bearer $jwtToken", proposalId)
                call.enqueue(object : Callback<ResponseBody> {
                    override fun onResponse(
                        call: Call<ResponseBody>,
                        response: Response<ResponseBody>
                    ) {
                        if (response.isSuccessful) {
                            val file = response.body()
                            if (file != null) {
                                downloadAttachment(file)
                            }
                        } else {
                            Constants.error(context, "Unsuccessful response: ${response.code()}")
                        }
                    }

                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        Constants.error(context, "Response is Unsuccessful: ${t.message}")
                    }

                })

            } else {
                Constants.error(context, "Token is null. Please log in again.")
            }
        }

        private fun downloadAttachment(file: ResponseBody) {

            CoroutineScope(Dispatchers.IO).launch {

                try {
                    val fileName =data[position].filename
                    val fileNameOnDevice = UUID.randomUUID().toString() + fileName
                    val downloadsDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
                    val fileOnDevice = File(downloadsDir, fileNameOnDevice)

                    // Write the file content to the device's storage
                    FileOutputStream(fileOnDevice).use { output ->
                        output.write(file.bytes())
                    }

                    showToast("Attachment downloaded successfully")

                } catch (e: Exception) {
                    e.printStackTrace()
                    showToast("Failed to download Attachment")
                }
            }
        }

        private suspend fun showToast(message: String) {
            withContext(Dispatchers.Main) {
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
            }
        }

        private fun getToken(): String? {
            return context.getSharedPreferences("Token", Context.MODE_PRIVATE)
                .getString("jwtToken", null)
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val binding = GetProposalAssignLeadBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return  ViewHolder(binding)
    }

    @SuppressLint("SuspiciousIndentation")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
     val proposalItem = data[position]
        holder.bind(proposalItem)
    }

    override fun getItemCount(): Int {
        return data.size
    }
}