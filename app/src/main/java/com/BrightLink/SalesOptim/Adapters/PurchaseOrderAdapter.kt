package com.BrightLink.SalesOptim.Adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.BrightLink.SalesOptim.Activities.PurchaseOrder.BillingPurchaseOrderFragment
import com.BrightLink.SalesOptim.Activities.PurchaseOrder.OwnPurchaseOrderFragment
import com.BrightLink.SalesOptim.Activities.PurchaseOrder.ProductPurchaseOrderFragment
import com.BrightLink.SalesOptim.Activities.PurchaseOrder.ShippingFragment
import com.BrightLink.SalesOptim.Activities.PurchaseOrder.TandCFragment

class PurchaseOrderAdapter(fm: FragmentManager, lifecycle: Lifecycle) : FragmentStateAdapter(fm, lifecycle) {

    override fun getItemCount(): Int {
        return 5
    }

    override fun createFragment(position: Int): Fragment {
        return when(position){
            0 -> OwnPurchaseOrderFragment()
            1 -> BillingPurchaseOrderFragment()
            2 -> ShippingFragment()
            3 -> ProductPurchaseOrderFragment()
            4 -> TandCFragment()
            else -> throw IllegalStateException("Invalid position")
        }
    }
}