package com.BrightLink.SalesOptim.Adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import android.content.Context
import android.view.animation.AnimationUtils
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.ResponseModel.TaskListModelItem
import com.BrightLink.SalesOptim.databinding.HeadingsTaskListBinding


class TaskListAdapter(val context: Context, private var dataList: List<TaskListModelItem>?,
                      private var customerMap: HashMap<String, String>):RecyclerView.Adapter<TaskListAdapter.ViewHolder>() {

    private lateinit var mListener: TaskListAdapter.onItemClickListener

    interface onItemClickListener {
        fun onItemClick(position: Int)
    }
    fun setOnItemClickListener(listener: onItemClickListener)
    {
        mListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = HeadingsTaskListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val taskId = dataList!![position].id ?: 0
        val customerName = customerMap[taskId.toString()] ?: "Unknown"

        holder.binding.taskCustomerName.text = dataList!![position].customerName
        holder.binding.DueDate.text = dataList!![position].duedate
        holder.binding.Subject.text = dataList!![position].subject
        holder.binding.TaskOwner.text = dataList!![position].taskOwner

        holder.binding.layout.startAnimation(
            AnimationUtils.loadAnimation(
                holder.itemView.context,
                R.anim.recycler_view_anime_2
            )
        )
    }

    override fun getItemCount(): Int {
        return dataList!!.size ?:0
    }

    inner class ViewHolder( val binding: HeadingsTaskListBinding) : RecyclerView.ViewHolder(binding.root) {
        init {
            itemView.setOnClickListener{
                mListener.onItemClick(adapterPosition)
            }
        }
    }
}