package com.BrightLink.SalesOptim.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.BrightLink.SalesOptim.ResponseModel.LeadDetailsAccountModelItem
import com.BrightLink.SalesOptim.databinding.AccountLeadDetailsBinding

class AccountLeadDetailsAdapter(val context: Context, private var datalist: List<LeadDetailsAccountModelItem>) : RecyclerView.Adapter<AccountLeadDetailsAdapter.ViewHolder>() {
    class ViewHolder(val binding : AccountLeadDetailsBinding) : RecyclerView.ViewHolder(binding.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = AccountLeadDetailsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return datalist.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.createdDate.text = datalist[position].createdDate
        holder.binding.customerName.text = datalist[position].customerName
        holder.binding.requirement.text = datalist[position].requirement
        holder.binding.actualRevenue.text = datalist[position].totalRevenue.toString()
        holder.binding.priority.text = datalist[position].leadType
        holder.binding.status.text = datalist[position].leadStatus
        holder.binding.phone.text = datalist[position].mobileNumber
    }
}