package com.BrightLink.SalesOptim.Adapters.LeadDealsFragmentAdapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.RecyclerView
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.ResponseModel.LeadDeal.DealModel
import com.BrightLink.SalesOptim.databinding.DealsTableBinding

class DealsUntouchAdapter(val context: Context, private var dataList: DealModel?):RecyclerView.Adapter<DealsUntouchAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup,  viewType: Int): DealsUntouchAdapter.ViewHolder {
        val  binding = DealsTableBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: DealsUntouchAdapter.ViewHolder, position: Int) {

        holder.binding.customerName.text = dataList!!.dealsUntouchList.get(position).customerName
        holder.binding.customerEmail.text = dataList!!.dealsUntouchList.get(position).email
        holder.binding.customerRequirement.text = dataList!!.dealsUntouchList.get(position).requirement
        holder.binding.amount.text = dataList!!.dealsUntouchList.get(position).leadRevenue
        holder.binding.createdDate.text = dataList!!.dealsUntouchList.get(position).createdDate
        holder.binding.layout.startAnimation(
            AnimationUtils.loadAnimation(
                holder.itemView.context,
                R.anim.recycler_view_anime_2
            )
        )
    }

    override fun getItemCount(): Int {
        return dataList!!.dealsUntouchList.size
    }

    class ViewHolder(val binding: DealsTableBinding) : RecyclerView.ViewHolder(binding.root)

}