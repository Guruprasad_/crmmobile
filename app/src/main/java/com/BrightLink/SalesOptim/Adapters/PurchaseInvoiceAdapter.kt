package com.BrightLink.SalesOptim.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.BrightLink.SalesOptim.Model.PurchaseOrderItem
import com.BrightLink.SalesOptim.databinding.PurchaseOrderInvoiceTableBinding
import com.google.gson.Gson

class PurchaseInvoiceAdapter(private val items : MutableList<PurchaseOrderItem>) :
    RecyclerView.Adapter<PurchaseInvoiceAdapter.onViewHolder>() {

    class onViewHolder(val binding : PurchaseOrderInvoiceTableBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): onViewHolder {
        val view = PurchaseOrderInvoiceTableBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return onViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: onViewHolder, position: Int) {
        holder.binding.srNo.text = (position + 1).toString()
        holder.binding.description.text = items.get(position).productName.toString()
        holder.binding.qty.text = items.get(position).quantity.toString()
        holder.binding.price.text = items.get(position).unitPrice.toString()

        val unitPrice = items[position].unitPrice!!.toInt()
        val quantity = items[position].quantity!!.toInt()
        val total = unitPrice * quantity
        holder.binding.totalPrice.text = total.toString()
    }

    fun removeLastItem(context : Context) {
        if (items.isNotEmpty()) {
            val lastItem = items.removeAt(items.size - 1)
            notifyDataSetChanged()
            removeProduct(context, lastItem)
        }
    }


    fun removeProduct(context: Context, product: PurchaseOrderItem) {
        val prefs = context.getSharedPreferences("PurchaseOrderProduct", Context.MODE_PRIVATE)
        val editor = prefs.edit()
        val gson = Gson()

        // Get the current list of products from SharedPreferences
        val productListJson = prefs.getString("Product", null)
        val productList = gson.fromJson(productListJson, Array<PurchaseOrderItem>::class.java)?.toMutableList()
        productList?.remove(product)
        val updatedListJson = gson.toJson(productList)
        editor.putString("Product", updatedListJson)
        editor.apply()
    }

}