package com.BrightLink.SalesOptim.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.ResponseModel.Pipetable
import com.BrightLink.SalesOptim.databinding.PiplineTableLayoutBinding

class PiplineTableAdapter(val context: Context , val datalist : List<Pipetable>) : RecyclerView.Adapter<PiplineTableAdapter.onViewHolder>() {

    class onViewHolder(val binding : PiplineTableLayoutBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): onViewHolder {
        val view = PiplineTableLayoutBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return onViewHolder(view)
    }

    override fun getItemCount(): Int {
        return datalist.size
    }

    override fun onBindViewHolder(holder: onViewHolder, position: Int) {
        with(holder.binding){
            customer.text = datalist.get(position).customerName
            revenue.text = datalist.get(position).leadRevenue
            negoRevenue.text = datalist.get(position).totalRevenue.toString()
            val status = datalist.get(position).leadStatus
            statusColor(status, holder)
            Status.text = datalist.get(position).leadStatus
            date.text = datalist.get(position).modifyDate



        }
    }

    @SuppressLint("ResourceAsColor")
    private fun statusColor(status: String, holder: PiplineTableAdapter.onViewHolder) {
        if (status == "New Lead")
        {
            changeTextViewBackgroundTint(context, R.color.newLead,holder)
        }
        if (status == "Contacted")
        {
            changeTextViewBackgroundTint(context, R.color.contacted,holder)
        }
        if (status == "Email Sent")
        {
            changeTextViewBackgroundTint(context, R.color.emailSent,holder)
        }
        if (status == "Not Contacted"){
            changeTextViewBackgroundTint(context, R.color.notContacted,holder)
        }
        if (status == "Qualified"){
            changeTextViewBackgroundTint(context, R.color.qualified,holder)
        }
        if (status == "Not Qualified")
        {
            changeTextViewBackgroundTint(context, R.color.notQualified,holder)
        }
        if (status == "Proposal Sent")
        {
            changeTextViewBackgroundTint(context, R.color.proposalSend,holder)
        }
        if (status == "Lead Won")
        {
            changeTextViewBackgroundTint(context, R.color.leadWon,holder)
        }
        if (status == "Lead Lost")
        {
            changeTextViewBackgroundTint(context, R.color.leadLost,holder)
        }


    }

    fun changeTextViewBackgroundTint(context: Context, colorResId: Int , holder: PiplineTableAdapter.onViewHolder) {
        val color = ContextCompat.getColor(context, colorResId)
        val colorStateList = ColorStateList.valueOf(color)
        holder.binding.Status.backgroundTintList = colorStateList
    }
}