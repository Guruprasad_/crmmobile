package com.BrightLink.SalesOptim.Adapters.ActivityAssignLead

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.ResponseModel.ActivityStatus.QualifyStatusItem
import com.BrightLink.SalesOptim.databinding.GetQualifyAssignLeadBinding

class ActivityQualifyAdapter (val context: Context, private var data : List<QualifyStatusItem>): RecyclerView.Adapter<ActivityQualifyAdapter.ViewHolder>() {

   inner class ViewHolder (val binding: GetQualifyAssignLeadBinding) : RecyclerView.ViewHolder(binding.root){
       fun bind(qualifyItem: QualifyStatusItem){
           binding.titleQualify.text = qualifyItem.title
           binding.qualifyStatus.text = qualifyItem.qualifyStatus
           val combinedText = "${qualifyItem.requirement}${qualifyItem.reason}"
           binding.quallifyDesc.text = combinedText

           when (qualifyItem.qualifyStatus) {
               // Add conditions for each possible text value and set text color accordingly
               "Qualified" -> binding.qualifyStatus.setTextColor(context.resources.getColor(R.color.DarkOrange))
               "Not Qualified" -> binding.qualifyStatus.setTextColor(context.resources.getColor(R.color.Red))
               // Add more conditions for other text values if needed
               else -> binding.qualifyStatus.setTextColor(context.resources.getColor(R.color.black))
           }
       }

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = GetQualifyAssignLeadBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder:ViewHolder, position: Int) {
        val qualifyItem = data[position]
        holder.bind(qualifyItem)
    }
}