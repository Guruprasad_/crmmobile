package com.BrightLink.SalesOptim.Adapters.ContactLANadapters

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.BrightLink.SalesOptim.ResponseModel.ContactAttachmentsResModel
import com.BrightLink.SalesOptim.databinding.GetLinksBinding

class ContactLinksAdapter(val context: Context, private var datalist: List<ContactAttachmentsResModel>, private val linkClickListener: LinkClickListener ): RecyclerView.Adapter<ContactLinksAdapter.ViewHolder>() {

    interface LinkClickListener {
        fun onLinkClick(url: String)
    }

    class ViewHolder(val binding: GetLinksBinding, val context: Context, private var datalist: List<ContactAttachmentsResModel>, private val linkClickListener: LinkClickListener) : RecyclerView.ViewHolder(binding.root), View.OnClickListener {
        init {
            binding.linkUrl.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val url = datalist[position].url
            val formattedUrl = if (!url.startsWith("http://") && !url.startsWith("https://")) {
                "http://$url" // Add http:// prefix if missing
            } else {
                url
            }

            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(formattedUrl))
            context.startActivity(intent)
        }
    }




    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = GetLinksBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding, context, datalist, linkClickListener)
    }

    override fun getItemCount(): Int {
        return datalist.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.linkLabel.text = datalist[position].label
        holder.binding.linkCreatedBy.text = datalist[position].createdBy
        holder.binding.linkCreatedDate.text = datalist[position].createdDate
        holder.binding.linkUrl.text= datalist[position].url
    }
}

