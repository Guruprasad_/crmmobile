package com.BrightLink.SalesOptim.Adapters

import android.content.Context
import android.graphics.Color
import android.os.Environment
import android.util.Base64
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.ResponseModel.PurchaseOrderModelItem
import com.BrightLink.SalesOptim.databinding.GetpurchaseorderBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File
import java.io.FileOutputStream
import java.util.Locale
import java.util.UUID


class PurchaseListAdapter(val context: Context, private  var dataList: List<PurchaseOrderModelItem>):RecyclerView.Adapter<PurchaseListAdapter.ViewHolder>(){
    override fun onCreateViewHolder( parent: ViewGroup, viewType: Int):ViewHolder
    {
        val binding = GetpurchaseorderBinding.inflate(LayoutInflater.from(parent.context), parent, false )
        return ViewHolder(binding)
    }

    private var filteredList: List<PurchaseOrderModelItem> = dataList
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val currentItem = filteredList[position]


        holder.binding.Createddate.text = currentItem.createdDate
        holder.binding.createdby.text = currentItem.createdBy
        holder.binding.sendingto.text = currentItem.sendingTo
        holder.binding.vendorname.text = currentItem.vendorName
        holder.binding.purchasePreview.text = "File"
        holder.binding.purchasePreview.setTextColor(Color.BLUE)
        holder.binding.purchasePreview.setOnClickListener {
            val attachment = currentItem.invoice
            downloadAttachment(attachment, UUID.randomUUID().toString()+currentItem.filename)
        }

        holder.binding.layout.startAnimation(
            AnimationUtils.loadAnimation(
                holder.itemView.context,
                R.anim.recycler_view_anime_2
            )
        )
    }


    private fun downloadAttachment(fileUrl: String, fileName: String) {
        CoroutineScope(Dispatchers.IO).launch {
            if (fileUrl == null) {
                showToast("Invalid file URL")
                return@launch
            }

            try {
                val downloadsDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
                val file = File(downloadsDir, fileName)

                val outputStream = FileOutputStream(file)

                val decodedBytes: ByteArray = Base64.decode(fileUrl, Base64.DEFAULT)
                outputStream.write(decodedBytes)

                outputStream.flush()
                outputStream.close()

                showToast("Attachment downloaded successfully")
            } catch (e: Exception) {
                showToast("Failed to download Attachment ${e.message}")
            }
        }
    }


    private suspend fun showToast(message: String) {
        withContext(Dispatchers.Main) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        }
    }

    fun filter(text: String) {
        val searchText = text.lowercase(Locale.getDefault())
        filteredList = if (searchText.isEmpty()) {
            dataList
        } else {
            dataList.filter { item ->
                item.vendorName.lowercase(Locale.getDefault()).contains(searchText) ||
                        item.sendingTo.lowercase(Locale.getDefault()).contains(searchText)
            }
        }
        notifyDataSetChanged()
    }
    override fun getItemCount(): Int {
        return dataList.size
    }


    inner class ViewHolder( val binding: GetpurchaseorderBinding) : RecyclerView.ViewHolder(binding.root)
}