package com.BrightLink.SalesOptim.Adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.BrightLink.SalesOptim.ResponseModel.TopLead
import com.BrightLink.SalesOptim.databinding.TaskDashboardRecyclerViewLayoutBinding

class TaskDashBoardTopLeadAdapter(private val datalist: List<TopLead>) :
    RecyclerView.Adapter<TaskDashBoardTopLeadAdapter.onViewHolder>() {

    class onViewHolder(val binding : TaskDashboardRecyclerViewLayoutBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): onViewHolder {
        val view = TaskDashboardRecyclerViewLayoutBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return onViewHolder(view)
    }

    override fun getItemCount(): Int {
       return datalist.size
    }

    override fun onBindViewHolder(holder: onViewHolder, position: Int) {
        with(holder.binding){
            name.text = datalist.get(position).customerName
            revenue.text = datalist.get(position).totalRevenue.toString()
            val no = position+1
            srNo.text = no.toString()
        }
    }
}