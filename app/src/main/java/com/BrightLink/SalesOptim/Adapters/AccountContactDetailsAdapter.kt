package com.BrightLink.SalesOptim.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.BrightLink.SalesOptim.ResponseModel.AccountContactDetailsModelItem
import com.BrightLink.SalesOptim.databinding.AccountContactDetailsBinding

class AccountContactDetailsAdapter(val context: Context, private var datalist: List<AccountContactDetailsModelItem>) : RecyclerView.Adapter<AccountContactDetailsAdapter.ViewHolder>() {
    class ViewHolder(val binding : AccountContactDetailsBinding) : RecyclerView.ViewHolder(binding.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = AccountContactDetailsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return datalist.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.contactName.text = datalist[position].fullName
        holder.binding.email.text = datalist[position].email
        holder.binding.phone.text = datalist[position].phone
        holder.binding.department.text = datalist[position].department
        holder.binding.leadSource.text = datalist[position].leadSource
    }
}