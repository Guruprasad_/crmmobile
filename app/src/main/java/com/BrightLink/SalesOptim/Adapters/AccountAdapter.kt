package com.BrightLink.SalesOptim.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.RecyclerView
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.ResponseModel.ShowAccountsModelItem
import com.BrightLink.SalesOptim.databinding.AccountTableBinding

class AccountAdapter(private val context: Context, private var dataList: ArrayList<ShowAccountsModelItem>) : RecyclerView.Adapter<AccountAdapter.ViewHolder>() {

    private lateinit var mListener: onItemClickListener
//
    interface onItemClickListener{
        fun onItemClick(position: Int)
    }


    fun setOnItemClickListener(listener: onItemClickListener){
        mListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = AccountTableBinding.inflate(LayoutInflater.from(parent.context), parent, false)
//        return ViewHolder(binding)
        return ViewHolder(binding,mListener)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentItem = dataList[position]

        holder.binding.accountName.text = currentItem.accountName
        holder.binding.phone.text = currentItem.phone
        holder.binding.accountType.text = currentItem.accountType
        holder.binding.annualRevenue.text = currentItem.revenue

        val accountId = currentItem.accountId

        val accountSite= currentItem.accountSite
        val accountNumber= currentItem.accountNumber
        val industry= currentItem.industry
        val rating= currentItem.rating
        val fax= currentItem.fax
        val website= currentItem.website
        val tickerSymbol= currentItem.tickerSymbol
        val ownership= currentItem.ownership
        val employees= currentItem.employees
        val billingStreet= currentItem.billingStreet
        val billingCode= currentItem.billingCode
        val shippingStreet= currentItem.shippingStreet
        val shippingCode= currentItem.shippingCode
        val description= currentItem.description
        val billingCountryId= currentItem.billingCountryId
        val billingStateId= currentItem.billingStateId
        val billingCityId= currentItem.billingCityId
        val shippingCountryId= currentItem.shippingCountryId
        val shippingStateId= currentItem.shippingStateId
        val shippingCityId= currentItem.shippingCityId

        holder.binding.layout.startAnimation(
            AnimationUtils.loadAnimation(
                holder.itemView.context,
                R.anim.recycler_view_anime_2
            )
        )
//        holder.itemView.setOnClickListener { view ->
//            val navController = Navigation.findNavController(view)
//            navController.navigate(R.id.updateAccount)
//        }
    }


    override fun getItemCount(): Int {
        return dataList.size
    }



  class ViewHolder(val binding: AccountTableBinding, listener: onItemClickListener) : RecyclerView.ViewHolder(binding.root){
//      class ViewHolder(val binding: AccountTableBinding) : RecyclerView.ViewHolder(binding.root){

        init {
            itemView.setOnClickListener {
                listener.onItemClick(adapterPosition)
            }
        }

    }
}