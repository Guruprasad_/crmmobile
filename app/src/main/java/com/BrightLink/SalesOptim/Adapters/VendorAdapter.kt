package com.BrightLink.SalesOptim.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.RecyclerView
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.ResponseModel.ShowVendorModelItem
import com.BrightLink.SalesOptim.databinding.GetvendorBinding
import java.util.Locale

class VendorAdapter(val context: Context,private var dataList: List<ShowVendorModelItem>): RecyclerView.Adapter<VendorAdapter.ViewHolder>() {

    private lateinit var mListener: VendorAdapter.onItemClickListener

    private var filteredList: List<ShowVendorModelItem> = dataList

    interface onItemClickListener{
        fun onItemClick(position: Int)
    }
    fun setOnItemClickListener(listener: onItemClickListener)
    {
        mListener = listener
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VendorAdapter.ViewHolder {
        val binding = GetvendorBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding, mListener)
    }

    override fun onBindViewHolder(holder: VendorAdapter.ViewHolder, position: Int) {
        val currentItem = filteredList[position]

        holder.binding.vendorName.text = currentItem.vendorName
        holder.binding.vendorEmail.text = currentItem.email
        holder.binding.vendorPhone.text = currentItem.phone
        holder.binding.vendorWebsite.text = currentItem.website
        holder.binding.vendorOwner.text = currentItem.vendorOwner
        holder.binding.layout.startAnimation(
            AnimationUtils.loadAnimation(
                holder.itemView.context,
                R.anim.recycler_view_anime_2
            )
        )
    }
    override fun getItemCount(): Int {
        return dataList.size
    }

    fun filter(text: String) {
        val searchText = text.lowercase(Locale.getDefault())
        filteredList = if (searchText.isEmpty()) {
            dataList
        } else {
            dataList.filter { item ->
                item.vendorName!!.lowercase(Locale.getDefault()).contains(searchText)
            }
        }
        notifyDataSetChanged()
    }

    fun getVendorId(position: Int):Int{
        return dataList[position].vendorId!!
    }

    inner class ViewHolder(val binding: GetvendorBinding, listener: onItemClickListener): RecyclerView.ViewHolder(binding.root) {

        init {
            itemView.setOnClickListener {
                listener.onItemClick(adapterPosition)
            }
        }

    }
}