package com.BrightLink.SalesOptim.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.BrightLink.SalesOptim.Constants.SharedPreferecneManager
import com.BrightLink.SalesOptim.Model.SalesOrderInvoiceProductModel
import com.BrightLink.SalesOptim.databinding.SalesOrderInvoiceProductItemLayoutBinding

class SalesOrderInvoiceProductAdapter(private var items: MutableList<SalesOrderInvoiceProductModel>) :
    RecyclerView.Adapter<SalesOrderInvoiceProductAdapter.onViewHolder>() {

    class onViewHolder(val binding : SalesOrderInvoiceProductItemLayoutBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): onViewHolder {
        val view = SalesOrderInvoiceProductItemLayoutBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return onViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: SalesOrderInvoiceProductAdapter.onViewHolder, position: Int) {
        holder.binding.srno.text = position.toString()
        holder.binding.productDes.text = items.get(position).productName.toString()
        holder.binding.qty.text = items.get(position).quantity.toString()
        holder.binding.price.text = items.get(position).price.toString()

        val price = items[position].price!!.toInt()
        val quantity  = items[position].quantity!!.toInt()
        val total = price * quantity
        holder.binding.totalPrice.text = total.toString()
    }

    fun removeLastItem(context : Context) {
        if (items.isNotEmpty()) {
            val lastItem = items.removeAt(items.size - 1)
            notifyDataSetChanged()
            SharedPreferecneManager.removeSalesOrderInvoiceProduct(context, lastItem)
        }
    }

}