package com.BrightLink.SalesOptim.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.BrightLink.SalesOptim.ResponseModel.ContactTaskDetailsModelItem
import com.BrightLink.SalesOptim.databinding.ContactTaskDetailsBinding

class ContactTaskDetailsAdapter(val context: Context, private var datalist: List<ContactTaskDetailsModelItem>) : RecyclerView.Adapter<ContactTaskDetailsAdapter.ViewHolder>() {
    class ViewHolder(val binding : ContactTaskDetailsBinding) : RecyclerView.ViewHolder(binding.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ContactTaskDetailsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return datalist.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.subject.text = datalist[position].subject
        holder.binding.contactName.text = datalist[position].customerName
        holder.binding.dueDate.text = datalist[position].duedate
        holder.binding.taskStatus.text = datalist[position].status
        holder.binding.taskPriority.text = datalist[position].priority
    }
}