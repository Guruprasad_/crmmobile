package com.BrightLink.SalesOptim.ResponseModel

data class ProductResponseModelX(
    val employeeId: Int,
    val productId: Int,
    val productName: String,
    val quantity: Int,
    val unitPrice: Double
)