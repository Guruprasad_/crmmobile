package com.BrightLink.SalesOptim.ResponseModel.NAL_ResModels

data class TaskLinksResModelItem(
    val accountId: Int,
    val contactid: Int,
    val createdBy: String,
    val createdDate: String,
    val employeeId: Int,
    val id: Int,
    val label: String,
    val leadId: Int,
    val linkDescription: String,
    val taskId: Int,
    val url: String
)