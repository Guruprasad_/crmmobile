package com.BrightLink.SalesOptim.ResponseModel.LeadDeal

data class TotalLead(
    val month: Int,
    val targetCount: Double,
    val year: Int
)