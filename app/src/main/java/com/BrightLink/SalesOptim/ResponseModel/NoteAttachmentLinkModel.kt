package com.BrightLink.SalesOptim.ResponseModel

data class NoteAttachmentLinkModel(
    val attachments: List<Attachment>,
    val links: List<Link>,
    val note: List<Note>
)