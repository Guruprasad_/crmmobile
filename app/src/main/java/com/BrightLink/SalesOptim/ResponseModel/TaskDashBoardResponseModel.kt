package com.BrightLink.SalesOptim.ResponseModel

data class TaskDashBoardResponseModel(
    val callLeadCount: Int,
    val emailLeadCount: Int,
    val negociatationLeadCount: Int,
    val proposalLeadCount: Int,
    val qualifiedLeadCount: Int,
    val topLeads: List<TopLead>
)