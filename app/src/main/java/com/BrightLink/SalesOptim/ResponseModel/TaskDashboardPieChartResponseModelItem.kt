package com.BrightLink.SalesOptim.ResponseModel

data class TaskDashboardPieChartResponseModelItem(
    val contactedLeadsPercentage: Double,
    val negociationLeadCountPercentage: Double,
    val proposalLeadPercentage: Double,
    val qualifiedLeadPercentage: Double
)