package com.BrightLink.SalesOptim.ResponseModel

data class LeadLineChartResponseModel(
    val resultList: List<Result>,
    val totalLeadList: List<TotalLead>
)