package com.BrightLink.SalesOptim.ResponseModel

data class LeadDashBoardResponseModel(
    val coldLeadCount: Int,
    val contactedLeads: Int,
    val hotLeadCount: Int,
    val lostLeadCount: Int,
    val proposalLeadCount: Int,
    val qualifiedLeadCount: Int,
    val totalLeadCount: Int,
    val warmLeadCount: Int,
    val wonLeadCount: Int
)