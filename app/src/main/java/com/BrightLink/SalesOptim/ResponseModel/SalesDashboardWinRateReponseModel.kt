package com.BrightLink.SalesOptim.ResponseModel

data class SalesDashboardWinRateReponseModel(
    val totalLeadCount: Int,
    val wonLeadCount: Int,
    val wonLeadPercentage: Double
)