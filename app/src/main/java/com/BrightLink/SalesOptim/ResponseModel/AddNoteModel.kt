package com.BrightLink.SalesOptim.ResponseModel

data class AddNoteModel(
    val accountId: Int,
    val contactid: Int,
    val contentType: String,
    val createdBy: String,
    val createdDate: String,
    val employeeId: Int,
    val fileName: String,
    val fileSize: String,
    val id: Int,
    val leadId: Int,
    val noteDescription: String,
    val noteFile: String,
    val taskId: Int
)