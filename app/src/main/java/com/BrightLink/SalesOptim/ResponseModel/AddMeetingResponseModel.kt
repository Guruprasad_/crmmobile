package com.BrightLink.SalesOptim.ResponseModel

data class AddMeetingResponseModel(
    val assignTo: String,
    val clientEmail: String,
    val contactId: Int,
    val contactName: String,
    val leadId: Int,
    val locationType: String,
    val meeting: List<Any>,
    val meetingFrom: String,
    val meetingId: Int,
    val meetingLinkOrLocation: String,
    val meetingTo: String,
    val proposal: Any,
    val title: String
)
