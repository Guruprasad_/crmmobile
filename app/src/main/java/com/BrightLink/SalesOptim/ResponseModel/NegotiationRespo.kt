package com.BrightLink.SalesOptim.ResponseModel

data class NegotiationRespo(
    val actualRevenue: Int,
    val description: String,
    val empId: Int,
    val leadId: Int,
    val negociableRevenue: Int,
    val negociationId: Int,
    val proposalStatus: String,
    val reason: String,
    val title: String,
    val userId: Int
)