package com.BrightLink.SalesOptim.ResponseModel

data class AddProductResponseModel(
    val productId: Int,
    val productName: String,
    val quantity: String,
    val unitPrice: String

)
