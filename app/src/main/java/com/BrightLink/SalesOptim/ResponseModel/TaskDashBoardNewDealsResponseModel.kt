package com.BrightLink.SalesOptim.ResponseModel

data class TaskDashBoardNewDealsResponseModel(
    val resultList: List<ResultX>,
    val totalLeadList: List<TotalLeadX>
)