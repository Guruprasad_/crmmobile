package com.BrightLink.SalesOptim.ResponseModel

data class PipelineReport(
    val actualLeads: Int,
    val actualRevenue: Any,
    val actualWonLeads: Int,
    val empName: String,
    val endDate: String,
    val monthlyLeads: Int,
    val monthlyRevenue: Int,
    val pid: Int,
    val startDate: String,
    val userId: Int,
    val winLeads: Int
)