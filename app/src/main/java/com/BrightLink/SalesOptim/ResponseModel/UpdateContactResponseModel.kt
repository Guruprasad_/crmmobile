package com.BrightLink.SalesOptim.ResponseModel

data class UpdateContactResponseModel(
    val leadOwnerName: String,
    val leadSource: String,
    val fullName: String,
    val title: String,
    val accountName: String,
    val vendorName: String,
    val phone: String,
    val alternatePhone: String,
    val department: String,
    val email: String,
    val twitter: String,
    val skypeId: String,
    val assistance: String,
    val assistantPhone: String,
    val fax: String,
    val mailingStreet: String,
    val otherStreet: String,
    val mailingCity: String,
    val otherCity: String,
    val mailingCountry: String,
    val otherCountry: String,
    val zipCode: String,
    val otherZipCode: String,
    val description: String,

    val accountId: Int,
    val vendorId: Int,

    val assistant: String,

    val mailingState: String,




    val othereState: String,
)
