package com.BrightLink.SalesOptim.ResponseModel

data class TaskAttachmentsResModel(
    val attachments: List<Attachment>,
    val links: List<Link>,
    val note:List<Note>
)