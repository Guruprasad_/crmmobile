package com.BrightLink.SalesOptim.ResponseModel

data class Contact(
    val callDuration: String,
    val contactBy: String,
    val contactId: Int,
    val contactType: String,
    val empId: Int,
    val leadId: Int,
    val modelDescription: String,
    val status: String,
    val subject: String,
    val taskTime: String,
    val title: String,
    val userId: Int
)