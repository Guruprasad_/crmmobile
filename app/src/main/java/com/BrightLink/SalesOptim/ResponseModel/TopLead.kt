package com.BrightLink.SalesOptim.ResponseModel

data class TopLead(
    val customerName: String,
    val totalRevenue: Int
)