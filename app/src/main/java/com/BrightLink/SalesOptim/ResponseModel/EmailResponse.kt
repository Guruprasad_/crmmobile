package com.BrightLink.SalesOptim.ResponseModel

data class EmailResponse(
    val emails: List<String>
)