package com.BrightLink.SalesOptim.ResponseModel.LeadDeal

data class Result(
    val actualCount: Int,
    val month: Int,
    val year: Int
)