package com.BrightLink.SalesOptim.ResponseModel

data class AttachmentResponse(
    val accountId: Int,
    val attachFile: String,
    val attachedBy: String,
    val contactid: Int,
    val contentType: String,
    val createdBy: String,
    val createdDate: String,
    val employeeId: Int,
    val fileName: String,
    val fileSize: Int,
    val id: Int,
    val leadId: Int,
    val taskId: Int
)