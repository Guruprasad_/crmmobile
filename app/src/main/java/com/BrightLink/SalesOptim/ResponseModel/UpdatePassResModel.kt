package com.BrightLink.SalesOptim.ResponseModel

data class UpdatePassResModel(
    val success: String,
    val message: String
)
