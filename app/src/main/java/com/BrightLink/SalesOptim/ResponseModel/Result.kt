package com.BrightLink.SalesOptim.ResponseModel

data class Result(
    val month: Int,
    val totalTask: Int,
    val year: Int
)