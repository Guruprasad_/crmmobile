package com.BrightLink.SalesOptim.ResponseModel.LeadDeal

data class LocationData (
    val latitude: Double,
    val longitude: Double
)
