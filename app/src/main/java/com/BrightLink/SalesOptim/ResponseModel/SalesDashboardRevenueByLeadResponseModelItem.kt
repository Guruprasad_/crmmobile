package com.BrightLink.SalesOptim.ResponseModel

data class SalesDashboardRevenueByLeadResponseModelItem(
    val coldCallCount: Double,
    val emailCount: Double,
    val facebookCount: Double,
    val instagramCount: Double,
    val linkedinCount: Double,
    val otherCount: Double,
    val referenceCount: Double
)