package com.BrightLink.SalesOptim.ResponseModel

data class PipliineDashboardResponseModel(
    val completeLeadStatusCount: Int,
    val inProcessTaskDiscovery: Int,
    val lastDate: Any,
    val meetingVisitCount: Int,
    val monthlyLead: Int,
    val monthlyRevenue: Any,
    val newLeadCount: Int,
    val pipelineReportList: List<PipelineReport>,
    val pipetable: List<Pipetable>,
    val proposalCount: Int,
    val targetDetails: List<TargetDetail>,
    val totalRevenue: Int,
    val wonDeals: Int
)