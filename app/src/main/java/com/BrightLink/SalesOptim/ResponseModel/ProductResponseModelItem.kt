package com.BrightLink.SalesOptim.ResponseModel

data class ProductResponseModelItem(
    val employeeId: Int,
    val productId: Int,
    val productName: String,
    val quantity: Int,
    val unitPrice: Double
)