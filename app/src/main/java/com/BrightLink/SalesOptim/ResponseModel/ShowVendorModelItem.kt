package com.BrightLink.SalesOptim.ResponseModel

import java.io.Serializable

data class ShowVendorModelItem(
    val category: String?,
    val city: String?,
    val country: String?,
    val description: String?,
    val email: String?,
    val emplyoeeId: Int?,
    val glaccount: String?,
    val phone: String?,
    val state: String?,
    val street: String?,
    val userId: Int?,
    val vendorId: Int?,
    val vendorName: String?,
    val vendorOwner: String?,
    val website: String?,
    val zipCode: String?
): Serializable
