package com.BrightLink.SalesOptim.ResponseModel

data class SalesDashBoardForcastResponseModel(
    val resultList: List<SalesDashBoardFrocastResult>
)