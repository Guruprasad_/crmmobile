package com.BrightLink.SalesOptim.ResponseModel

data class SalesOrderResponseModel(
    val createdBy: String,
    val createdDate: String,
    val employeeId: Int,
    val fileSize: String,
    val filename: String,
    val invoice: String,
    val salesOrderId: Int,
    val sendingTo: String,
    val userId: Int,
    val vendorId: Int,
    val vendorName: String
)