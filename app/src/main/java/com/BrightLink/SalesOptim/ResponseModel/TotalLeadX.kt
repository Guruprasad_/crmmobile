package com.BrightLink.SalesOptim.ResponseModel

data class TotalLeadX(
    val month: Int,
    val targetCount: Double,
    val year: Int
)