package com.BrightLink.SalesOptim.ResponseModel

data class AddAccountResponseModel(

    val accountOwner: String,
    val accountName: String,
    val accountSite: String,
    val accountNumber: String,
    val accountType: String,
    val industry: String,
    val revenue: Int,
    val rating: String,
    val phone: String,
    val fax: String,
    val website: String,
    val tickerSymbol: String,
    val ownership: String,
    val employees: Int,
    val siccode: String,
    val billingStreet: String,
    val billingCode: String,
    val shippingStreet: String,
    val shippingCode: String,
    val description: String,
    val billingCountryId: Int,
    val billingStateId: Int,
    val billingCityId: Int,
    val shippingCountryId: Int,
    val shippingStateId: Int,
    val shippingCityId: Int
)