package com.BrightLink.SalesOptim.ResponseModel

data class StateModelItem(
    val countries: Countries,
    val id: Int,
    val name: String

)
