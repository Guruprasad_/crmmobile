package com.BrightLink.SalesOptim.ResponseModel

data class Location(
    val alertZone: Any,
    val employeeId: Int,
    val latitude: Double,
    val loactionId: Int,
    val longitude: Double,
    val name: String,
    val timestamp: Long,
    val userId: Int
)