package com.BrightLink.SalesOptim.ResponseModel

data class TaskDashBoardAcheiveLeadsResponseModelItem(
    val month: Int,
    val wonLead: Int,
    val year: Int
)