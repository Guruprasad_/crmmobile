package com.BrightLink.SalesOptim.ResponseModel

data class ResultX(
    val actualCount: Int,
    val month: Int,
    val year: Int
)