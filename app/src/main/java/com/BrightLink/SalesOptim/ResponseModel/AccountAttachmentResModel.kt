package com.BrightLink.SalesOptim.ResponseModel

data class AccountAttachmentResModel (
//    val note:List<Note>,
//    val attachments: List<Attachment>,
//    val links: List<Link>

    val id: Int,
    val fileName: String,
    val contentType: String,
    val fileSize: String,
    val attachedBy: String,
    val createdBy: String,
    val createdDate: String,
    val taskId: Int,
    val leadId: Int,
    val accountId: Int?,
    val contactId: Int,
    val employeeId: Int,
    val title: Int,
    val linkDescription: String,
    val url: String,
    val label: String

)
