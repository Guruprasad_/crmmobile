package com.BrightLink.SalesOptim.ResponseModel

import java.io.Serializable

data class TaskListModelItem(
    val id: Int?,
    val taskOwner: String?,
    val subject: String?,
    val duedate: String?,
    val status: String?,
    val priority: String?,
    val createdBy: String?,
    val modifyBy: String?,
    val customerName: String?,
    val description: String?,
    val reminderDate: String?,
    val createdDate: String?,
    val modifyDate: String?,
    val leadOrAccountType: String?,
    val leadIdOrAccountId: Int?,
    val employeeId: Int?,
): Serializable
