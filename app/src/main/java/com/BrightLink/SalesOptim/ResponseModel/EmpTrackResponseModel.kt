package com.BrightLink.SalesOptim.ResponseModel

data class EmpTrackResponseModel(
    val emplyoeeId: Int,
    val latitud: Double,
    val longitude: Double,
    val name: String,
    val timestamp: Long,
    val userId: Int
)