package com.BrightLink.SalesOptim.ResponseModel.ActivityStatus

data class ProposalStatusItem(
    val contentType: String,
    val createdDate: String,
    val description: String,
    val empId: Int,
    val fileSize: String,
    val filename: String,
    val leadId: Int,
    val proposalEmail: String,
    val proposalId: Int,
    val status: String,
    val title: String,
    val userId: Int
)