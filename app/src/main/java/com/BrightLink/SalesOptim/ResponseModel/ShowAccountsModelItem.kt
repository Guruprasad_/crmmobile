package com.BrightLink.SalesOptim.ResponseModel

import java.io.Serializable

data class ShowAccountsModelItem(

    val accountName: String?,
    val accountNumber: String?,
    val accountOwner: String?,
    val accountSite: String?,
    val accountType: String?,
    val billingCityId: String?,
    val billingCity: String?,
    val billingCode: String?,
    val billingCountryId: String?,
    val billingCountry: String?,
    val billingStateId: String?,
    val billingState: String?,
    val billingStreet: String?,
    val description: String?,
    val employeeId: Int?,
    val employees: Int?,
    val fax: String?,
    val industry: String?,
    val ownership: String?,
    val phone: String?,
    val rating: String?,
    val revenue: String?,
    val shippingCityId: Int?,
    val shippingCity: String?,
    val shippingCode: String?,
    val shippingCountryId: Int?,
    val shippingCountry: String?,
    val shippingStateId: Int?,
    val shippingState: String?,
    val shippingStreet: String?,
    val siccode: String?,
    val tickerSymbol: String?,
    val website: String?,
    val accountId: Int?,
    val vendorName: String?
): Serializable {

    override fun hashCode(): Int {
        var result = 17
        result = 31 * result + (accountName?.hashCode() ?: 0)
        result = 31 * result + (accountNumber?.hashCode() ?: 0)
        result = 31 * result + (accountOwner?.hashCode() ?: 0)
        result = 31 * result + (accountSite?.hashCode() ?: 0)
        result = 31 * result + (accountType?.hashCode() ?: 0)
        result = 31 * result + (billingCityId?.hashCode() ?: 0)
        result = 31 * result + (billingCode?.hashCode() ?: 0)
        result = 31 * result + (billingCountryId?.hashCode() ?: 0)
        result = 31 * result + (billingStateId?.hashCode() ?: 0)
        result = 31 * result + (billingStreet?.hashCode() ?: 0)
        result = 31 * result + (description?.hashCode() ?: 0)
        result = 31 * result + (employeeId ?: 0)
        result = 31 * result + (employees ?: 0)
        result = 31 * result + (fax?.hashCode() ?: 0)
        result = 31 * result + (industry?.hashCode() ?: 0)
        result = 31 * result + (ownership?.hashCode() ?: 0)
        result = 31 * result + (phone?.hashCode() ?: 0)
        result = 31 * result + (rating?.hashCode() ?: 0)
        result = 31 * result + (revenue?.hashCode() ?: 0)
        result = 31 * result + (shippingCityId ?: 0)
        result = 31 * result + (shippingCode?.hashCode() ?: 0)
        result = 31 * result + (shippingCountryId ?: 0)
        result = 31 * result + (shippingStateId ?: 0)
        result = 31 * result + (shippingStreet?.hashCode() ?: 0)
        result = 31 * result + (siccode?.hashCode() ?: 0)
        result = 31 * result + (tickerSymbol?.hashCode() ?: 0)
        result = 31 * result + (website?.hashCode() ?: 0)
        result = 31 * result + (accountId ?: 0)
//        result = 31 * result + (vendorName?.hashCode() ?: 0)
        return result
    }
}
