package com.BrightLink.SalesOptim.ResponseModel

data class SalesListModelItem(
    val createdBy: String,
    val createdDate: String,
    val employeeId: Int,
    val invoice: String,
    val salesOrderId: Int,
    val sendingTo: String,
    val userId: Int,
    val vendorId: Int,
    val vendorName: String,
    val filename : String,
    val fileSize : String
)