package com.BrightLink.SalesOptim.ResponseModel

data class AddContactsProposalResponse(
    val callDuration: String,
    val contactBy: String,
    val contactId: Int,
    val contactType: String,
    val empId: Int,
    val leadId: Int,
    val modelDescription: String,
    val status: Any,
    val subject: String,
    val taskTime: String,
    val title: String,
    val userId: Int

)
