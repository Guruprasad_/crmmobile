package com.BrightLink.SalesOptim.ResponseModel

data class States(
    val countries: CountriesX,
    val id: Int,
    val name: String

)
