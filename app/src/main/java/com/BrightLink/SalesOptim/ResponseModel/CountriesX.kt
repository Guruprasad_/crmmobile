package com.BrightLink.SalesOptim.ResponseModel

data class CountriesX(
    val id: Int,
    val name: String,
    val phoneCode: Int,
    val shortName: String
)
