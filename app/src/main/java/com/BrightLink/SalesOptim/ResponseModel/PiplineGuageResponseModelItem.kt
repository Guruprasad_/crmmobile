package com.BrightLink.SalesOptim.ResponseModel

data class PiplineGuageResponseModelItem(
    val actualLeads: Int,
    val actualRevenue: Int,
    val actualWonLeads: Int,
    val empName: String,
    val endDate: String,
    val monthlyLeads: Int,
    val monthlyRevenue: Int,
    val pid: Int,
    val startDate: String,
    val userId: Int,
    val winLeads: Int
)