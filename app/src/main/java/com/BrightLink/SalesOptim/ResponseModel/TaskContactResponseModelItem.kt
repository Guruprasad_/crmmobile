package com.BrightLink.SalesOptim.ResponseModel

data class TaskContactResponseModelItem(
    val assignedTo: String,
    val callDuration: String,
    val city: String,
    val companyName: String,
    val contactId: Int,
    val createdDate: String,
    val customerName: String,
    val description: String,
    val emplyoeeId: Int,
    val leadDescription: String,
    val leadId: Int,
    val leadRevenue: String,
    val leadStatus: String,
    val leadcreatedDate: String,
    val negotiationId: Int,
    val proposalId: Int,
    val qualifyId: Int,
    val status: String,
    val taskAssignedBy: String,
    val taskId: Int,
    val taskTitle: String,
    val taskType: String,
    val tasktimeDate: Any
)