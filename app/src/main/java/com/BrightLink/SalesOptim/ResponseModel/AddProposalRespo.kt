package com.BrightLink.SalesOptim.ResponseModel

data class AddProposalRespo(
    val contentType: String,
    val createdDate: String,
    val description: String,
    val empId: Int,
    val invoice: String,
    val leadId: Int,
    val proposalEmail: String,
    val proposalId: Int,
    val status: String,
    val title: String,
    val userId: Int
)