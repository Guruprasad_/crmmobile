package com.BrightLink.SalesOptim.ResponseModel.ActivityStatus

data class QualifyStatusItem(
    val empId: Int,
    val leadId: Int,
    val qualifyId: Int,
    val qualifyStatus: String,
    val reason: String,
    val requirement: String,
    val title: String,
    val userId: Int
)