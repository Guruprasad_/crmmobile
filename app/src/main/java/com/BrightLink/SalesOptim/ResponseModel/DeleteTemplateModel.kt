package com.BrightLink.SalesOptim.ResponseModel

data class DeleteTemplateModel(
    val `data`: Any,
    val message: String,
    val status: String
)