package com.BrightLink.SalesOptim.ResponseModel

data class SalesDashBoardFrocastResult(
    val achievementCount: Int,
    val month: Int,
    val opportunityCount: Double,
    val year: Int
)