package com.BrightLink.SalesOptim.ResponseModel

data class ContactTaskDetailsModelItem(
    val assginTo: Any,
    val color: Any,
    val createdBy: String,
    val createdDate: String,
    val customerName: String,
    val description: String,
    val duedate: String,
    val employeeId: Int,
    val endDate: Any,
    val id: Int,
    val leadIdOrAccountId: Int,
    val leadOrAccountType: String,
    val modifyBy: String,
    val modifyDate: String,
    val priority: String,
    val reminderDate: String,
    val status: String,
    val subject: String,
    val taskOwner: String
)