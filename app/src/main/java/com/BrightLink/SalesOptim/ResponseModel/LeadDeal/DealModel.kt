package com.BrightLink.SalesOptim.ResponseModel.LeadDeal

data class DealModel(
    val dealsLossList: List<DealsLoss>,
    val dealsProposalList: List<DealsProposal>,
    val dealsQualifiedList: List<DealsQualified>,
    val dealsUntouchList: List<DealsUntouch>,
    val dealsWonList: List<DealsWon>,
    val lossCount: Int,
    val lossPercentage: Int,
    val lossRevenue: Double,
    val proposalCount: Int,
    val proposalPercentage: Int,
    val proposalReveune: Double,
    val qualifiedCount: Int,
    val qualifiedPercentage: Int,
    val qualifiedReveune: Double,
    val untouchPercentage: Int,
    val untouchedCount: Int,
    val untouchedRevenue: Double,
    val wonCount: Int,
    val wonPercentage: Int,
    val wonReveune: Double
)