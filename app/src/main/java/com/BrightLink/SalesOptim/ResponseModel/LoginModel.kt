package com.BrightLink.SalesOptim.ResponseModel

data class LoginModel(
    val adminId: Int,
    val companyAddress: String,
    val companyEmail: String,
    val companyName: String,
    val companyPhone: String,
    val email: String,
    val emailAccess: Int,
    val empID: Int,
    val empStatus: String,
    val enabled: Boolean,
    val expiryDate: String,
    val gstin: String,
    val id: Int,
    val integrationAccess: Int,
    val jwtToken: String,
    val leadDashboardAccess: Int,
    val name: String,
    val pipelineAccess: Int,
    val planName: String,
    val role: String,
    val taskDashboardAccess: Int,
    val timeZone: String,
    val trackingAccess: Int
)