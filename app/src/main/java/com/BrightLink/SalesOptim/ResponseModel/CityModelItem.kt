package com.BrightLink.SalesOptim.ResponseModel

data class CityModelItem(
    val id: Int,
    val name: String,
    val states: States
)
