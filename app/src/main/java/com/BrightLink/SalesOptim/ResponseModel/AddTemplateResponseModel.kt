package com.BrightLink.SalesOptim.ResponseModel

data class AddTemplateResponseModel(
    val createdBy: String,
    val createdDate: String,
    val description: String,
    val lastModified: String,
    val templateId: Int,
    val title: String,
    val type: String
)