package com.BrightLink.SalesOptim.ResponseModel

data class PasswordResponse(
    val success: String
)