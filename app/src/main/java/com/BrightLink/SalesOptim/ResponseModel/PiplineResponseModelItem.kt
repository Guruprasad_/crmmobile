package com.BrightLink.SalesOptim.ResponseModel

import com.google.gson.annotations.SerializedName

data class PiplineResponseModelItem(
    @SerializedName("Achieved Leads")
    val Achieved_Leads: Int,
    @SerializedName("Achieved Revenue")
    val Achieved_Revenue: Int,
    @SerializedName("Achieved Won Leads")
    val Achieved_Won_Leads: Int,
    @SerializedName("Contact Count")
    val Contact_Count: Int,
    @SerializedName("Negociation Count")
    val Negociation_Count: Int,
    @SerializedName("New Lead")
    val New_Lead: Int,
    @SerializedName("Proposal Count")
    val Proposal_Count: Int,
    @SerializedName("Qualify Count")
    val Qualify_Count: Int
)