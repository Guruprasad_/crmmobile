package com.BrightLink.SalesOptim.ResponseModel

data class LeadBarChartResponseModelItem(
    val lostLeadPercentage: Double,
    val proposalLeadPercentage: Double,
    val qualifiedLeadPercentage: Double,
    val wonLeadPercentage: Double
)