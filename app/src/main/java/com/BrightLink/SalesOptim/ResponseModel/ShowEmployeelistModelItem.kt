package com.BrightLink.SalesOptim.ResponseModel

data class ShowEmployeelistModelItem(
    val base64Content: Any,
    val cId: Int,
    val department: String,
    val description: String,
    val disableDate: Any,
    val email: String,
    val emailAccess: Int,
    val enableDate: Any,
    val gender: String,
    val hiredate: String,
    val image: Any,
    val images: String,
    val integrationAccess: Int,
    val leaveDate: Any,
    val name: String,
    val phone: String,
    val pipelineReports: List<Any>,
    val secondName: String,
    val shiftTime: String,
    val status: Boolean,
    val timeZone: String,
    val trackingAccess: Int,
    val work: String
)