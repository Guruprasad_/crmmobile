package com.BrightLink.SalesOptim.ResponseModel

data class Countries(
    val id: Int,
    val name: String,
    val phoneCode: Int,
    val shortName: String

)
