package com.BrightLink.SalesOptim.ResponseModel

data class ActivityAssignLeadResponse(
    val contactList: List<Contact>,
    val negociationList: List<Negociation>,
    val proposalList: List<Proposal>,
    val qualifyList: List<Qualify>
)