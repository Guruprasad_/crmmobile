package com.BrightLink.SalesOptim.ResponseModel

import com.google.gson.annotations.SerializedName

data class SalesDashboardResp(
    @SerializedName("resultList")
    val resultList: List<SalesDashBoardFrocastResult>
)