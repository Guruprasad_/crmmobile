package com.BrightLink.SalesOptim.ResponseModel

data class TotalLead(
    val month: Int,
    val totalTask: Int,
    val year: Int
)