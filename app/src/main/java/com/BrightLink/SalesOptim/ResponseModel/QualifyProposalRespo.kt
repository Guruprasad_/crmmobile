package com.BrightLink.SalesOptim.ResponseModel

data class QualifyProposalRespo(
    val empId: Int,
    val leadId: Int,
    val qualifyId: Int,
    val qualifyStatus: String,
    val reason: String,
    val requirement: String,
    val title: String,
    val userId: Int
)