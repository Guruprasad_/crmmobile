package com.BrightLink.SalesOptim.ResponseModel

import java.io.Serializable

data class ThirdPartyIntegrationResModel(
    val adsId: String?=" ",
    val appId: String?=" ",
    val appSecret: String?=" ",

    val platform: String?=" ",
    val tokenOrUrl: String?=" ",
    val id: Int,
): Serializable