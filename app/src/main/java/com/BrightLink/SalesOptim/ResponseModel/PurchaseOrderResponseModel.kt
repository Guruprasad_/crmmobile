package com.BrightLink.SalesOptim.ResponseModel

data class PurchaseOrderResponseModel(
    val createdBy: String,
    val createdDate: String,
    val employeeId: Int,
    val invoice: String,
    val purchaseOrderId: Int,
    val sendingTo: String,
    val userId: Int,
    val vendorId: Int,
    val vendorName: String
)