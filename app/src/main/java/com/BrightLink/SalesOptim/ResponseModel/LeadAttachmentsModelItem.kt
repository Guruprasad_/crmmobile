package com.BrightLink.SalesOptim.ResponseModel

data class LeadAttachmentsModelItem(
    val accountId: Int,
    val attachedBy: Any,
    val contactId: Int,
    val contentType: String,
    val createdBy: String,
    val createdDate: String,
    val employeeId: Int,
    val fileName: String,
    val fileSize: String,
    val id: Int,
    val leadId: Int,
    val taskId: Int
)