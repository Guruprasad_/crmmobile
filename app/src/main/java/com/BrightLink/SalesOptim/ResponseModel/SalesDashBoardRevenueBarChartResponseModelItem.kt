package com.BrightLink.SalesOptim.ResponseModel

data class SalesDashBoardRevenueBarChartResponseModelItem(
    val month: Int,
    val revenue1: String,
    val year: Int
)