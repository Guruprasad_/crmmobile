package com.BrightLink.SalesOptim.ResponseModel

data class GetMapResponseModel(
    val activeCount: Int,
    val inactiveCount: Int,
    val locations: List<Location>,
    val totalCount: Int
)