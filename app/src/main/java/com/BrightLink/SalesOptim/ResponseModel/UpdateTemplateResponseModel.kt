package com.BrightLink.SalesOptim.ResponseModel

data class UpdateTemplateResponseModel(
    val createdBy: String,
    val createdDate: String,
    val description: String,
    val lastModified: String,
    val templateId: Int,
    val title: String,
    val type: String
)