package com.BrightLink.SalesOptim.ResponseModel.LeadDeal

data class TaskDashboardNewDealResponseModel(
    val resultList: List<Result>,
    val totalLeadList: List<TotalLead>
)