package com.BrightLink.SalesOptim.Model

data class SalesOrderInvoiceProductModel(
    val productName : String?="",
    val quantity : String?="",
    val price : String?="",
    val currency : String?="",
    val tax : String?="",
    val shippingCharges : String?=""
)
