package com.BrightLink.placesapi.remote.data


class CountryResponseDto : ArrayList<CountryResponseDto.CountryResponseDtoItem>(){
    data class CountryResponseDtoItem(
        val id: Int?,
        val name: String?,
        val phoneCode: Int?,
        val shortName: String?
    )
}