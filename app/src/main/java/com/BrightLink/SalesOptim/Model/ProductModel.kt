package com.BrightLink.SalesOptim.Model

data class ProductModel(

    val productName : String?="",
    val quantity : String?="",
    val unitPrice : String?="",

)
