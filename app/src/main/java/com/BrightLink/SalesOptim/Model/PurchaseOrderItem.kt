package com.BrightLink.SalesOptim.Model

data class PurchaseOrderItem(
    val productName : String?="",
    val quantity : String?="",
    val unitPrice : String?="",

)
