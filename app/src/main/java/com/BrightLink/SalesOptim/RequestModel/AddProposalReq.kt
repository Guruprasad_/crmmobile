package com.BrightLink.SalesOptim.RequestModel

data class AddProposalReq(
    val description: String,
    val leadId: Int,
    val proposalEmail: String,
    val title: String
)