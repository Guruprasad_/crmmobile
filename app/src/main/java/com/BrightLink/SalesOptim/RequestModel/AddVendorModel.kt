package com.BrightLink.SalesOptim.RequestModel

data class AddVendorModel(
    val category: String,
    val city: String,
    val country: String,
    val description: String,
    val email: String,
    val glaccount: String,
    val phone: String,
    val state: String,
    val street: String,
    val vendorName: String,
    val vendorOwner: String,
    val website: String,
    val zipCode: String
)