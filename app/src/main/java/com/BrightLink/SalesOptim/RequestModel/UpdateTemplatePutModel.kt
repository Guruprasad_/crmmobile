package com.BrightLink.SalesOptim.RequestModel

data class UpdateTemplatePutModel(
    val description: String,
    val templateId: String,
    val title: String,
    val type: String
)