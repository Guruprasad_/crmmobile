package com.BrightLink.SalesOptim.RequestModel

import java.io.File

data class AttachmentModel(
    val accountId: Int?,
    val attachFile: File,
    val attachedBy: String?,
    val contactid: Int?,
    val contentType: String?,
    val createdBy: String?,
    val createdDate: String?,
    val employeeId: Int?,
    val fileName: String?,
    val fileSize: Int?,
    val id: Int?,
    val leadId: Int?,
    val taskId: Int?

)
