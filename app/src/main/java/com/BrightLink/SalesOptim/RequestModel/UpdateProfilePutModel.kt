package com.BrightLink.SalesOptim.RequestModel

data class UpdateProfilePutModel(
    val dateOfBirth: String,
    val dept: String,
    val empStatus: String,
    val file: String,
    val gender: String,
    val id: Int,
    val jobTitle: String,
    val name: String,
    val permanantAddr: String,
    val phone: String
)