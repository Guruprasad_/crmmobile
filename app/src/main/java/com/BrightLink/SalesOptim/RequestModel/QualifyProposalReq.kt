package com.BrightLink.SalesOptim.RequestModel

data class QualifyProposalReq(
    val leadId: Int,
    val qualifyStatus: String?,
    val reason: String,
    val requirement: String,
    val title: String
)