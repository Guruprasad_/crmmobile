package com.BrightLink.SalesOptim.RequestModel

data class UpdateProductRequestModel(
    val employeeId: Int,
    val productId: Int,
    val productName: String,
    val quantity: String,
    val unitPrice: Double
)