package com.BrightLink.SalesOptim.RequestModel

data class RequestModelAddLead(
    val address: String,
    val alternateMobileNumber: String,
    val cityid: String,
    val companyName: String,
    val countryid: String,
    val customerName: String,
    val description: String,
    val email: String,
    val followUpdate: String,
    val leadCreatedBy: String,
    val leadRevenue: String,
    val leadSource: String,
    val leadStatus: String,
    val leadType: String,
    val mobileNumber: String,
    val requirement: String,
    val stateid: String,
    val totalRevenue: String,
    val website: String,
    val zip: String
)