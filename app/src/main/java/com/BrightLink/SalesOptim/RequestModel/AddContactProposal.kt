package com.BrightLink.SalesOptim.RequestModel

data class AddContactProposal(
    var callDuration: String,
    val contactBy: String,
    var contactType: String,
    val leadId: Int?,
    val modelDescription: String,
    val subject: String,
    val taskTime: String,
    var title: String

)
