package com.BrightLink.SalesOptim.RequestModel

data class AddProductRequestModel(
    val employeeId: String,
    val productName: String,
    val quantity: String,
    val unitPrice: String
)