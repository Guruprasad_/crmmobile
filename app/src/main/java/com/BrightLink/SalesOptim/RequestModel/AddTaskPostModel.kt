package com.BrightLink.SalesOptim.RequestModel

import java.io.Serializable

data class AddTaskPostModel(
    val description: String,
    val duedate: String,
    val leadIdOrAccountId: String,
    val leadOrAccountType: String,
    val priority: String,
    val reminderDate: String,
    val status: String,
    val subject: String,
    val taskOwner: String
):Serializable