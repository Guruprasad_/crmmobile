package com.BrightLink.SalesOptim.RequestModel

data class AddLeadPostModel(
    val accountId: String?,
    val address: String,
    val alternateMobileNumber: String,
    val city: String,
    val companyName: String,
    val country: String,
    val customerName: String,
    val description: String,
    val email: String,
    val followUpdate: String,
    val leadCreatedBy: String,
    val leadRevenue: String,
    val leadSource: String,
    val leadStatus: String,
    val leadType: String,
    val mobileNumber: String,
    val requirement: String,
    val state: String,
    val website: String,
    val zip: String

)