package com.BrightLink.SalesOptim.RequestModel

data class AttachmentDto (
    val leadId: Int,
    val taskId: Int,
    val contactId: Int,
    val accountId: Int
)