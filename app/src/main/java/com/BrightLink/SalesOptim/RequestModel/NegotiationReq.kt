package com.BrightLink.SalesOptim.RequestModel

data class NegotiationReq(
    val actualRevenue: String,
    val description: String,
    val leadId: Int,
    val negociableRevenue: String,
    val proposalStatus: String,
    val reason: String,
    val title: String
)