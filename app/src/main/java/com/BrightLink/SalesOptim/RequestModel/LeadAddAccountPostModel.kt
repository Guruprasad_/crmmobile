package com.BrightLink.SalesOptim.RequestModel

data class LeadAddAccountPostModel(
    val accountEmail: String,
    val accountName: String,
    val accountType: String,
    val phone: String

)
