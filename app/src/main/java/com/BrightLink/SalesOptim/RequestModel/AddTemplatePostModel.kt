package com.BrightLink.SalesOptim.RequestModel

data class AddTemplatePostModel(
    val description: String,
    val title: String,
    val type: String
)