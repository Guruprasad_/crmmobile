package com.BrightLink.SalesOptim.RequestModel

data class AddContactReq(
    val callDuration: String,
    val contactBy: String,
    val contactType: String,
    val leadId: Int,
    val modelDescription: String,
    val subject: String,
    val taskTime: String,
    val title: String
)