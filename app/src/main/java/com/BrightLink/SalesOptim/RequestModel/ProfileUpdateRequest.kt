package com.BrightLink.SalesOptim.RequestModel

data class ProfileUpdateRequest(
    val dateOfBirh: String,
    val dept: String,
    val empStatus: String,
    val gender: String,
    val id: Int,
    val jobTitle: String,
    val name: String,
    val permanantAddr: String,
    val phone: String
)