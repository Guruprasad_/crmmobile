package com.BrightLink.SalesOptim.RequestModel

data class ALMeetingPostModel(
    val assignTo: String,
    val clientEmail: String,
    val leadId: Int,
    val locationType: String,
    val meetingFrom: String,
    val meetingLinkOrLocation: String,
    val meetingTo: String,
    val title: String
)