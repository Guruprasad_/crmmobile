package com.BrightLink.SalesOptim.RequestModel

data class CountryModel(
    val id: Int,
    val shortName: String?="",
    val name: String?="",
    val phoneCode: Int
)
