package com.BrightLink.SalesOptim.RequestModel

data class ThirdPartyIntegrationPostModel(
    val adsId: String,
    val appId: String,
    val appSecret: String,
    val platform: String,
    val tokenOrUrl: String
)