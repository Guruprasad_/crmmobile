package com.BrightLink.SalesOptim.RequestModel

data class UpdateTaskReqModel(
    val description: String,
    val duedate: String,
    val id: String,
    val leadIdOrAccountIdOrContactId: String,
    val leadOrAccountOrContactType: String,
    val priority: String,
    val reminderDate: String,
    val status: String,
    val subject: String,
    val taskOwner: String
)