package com.BrightLink.SalesOptim.RequestModel

data class AddAttachments(
    val accountId: Int,
    val contactid: Int,
    val file: Any,
    val leadId: Int,
    val taskId: Int

)
