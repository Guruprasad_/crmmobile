package com.BrightLink.SalesOptim.RequestModel

data class LoginPostModel(
    val password: String,
    val username: String
)