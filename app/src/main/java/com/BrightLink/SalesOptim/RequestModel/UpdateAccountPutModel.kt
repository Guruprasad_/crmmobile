package com.BrightLink.SalesOptim.RequestModel

data class UpdateAccountPutModel(
    val accountName: String?=" ",
    val accountSite: String?=" ",
    val accountNumber: String?=" ",
    val accountType: String?=" ",
    val industry: String?=" ",
    val revenue: String?=" ",
    val rating: String?=" ",
    val phone: String?=" ",
    val fax: String?=" ",
    val website: String?=" ",
    val tickerSymbol: String?=" ",
    val ownership: String?=" ",
    val employees: Int?,
    val billingStreet: String?=" ",
    val billingCode: String?=" ",
    val shippingStreet: String?=" ",
    val shippingCode: String?=" ",
    val description: String?=" ",
    val billingCountryId: Int?,
    val billingStateId: Int?,
    val billingCityId: Int?,
    val shippingCountryId: Int?,
    val shippingStateId: Int?,
    val shippingCityId: Int?,
    val accountId: Int?




)
