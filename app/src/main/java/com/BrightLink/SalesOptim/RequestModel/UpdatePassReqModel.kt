package com.BrightLink.SalesOptim.RequestModel

data class UpdatePassReqModel(
    val newPassword: String,
    val oldPassword: String
)