package com.BrightLink.SalesOptim.RequestModel

data class CreatePurchaseModel(
    val createdBy: String,
    val sendingTo: String,
    val vendorId: String
)
