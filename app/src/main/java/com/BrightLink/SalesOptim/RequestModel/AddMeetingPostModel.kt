package com.BrightLink.SalesOptim.RequestModel

data class AddMeetingPostModel(
//    val contactid: Int,
    val title: String,
    val meetingFrom: String,
    val meetingTo: String,
    val contactName: String,
    val clientEmail: String,
    val locationType: String,
    val meetingLinkOrLocation: String,
    val contactId: Int,
)
