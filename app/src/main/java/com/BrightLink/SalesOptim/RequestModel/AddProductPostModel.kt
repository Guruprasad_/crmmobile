package com.BrightLink.SalesOptim.RequestModel

data class AddProductPostModel(
    val productId: Int,
    val productName: String,
    val quantity: String,
    val unitPrice: String
)
