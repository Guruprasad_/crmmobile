package com.BrightLink.SalesOptim.RequestModel

data class AddLinkReqModel(
    val accountId: Int,
    val contactid: Int,
    val label: String,
    val leadId: Int,
    val linkDescription: String,
    val taskId: Int,
    val url: String
)