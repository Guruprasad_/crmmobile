package com.BrightLink.SalesOptim.RequestModel

data class AddNoteRequest(
    val accountId: Int,
    val contactid: Int,
    val leadId: Int,
    val noteDescription: String,
    val taskId: Int
)