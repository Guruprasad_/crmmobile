package com.BrightLink.SalesOptim.Retrofit


import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminReqModel.AdminAddTaskReqModel
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminRequestModel.AddEmployeePostModel
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminRequestModel.AddPipelinePostModel
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminRequestModel.AdminPostMeetingAL
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminRequestModel.UpdateEmployeePostModel
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminRequestModel.UpdatePipelineReportPutModel
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel.AccountNameByEmpIdResponseItem
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel.AddEmployeeAdminResponseModel
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel.AddPipelineResponseModel
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel.AdminAcheiveLeadResponseModelItem
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel.AdminEmployeeDashboardResponseModel
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel.AdminLeadBarChartResponseModel
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel.AdminLeadLineChartResponseModel
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel.AdminLeadListModelItem
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel.AdminLeadReportModelItem
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel.AdminMeetingPostRes
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel.AdminNewDealsResponseModel
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel.AssignLeadToResponse
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel.EmployeeDetailsByIdModel
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel.GetPipelineDetailsByIdModel
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel.PipelineReportModelItem
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel.UpdateEmployeeResponseModel
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel.UpdatePipelineReportResponseModel
import com.BrightLink.SalesOptim.RequestModel.ALMeetingPostModel
import com.BrightLink.SalesOptim.RequestModel.AddAccountPostModel
import com.BrightLink.SalesOptim.RequestModel.AddContactPostModel
import com.BrightLink.SalesOptim.RequestModel.AddContactProposal
import com.BrightLink.SalesOptim.RequestModel.AddContactReq
import com.BrightLink.SalesOptim.RequestModel.AddLeadPostModel
import com.BrightLink.SalesOptim.RequestModel.AddLinkReqModel
import com.BrightLink.SalesOptim.RequestModel.AddMeetingPostModel
import com.BrightLink.SalesOptim.RequestModel.AddNoteRequest
import com.BrightLink.SalesOptim.RequestModel.AddProductRequestModel
import com.BrightLink.SalesOptim.RequestModel.AddProposalReq
import com.BrightLink.SalesOptim.RequestModel.AddTaskPostModel
import com.BrightLink.SalesOptim.RequestModel.AddTemplatePostModel
import com.BrightLink.SalesOptim.RequestModel.AddVendorModel
import com.BrightLink.SalesOptim.RequestModel.AdminAddContactPostModel
import com.BrightLink.SalesOptim.RequestModel.AdminUpdateContactPutModel
import com.BrightLink.SalesOptim.RequestModel.AttachmentDto
import com.BrightLink.SalesOptim.RequestModel.CreatePurchaseModel
import com.BrightLink.SalesOptim.RequestModel.LeadAddAccountPostModel
import com.BrightLink.SalesOptim.RequestModel.LoginPostModel
import com.BrightLink.SalesOptim.RequestModel.NegotiationReq
import com.BrightLink.SalesOptim.RequestModel.ProfileUpdateRequest
import com.BrightLink.SalesOptim.RequestModel.QualifyProposalReq
import com.BrightLink.SalesOptim.RequestModel.ThirdPartyIntegrationPostModel
import com.BrightLink.SalesOptim.RequestModel.UpdateAccountPutModel
import com.BrightLink.SalesOptim.RequestModel.UpdateContactPutModel
import com.BrightLink.SalesOptim.RequestModel.UpdateLeadInfoPutModel
import com.BrightLink.SalesOptim.RequestModel.UpdatePassReqModel
import com.BrightLink.SalesOptim.RequestModel.UpdateProductRequestModel
import com.BrightLink.SalesOptim.RequestModel.UpdateProfilePutModel
import com.BrightLink.SalesOptim.RequestModel.UpdateTaskReqModel
import com.BrightLink.SalesOptim.RequestModel.UpdateTemplatePutModel
import com.BrightLink.SalesOptim.RequestModel.UpdateVendorPutmodel
import com.BrightLink.SalesOptim.ResponseModel.ALMeetingModelItem
import com.BrightLink.SalesOptim.ResponseModel.ALMeetingResponseModel
import com.BrightLink.SalesOptim.ResponseModel.AccountAttachmentResModel
import com.BrightLink.SalesOptim.ResponseModel.AccountContactDetailsModelItem
import com.BrightLink.SalesOptim.ResponseModel.AccountDetailsByIdModel
import com.BrightLink.SalesOptim.ResponseModel.AccountTaskDetailsModelItem
import com.BrightLink.SalesOptim.ResponseModel.ActivityAssignLeadResponse
import com.BrightLink.SalesOptim.ResponseModel.ActivityStatus.ContactStatus
import com.BrightLink.SalesOptim.ResponseModel.ActivityStatus.NegotiationStatus
import com.BrightLink.SalesOptim.ResponseModel.ActivityStatus.ProposalStatus
import com.BrightLink.SalesOptim.ResponseModel.ActivityStatus.QualifyStatus
import com.BrightLink.SalesOptim.ResponseModel.AddAccountResponseModel
import com.BrightLink.SalesOptim.ResponseModel.AddContactRespo
import com.BrightLink.SalesOptim.ResponseModel.AddContactResponseModel
import com.BrightLink.SalesOptim.ResponseModel.AddContactsProposalResponse
import com.BrightLink.SalesOptim.ResponseModel.AddLeadModel
import com.BrightLink.SalesOptim.ResponseModel.AddLinkResponseModel
import com.BrightLink.SalesOptim.ResponseModel.AddMeetingResponseModel
import com.BrightLink.SalesOptim.ResponseModel.AddNoteModel
import com.BrightLink.SalesOptim.ResponseModel.AddProductResponseModel
import com.BrightLink.SalesOptim.ResponseModel.AddProposalRespo
import com.BrightLink.SalesOptim.ResponseModel.AddTaskResModel
import com.BrightLink.SalesOptim.ResponseModel.AddTemplateResponseModel
import com.BrightLink.SalesOptim.ResponseModel.Admin.ShowEmployeeModelItem
import com.BrightLink.SalesOptim.ResponseModel.AttaModel
import com.BrightLink.SalesOptim.ResponseModel.CityModelItem
import com.BrightLink.SalesOptim.ResponseModel.ContactAttachmentsResModel
import com.BrightLink.SalesOptim.ResponseModel.ContactMeetingDetailsModelItem
import com.BrightLink.SalesOptim.ResponseModel.ContactTaskDetailsModelItem
import com.BrightLink.SalesOptim.ResponseModel.CountryModelItem
import com.BrightLink.SalesOptim.ResponseModel.DeleteTemplateModel
import com.BrightLink.SalesOptim.ResponseModel.EmpTrackResponseModel
import com.BrightLink.SalesOptim.ResponseModel.GetMapResponseModel
import com.BrightLink.SalesOptim.ResponseModel.LeadAddAccountResponseModel
import com.BrightLink.SalesOptim.ResponseModel.LeadAttachmentsModelItem
import com.BrightLink.SalesOptim.ResponseModel.LeadBarChartResponseModel
import com.BrightLink.SalesOptim.ResponseModel.LeadDashBoardResponseModel
import com.BrightLink.SalesOptim.ResponseModel.LeadDeal.DealModel
import com.BrightLink.SalesOptim.ResponseModel.LeadDetailResponseModel
import com.BrightLink.SalesOptim.ResponseModel.LeadDetailsAccountModelItem
import com.BrightLink.SalesOptim.ResponseModel.LeadDetailsModel
import com.BrightLink.SalesOptim.ResponseModel.LeadLineChartResponseModel
import com.BrightLink.SalesOptim.ResponseModel.LeadLinksModelItem
import com.BrightLink.SalesOptim.ResponseModel.LeadNotesModelItem
import com.BrightLink.SalesOptim.ResponseModel.LeadReportModelItem
import com.BrightLink.SalesOptim.ResponseModel.LoginModel
import com.BrightLink.SalesOptim.ResponseModel.MeetingListModelItem
import com.BrightLink.SalesOptim.ResponseModel.NAL_ResModels.TaskAttaResModelItem
import com.BrightLink.SalesOptim.ResponseModel.NAL_ResModels.TaskLinksResModelItem
import com.BrightLink.SalesOptim.ResponseModel.NAL_ResModels.TaskNotesResModelItem
import com.BrightLink.SalesOptim.ResponseModel.NegotiationRespo
import com.BrightLink.SalesOptim.ResponseModel.NoteAttachmentLinkModel
import com.BrightLink.SalesOptim.ResponseModel.PasswordResponse
import com.BrightLink.SalesOptim.ResponseModel.PipliineDashboardResponseModel
import com.BrightLink.SalesOptim.ResponseModel.PiplineGuageResponseModelItem
import com.BrightLink.SalesOptim.ResponseModel.PiplineResponseModelItem
import com.BrightLink.SalesOptim.ResponseModel.ProductResponseModel
import com.BrightLink.SalesOptim.ResponseModel.ProductResponseModelX
import com.BrightLink.SalesOptim.ResponseModel.ProfileResponseModel
import com.BrightLink.SalesOptim.ResponseModel.PurchaseOrderModelItem
import com.BrightLink.SalesOptim.ResponseModel.PurchaseOrderResponseModel
import com.BrightLink.SalesOptim.ResponseModel.QualifyProposalRespo
import com.BrightLink.SalesOptim.ResponseModel.SalesDashBoardRevenueBarChartResponseModelItem
import com.BrightLink.SalesOptim.ResponseModel.SalesDashboardResp
import com.BrightLink.SalesOptim.ResponseModel.SalesDashboardRevenueByLeadResponseModelItem
import com.BrightLink.SalesOptim.ResponseModel.SalesDashboardWinRateReponseModel
import com.BrightLink.SalesOptim.ResponseModel.SalesListModelItem
import com.BrightLink.SalesOptim.ResponseModel.SalesOrderResponseModel
import com.BrightLink.SalesOptim.ResponseModel.ShowAccountsModelItem
import com.BrightLink.SalesOptim.ResponseModel.ShowContactModelItem
import com.BrightLink.SalesOptim.ResponseModel.ShowEmployeelistModelItem
import com.BrightLink.SalesOptim.ResponseModel.ShowLeadsModelItem
import com.BrightLink.SalesOptim.ResponseModel.ShowVendorModelItem
import com.BrightLink.SalesOptim.ResponseModel.StateModelItem
import com.BrightLink.SalesOptim.ResponseModel.TaskContactResponseModelItem
import com.BrightLink.SalesOptim.ResponseModel.TaskDashBoardAcheiveLeadsResponseModelItem
import com.BrightLink.SalesOptim.ResponseModel.TaskDashBoardNewDealsResponseModel
import com.BrightLink.SalesOptim.ResponseModel.TaskDashBoardResponseModel
import com.BrightLink.SalesOptim.ResponseModel.TaskDashboardPieChartResponseModelItem
import com.BrightLink.SalesOptim.ResponseModel.TaskListModelItem
import com.BrightLink.SalesOptim.ResponseModel.TemplateDetailsByIdModel
import com.BrightLink.SalesOptim.ResponseModel.TemplateModelItem
import com.BrightLink.SalesOptim.ResponseModel.ThirdPartyIntegrationResModel
import com.BrightLink.SalesOptim.ResponseModel.UpdateAccountResponseModel
import com.BrightLink.SalesOptim.ResponseModel.UpdateContactResponseModel
import com.BrightLink.SalesOptim.ResponseModel.UpdateLeadInfoResponseModel
import com.BrightLink.SalesOptim.ResponseModel.UpdateTaskResModel
import com.BrightLink.SalesOptim.ResponseModel.UpdateTemplateResponseModel
import com.BrightLink.SalesOptim.ResponseModel.UpdateVendorResponseModel
import com.BrightLink.SalesOptim.ResponseModel.VendorDetailsByIdModel
import com.BrightLink.SalesOptim.ResponseModel.VendorListResponseModelItem
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Part
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiInterface {
    @POST("/signin")
    fun login(@Body model : LoginPostModel): Call<LoginModel>

    @GET("/employee/meetinglist")
    fun meetinglist(@Header("Authorization") authorizationHeader: String):Call<List<MeetingListModelItem>>

    @POST("/employee/addlead")
    fun addlead(@Header("Authorization") authorizationHeader : String ,@Body model : AddLeadPostModel):Call<AddLeadModel>

    @POST("/employee/addaccount")
    fun addaccount(@Header("Authorization") jwtToken: String, @Body requestBody: AddAccountPostModel): Call<AddAccountResponseModel>

    @GET("/employee/accountlist")
    fun showaccounts(@Header("Authorization") authorizationHeader : String):Call<ArrayList<ShowAccountsModelItem>>

    @GET("/employee/taskreminder")
    fun taskreminder(@Header("Authorization") authorizationHeader : String):Call<ArrayList<TaskListModelItem>>
    @GET("/employee/followupdate")
    fun notification(@Header("Authorization") authorizationHeader : String):Call<ArrayList<ShowLeadsModelItem>>

    @GET("/employee/leadlist")
    fun showleads(@Header("Authorization") authorizationHeader : String):Call<List<ShowLeadsModelItem>>

    @GET("/employee/leadreport")
    fun showLeadReport(@Header("Authorization") authorizationHeader : String): Call<List<LeadReportModelItem>>

    @GET("/employee/vendorlist")
    fun showvendor(@Header("Authorization") authorizationHeader : String):Call<List<ShowVendorModelItem>>

    @GET("/employee/contactlist")
    fun showcontact(@Header("Authorization") authorizationHeader : String):Call<List<ShowContactModelItem>>

    @PUT("/employee/accountupdate")
    fun accountupdate(@Header("Authorization") jwtToken: String, @Body requestBody: UpdateAccountPutModel): Call<UpdateAccountResponseModel>

    @POST("/employee/addcontact")
    fun addcontact(@Header("Authentication")authorizationHeader: String)

    @POST("/employee/addcontact")
    fun addcontact(@Header("Authorization") jwtToken: String, @Body requestBody: AddContactPostModel): Call<AddContactResponseModel>

    @GET("/employee/tasklist")
    fun tasklist(@Header("Authorization")authorizationHeader: String):Call<List<TaskListModelItem>>

    @GET("/employee/salesorderlist")
    fun salesOrderList(@Header("Authorization")authorizationHeader: String):Call<List<SalesListModelItem>>

    @GET("/employee/leaddeals")
    fun leadDeals(@Header("Authorization")authorizationHeader: String):Call<DealModel>

    @GET("/employee/purchaseorder")
    fun purchaseorderdata(@Header("Authorization")authorizationHeader: String):Call<List<PurchaseOrderModelItem>>

    @GET("employee/vendorlist")
    fun getVendorList(@Header("Authorization")authorizationHeader: String):Call<List<VendorListResponseModelItem>>

    @POST("employee/addvendor")
    fun addVendofromAPI(@Header("Authorization") jwtToken: String, @Body requestBody: AddVendorModel): Call<AddVendorModel>

       @GET("/employee/contacts/attachments/{contactId}")
    fun getContactAttachments(@Header("Authorization")jwtToken: String, @Path("contactId") contactId: Int):Call<List<ContactAttachmentsResModel>>

    @GET("/employee/contacts/notes/{contactId}")
    fun getContactNotes(@Header("Authorization")jwtToken: String, @Path("contactId") contactId: Int):Call<List<ContactAttachmentsResModel>>

    @GET("/employee/contacts/links/{contactId}")
    fun getContactLinks(@Header("Authorization")jwtToken: String, @Path("contactId") contactId: Int):Call<List<ContactAttachmentsResModel>>


    @GET("/employee/accounts/attachments/{accountId}")
    fun getAccountAttachments(@Header("Authorization")jwtToken: String, @Path("accountId") accountId: Int):Call<List<AccountAttachmentResModel>>

    @GET("/employee/accounts/notes/{accountId}")
    fun getAccountNotes(@Header("Authorization")jwtToken: String, @Path("accountId") accountId: Int):Call<List<AccountAttachmentResModel>>

    @GET("/employee/accounts/links/{accountId}")
    fun getAccountLinks(@Header("Authorization")jwtToken: String, @Path("accountId") accountId: Int):Call<List<AccountAttachmentResModel>>

    @GET("employee/noteview/{noteId}")
    fun notePreview(@Header("Authorization") jwtToken: String, @Path("noteId") noteId: Int): Call<ResponseBody>


    @Multipart
    @POST("employee/addpurchaseorder")
    fun addPurchaseOrder(@Header("Authorization") jwtToken: String,
                         @Part file : MultipartBody.Part,
                         @Part ("dto") dto : CreatePurchaseModel
                         ): Call<PurchaseOrderResponseModel>


    @Multipart
    @POST("employee/addsalesorder")
    fun addSalesOrder(@Header("Authorization") jwtToken: String,
                         @Part file : MultipartBody.Part,
                         @Part ("dto") dto : CreatePurchaseModel
    ): Call<SalesOrderResponseModel>


    @GET("/employee/map/{latitude}/{longitude}")
    fun getEmployeeLocation(@Path("latitude") latitude: Double, @Path("longitude") longitude: Double,
                            @Header("Authorization") authorizationHeader: String): Call<Void>
    @PUT("/employee/leadupdate")
    fun leadInfoUpdate(@Header("Authorization") jwtToken: String, @Body requestBody: UpdateLeadInfoPutModel): Call<UpdateLeadInfoResponseModel>

    @PUT("/employee/taskupdate")
    fun updateTask(@Header("Authorization")jwtToken: String, @Body requestBody: UpdateTaskReqModel): Call<UpdateTaskResModel>

    @GET("/employee/employeelist")
    fun employeelist(@Header("Authorization") authorizationHeader: String):Call<ArrayList<ShowEmployeelistModelItem>>

    @PUT("/employee/contactupdate")
    fun contactupdate(@Header("Authorization") jwtToken: String, @Body requestBody: UpdateContactPutModel): Call<UpdateContactResponseModel>

    @PUT("/employee/vendorupdate")
    fun vendorUpdate(@Header("Authorization") jwtToken: String, @Body requestBody: UpdateVendorPutmodel): Call<UpdateVendorResponseModel>

    @POST("/employee/addmeeting")
    fun addmeeting(@Header("Authorization") jwtToken: String, @Body requestBody: AddMeetingPostModel): Call<AddMeetingResponseModel>

    @POST("/employee/{leadId}/addcontact")
    fun ProposalContact(@Header("Authorization")jwtToken: String, @Path("leadId") leadId: Int?,
                        @Body requestBody: AddContactProposal): Call<AddContactsProposalResponse>

    @GET("/getstates/{id}")
    fun getState(@Header("Authorization")jwtToken: String, @Path("id") countryid: Int):Call<List<StateModelItem>>

    @GET("/getcities/{id}")
    fun getCity(@Header("Authorization")jwtToken: String, @Path("id") stateid: Int):Call<List<CityModelItem>>

    @GET("/getcountries")
    fun getCountry(@Header("Authorization")jwtToken: String):Call<List<CountryModelItem>>

    @POST("employee/lead/addaccount")
    fun leadAddAccount(@Header("Authorization") jwtToken: String, @Body requestBody: LeadAddAccountPostModel): Call<LeadAddAccountResponseModel>

    @GET("employee/getallemailtosend")
    fun getEmails(@Header ("Authorization") jwtToken: String) : Call<ResponseBody>

    @GET("/employee/lead/attachments/{leadId}")
    fun getAttachmentListLead(@Header("Authorization")jwtToken: String, @Path("leadId") leadId: Int):Call<List<LeadAttachmentsModelItem>>

    @GET("/employee/lead/notes/{leadId}")
    fun getNoteListLead(@Header("Authorization")jwtToken: String, @Path("leadId") leadId: Int):Call<List<LeadNotesModelItem>>

    @GET("/employee/lead/links/{leadId}")
    fun getLinkListLead(@Header("Authorization")jwtToken: String, @Path("leadId") leadId: Int):Call<List<LeadLinksModelItem>>

    @GET("/employee/noteattachmentlinklistlead/{accountId}")
    fun noteAttachmentLinkListAccount(@Header("Authorization")jwtToken: String, @Path("accountId") leadId: Int):Call<NoteAttachmentLinkModel>


    @GET("/employee/employeemeetinglistlead/{leadId}")
    fun getMeeitngListAL(@Header("Authorization")jwtToken: String, @Path("leadId") leadId: Int):Call<List<ALMeetingModelItem>>

    @GET("/employee/leadDashboard")
    fun getLeadDashboard(@Header("Authorization")jwtToken: String):Call<LeadDashBoardResponseModel>

    @GET("/employee/getLeadStatistics")
    fun getLeadBarChart(@Header("Authorization")jwtToken: String):Call<LeadBarChartResponseModel>

    @POST("/employee/addtask")
    fun addTask(@Header("Authorization")jwtToken: String,@Body requestBody: AddTaskPostModel):Call<AddTaskResModel>

    @Multipart
    @POST("/employee/addattachment")
    fun addAttachment(@Header("Authorization") jwtToken: String,
                      @Part file: MultipartBody.Part,
                      @Part ("dto") dto: AttachmentDto
    ):Call<AttaModel>
    @GET("/employee/leaddetails/{lid}")
    fun getLeadDetailsById(@Header("Authorization")jwtToken: String, @Path("lid") leadId: Int):Call<LeadDetailsModel>

    @GET("/employee/getTaskCount")
    fun getLeadLineChart(@Header("Authorization")jwtToken: String):Call<LeadLineChartResponseModel>


    @GET("/employee/user/{id}")
    fun getEmployeeProfile(@Header("Authorization")jwtToken: String, @Path("id") id: Int): Call<ProfileResponseModel>

    @PUT("/employee/updatePassword")
    fun updatePassword(@Header("Authorization")jwtToken: String, requestBody: UpdatePassReqModel): Call<PasswordResponse>

    @PUT("employee/profileupdate")
    fun updateProfileInfo(@Header("Authorization")jwtToken: String,requestBody: UpdateProfilePutModel): Call<ProfileResponseModel>

    @GET("/employee/getRevenue")
    fun getSalesDashboardRevenue(@Header("Authorization") jwtToken: String): Call<List<SalesDashBoardRevenueBarChartResponseModelItem>>

    @GET("/employee/getForecastCount")
    fun getSalesDashboardForecast(@Header("Authorization") jwtToken: String): Call<SalesDashboardResp>

    @GET("/employee/salesDashboard")
    fun getSalesDashboardWinRate(@Header("Authorization") jwtToken: String): Call<SalesDashboardWinRateReponseModel>

    @GET("/employee/getRevenueByLead")
    fun getSalesDashboardRevenueByLead(@Header("Authorization") jwtToken: String): Call<List<SalesDashboardRevenueByLeadResponseModelItem>>

    @GET("/employee/taskDashboard")
    fun getTaskDashboard(@Header("Authorization") jwtToken: String): Call<TaskDashBoardResponseModel>

    @GET("/employee/getLeadTypeTotal")
    fun getTaskDashboardPieChart(@Header("Authorization") jwtToken: String): Call<List<TaskDashboardPieChartResponseModelItem>>

    @GET("/employee/getNewDealsCount")
    fun getTaskDashboardNewDealsChart(@Header("Authorization") jwtToken: String): Call<TaskDashBoardNewDealsResponseModel>
 @GET("/employee/getAcheiveLeads")
    fun getTaskDashboardAcheiveChart(@Header("Authorization") jwtToken: String): Call<List<TaskDashBoardAcheiveLeadsResponseModelItem>>

    @Multipart
    @POST("/employee/addnote")
    fun addnote(@Header("Authorization") jwtToken: String,
                @Part file: MultipartBody.Part,
                @Part ("dto") dto: AddNoteRequest
    ):Call<AddNoteModel>

    @Multipart
    @PUT("/employee/profileupdate")
    fun updateProfile(@Header("Authorization")jwtToken: String,
                      @Part file: MultipartBody.Part,
                      @Part ("dto") dto : ProfileUpdateRequest
    ): Call<ProfileResponseModel>

    @POST("/employee/addlink")
    fun addLink(@Header("Authorization") jwtToken: String , @Body requestBody: AddLinkReqModel):Call<AddLinkResponseModel>

    @GET("employee/templatelist")
    fun templateList(@Header("Authorization")jwtToken: String):Call<MutableList<TemplateModelItem>>

    @PUT("/employee/templateupdate")
    fun templateUpdate(@Header("Authorization") jwtToken: String, @Body requestBody: UpdateTemplatePutModel): Call<UpdateTemplateResponseModel>

    @GET("/employee/template/{tid}")
    fun getTemplateDetailsById(@Header("Authorization")jwtToken: String, @Path("tid") tid: Int):Call<TemplateDetailsByIdModel>

    @POST("employee/addtemplate")
    fun addTemplate(@Header("Authorization") jwtToken: String, @Body requestBody: AddTemplatePostModel): Call<AddTemplateResponseModel>

    @GET("/employee/getPiplineChartData")
    fun getPiplineChart(@Header("Authorization")jwtToken: String):Call<List<PiplineResponseModelItem>>
     @GET("/employee/getPipelineUserData")
    fun getGuageChart(@Header("Authorization")jwtToken: String):Call<List<PiplineGuageResponseModelItem>>

    @GET("/employee/pipeline_dashboard")
    fun getTargetDetails(@Header("Authorization")jwtToken: String):Call<PipliineDashboardResponseModel>

    @GET("/employee/pipeline_dashboard")
    fun getPiplineTable(@Header("Authorization")jwtToken: String):Call<PipliineDashboardResponseModel>
    @POST("/employee/{leadId}/addqualify")
    fun proposalQualify(@Header("Authorization")jwtToken: String, @Path("leadId") leadId: Int?,
                        @Body requestBody: QualifyProposalReq): Call<QualifyProposalRespo>

    @POST("/employee/{leadId}/addnegociation")
    fun proposalNegotiation(@Header("Authorization")jwtToken: String,
                            @Path("leadId") leadId: Int?,
                            @Body requestBody: NegotiationReq): Call<NegotiationRespo>

    @GET("/employee/lead_details/{accountId}")
    fun leadDetailsByAccountId(@Header("Authorization")jwtToken: String, @Path("accountId") aid: Int):Call<List<LeadDetailsAccountModelItem>>

    @GET("/employee/task_details/{accountId}")
    fun taskDetailsByAccountId(@Header("Authorization")jwtToken: String, @Path("accountId") aid: Int):Call<List<AccountTaskDetailsModelItem>>

    @GET("/employee/contact_details/{accountId}")
    fun contactDetailsByAccountId(@Header("Authorization")jwtToken: String, @Path("accountId") aid: Int):Call<List<AccountContactDetailsModelItem>>

    @GET("/employee/meeting_details/{contactId}")
    fun meetingDetailsByContactId(@Header("Authorization")jwtToken: String, @Path("contactId") aid: Int):Call<List<ContactMeetingDetailsModelItem>>

    @GET("/employee/task_details_contact/{contactId}")
    fun taskDetailsByContactId(@Header("Authorization")jwtToken: String, @Path("contactId") aid: Int):Call<List<ContactTaskDetailsModelItem>>


    @GET("/employee/task/notes/{taskId}")
    fun getTaskNotesList(@Header("Authorization")jwtToken: String, @Path("taskId") taskId: Int):Call<List<TaskNotesResModelItem>>

    @GET("/employee/task/links/{taskId}")
    fun getTaskLinksList(@Header("Authorization")jwtToken: String, @Path("taskId") taskId: Int):Call<List<TaskLinksResModelItem>>

    @GET("/employee/task/attachments/{taskId}")
    fun getTaskAttaList(@Header("Authorization")jwtToken: String, @Path("taskId") taskId: Int):Call<List<TaskAttaResModelItem>>

    @GET("/employee/noteview/{noteId}")
    fun getTaskNoteView(@Header("Authorization")jwtToken: String, @Path("noteId") noteId:Int):Call<ResponseBody>

    @GET("/employee/noteattachmentlinklistcontact/{contactId}")
    fun getContactNoteAttachmentLink(@Header("Authorization")jwtToken: String, @Path("contactId") contactId: Int):Call<ContactAttachmentsResModel>



    @Multipart
    @POST("/employee/{leadId}/addproposal")
    fun addProposal(@Header("Authorization")jwtToken: String,
                      @Path("leadId") leadId: Int?,
                      @Part file: MultipartBody.Part,
                      @Part ("dto") dto : AddProposalReq
    ): Call<AddProposalRespo>

    @Multipart
    @POST("/employee/{leadId}/addcontact")
    fun addContact(@Header("Authorization")jwtToken: String,
                    @Path("leadId") leadId: Int?,
                    @Part file: MultipartBody.Part?,
                    @Part ("dto") dto : AddContactReq
    ): Call<AddContactRespo>


    @GET("/employee/accounts/{aid}")
    fun getAccountDetailsById(@Header("Authorization")jwtToken: String, @Path("aid") aid: Int):Call<AccountDetailsByIdModel>

    @GET("/employee/leadactivityalllist/{leadId}")
    fun getActivity(@Header("Authorization")jwtToken: String,
                    @Path("leadId") leadId: Int):Call<ActivityAssignLeadResponse>

    @GET("employee/employeetasklistlead/{leadId}")
    fun getTaskContact(@Header("Authorization")jwtToken: String, @Path("leadId") leadId: Int):Call<List<TaskContactResponseModelItem>>

    @GET("employee/updatelead/Called/{ContactId}/{LeadId}")
    fun getChangeTaskStatus(@Header("Authorization")jwtToken: String, @Path("ContactId") contactId: Int , @Path("LeadId") leadId: Int):Call<Void>

    @POST("employee/assignmeeting")
    fun assignMeeting(@Header("Authorization") jwtToken: String, @Body requestBody: ALMeetingPostModel): Call<ALMeetingResponseModel>


    @GET("/employee/noteattachmentlinklistaccount/{accountId}")
    fun getAccountNoteAttachmentLink(@Header("Authorization")jwtToken: String, @Path("accountId") accountId: Int):Call<AccountAttachmentResModel>

    @GET("employee/lead/activity/contactlist/{leadId}")
    fun getAssignContactStatus(@Header("Authorization")jwtToken: String,
                              @Path("leadId") leadId: Int):Call<ContactStatus>

    @GET("employee/lead/activity/qualifyList/{leadId}")
    fun getAssignQualifyStatus(@Header("Authorization")jwtToken: String,
                               @Path("leadId") leadId: Int):Call<QualifyStatus>

    @GET("employee/lead/activity/proposalList/{leadId}")
    fun getAssignProposalStatus(@Header("Authorization")jwtToken: String,
                               @Path("leadId") leadId: Int):Call<ProposalStatus>

    @GET("employee/lead/activity/negociationList/{leadId}")
    fun getAssignNegotiationStatus(@Header("Authorization")jwtToken: String,
                                @Path("leadId") leadId: Int):Call<NegotiationStatus>

    @GET("/employee/vendor/{vid}")
    fun getVendorDetailsById(@Header("Authorization")jwtToken: String, @Path("vid") vid: Int):Call<VendorDetailsByIdModel>

    @DELETE("/employee/deleteTemplate/{templateId}")
    fun deleteTemplate(@Header("Authorization") jwtToken: String, @Path("templateId") templateId: Int): Call<DeleteTemplateModel>

    @GET("employee/attachmentview/{attachmentId}")
    fun attachmentPreview(@Header("Authorization") jwtToken: String, @Path("attachmentId") attachmentId: Int): Call<ResponseBody>

    @GET("employee/leaddetails/{leadId}")
    fun getLeadDetails(@Header("Authorization") jwtToken: String, @Path("leadId") leadId: Int): Call<LeadDetailResponseModel>

    @GET("employee/proposalview/{proposalId}")
    fun getProposalInvoice(@Header("Authorization") jwtToken: String, @Path("proposalId") proposalId: Int): Call<ResponseBody>


    //Admin

    @GET("user/template_list")
    fun templateListAdmin(@Header("Authorization")jwtToken: String):Call<MutableList<TemplateModelItem>>

    @DELETE("/user/deleteTemplate/{templateId}")
    fun deleteTemplateAdmin(@Header("Authorization") jwtToken: String, @Path("templateId") templateId: Int): Call<DeleteTemplateModel>

    @POST("user/addTemplate")
    fun addTemplateAdmin(@Header("Authorization") jwtToken: String, @Body requestBody: AddTemplatePostModel): Call<AddTemplateResponseModel>

    @PUT("/user/updateTemplate")
    fun updateTemplateAdmin(@Header("Authorization") jwtToken: String, @Body requestBody: UpdateTemplatePutModel): Call<UpdateTemplateResponseModel>

    @GET("/user/template/{Id}")
    fun getTemplateDetailsByIdAdmin(@Header("Authorization")jwtToken: String, @Path("Id") tid: Int):Call<TemplateDetailsByIdModel>

    @GET("user/shows_employee")
    fun showsEmployeeAdmin(@Header("Authorization")jwtToken: String):Call<List<ShowEmployeeModelItem>>

    @GET("user/shows_leads")
    fun adminLeadList(@Header("Authorization")jwtToken: String): Call<List<AdminLeadListModelItem>>

    @GET("user/leadreports")
    fun adminLeadReport(@Header("Authorization")jwtToken: String): Call<List<AdminLeadReportModelItem>>

    @GET("user/lead/{Id}")
    fun getAdminLeadDetails(@Header("Authorization")jwtToken: String, @Path("Id") leadId: Int):Call<LeadDetailsModel>

    @GET("user/lead/attachments/{leadId}")
    fun adminAttachmentsList(@Header("Authorization")jwtToken: String, @Path("leadId") leadId: Int):Call<List<LeadAttachmentsModelItem>>

    @GET("user/lead/notes/{leadId}")
    fun adminNotesList(@Header("Authorization")jwtToken: String, @Path("leadId") leadId: Int):Call<List<LeadNotesModelItem>>

    @GET("user/lead/links/{leadId}")
    fun adminLinksList(@Header("Authorization")jwtToken: String, @Path("leadId") leadId: Int):Call<List<LeadLinksModelItem>>

    @GET("user/lead/activity/contactlist/{leadId}")
    fun adminContactStatus(@Header("Authorization")jwtToken: String,
                               @Path("leadId") leadId: Int):Call<ContactStatus>

    @GET("user/lead/activity/qualifyList/{leadId}")
    fun adminQualifyStatus(@Header("Authorization")jwtToken: String,
                               @Path("leadId") leadId: Int):Call<QualifyStatus>

    @GET("user/lead/activity/proposalList/{leadId}")
    fun adminProposalStatus(@Header("Authorization")jwtToken: String,
                                @Path("leadId") leadId: Int):Call<ProposalStatus>

    @GET("user/lead/activity/negociationList/{leadId}")
    fun adminNegotiationStatus(@Header("Authorization")jwtToken: String,
                                   @Path("leadId") leadId: Int):Call<NegotiationStatus>
    @GET("user/pipeline")
    fun pipelineReportAdmin(@Header("Authorization")jwtToken: String):Call<List<PipelineReportModelItem>>

    @POST("user/setTarget")
    fun setTargetPipelineAdmin(@Header("Authorization") jwtToken: String, @Body requestBody: AddPipelinePostModel): Call<AddPipelineResponseModel>

    @GET("/user/pipelinereport/{Id}")
    fun getPipelineDetailsByIdAdmin(@Header("Authorization")jwtToken: String, @Path("Id") pid: Int):Call<GetPipelineDetailsByIdModel>

    @PUT("/user/update_target")
    fun updateTargetAdmin(@Header("Authorization") jwtToken: String, @Body requestBody: UpdatePipelineReportPutModel): Call<UpdatePipelineReportResponseModel>

    @Multipart
    @POST("/user/add_employee")
    fun addEmployeeAdmin(@Header("Authorization") jwtToken: String,
                         @Part file : MultipartBody.Part?,
                         @Part ("dto") dto : AddEmployeePostModel
    ): Call<AddEmployeeAdminResponseModel>

    @GET("/user/employee/{Id}")
    fun getEmployeeDetailsByIdAdmin(@Header("Authorization")jwtToken: String, @Path("Id") cId: Int):Call<EmployeeDetailsByIdModel>

    @Multipart
    @POST("user/update_employee")
    fun updateEmployeeAdmin(@Header("Authorization") jwtToken: String,
                         @Part file : MultipartBody.Part?,
                         @Part ("dto") dto : UpdateEmployeePostModel
    ): Call<UpdateEmployeeResponseModel>

    @GET("user/attachmentview/{attachmentId}")
    fun adminAttachment(@Header("Authorization") jwtToken: String, @Path("attachmentId") attachmentId: Int): Call<ResponseBody>

    @GET("user/noteview/{noteId}")
    fun adminNotes(@Header("Authorization") jwtToken: String, @Path("noteId") noteId: Int): Call<ResponseBody>

    @GET("user/lead/{Id}")
    fun adminLeadDetailsById(@Header("Authorization")jwtToken: String, @Path("Id") leadId: Int):Call<LeadDetailsModel>

    @PUT("user/update_lead")
    fun adminUpdateLead(@Header("Authorization") jwtToken: String, @Body requestBody: UpdateLeadInfoPutModel): Call<UpdateLeadInfoResponseModel>

    @GET("user/accountlist")
    fun adminShowAccounts(@Header("Authorization") authorizationHeader : String):Call<ArrayList<ShowAccountsModelItem>>

    @GET("user/shows_employee")
    fun adminEmployeeList(@Header("Authorization") authorizationHeader: String):Call<ArrayList<ShowEmployeelistModelItem>>

    @GET("user/account/{Id}")
    fun adminAccountDetailsById(@Header("Authorization")jwtToken: String, @Path("Id") aid: Int):Call<AccountDetailsByIdModel>
    @Multipart
    @POST("user/{leadId}/assign_lead")
    fun leadAssignTo(@Header("Authorization") jwtToken: String,
                     @Path("leadId") leadId: Int?,
                     @Part ("assignedTo") assignedTo: RequestBody
    ): Call<AssignLeadToResponse>

    @POST("user/{leadId}/setMeeting")
    fun adminAddMeeting(@Header("Authorization") jwtToken: String,@Path ("leadId") leadId: Int, @Body requestBody: AdminPostMeetingAL): Call<AdminMeetingPostRes>

    @GET("user/{leadId}/meetings")
    fun adminLeadMeetingList(@Header("Authorization")jwtToken: String, @Path("leadId") leadId: Int):Call<List<ALMeetingModelItem>>

    @GET("user/getAccountNamesByEmployee/{employeeId}")
    fun getAccountNameByEmpId(@Header("Authorization")jwtToken: String, @Path("employeeId") leadId: Int):Call<ArrayList<AccountNameByEmpIdResponseItem>>

    @GET("user/account/{Id}")
    fun accountDetailsById(@Header("Authorization")jwtToken: String, @Path("Id") aid: Int):Call<AccountDetailsByIdModel>

    @POST("user/add_lead")
    fun adminAddLead(@Header("Authorization") authorizationHeader : String ,@Body model : AddLeadPostModel):Call<AddLeadModel>



    @GET("user/tasklist")
    fun showTaskListAdmin(@Header("Authorization")jwtToken: String):Call<List<TaskListModelItem>>

    @GET("/user/meetinglist")
    fun showMeetingListAdmin(@Header("Authorization")jwtToken: String):Call<List<MeetingListModelItem>>

    @GET("/user/contactlist")
    fun showContactListAdmin(@Header("Authorization")jwtToken: String):Call<List<ShowContactModelItem>>

    @POST("/user/setMeeting")
    fun addMeetingAdmin(@Header("Authorization")jwtToken: String,@Body requestBody: AddMeetingPostModel): Call<AddMeetingResponseModel>

    @PUT("/user/updateTask")
    fun updateTaskAdmin(@Header("Authorization")jwtToken: String, @Body requestBody: UpdateTaskReqModel): Call<UpdateTaskResModel>

    @GET("/user/task/notes/{taskId}")
    fun getAdminTaskNotesList(@Header("Authorization")jwtToken: String, @Path("taskId") taskId: Int):Call<List<TaskNotesResModelItem>>

    @GET("/user/task/links/{taskId}")
    fun getAdminTaskLinksList(@Header("Authorization")jwtToken: String, @Path("taskId") taskId: Int):Call<List<TaskLinksResModelItem>>

    @GET("/user/task/attachments/{taskId}")
    fun getAdminTaskAttaList(@Header("Authorization")jwtToken: String, @Path("taskId") taskId: Int):Call<List<TaskAttaResModelItem>>

    @GET("/user/shows_employee")
    fun Adminemployeelist(@Header("Authorization") authorizationHeader: String):Call<ArrayList<ShowEmployeelistModelItem>>

    @GET("/user/shows_leads")
    fun showleadsAdmin(@Header("Authorization") authorizationHeader : String):Call<List<ShowLeadsModelItem>>

    @GET("/user/contactlist")
    fun showcontactAdmin(@Header("Authorization") authorizationHeader : String):Call<List<ShowContactModelItem>>

    @GET("/user/accountlist")
    fun showaccountsAdmin(@Header("Authorization") authorizationHeader : String):Call<ArrayList<ShowAccountsModelItem>>

    @POST("/user/addTask")
    fun addTaskAdmin(@Header("Authorization")jwtToken: String,@Body requestBody: AdminAddTaskReqModel):Call<AddTaskResModel>

    @POST("/user/addaccount")
    fun adminaddaccount(@Header("Authorization") jwtToken: String, @Body requestBody: AddAccountPostModel): Call<AddAccountResponseModel>

    @GET("/user/accounts/attachments/{accountId}")
    fun admingetAccountAttachments(@Header("Authorization")jwtToken: String, @Path("accountId") accountId: Int):Call<List<AccountAttachmentResModel>>

    @GET("/user/accounts/links/{accountId}")
    fun admingetAccountLinks(@Header("Authorization")jwtToken: String, @Path("accountId") accountId: Int):Call<List<AccountAttachmentResModel>>

    @GET("/user/accounts/notes/{accountId}")
    fun admingetAccountNotes(@Header("Authorization")jwtToken: String, @Path("accountId") accountId: Int):Call<List<AccountAttachmentResModel>>

    @PUT("/user/updateaccount")
    fun adminaccountupdate(@Header("Authorization") jwtToken: String, @Body requestBody: UpdateAccountPutModel): Call<UpdateAccountResponseModel>

    @GET("/user/contact_details/{accountId}")
    fun admincontactDetailsByAccountId(@Header("Authorization")jwtToken: String, @Path("accountId") aid: Int):Call<List<AccountContactDetailsModelItem>>

    @GET("/user/task_details/{accountId}")
    fun admintaskDetailsByAccountId(@Header("Authorization")jwtToken: String, @Path("accountId") aid: Int):Call<List<AccountTaskDetailsModelItem>>

    @GET("/user/lead_details/{accountId}")
    fun adminleadDetailsByAccountId(@Header("Authorization")jwtToken: String, @Path("accountId") aid: Int):Call<List<LeadDetailsAccountModelItem>>

    @GET("/user/accountlist")
    fun adminshowaccounts(@Header("Authorization") authorizationHeader : String):Call<ArrayList<ShowAccountsModelItem>>

    @GET("/user/shows_employee")
    fun adminemployeelist(@Header("Authorization") authorizationHeader: String):Call<ArrayList<ShowEmployeelistModelItem>>

    @POST("/user/addorupdatecontact")
    fun adminaddcontact(@Header("Authorization") jwtToken: String, @Body requestBody: AdminAddContactPostModel): Call<AddContactResponseModel>

    @GET("/user/contactlist")
    fun adminshowcontact(@Header("Authorization") authorizationHeader : String):Call<List<ShowContactModelItem>>

    @PUT("/user/updateintegration")
    fun adminupdateintegration(@Header("Authorization") jwtToken: String, @Body requestBody: ThirdPartyIntegrationResModel): Call<ThirdPartyIntegrationResModel>

    @POST("/user/addintegration")
    fun addintegration(@Header("Authorization") jwtToken: String, @Body requestBody: ThirdPartyIntegrationPostModel): Call<ThirdPartyIntegrationResModel>

    @GET("/user/integrationlist")
    fun integrationlist(@Header("Authorization") authorizationHeader: String):Call<List<ThirdPartyIntegrationResModel>>

    @GET("/user/getLeadTypeTotal")
    fun getAdminTaskDashboardPieChart(@Header("Authorization") jwtToken: String): Call<List<TaskDashboardPieChartResponseModelItem>>

    @GET("/user/getAchieveLeads")
    fun getAdminTaskDashboardAcheiveChart(@Header("Authorization") jwtToken: String): Call<List<AdminAcheiveLeadResponseModelItem>>

    @GET("/user/getNewDealsCount")
    fun getAdminTaskDashboardNewDealsChart(@Header("Authorization") jwtToken: String): Call<AdminNewDealsResponseModel>

    @GET("/user/taskDashboard")
    fun getAdminTaskDashboard(@Header("Authorization") jwtToken: String): Call<TaskDashBoardResponseModel>

    @GET("/user/leadDashboard")
    fun getAdminLeadDashboard(@Header("Authorization")jwtToken: String):Call<LeadDashBoardResponseModel>

    @GET("/user/getTaskCount")
    fun getAdminLeadLineChart(@Header("Authorization")jwtToken: String):Call<AdminLeadLineChartResponseModel>

    @POST("/user/addorupdatecontact")
    fun admincontactupdate(@Header("Authorization") jwtToken: String, @Body requestBody: AdminUpdateContactPutModel): Call<UpdateContactResponseModel>

    @GET("/user/getLeadStatistics")
    fun getAdminLeadBarChart(@Header("Authorization")jwtToken: String):Call<AdminLeadBarChartResponseModel>

    @GET("/user/employeedashboard")
    fun getAdminEmployeeDashboard(@Header("Authorization") jwtToken: String): Call<AdminEmployeeDashboardResponseModel>

    @GET("employee/getAllProducts")
    fun getProducts(@Header("Authorization") jwtToken: String): Call<ProductResponseModel>

    @POST("employee/addProducts")
    fun AddProducts(@Header("Authorization") jwtToken: String , @Body model : AddProductRequestModel ): Call<ProductResponseModelX>



    @GET("user/proposalview/{proposalId}")
    fun getProposalAdmin(@Header("Authorization") jwtToken: String, @Path("proposalId") proposalId: Int): Call<ResponseBody>

    @PUT("/employee/updateProduct")
    fun updateProduct(@Header("Authorization") authorizationHeader : String ,@Body model : UpdateProductRequestModel):Call<UpdateProductRequestModel>

    @GET("/employee/getAllProducts")
    fun getAllProducts(@Header("Authorization")jwtToken: String):Call<List<AddProductResponseModel>>

    @DELETE("/employee/deleteProduct/{id}")
    fun deleteProduct(@Header("Authorization")jwtToken: String, @Path("id") productId: Int):Call<StateModelItem>

    @GET("/user/getMap")
     fun getMap(@Header("Authorization")jwtToken: String):Call<GetMapResponseModel>

    @GET("user/getallemailtosend")
    fun adminGetEmails(@Header ("Authorization") jwtToken: String) : Call<ResponseBody>

    @GET("user/getLiveData/{id}")
    fun getEmpLocation(@Header ("Authorization") jwtToken: String , @Path("id") Empid : Int) : Call<EmpTrackResponseModel>

}
