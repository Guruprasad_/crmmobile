package com.BrightLink.SalesOptim.Activities.PurchaseOrder

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.databinding.PurchaseOwnBinding
import com.google.android.material.datepicker.MaterialDatePicker
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class OwnPurchaseOrderFragment : Fragment()  {
    private var _binding: PurchaseOwnBinding? = null
    private val binding get() = _binding!!
    private val PREF_NAME = "MyPrefs"
    private var visibility : Boolean = true

    private val imagePickerLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            val data: Intent? = result.data
            val uri = data?.data
            uri?.let { selectedUri ->
                // Handle the selected image URI here
                showToast("Image selected")
                // You can save the selected image URI to SharedPreferences or perform any other operation
                saveImageUriToPreferences(selectedUri)
            }
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = PurchaseOwnBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.date.setOnClickListener {
            openDatePicker()
        }

        binding.validUntil.setOnClickListener{
            openValidDatePicker()
        }

        binding.chooseimg.setOnClickListener {
            ImagePicker()
        }

        binding.visible.setOnClickListener {
            visibility = true
            binding.visible.setBackgroundResource(R.color.statusbar)
            val color = ContextCompat.getColor(requireContext(), R.color.white)
            binding.visible.setColorFilter(color)
            val default = ContextCompat.getColor(requireContext(), R.color.black)
            binding.invisible.setColorFilter(default)
            binding.invisible.setBackgroundResource(android.R.color.transparent)
        }

        binding.invisible.setOnClickListener {
            visibility = false
            binding.invisible.setBackgroundResource(R.color.statusbar)
            val color = ContextCompat.getColor(requireContext(), R.color.white)
            binding.invisible.setColorFilter(color)
            val default = ContextCompat.getColor(requireContext(), R.color.black)
            binding.visible.setColorFilter(default)
            binding.visible.setBackgroundResource(android.R.color.transparent)
        }


        binding.savebtn.setOnClickListener {
            val companyName = binding.companyName.text.toString()
            val address = binding.address.text.toString()
            val phone = binding.phone.text.toString()
            val email = binding.email.text.toString()
            val date = binding.date.text.toString()
            val validDate = binding.validUntil.text.toString()
            val quotationId = binding.quotationId.text.toString()
            val gst = binding.gstin.text.toString()

            val sharedPreferences =
                requireContext().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putString("companyName", companyName)
            editor.putString("address", address)
            editor.putString("phone", phone)
            editor.putString("email", email)
            editor.putString("date", date)
            editor.putString("validDate", validDate)
            editor.putString("quotationId", quotationId)
            editor.putString("gst", gst)
            editor.putBoolean("visibility",visibility)
            editor.apply()

            Constants.success(context, "Owner details saved successfully")

        }

        return root
    }

    private fun openDatePicker() {
        val builder = MaterialDatePicker.Builder.datePicker().build()

        builder.addOnPositiveButtonClickListener { selection ->
            val dateFormat = SimpleDateFormat("MM/dd/yyyy", Locale.US)
            val selectedDate = dateFormat.format(Date(selection))
            binding.date.text = selectedDate
        }
        builder.show(requireActivity().supportFragmentManager, builder.toString())
    }

    private fun openValidDatePicker() {
        val builder = MaterialDatePicker.Builder.datePicker().build()

        builder.addOnPositiveButtonClickListener { selection ->
            val dateFormat = SimpleDateFormat("MM/dd/yyyy", Locale.US)
            val selectedDate = dateFormat.format(Date(selection))
            binding.validUntil.text = selectedDate
        }
        builder.show(requireActivity().supportFragmentManager, builder.toString())
    }

    private fun ImagePicker() {
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        imagePickerLauncher.launch(intent)
    }


    private fun saveImageUriToPreferences(uri: Uri) {
        // Save the selected image URI to SharedPreferences or perform any other operation
        val sharedPreferences = requireContext().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("imageUri", uri.toString())
        editor.apply()
    }
    fun color(colorr : String): ColorPickerDialogFragment {
        val colorPickerDialogFragment = ColorPickerDialogFragment()
        colorPickerDialogFragment.setColorPickerListener(object :
            ColorPickerDialogFragment.ColorPickerListener {
            override fun onColorSelected(color: Int) {

                val hexColor = String.format("#%06X", 0xFFFFFF and color)

                val sharedPreferences = requireContext().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
                val editor = sharedPreferences.edit()
                editor.putString(colorr, hexColor)
                editor.apply()

                when (colorr) {
                    "background" -> binding.background1.setBackgroundColor(color)
                    "header" -> binding.head1.setBackgroundColor(color)
                    "text" -> binding.text1.setBackgroundColor(color)
                }
            }
        })
        return colorPickerDialogFragment
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.backgroundclr.setOnClickListener {
            color("background").show(childFragmentManager, "colorPicker")
           // binding.background.setBackgroundColor(android.graphics.Color.parseColor("background"))
        }
        binding.headingsclr.setOnClickListener {
            color("header").show(childFragmentManager,"colorPicker")
        }
        binding.textClr.setOnClickListener {
            color("text").show(childFragmentManager,"colorPicker")
        }
    }

    private fun showToast(message: String) {
        // Example method to show a toast message
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }

    override fun onStart() {
        super.onStart()

        val sharedPreferences = requireContext().getSharedPreferences("UserDetails", Context.MODE_PRIVATE)
        val companyName = sharedPreferences.getString("companyName", "")
        val companyPhone = sharedPreferences.getString("companyPhone", "")
        val companyAddress = sharedPreferences.getString("companyAddress", "")
        val email = sharedPreferences.getString("companyEmail", "")
        val gstin = sharedPreferences.getString("gstin", "")

        binding.companyName.setText(companyName)
        binding.address.setText(companyAddress)
        binding.phone.setText(companyPhone)
        binding.email.setText(email)
        binding.gstin.setText(gstin)
    }

}