package com.BrightLink.SalesOptim.Activities.Vendors

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.RequestModel.UpdateVendorPutmodel
import com.BrightLink.SalesOptim.ResponseModel.ShowVendorModelItem
import com.BrightLink.SalesOptim.ResponseModel.UpdateVendorResponseModel
import com.BrightLink.SalesOptim.ResponseModel.VendorDetailsByIdModel
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.FragmentVendorInformationBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UpdateVendor : Fragment() {
    private var _binding: FragmentVendorInformationBinding? = null
    private val binding get() = _binding!!

    private lateinit var accountTypeSpinner: Spinner

    override fun onCreateView(
        inflater: LayoutInflater,  container: ViewGroup?,  savedInstanceState: Bundle?
    ): View {
        _binding = FragmentVendorInformationBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.updateVendorActionBar.activityName.text = "Update Vendor"
        binding.updateVendorActionBar.back.setOnClickListener{
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }
        binding.updateVendorActionBar.option.visibility = View.INVISIBLE

        val selectedItem = arguments?.getSerializable("selectedItem") as? ShowVendorModelItem
        if (selectedItem == null)
        {
            Constants.error(requireContext(), "Selected Item is Null")
            return root
        }
        val vid = selectedItem.vendorId ?: 0
        val vendorId = vid.toString()

        binding.vendorUpdateBtn.setOnClickListener{
            performUpdateVendor(vendorId)

        }

        accountTypeSpinner = binding.vendorAccountType
        val accountTypeAdapter = ArrayAdapter.createFromResource(requireContext(), R.array.GL_Account, R.layout.spinner_item_layout_currency)
        accountTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        accountTypeSpinner.adapter = accountTypeAdapter

        fetchVendorDetailsById(vid)

        return root
    }

    private fun fetchVendorDetailsById(vid: Int) {
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null){
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val  call = service.getVendorDetailsById("Bearer $jwtToken", vid)
            call.enqueue(object : Callback<VendorDetailsByIdModel> {
                override fun onResponse(
                    call: Call<VendorDetailsByIdModel>,
                    response: Response<VendorDetailsByIdModel>
                ) {
                    if (response.isSuccessful) {
                        val data = response.body()
                        if (data != null) {
                            val vendor = data
                            Log.d("Vendor", "Vendor: $vendor")

                            binding.vendorOwner.text = vendor.vendorOwner
                            binding.vendorPhoneNumber.setText(vendor.phone)
                            binding.vendorWebsite.setText(vendor.website)
                            binding.vendorCategory.setText(vendor.category)
                            binding.vendorName.setText(vendor.vendorName)
                            binding.vendorEmail.setText(vendor.email)
                            binding.vendorAccountType.setSelection(getIndex(binding.vendorAccountType, vendor.glaccount))
                            Log.d("account", "account type: ${vendor.glaccount}")
                            //binding.addCountry.setSelection(getIndex(binding.addCountry, vendor.country?.toString() ?: ""))
                            binding.addCountry.setText(vendor.country)
                            Log.d("account", "country: ${vendor.country}")
                            //binding.addState.setSelection(getIndex(binding.addState, vendor.state))
                            binding.addState.setText(vendor.state)
                            Log.d("account", "state: ${vendor.state}")
                            //binding.addCity.setSelection(getIndex(binding.addCity, vendor.city))
                            binding.addCity.setText(vendor.city)
                            Log.d("account", "city: ${vendor.city}")
                            binding.addStreet.setText(vendor.street)
                            val zipCode = vendor.zipCode
                            if (zipCode != null) {
                                binding.zipCode.setText(zipCode.toString())
                            } else {
                                // Handle the case where zipCode is null
                                Log.e(zipCode, "Zip code is null for vendor: $vendor")
                            }
                            binding.addTaskDesEditText.setText(vendor.description)

                        } else {
                            Constants.error(requireContext(), "Vendor details not found")
                        }
                    } else {
                        Constants.error(
                            requireContext(),
                            "Something went wrong, Try again"
                        )
                    }
                }

                override fun onFailure(call: Call<VendorDetailsByIdModel>, t: Throwable) {
                    Constants.error(requireContext(), "Failed to Fetch Vendor Details")
                }
            })
        }
        else{
            Constants.error(requireContext(),"Please try to Login Again")
        }
    }

    private fun performUpdateVendor(vendorId: String) {
        val phoneNumber = binding.vendorPhoneNumber.text.toString()
        val website = binding.vendorWebsite.text.toString()
        val category = binding.vendorCategory.text.toString()
        val vname = binding.vendorName.text.toString()
        val vemail = binding.vendorEmail.text.toString()
        val actype = binding.vendorAccountType.selectedItem.toString()
        val street = binding.addStreet.text.toString()
        val state = binding.addState.text.toString()
        val country = binding.addCountry.text.toString()
        val city = binding.addCity.text.toString()
        val desc = binding.addTaskDesEditText.text.toString()
        val zipCode = binding.zipCode.text.toString()
        val ownername = binding.vendorOwner.text.toString()

        val requestBody = UpdateVendorPutmodel(category, city, country, desc, vemail, actype, phoneNumber, state, street, vendorId, vname, ownername, website, zipCode)

        val jwtToken = gettoken(requireContext())
        if (jwtToken != null) {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            //Now Making a PUT API calling
            val call = service.vendorUpdate("Bearer $jwtToken", requestBody)
            call.enqueue(object : Callback<UpdateVendorResponseModel>{
                override fun onResponse(
                    call: Call<UpdateVendorResponseModel>,
                    response: Response<UpdateVendorResponseModel>
                ) {
                    if(response.isSuccessful)
                    {
                        Constants.success(requireContext(),"Vendor Updated Successfully")
                        Log.d("country", "updated country: $country")
                        findNavController().popBackStack()
                    } else {
                        Constants.error(requireContext(), "Failed to Update Vendor. Try again")
                    }
                }

                override fun onFailure(call: Call<UpdateVendorResponseModel>, t: Throwable) {
                    Constants.error(requireContext(), "Something went wrong, Try again")
                }

            })
        }
        else{
            Constants.error(requireContext(),"Please try to Login Again")
        }

    }

    private fun gettoken(context: Context): String? {
        val sharedPreferences = context.getSharedPreferences(
            "Token",
            Context.MODE_PRIVATE
        )
        return sharedPreferences.getString("jwtToken", null)
    }

    private fun getIndex(spinner: Spinner, myString: String): Int {
        for (i in 0 until spinner.count) {
            if (spinner.getItemAtPosition(i).toString() == myString) {
                return i
            }
        }
        return 0
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
