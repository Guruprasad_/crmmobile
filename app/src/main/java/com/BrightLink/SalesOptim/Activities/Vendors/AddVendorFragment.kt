package com.example.project.Activities.Vendors

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.RequestModel.AddVendorModel
import com.BrightLink.SalesOptim.ResponseModel.ShowEmployeelistModelItem
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.CreateVendorBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddVendorFragment: Fragment(){

    private var _binding: CreateVendorBinding? = null
    private val binding get() = _binding!!

    private lateinit var accountTypeSpinner: Spinner

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = CreateVendorBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.updateVendorActionBar.activityName.text = "Create Vendor"
        binding.updateVendorActionBar.back.setOnClickListener{
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }
        binding.updateVendorActionBar.option.visibility = View.INVISIBLE

        fetchOwnerName()

        accountTypeSpinner = binding.vendorAccountType
        val accountTypeAdapter = ArrayAdapter.createFromResource(requireContext(), R.array.GL_Account, R.layout.spinner_item_layout)
        accountTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        accountTypeSpinner.adapter = accountTypeAdapter

        binding.createVendorSubmitBtn.setOnClickListener{
            performAddVendor()
        }

        binding.createVendorResetBtn.setOnClickListener {
            binding.vendorNameInput.text.clear()
            binding.vendorPhoneInput.text.clear()
            binding.vendorEmailInput.text.clear()
            binding.vendorWebsiteInput.text.clear()
            binding.vendorAccountType.setSelection(0)
            binding.vendorCategoryInput.text.clear()
            binding.vendorCountryInput.text.clear()
            binding.vendorStateInput.text.clear()
            binding.vendorCityInput.text.clear()
            binding.vendorStreetInput.text.clear()
            binding.vendorZipcodeInput.text.clear()
            binding.vendorDescInput.text.clear()
        }

        return root
    }

    private fun fetchOwnerName() {
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null){
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val callEmployeeList = service.employeelist("Bearer $jwtToken")

            callEmployeeList.enqueue(object : Callback<ArrayList<ShowEmployeelistModelItem>>{
                override fun onResponse(
                    call: Call<ArrayList<ShowEmployeelistModelItem>>,
                    response: Response<ArrayList<ShowEmployeelistModelItem>>
                ) {
                    if (response.isSuccessful){
                        val employeelist = response.body()
                        if (employeelist != null && employeelist.isNotEmpty()){
                            val employee = employeelist[0]
                            binding.vendorOwnerInput.text = employee.name
                        } else {
                            Constants.error(requireContext(), "Employee List is null or empty")
                        }
                    } else {
                        Constants.error(requireContext(), "Unsuccessful response: ${response.code()}")
                    }
                }

                override fun onFailure(
                    call: Call<ArrayList<ShowEmployeelistModelItem>>,
                    t: Throwable
                ) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }

            })
        }
    }

    private fun performAddVendor() {
        val vendorOwner = binding.vendorOwnerInput.text.toString()
        val vendorName = binding.vendorNameInput.text.toString()
        val vendorPhone = binding.vendorPhoneInput.text.toString()
        val vendorEmail = binding.vendorEmailInput.text.toString()
        val vendorWebsite = binding.vendorWebsiteInput.text.toString()
        val vendorAccType = binding.vendorAccountType.selectedItem.toString()
        Log.d("accountType", "accountType: $vendorAccType")
        val vendorCategory = binding.vendorCategoryInput.text.toString()
        val vendorStreet = binding.vendorStreetInput.text.toString()
        val vendorCity = binding.vendorCityInput.text.toString()
        val vendorState = binding.vendorStateInput.text.toString()
        val vendorZipCode = binding.vendorZipcodeInput.text.toString()
        val vendorCountry = binding.vendorCountryInput.text.toString()
        Log.d("country", "country: $vendorCountry")
        val vendorDesc = binding.vendorDescInput.text.toString()

        if (vendorName.isBlank()){
            Constants.error(requireContext(), "Vendor Name is Required")
            return
        }

        if (!vendorName.matches("[A-Za-z ]{1,100}".toRegex())) {
            Constants.warning(
                requireContext(),
                "Customer name contains only alphabetical characters and could not exceed 100 characters."
            )
            return
        }

        if (!vendorPhone.matches("""(\+?[0-9]{1,15})""".toRegex())) {
            Constants.warning(
                requireContext(),
                "Mobile number should contain upto 15 digits and optionally start with '+'."
            )
            return
        }

        if (vendorEmail.length > 100) {
            Constants.warning(requireContext(), "Email should be upto 100 characters only.")
            return
        }

        if (vendorStreet.length > 255) {
            Constants.warning(requireContext(), "Street Address should be upto 255 characters only.")
            return
        }

        if (!vendorZipCode.matches("[A-Za-z0-9]{1,15}".toRegex())) {
            Constants.warning(
                requireContext(),
                "ZIP code should contain up to 15 alphanumeric characters only."
            )
            return
        }

        val requestBody = AddVendorModel(vendorCategory, vendorCity, vendorCountry, vendorDesc, vendorEmail, vendorAccType, vendorPhone, vendorState, vendorStreet, vendorName, vendorOwner, vendorWebsite, vendorZipCode)

        val jwtToken = gettoken(requireContext())
        if(jwtToken!=null)
        {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.addVendofromAPI("Bearer $jwtToken", requestBody)
            call.enqueue(object: Callback<AddVendorModel>
            {
                override fun onResponse(
                    call: Call<AddVendorModel>,
                    response: Response<AddVendorModel>
                ) {
                    if(response.isSuccessful)
                    {
                        Constants.success(
                            requireContext(),
                            "Vendor Added Successfully"
                        )
                        Log.d("vendor response", "vendor response: $response")
                        findNavController().popBackStack()
                    }
                    else
                    {
                        Constants.error(
                            requireContext(),
                            "Something went wrong, Try again"
                        )
                    }
                }
                override fun onFailure(call: Call<AddVendorModel>, t: Throwable) {
                    Constants.error(requireContext(),"Failed to Add Vendor. Try again")
                }

            })
        }
        else{
            Constants.error(requireContext(),"Please try to Login Again")
        }
    }

    private fun gettoken(context: Context?): String? {
        val sharedPreferences = requireContext().getSharedPreferences(
            "Token",
            Context.MODE_PRIVATE
        )
        return sharedPreferences.getString("jwtToken", null)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}