package com.BrightLink.SalesOptim.Activities.SalesOrderInvoice

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import com.BrightLink.SalesOptim.Adapters.SalesOrderInvoiceProductAdapter
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.Constants.SharedPreferecneManager
import com.BrightLink.SalesOptim.Constants.WrapContentLinearLayoutManager
import com.BrightLink.SalesOptim.Model.SalesOrderInvoiceProductModel
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.databinding.FragmentInvoiceProductBinding

class ProductFragment : Fragment() {

    private var _binding: FragmentInvoiceProductBinding? = null
    private val binding get() = _binding!!

    private val items = mutableListOf<SalesOrderInvoiceProductModel>()
    private lateinit var adapter : SalesOrderInvoiceProductAdapter

    private val PREF_NAME = "SalesPrefs"


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentInvoiceProductBinding.inflate(inflater, container, false)
        val root: View = binding.root



        adapter = SalesOrderInvoiceProductAdapter(items)
        binding.productview.adapter  = adapter
        binding.productview.layoutManager = WrapContentLinearLayoutManager(context)

        val currencyAdapter = ArrayAdapter.createFromResource(requireContext(),
            R.array.currencies_array,R.layout.spinner_item_layout_currency)
        currencyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.currency.adapter = currencyAdapter

        val taxTypeAdapter = ArrayAdapter.createFromResource(requireContext(),
            R.array.Tax_Type,R.layout.spinner_item_layout_currency)
        taxTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.taxType.adapter = taxTypeAdapter

        binding.addproduct.setOnClickListener {

            val productName = binding.productName.selectedItem.toString()
            val quantity = binding.quantity.text.toString()
            val price = binding.price.text.toString()
            val tax = binding.tax.text.toString()
            val shippingCharges = binding.shippingCharges.text.toString()

            val item = SalesOrderInvoiceProductModel(productName,quantity,price,"",tax,shippingCharges)
            items.add(item)
            adapter.notifyDataSetChanged()
            SharedPreferecneManager.saveSalesOrderInvoiceItems(requireContext(), items)
            Constants.success(context,"Product saved successfully")

            val sharedPreferences = requireContext().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putString("tax", tax)
            editor.putString("shippingCharges", shippingCharges)
            editor.apply()

        }

        binding.remove.setOnClickListener {
            adapter.removeLastItem(requireContext())
            Constants.success(requireContext(),"Product has successfully removed")
        }

        return root
    }




}