package com.BrightLink.SalesOptim.Activities.PurchaseOrder

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.databinding.PurchaseCustomerBinding

class BillingPurchaseOrderFragment : Fragment() {
    private var _binding: PurchaseCustomerBinding? = null
    private val binding get() = _binding!!
    private val PREF_NAME = "MyPrefs"
    private var visibility : Boolean = true


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = PurchaseCustomerBinding.inflate(inflater, container, false)
        val root: View = binding.root



        binding.visible.setOnClickListener {
            visibility = true
            binding.messageLayout.visibility = View.VISIBLE
            binding.visible.setBackgroundResource(R.color.statusbar)
            val color = ContextCompat.getColor(requireContext(), R.color.white)
            binding.visible.setColorFilter(color)
            val default = ContextCompat.getColor(requireContext(), R.color.black)
            binding.invisible.setColorFilter(default)
            binding.invisible.setBackgroundResource(android.R.color.transparent)
        }

        binding.invisible.setOnClickListener {
            visibility = false
            binding.messageLayout.visibility = View.INVISIBLE
            binding.invisible.setBackgroundResource(R.color.statusbar)
            val color = ContextCompat.getColor(requireContext(), R.color.white)
            binding.invisible.setColorFilter(color)
            val default = ContextCompat.getColor(requireContext(), R.color.black)
            binding.visible.setColorFilter(default)
            binding.visible.setBackgroundResource(android.R.color.transparent)
        }


        binding.savee.setOnClickListener {
            val customerName = binding.customerName.text.toString()
            val customerCompanyName = binding.customerCompanyName.text.toString()
            val customerAddress = binding.customerAddress.text.toString()
            val customerPhone = binding.customerPhone.text.toString()
            val customerSubject = binding.subject.text.toString()
            val customerMessage = binding.message.text.toString()

            val sharedPreferences = requireContext().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putString("customerName", customerName)
            editor.putString("customerCompanyName", customerCompanyName)
            editor.putString("customerAddress", customerAddress)
            editor.putString("customerPhone", customerPhone)
            editor.putString("customerSubject", customerSubject)
            editor.putString("customerMessage", customerMessage)
            editor.putBoolean("customerVisibility", visibility)
            editor.apply()

            Constants.success(context , "Customer details saved successfully")
        }

        return root
    }
}