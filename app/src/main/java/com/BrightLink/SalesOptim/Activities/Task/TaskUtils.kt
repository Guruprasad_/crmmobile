package com.BrightLink.SalesOptim.Activities.Task

import android.app.DatePickerDialog
import android.content.Context
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Spinner
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.ResponseModel.ShowEmployeelistModelItem
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

object TaskUtils {

    //Fetching task owner name
    fun fetchTaskOwnerName(context: Context, onSuccess: (String) -> Unit, onError: (String) -> Unit) {
        val jwtToken = gettoken(context)
        if (jwtToken != null) {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val callEmpList = service.employeelist("Bearer $jwtToken")

            callEmpList.enqueue(object : Callback<ArrayList<ShowEmployeelistModelItem>> {
                override fun onResponse(
                    call: Call<ArrayList<ShowEmployeelistModelItem>>,
                    response: Response<ArrayList<ShowEmployeelistModelItem>>
                ) {
                    if (response.isSuccessful) {
                        val taskOwnerList = response.body()
                        if (taskOwnerList != null && taskOwnerList.isNotEmpty()) {
                            val taskOwner = taskOwnerList[0]
                            onSuccess(taskOwner.name)
                        } else {
                            onError("Employee list is null or empty")
                        }
                    } else {
                        onError("Unsuccessful response: ${response.code()}")
                    }
                }

                override fun onFailure(
                    call: Call<ArrayList<ShowEmployeelistModelItem>>,
                    t: Throwable
                ) {
                    onError("Error: ${t.message}")
                }
            })
        } else {
            onError("Token is null. Please log in again.")
        }
    }
    //Calendars due date and reminder date logic
    fun showDatePickerDialog(
        context: Context,
        editText: EditText,
        selectedReminderDate: Date?,
        selectedDueDate: Date?,
        isDueDate: Boolean,
        onDateSelected: (Date) -> Unit
    ) {
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)

        val datePickerDialog = DatePickerDialog(
            context, R.style.MyDatePickerDialogTheme,
            DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                val selectedCalendar = Calendar.getInstance()
                selectedCalendar.set(year, monthOfYear, dayOfMonth)
                val selectedDate = selectedCalendar.time

                val formattedDate = SimpleDateFormat("dd/MM/yy hh:mm a", Locale.getDefault()).format(selectedDate)
                editText.setText(formattedDate)
                onDateSelected(selectedDate)
            },
            year,
            month,
            day
        )
        // Set minimum date for due date and maximum date for reminder date
        if (isDueDate) {
            datePickerDialog.datePicker.minDate = System.currentTimeMillis()
        } else {
            selectedDueDate?.let {
                datePickerDialog.datePicker.minDate = System.currentTimeMillis()
                datePickerDialog.datePicker.maxDate = it.time
            }
        }

        datePickerDialog.show()
    }

    //Spinner Setup method for task-screens
    fun setupSpinners(
        context: Context,
        taskStatusSpinner: Spinner,
        taskPrioritySpinner: Spinner,
        taskTypeSpinner: Spinner,
//            taskTypeElementsSpinner: Spinner
    ) {
        // Setup task status spinner
        val taskStatusList = context.resources.getStringArray(R.array.Task_Status)
        // Add your status values heres
        val taskStatusAdapter = ArrayAdapter(context, R.layout.dropdown_items, taskStatusList)
        taskStatusSpinner.adapter = taskStatusAdapter

        // Setup task priority spinner
        val taskPriorityList = context.resources.getStringArray(R.array.Task_Priority) // Add your priority values here
        val taskPriorityAdapter = ArrayAdapter(context, R.layout.dropdown_items, taskPriorityList)
        taskPrioritySpinner.adapter = taskPriorityAdapter

        //Setup task type spinner
        val tasktypeList = context.resources.getStringArray(R.array.Task_type)
        val taskTypeAdapter = ArrayAdapter(context,R.layout.dropdown_items,tasktypeList)
        taskTypeSpinner.adapter = taskTypeAdapter

        // Setup other spinners as needed
    }

    //Function to fetch selected value from the spinner
    fun getSelectedItem(spinner: Spinner): String {
        return spinner.selectedItem.toString()
    }

    fun gettoken(context: Context?): String? {
        val sharedPreferences = context?.getSharedPreferences("Token", Context.MODE_PRIVATE)
        return sharedPreferences?.getString("jwtToken", null)
    }

}