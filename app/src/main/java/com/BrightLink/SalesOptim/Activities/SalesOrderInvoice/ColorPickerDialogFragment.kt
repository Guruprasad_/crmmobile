package com.BrightLink.SalesOptim.Activities.SalesOrderInvoice

import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import com.skydoves.colorpickerview.ColorPickerDialog
import com.skydoves.colorpickerview.listeners.ColorEnvelopeListener


class ColorPickerDialogFragment : DialogFragment(){
    private var colorPickerListener: ColorPickerListener? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return ColorPickerDialog.Builder(requireContext())
            .setTitle("Pick a color")
            .setPreferenceName("MyColorPickerDialog")
            .setPositiveButton("Ok", ColorEnvelopeListener { envelope, _ ->
                colorPickerListener?.onColorSelected(envelope.color)
            })
            .setNegativeButton("Cancel") { _, _ -> }
            .attachAlphaSlideBar(true)
            .attachBrightnessSlideBar(true)
            .setBottomSpace(12)
            .create()
    }

    fun setColorPickerListener(listener: ColorPickerListener) {
        colorPickerListener = listener
    }

    interface ColorPickerListener {
        fun onColorSelected(color: Int)
    }
}