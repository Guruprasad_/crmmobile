package com.BrightLink.SalesOptim.Activities.AssignLead.Contact

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.BrightLink.SalesOptim.databinding.QualifyAssignLeadBinding

class ActivityQualifyFragment : Fragment() {

    private var _binding: QualifyAssignLeadBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = QualifyAssignLeadBinding.inflate(inflater, container, false)
        val root: View = binding.root



        return root
    }

}