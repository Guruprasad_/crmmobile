package com.BrightLink.SalesOptim.Activities.Accounts

import AccountLinksAdapter
import AccountNotesAdapter
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.OpenableColumns
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.MimeTypeMap
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import android.widget.Spinner
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.BrightLink.SalesOptim.Activities.Contact.UpdateContact
import com.BrightLink.SalesOptim.Adapters.AccountAttachmentAdapter
import com.BrightLink.SalesOptim.Adapters.AccountContactDetailsAdapter
import com.BrightLink.SalesOptim.Adapters.AccountLeadDetailsAdapter
import com.BrightLink.SalesOptim.Adapters.AccountTaskDetailsAdapter
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.Constants.WrapContentLinearLayoutManager
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.RequestModel.AddLinkReqModel
import com.BrightLink.SalesOptim.RequestModel.AddNoteRequest
import com.BrightLink.SalesOptim.RequestModel.AttachmentDto
import com.BrightLink.SalesOptim.RequestModel.UpdateAccountPutModel
import com.BrightLink.SalesOptim.ResponseModel.AccountAttachmentResModel
import com.BrightLink.SalesOptim.ResponseModel.AccountContactDetailsModelItem
import com.BrightLink.SalesOptim.ResponseModel.AccountTaskDetailsModelItem
import com.BrightLink.SalesOptim.ResponseModel.AddLinkResponseModel
import com.BrightLink.SalesOptim.ResponseModel.AddNoteModel
import com.BrightLink.SalesOptim.ResponseModel.AttaModel
import com.BrightLink.SalesOptim.ResponseModel.CityModelItem
import com.BrightLink.SalesOptim.ResponseModel.CountryModelItem
import com.BrightLink.SalesOptim.ResponseModel.LeadDetailsAccountModelItem
import com.BrightLink.SalesOptim.ResponseModel.ShowAccountsModelItem
import com.BrightLink.SalesOptim.ResponseModel.StateModelItem
import com.BrightLink.SalesOptim.ResponseModel.UpdateAccountResponseModel
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.ActivityUpdateAccountBinding
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class UpdateAccount : Fragment() {

    private var _binding: ActivityUpdateAccountBinding? = null
    private val binding get() = _binding!!
//    private val binding get() = _binding ?: throw IllegalStateException("Binding not available")

    private val resetUIHandler = Handler(Looper.getMainLooper())

    private var initialData: ShowAccountsModelItem? = null

    private lateinit var countrySpinner: Spinner
    private lateinit var countryAdapter: ArrayAdapter<String>
    private var countryNames: ArrayList<String> = ArrayList()

    private lateinit var stateSpinner: Spinner
    private lateinit var stateAdapter: ArrayAdapter<String>
    private var stateNames: ArrayList<String> = ArrayList()

    private lateinit var citySpinner: Spinner
    private lateinit var cityAdapter: ArrayAdapter<String>
    private var cityNames: ArrayList<String> = ArrayList()

    private lateinit var countryList: List<CountryModelItem>
    private lateinit var stateList: List<StateModelItem>
    private lateinit var cityList: List<CityModelItem>


    private lateinit var countrySpinner1: Spinner
    private lateinit var countryAdapter1: ArrayAdapter<String>
    private var countryNames1: ArrayList<String> = ArrayList()

    private lateinit var stateSpinner1: Spinner
    private lateinit var stateAdapter1: ArrayAdapter<String>
    private var stateNames1: ArrayList<String> = ArrayList()

    private lateinit var citySpinner1: Spinner
    private lateinit var cityAdapter1: ArrayAdapter<String>
    private var cityNames1: ArrayList<String> = ArrayList()

    private lateinit var countryList1: List<CountryModelItem>
    private lateinit var stateList1: List<StateModelItem>
    private lateinit var cityList1: List<CityModelItem>

    private val PICKFILE_RESULT_CODE = 1
    private var filePath: String? = null
    private var dialog: Dialog? = null
    private var path: String = ""
    private var AccountId: Int? = 0
    private lateinit var Desc: String

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        _binding = ActivityUpdateAccountBinding.inflate(inflater, container, false)
        val root: View? = binding.root

        binding.actionBar.activityName.text = "Update Account"
        binding.actionBar.back.setOnClickListener {
            requireActivity().onBackPressed()
        }
        binding.actionBar.option.visibility = View.INVISIBLE



        countrySpinner = binding.accountBillingCountry
        countryAdapter = ArrayAdapter(
            requireContext(),
            R.layout.spinner_item_layout,
            countryNames
        ).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
        countrySpinner.adapter = countryAdapter

        stateSpinner = binding.accountBillingState
        stateAdapter = ArrayAdapter(
            requireContext(),
            R.layout.spinner_item_layout,
            stateNames
        ).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
        stateSpinner.adapter = stateAdapter

        citySpinner = binding.accountBillingCity
        cityAdapter = ArrayAdapter(
            requireContext(),
            R.layout.spinner_item_layout,
            cityNames
        ).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
        citySpinner.adapter = cityAdapter





        countrySpinner1 = binding.accountShippingCountry
        countryAdapter1 = ArrayAdapter(
            requireContext(),
            R.layout.spinner_item_layout,
            countryNames1
        ).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
        countrySpinner1.adapter = countryAdapter1

        stateSpinner1 = binding.accountShippingState
        stateAdapter1 = ArrayAdapter(
            requireContext(),
            R.layout.spinner_item_layout,
            stateNames1
        ).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
        stateSpinner1.adapter = stateAdapter1

        citySpinner1 = binding.accountShippingCity
        cityAdapter1 = ArrayAdapter(
            requireContext(),
            R.layout.spinner_item_layout,
            cityNames1
        ).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
        citySpinner1.adapter = cityAdapter1

        callCountryApi()
        callCountryApi1()

        val selectedItem = arguments?.getSerializable("selectedItem") as? ShowAccountsModelItem
        if (selectedItem == null) {
            // Handle null case, perhaps show an error message
            Constants.error(requireContext(), "Selected item is null")
            return root
        }
//
        val accountId = selectedItem.accountId ?: 0
//        if (accountId != null) {
//            Constants.info(requireContext(),"Contact Id fetched ${accountId}")
//            fetchAttachments(accountId)        }
//        else{
//            Constants.error(requireContext(),"Contact Id is Null")
//        }


        refreshAttachmentList(accountId)

        binding.rvLeadDetails.layoutManager = WrapContentLinearLayoutManager(context)
        leadDetailsByAccountId(accountId)

        binding.rvTaskDetails.layoutManager = WrapContentLinearLayoutManager(context)
        taskDetailsByAccountId(accountId)

        binding.rvContactDetails.layoutManager = WrapContentLinearLayoutManager(context)
        contactDetailsByAccountId(accountId)

        binding.update.setOnClickListener {
            performUpdateAccount(accountId, selectedItem)
        }

        selectedItem?.let {
            binding.accountName.setText(it.accountName ?: "")
            binding.accountPhone.setText(it.phone ?: "")
            binding.accountTypeSpinner.setSelection(
                getIndex(
                    binding.accountTypeSpinner, it.accountType ?: ""
                )
            )
            binding.accountAnnualRevenue.setText(it.revenue ?: "")
            binding.accountSite.setText(it.accountSite ?: "")
            binding.accountNumber.setText(it.accountNumber ?: "")
            binding.accountIndustrySpinner.setSelection(
                getIndex(
                    binding.accountIndustrySpinner, it.industry ?: ""
                )
            )
            binding.accountRating.setSelection(
                getIndex(
                    binding.accountRating, it.rating ?: ""
                )
            )
            binding.accountFax.setText(it.fax ?: "")
            binding.accountWebsite.setText(it.website ?: "")
            binding.accountSymbol.setText(it.tickerSymbol ?: "")
            binding.accountOwnershipSpinner.setSelection(
                getIndex(
                    binding.accountOwnershipSpinner, it.ownership ?: ""
                )
            )
            binding.accountEmployee.setText(selectedItem?.employees?.toString() ?: "")
            binding.accountBillingStreet.setText(it.billingStreet ?: "")
            binding.accountBillingCode.setText(it.billingCode ?: "")
            binding.accountShippingStreet.setText(it.shippingStreet ?: "")
            binding.accountShippingCode.setText(it.shippingCode ?: "")
            binding.accountDescription.setText(it.description ?: "")
//            binding.accountBillingCountry.selectedItem(it.billingCountryId ?: "")
            binding.accountBillingCountry.setSelection(
                getIndex(
                    binding.accountBillingCountry, it.billingCountry ?: ""
                )
            )
            binding.accountBillingState.setSelection(
                getIndex(
                    binding.accountBillingState, it.billingState ?: ""
                )
            )
            binding.accountBillingCity.setSelection(
                getIndex(
                    binding.accountBillingCity, it.billingCity ?: ""
                )
            )
            binding.accountShippingCountry.setSelection(
                getIndex(
                    binding.accountShippingCountry, it.shippingCountry ?: ""
                )
            )
            binding.accountShippingState.setSelection(
                getIndex(
                    binding.accountShippingState, it.shippingState ?: ""
                )
            )
            binding.accountShippingCity.setSelection(
                getIndex(
                    binding.accountShippingCity, it.shippingCity ?: ""
                )
            )
//            binding.accountBillingState.setText(selectedItem?.billingStateId ?: "")
//            binding.accountBillingCity.setText(selectedItem?.billingCityId ?: "")
//            binding.accountShippingCountry.setText(selectedItem?.shippingCountryId?.toString() ?: "")
//            binding.accountShippingState.setText(selectedItem?.shippingStateId?.toString() ?: "")
//            binding.accountShippingCity.setText(selectedItem?.shippingCityId?.toString() ?: "")
            // Set other fields similarly
        }

        initialData = selectedItem

        // Other existing code...

        binding.reset.setOnClickListener {
            // Reset UI elements to initial state
            resetUI()
        }
//        Handler(Looper.getMainLooper()).postDelayed({
//            resetUI()
//        }, 3000)

        try {
            resetUIHandler.postDelayed({
                resetUI()
            }, 3000)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        binding.addNoteBtn.setOnClickListener {
            showDialog(R.layout.account_add_notes)
            AccountId = selectedItem.accountId!!
            val fileChooser = dialog!!.findViewById<Button>(R.id.noteFileOneChoose)
            fileChooser.setOnClickListener {
                showFileChooser()
            }
            val noteDescription = dialog!!.findViewById<EditText>(R.id.fileNoteDesEditText)

            val addNote = dialog!!.findViewById<Button>(R.id.addNote)

            addNote.setOnClickListener {
                Desc = noteDescription.text.toString()
                addNotes()

            }
        }

        binding.addAtta.setOnClickListener {
            showDialog(R.layout.account_add_attachment)
            AccountId = selectedItem.accountId!!
            val fileChooser = dialog!!.findViewById<Button>(R.id.linkFileOneChoose)
            fileChooser.setOnClickListener {
                showFileChooser()
            }
            val addAttachmnt = dialog!!.findViewById<Button>(R.id.addAccountAttachment)

            addAttachmnt.setOnClickListener {
                addAttachmentContact()
            }
        }

        binding.accountAddLink.setOnClickListener {
            showDialog(R.layout.account_add_links)

            val addLinkPop = dialog!!.findViewById<Button>(R.id.addLinkPop)
            val addLabel = dialog!!.findViewById<EditText>(R.id.addLabel)
            val addDesc = dialog!!.findViewById<EditText>(R.id.linkDesc)
            val addLinkUrl = dialog!!.findViewById<EditText>(R.id.linkUrl)

            addLinkPop.setOnClickListener {
                val label = addLabel.text.toString().trim()
                val desc = addDesc.text.toString().trim()
                val linkUrl = addLinkUrl.text.toString().trim()

                if (linkUrl.isEmpty()) {
                    Constants.error(requireContext(), "Link URL cannot be empty")
                } else {
                    addLinks(label, desc, linkUrl, accountId)
                }
            }
        }

        return root
    }


    private fun callCountryApi() {
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null) {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.getCountry("Bearer $jwtToken")
            call.enqueue(object : Callback<List<CountryModelItem>> {
                override fun onResponse(
                    call: Call<List<CountryModelItem>>,
                    response: Response<List<CountryModelItem>>
                ) {
                    if (isAdded) {
                        if (response.isSuccessful) {
                            val data = response.body()
                            if (data != null) {
                                countryList = data
                                for (item in data) {
                                    item.name?.let { countryNames.add(it) }
                                }
                                countryAdapter.notifyDataSetChanged()

                                countrySpinner.onItemSelectedListener =
                                    object : AdapterView.OnItemSelectedListener {
                                        override fun onItemSelected(
                                            parent: AdapterView<*>?,
                                            view: View?,
                                            position: Int,
                                            id: Long
                                        ) {
                                            val selectedCountryName = countryNames[position]
                                            val selectedCountry =
                                                data.find { it.name == selectedCountryName }
                                            selectedCountry?.let {
                                                callStateApi(it.id)
                                            } ?: run {
                                                Constants.warning(
                                                    requireContext(),
                                                    "Country ID not found"
                                                )
                                            }
                                        }

                                        override fun onNothingSelected(p0: AdapterView<*>?) {
                                            Constants.warning(requireContext(), "Select Country")
                                        }

                                    }

                            } else {
                                Constants.error(requireContext(), "Response body is null")
                            }
                        } else {
                            Constants.error(
                                requireContext(),
                                "Unsuccessful response: ${response.code()}"
                            )
                        }
                    }
                }

                override fun onFailure(call: Call<List<CountryModelItem>>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }

            })
        }
    }

    private fun callStateApi(id: Int) {
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null) {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.getState("Bearer $jwtToken", id)
            call.enqueue(object : Callback<List<StateModelItem>> {
                override fun onResponse(
                    call: Call<List<StateModelItem>>,
                    response: Response<List<StateModelItem>>
                ) {
                    if (isAdded) {
                        if (response.isSuccessful) {
                            val data = response.body()
                            if (data != null) {
                                stateList = data
                                stateNames.clear()
                                for (item in data) {
                                    item.name?.let { stateNames.add(it) }
                                }
                                stateAdapter.notifyDataSetChanged()

                                stateSpinner.onItemSelectedListener =
                                    object : AdapterView.OnItemSelectedListener {
                                        override fun onItemSelected(
                                            parent: AdapterView<*>?,
                                            view: View?,
                                            position: Int,
                                            id: Long
                                        ) {
                                            val selectedStateName = stateNames[position]
                                            val selectedState =
                                                data.find { it.name == selectedStateName }
                                            selectedState?.let {
                                                callCityApi(it.id)
                                            } ?: run {
                                                Constants.warning(
                                                    requireContext(),
                                                    "State ID not found"
                                                )
                                            }
                                        }

                                        override fun onNothingSelected(p0: AdapterView<*>?) {
                                            Constants.warning(requireContext(), "Select State")
                                        }

                                    }
                            } else {
                                Constants.error(requireContext(), "Response body is null")
                            }
                        } else {
                            Constants.error(
                                requireContext(),
                                "Unsuccessful response: ${response.code()}"
                            )
                        }
                    }
                }

                override fun onFailure(call: Call<List<StateModelItem>>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }

            })
        }
    }

    private fun callCityApi(id: Int) {
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null) {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.getCity("Bearer $jwtToken", id)
            call.enqueue(object
                : Callback<List<CityModelItem>> {
                override fun onResponse(
                    call: Call<List<CityModelItem>>,
                    response: Response<List<CityModelItem>>
                ) {
                    if (isAdded) {
                        if (response.isSuccessful) {
                            val data = response.body()
                            if (data != null) {
                                cityList = data
                                cityNames.clear()
                                for (item in data) {
                                    item.name?.let { cityNames.add(it) }
                                }
                                cityAdapter.notifyDataSetChanged()
                            } else {
                                Constants.error(requireContext(), "Response body is null")
                            }
                        } else {
                            Constants.error(
                                requireContext(),
                                "Unsuccessful response: ${response.code()}"
                            )
                        }
                    }
                }

                override fun onFailure(call: Call<List<CityModelItem>>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }

            })
        }
    }


    private fun callCountryApi1() {
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null) {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.getCountry("Bearer $jwtToken")
            call.enqueue(object : Callback<List<CountryModelItem>> {
                override fun onResponse(
                    call: Call<List<CountryModelItem>>,
                    response: Response<List<CountryModelItem>>
                ) {
                    if (isAdded) {
                        if (response.isSuccessful) {
                            val data = response.body()
                            if (data != null) {
                                countryList1 = data
                                for (item in data) {
                                    item.name?.let { countryNames1.add(it) }
                                }
                                countryAdapter1.notifyDataSetChanged()

                                countrySpinner1.onItemSelectedListener =
                                    object : AdapterView.OnItemSelectedListener {
                                        override fun onItemSelected(
                                            parent: AdapterView<*>?,
                                            view: View?,
                                            position: Int,
                                            id: Long
                                        ) {
                                            val selectedCountryName = countryNames1[position]
                                            val selectedCountry =
                                                data.find { it.name == selectedCountryName }
                                            selectedCountry?.let {
                                                callStateApi1(it.id)
                                            } ?: run {
                                                Constants.warning(
                                                    requireContext(),
                                                    "Country ID not found"
                                                )
                                            }
                                        }

                                        override fun onNothingSelected(p0: AdapterView<*>?) {
                                            Constants.warning(requireContext(), "Select Country")
                                        }

                                    }

                            } else {
                                Constants.error(requireContext(), "Response body is null")
                            }
                        } else {
                            Constants.error(
                                requireContext(),
                                "Unsuccessful response: ${response.code()}"
                            )
                        }
                    }
                }

                override fun onFailure(call: Call<List<CountryModelItem>>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }

            })
        }
    }

    private fun callStateApi1(id: Int) {
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null) {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.getState("Bearer $jwtToken", id)
            call.enqueue(object : Callback<List<StateModelItem>> {
                override fun onResponse(
                    call: Call<List<StateModelItem>>,
                    response: Response<List<StateModelItem>>
                ) {
                    if (isAdded) {
                        if (response.isSuccessful) {
                            val data = response.body()
                            if (data != null) {
                                stateList1 = data
                                stateNames1.clear()
                                for (item in data) {
                                    item.name?.let { stateNames1.add(it) }
                                }
                                stateAdapter1.notifyDataSetChanged()

                                stateSpinner1.onItemSelectedListener =
                                    object : AdapterView.OnItemSelectedListener {
                                        override fun onItemSelected(
                                            parent: AdapterView<*>?,
                                            view: View?,
                                            position: Int,
                                            id: Long
                                        ) {
                                            val selectedStateName = stateNames1[position]
                                            val selectedState =
                                                data.find { it.name == selectedStateName }
                                            selectedState?.let {
                                                callCityApi1(it.id)
                                            } ?: run {
                                                Constants.warning(
                                                    requireContext(),
                                                    "State ID not found"
                                                )
                                            }
                                        }

                                        override fun onNothingSelected(p0: AdapterView<*>?) {
                                            Constants.warning(requireContext(), "Select State")
                                        }

                                    }
                            } else {
                                Constants.error(requireContext(), "Response body is null")
                            }
                        } else {
                            Constants.error(
                                requireContext(),
                                "Unsuccessful response: ${response.code()}"
                            )
                        }
                    }
                }

                override fun onFailure(call: Call<List<StateModelItem>>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }

            })
        }
    }

    private fun callCityApi1(id: Int) {
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null) {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.getCity("Bearer $jwtToken", id)
            call.enqueue(object
                : Callback<List<CityModelItem>> {
                override fun onResponse(
                    call: Call<List<CityModelItem>>,
                    response: Response<List<CityModelItem>>
                ) {
                    if (isAdded) {
                        if (response.isSuccessful) {
                            val data = response.body()
                            if (data != null) {
                                cityList1 = data
                                cityNames1.clear()
                                for (item in data) {
                                    item.name?.let { cityNames1.add(it) }
                                }
                                cityAdapter1.notifyDataSetChanged()
                            } else {
                                Constants.error(requireContext(), "Response body is null")
                            }
                        } else {
                            Constants.error(
                                requireContext(),
                                "Unsuccessful response: ${response.code()}"
                            )
                        }
                    }
                }

                override fun onFailure(call: Call<List<CityModelItem>>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }

            })
        }
    }


    private fun refreshAttachmentList(accountId: Int) {
        gettoken(requireContext())?.let { jwtToken ->
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            fetchAndBind(service, jwtToken, accountId,
                { data ->
                    binding.recyclerViewAttachment.apply {
                        layoutManager = LinearLayoutManager(requireContext())
                        adapter = AccountAttachmentAdapter(requireContext(), data)
                    }
                },
                { data ->
                    val linkClickListener = object : AccountLinksAdapter.LinkClickListener {
                        override fun onLinkClick(url: String) {
                            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
                        }
                    }
                    binding.recyclerViewLinks.apply {
                        layoutManager = LinearLayoutManager(requireContext())
                        adapter = AccountLinksAdapter(requireContext(), data, linkClickListener)
                    }
                },
                { data ->
                    binding.recyclerViewNotes.apply {
                        layoutManager = LinearLayoutManager(requireContext())
                        adapter = AccountNotesAdapter(requireContext(), data)
                    }
                }
            )
        } ?: Constants.error(requireContext(), "Token is null. Please log in again.")
    }

    private fun fetchAndBind(
        service: ApiInterface,
        jwtToken: String,
        accountId: Int,
        attachmentHandler: (List<AccountAttachmentResModel>) -> Unit,
        linkHandler: (List<AccountAttachmentResModel>) -> Unit,
        notesHandler: (List<AccountAttachmentResModel>) -> Unit
    ) {
        val calls = listOf(
            service.getAccountAttachments("Bearer $jwtToken", accountId),
            service.getAccountLinks("Bearer $jwtToken", accountId),
            service.getAccountNotes("Bearer $jwtToken", accountId)
        )

        calls.forEachIndexed { index, call ->
            call.enqueue(object : Callback<List<AccountAttachmentResModel>> {
                override fun onResponse(
                    call: Call<List<AccountAttachmentResModel>>,
                    response: Response<List<AccountAttachmentResModel>>
                ) {
                    if (!isAdded) return
                    val data = response.body()
                    if (response.isSuccessful && data != null) {
                        when (index) {
                            0 -> attachmentHandler(data)
                            1 -> linkHandler(data)
                            2 -> notesHandler(data)
                        }
                    } else if (!response.isSuccessful) {
//                        Constants.error(requireContext(), "Unsuccessful response: ${response.code()}")
                    }
                }

                override fun onFailure(call: Call<List<AccountAttachmentResModel>>, t: Throwable) {
                    if (isAdded) {
                        Constants.error(requireContext(), "Error: ${t.message}")
                    }
                }
            })
        }
    }


    private fun gettoken(context: Context): String? {
        return context.getSharedPreferences("Token", Context.MODE_PRIVATE)
            .getString("jwtToken", null)
    }


    private fun showDialog(layoutResId: Int) {

        if (dialog == null) {
            dialog = Dialog(requireActivity())
            dialog!!.setCancelable(false)
            dialog!!.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        dialog!!.setContentView(layoutResId)
        dialog!!.show()

        val closeBtn = dialog!!.findViewById<ImageButton>(R.id.closeBtn)
        closeBtn.setOnClickListener {
            dialog!!.dismiss()
        }

    }

    private fun showFileChooser() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
            intent.type = "*/*"
            startActivityForResult(intent, PICKFILE_RESULT_CODE)
        } else {
            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.type = "*/*"
            intent.addCategory(Intent.CATEGORY_OPENABLE)
            startActivityForResult(
                Intent.createChooser(intent, "Select File"), PICKFILE_RESULT_CODE
            )
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == PICKFILE_RESULT_CODE && resultCode == Activity.RESULT_OK && data != null && data.data != null) {
            val uri: Uri = data.data!!
            path = getPathFromUri(requireContext(), uri)!!
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                updateTextViewWithFileName(uri)
            } else {
                // Handle for lower versions
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun updateTextViewWithFileName(uri: Uri) {
        val fileName = getFileNameFromUri(uri)
        filePath = uri.toString()
        if (fileName != null) {
            val selectedFile = dialog!!.findViewById<TextView>(R.id.selectedFile)
            selectedFile.text = Html.fromHtml("<b><font color='#7FFF00'>$fileName</font></b>")
        }
    }

    private fun getFileNameFromUri(uri: Uri): String? {
        var fileName: String? = null
        val cursor = requireContext().contentResolver.query(uri, null, null, null, null)
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                val displayNameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
                if (displayNameIndex != -1) {
                    fileName = cursor.getString(displayNameIndex)
                }
            }
            cursor.close()
        }
        return fileName
    }

    @SuppressLint("Range")
    private fun getPathFromUri(context: Context, uri: Uri): String? {
        var path: String? = null
        if ("content".equals(uri.scheme, ignoreCase = true)) {
            // Using ACTION_OPEN_DOCUMENT
            val cursor = context.contentResolver.query(uri, null, null, null, null)
            cursor?.use {
                if (it.moveToFirst()) {
                    val displayName = it.getString(it.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                    val type = it.getString(it.getColumnIndex("mime_type"))
                    val file = File(context.cacheDir, displayName)
                    try {
                        val inputStream = context.contentResolver.openInputStream(uri)
                        inputStream?.use { input ->
                            FileOutputStream(file).use { output ->
                                input.copyTo(output)
                            }
                        }
                        path = file.absolutePath
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
            }
        } else if ("file".equals(uri.scheme, ignoreCase = true)) {
            path = uri.path
        }
        return path
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun addAttachmentContact() {
        val attach = AttachmentDto(0, 0, 0, AccountId!!)

        if (path != null && path.isNotEmpty()) {
            val file = File(path)
            val content: ByteArray = file.readBytes()
            val fileExtension = MimeTypeMap.getFileExtensionFromUrl(file.absolutePath)
            val mediaType =
                UpdateContact.FileTypeHelper.getMediaTypeFromFileExtension(fileExtension)

            if (mediaType != null) {
                val requestBody = RequestBody.create(mediaType, content)
                val jwtToken = gettoken(requireContext())
                if (jwtToken != null) {
                    val service = ApiUtilities.getinstance().create(ApiInterface::class.java)

                    val body = MultipartBody.Part.createFormData("file", file.name, requestBody)
                    val call = service.addAttachment("Bearer $jwtToken", body, attach)
                    call.enqueue(object : Callback<AttaModel> {
                        override fun onResponse(
                            call: Call<AttaModel>, response: Response<AttaModel>
                        ) {
                            if (isAdded) {
                                if (response.isSuccessful) {
                                    val responseBody = response.body()
                                    Log.d("ResponseBody", "$responseBody")
                                    dialog!!.dismiss()
                                    Constants.success(
                                        requireContext(),
                                        "File Attached Successfully"
                                    )
                                } else {
                                    Constants.error(requireContext(), "Unsuccessful")
                                }
                            }
                        }

                        override fun onFailure(call: Call<AttaModel>, t: Throwable) {
                            Constants.error(requireContext(), "Error: ${t.message}")

                        }
                    })
                } else {
                    Constants.error(requireContext(), "Token Not Found")
                }
            } else {
                Constants.error(requireContext(), "Unsupported file type")
            }
        } else {
            Constants.error(requireContext(), "File not Selected, Please Select a file")
        }
    }

    private fun addNotes() {
        val note = AddNoteRequest(AccountId!!, 0, 0, Desc, 0)
        if (path != null && path.isNotEmpty()) {
            val file = File(path)
            val content: ByteArray = file.readBytes()
            val fileExtension = MimeTypeMap.getFileExtensionFromUrl(file.absolutePath)
            val mediaType =
                UpdateContact.FileTypeHelper.getMediaTypeFromFileExtension(fileExtension)

            if (mediaType != null) {
                val requestBody = RequestBody.create(mediaType, content)
                val jwtToken = gettoken(requireContext())
                if (jwtToken != null) {
                    val service = ApiUtilities.getinstance().create(ApiInterface::class.java)

                    val body = MultipartBody.Part.createFormData("file", file.name, requestBody)
                    val call = service.addnote("Bearer $jwtToken", body, note)
                    call.enqueue(object : Callback<AddNoteModel> {
                        override fun onResponse(
                            call: Call<AddNoteModel>,
                            response: Response<AddNoteModel>
                        ) {
                            if (isAdded) {
                                if (response.isSuccessful) {
                                    val responseBody = response.body()
                                    dialog!!.dismiss()
                                    Constants.success(requireContext(), "Note Sent Successfully")
                                } else {
                                    Constants.error(requireContext(), "Unsuccessful")
                                }
                            }
                        }

                        override fun onFailure(call: Call<AddNoteModel>, t: Throwable) {
                            Constants.error(requireContext(), "Error: ${t.message}")

                        }
                    })
                } else {
                    Constants.error(requireContext(), "Token Not Found")
                }
            } else {
                Constants.error(requireContext(), "Unsupported file type")
            }
        } else {
            Constants.error(requireContext(), "File not Selected, Please Select a file")
        }
    }

    private fun addLinks(label: String, desc: String, linkUrl: String, accountId: Int) {
        val requestBody = AddLinkReqModel(accountId, 0, label, 0, desc, 0, linkUrl)
        Log.d("label2", "$label")

        val jwtToken = gettoken(requireContext())
        if (jwtToken != null) {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)

            val call = service.addLink("Bearer $jwtToken", requestBody)
            call.enqueue(object : Callback<AddLinkResponseModel> {
                override fun onResponse(
                    call: Call<AddLinkResponseModel>,
                    response: Response<AddLinkResponseModel>
                ) {
                    if (isAdded) {
                        if (response.isSuccessful) {
                            val responseBody = response.body()
                            Log.d("ResponseBody", "$responseBody")
                            dialog!!.dismiss()
                            Constants.success(requireContext(), "Note sent Successfully")
                        } else {
                            Constants.error(requireContext(), "Unsuccessful")
                        }
                    }
                }

                override fun onFailure(call: Call<AddLinkResponseModel>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }
            })
        } else {
            Constants.error(requireContext(), "Token Not Found")
        }
    }


    private fun setUpUIWithData(selectedItem: ShowAccountsModelItem) {
        selectedItem?.let {
            // Set UI fields with data from the selected account item
            binding.accountOwner.setText(selectedItem.accountOwner ?: "")
            binding.accountName.setText(selectedItem.accountName ?: "")
            binding.accountPhone.setText(selectedItem.phone ?: "")
            // Set account type spinner
            binding.accountTypeSpinner.setSelection(
                getIndex(
                    binding.accountTypeSpinner,
                    selectedItem.accountType ?: ""
                )
            )
            binding.accountAnnualRevenue.setText(selectedItem.revenue ?: "")
            binding.accountSite.setText(selectedItem.accountSite ?: "")
            binding.accountNumber.setText(selectedItem.accountNumber ?: "")
            // Set industry spinner
            binding.accountIndustrySpinner.setSelection(
                getIndex(
                    binding.accountIndustrySpinner,
                    selectedItem.industry ?: ""
                )
            )
            // Set rating spinner
            binding.accountRating.setSelection(
                getIndex(
                    binding.accountRating,
                    selectedItem.rating ?: ""
                )
            )
            binding.accountFax.setText(selectedItem.fax ?: "")
            binding.accountWebsite.setText(selectedItem.website ?: "")
            binding.accountSymbol.setText(selectedItem.tickerSymbol ?: "")
            // Set ownership spinner
            binding.accountOwnershipSpinner.setSelection(
                getIndex(
                    binding.accountOwnershipSpinner,
                    selectedItem.ownership ?: ""
                )
            )
            binding.accountEmployee.setText(selectedItem.employees?.toString() ?: "")
            binding.accountBillingStreet.setText(selectedItem.billingStreet ?: "")
            binding.accountBillingCode.setText(selectedItem.billingCode ?: "")
            binding.accountShippingStreet.setText(selectedItem.shippingStreet ?: "")
            binding.accountShippingCode.setText(selectedItem.shippingCode ?: "")
            binding.accountDescription.setText(selectedItem.description ?: "")

            // Set billing country
            selectedItem.billingCountry?.let { country ->
                val accountIndex = countryNames.indexOf(country)
                if (accountIndex != -1) {
                    binding.accountBillingCountry.setSelection(accountIndex)
                }
            }
            // Set billing state
            selectedItem.billingState?.let { state ->
                val accountIndex = stateNames.indexOf(state)
                if (accountIndex != -1) {
                    binding.accountBillingState.setSelection(accountIndex)
                }
            }
            // Set billing city
            selectedItem.billingCity?.let { city ->
                val accountIndex = cityNames.indexOf(city)
                if (accountIndex != -1) {
                    binding.accountBillingCity.setSelection(accountIndex)
                }
            }
            // Set shipping country
            selectedItem.shippingCountry?.let { country ->
                val accountIndex = countryNames1.indexOf(country)
                if (accountIndex != -1) {
                    binding.accountShippingCountry.setSelection(accountIndex)
                }
            }
            // Set shipping state
            selectedItem.shippingState?.let { state ->
                val accountIndex = stateNames1.indexOf(state)
                if (accountIndex != -1) {
                    binding.accountShippingState.setSelection(accountIndex)
                }
            }
            // Set shipping city
            selectedItem.shippingCity?.let { city ->
                val vendorIndex = cityNames1.indexOf(city)
                if (vendorIndex != -1) {
                    binding.accountShippingCity.setSelection(vendorIndex)
                }
            }
            // Set other fields similarly
        }
    }

    private fun resetUI() {
        // Restore the initial state of UI elements
        initialData?.let { setUpUIWithData(it) }
    }

    // Function to get the index of a given value in a Spinner
    private fun getIndex(spinner: Spinner, value: String): Int {
        val adapter = spinner.adapter
        for (i in 0 until adapter.count) {
            if (adapter.getItem(i).toString() == value) {
                return i
            }
        }
        return 0 // Default to the first item if value not found
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // Set up spinner with custom adapter
        setupSpinner(binding.accountOwnershipSpinner, R.array.Ownership)
        setupSpinner(binding.accountTypeSpinner, R.array.Account_Type)
        setupSpinner(binding.accountIndustrySpinner, R.array.Industry)
        setupSpinner(binding.accountRating, R.array.Rating)
        resetUI()
    }

    private fun setupSpinner(spinner: Spinner, arrayResourceId: Int) {
        val adapter = ArrayAdapter.createFromResource(
            requireContext(), arrayResourceId, R.layout.spinner_item_layout_currency
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = adapter
    }
//    private fun getIndex(spinner: Spinner, myString: String): Int {
//        for (i in 0 until spinner.count) {
//            if (spinner.getItemAtPosition(i).toString() == myString) {
//                return i
//            }
//        }
//        return 0 // Default to 0 if not found
//    }

    private fun performUpdateAccount(accountId: Int, selectedItem: ShowAccountsModelItem?) {
        selectedItem?.let {
            // Fetch input data from the UI fields
            val accountName = binding.accountName.text?.toString()
            val accountSite = binding.accountSite.text?.toString()
            val accountNumber = binding.accountNumber.text?.toString()
            val accountType = binding.accountTypeSpinner.selectedItem?.toString()
            val industry = binding.accountIndustrySpinner.selectedItem?.toString()
            val revenue = binding.accountAnnualRevenue.text?.toString()
            val rating = binding.accountRating.selectedItem?.toString()
            val phone = binding.accountPhone.text?.toString()
            val fax = binding.accountFax.text?.toString()
            val website = binding.accountWebsite.text?.toString()
            val tickerSymbol = binding.accountSymbol.text?.toString()
            val ownership = binding.accountOwnershipSpinner.selectedItem?.toString()
            val employees = binding.accountEmployee.text.toString().toIntOrNull() ?: 0
            val billingStreet = binding.accountBillingStreet.text?.toString()
            val billingCode = binding.accountBillingCode.text?.toString()
            val shippingStreet = binding.accountShippingStreet.text?.toString()
            val shippingCode = binding.accountShippingCode.text?.toString()
            val description = binding.accountDescription.text?.toString()
            //        val billingCountryId = binding.accountBillingCountry.text?.toString()
            //        val billingStateId = binding.accountBillingState.text?.toString()
            //        val billingCityId = binding.accountBillingCity.text?.toString()
            //        val shippingCountryId = binding.accountShippingCountry.text.toString().toIntOrNull() ?: 0
            //        val shippingStateId = binding.accountShippingState.text.toString().toIntOrNull() ?: 0
            //        val shippingCityId = binding.accountShippingCity.text.toString().toIntOrNull() ?: 0

            val billingCountryId = countryList[countrySpinner.selectedItemPosition].id
            val billingStateId = stateList[stateSpinner.selectedItemPosition].id
            val billingCityId = cityList[citySpinner.selectedItemPosition].id
            val shippingCountryId = countryList1[countrySpinner1.selectedItemPosition].id
            val shippingStateId = stateList1[stateSpinner1.selectedItemPosition].id
            val shippingCityId = cityList1[citySpinner1.selectedItemPosition].id

            // Create the request body
            val requestBody = UpdateAccountPutModel(
                accountName,
                accountSite,
                accountNumber,
                accountType,
                industry,
                revenue,
                rating,
                phone,
                fax,
                website,
                tickerSymbol,
                ownership,
                employees,
                billingStreet,
                billingCode,
                shippingStreet,
                shippingCode,
                description,
                billingCountryId,
                billingStateId,
                billingCityId,
                shippingCountryId,
                shippingStateId,
                shippingCityId,
                accountId
            )

            // Get JWT token
            val jwtToken = gettoken(requireContext())
            if (jwtToken != null) {
                // Create Retrofit service instance
                val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
                // Make PUT API call
                val call = service.accountupdate("Bearer $jwtToken", requestBody)
                call.enqueue(object : Callback<UpdateAccountResponseModel> {
                    override fun onResponse(
                        call: Call<UpdateAccountResponseModel>,
                        response: Response<UpdateAccountResponseModel>
                    ) {
                        if (isAdded) {
                            if (response.isSuccessful) {
                                // Handle successful response
                                Constants.success(
                                    requireContext(), "Account updated successfully"
                                )
                                findNavController().popBackStack() // Go back to previous fragment
                            } else {
                                Constants.error(
                                    requireContext(), "Unsuccessful response: ${response.code()}"
                                )
                            }
                        }
                    }

                    override fun onFailure(call: Call<UpdateAccountResponseModel>, t: Throwable) {
                        Constants.error(requireContext(), "Error: ${t.message}")
                    }
                })
            } else {
                Constants.error(requireContext(), "Token is null. Please log in again.")
            }
        }
    }

    private fun contactDetailsByAccountId(aid: Int) {
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null) {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.contactDetailsByAccountId("Bearer $jwtToken", aid)
            call.enqueue(object : Callback<List<AccountContactDetailsModelItem>> {
                override fun onResponse(
                    call: Call<List<AccountContactDetailsModelItem>>,
                    response: Response<List<AccountContactDetailsModelItem>>
                ) {
                    if (isAdded) {
                        if (response.isSuccessful) {

                            response.body()?.let { data ->
                                Log.d("data", "data: $data")
                                val adapter = AccountContactDetailsAdapter(requireContext(), data)
                                binding.rvContactDetails.adapter = adapter
                            } ?: Constants.error(requireContext(), "Response body is null")

                        } else {
                            //Constants.error(context,"Response is not successful")
                            showEmptyView(true, binding.emptyView3)
                        }
                    }
                }

                override fun onFailure(
                    call: Call<List<AccountContactDetailsModelItem>>,
                    t: Throwable
                ) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }

            })
        }
    }

    private fun taskDetailsByAccountId(aid: Int) {
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null) {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.taskDetailsByAccountId("Bearer $jwtToken", aid)
            call.enqueue(object : Callback<List<AccountTaskDetailsModelItem>> {
                override fun onResponse(
                    call: Call<List<AccountTaskDetailsModelItem>>,
                    response: Response<List<AccountTaskDetailsModelItem>>
                ) {
                    if (isAdded) {
                        if (response.isSuccessful) {

                            response.body()?.let { data ->
                                Log.d("data", "data: $data")
                                val adapter = AccountTaskDetailsAdapter(requireContext(), data)
                                binding.rvTaskDetails.adapter = adapter
                            } ?: Constants.error(requireContext(), "Response body is null")

                        } else {
                            //Constants.error(context,"Response is not successful")
                            showEmptyView(true, binding.emptyView2)
                        }
                    }
                }

                override fun onFailure(
                    call: Call<List<AccountTaskDetailsModelItem>>,
                    t: Throwable
                ) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }
            })
        }
    }

    private fun leadDetailsByAccountId(aid: Int) {
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null) {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.leadDetailsByAccountId("Bearer $jwtToken", aid)
            call.enqueue(object : Callback<List<LeadDetailsAccountModelItem>> {
                override fun onResponse(
                    call: Call<List<LeadDetailsAccountModelItem>>,
                    response: Response<List<LeadDetailsAccountModelItem>>
                ) {
                    if (isAdded) {
                        if (response.isSuccessful) {

                            response.body()?.let { data ->
                                Log.d("data", "data: $data")
                                val adapter = AccountLeadDetailsAdapter(requireContext(), data)
                                binding.rvLeadDetails.adapter = adapter
                            } ?: Constants.error(requireContext(), "Response body is null")

                        } else {
                            //Constants.error(context,"Response is not successful")
                            showEmptyView(true, binding.emptyView1)
                        }
                    }
                }

                override fun onFailure(
                    call: Call<List<LeadDetailsAccountModelItem>>,
                    t: Throwable
                ) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }

            })
        }
    }

    private fun showEmptyView(show: Boolean, view: View) {
        view.visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun onDestroyView() {
        super.onDestroyView()
        resetUIHandler.removeCallbacksAndMessages(null)
        _binding = null
    }
}
