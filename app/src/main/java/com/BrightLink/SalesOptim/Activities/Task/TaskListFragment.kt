package com.BrightLink.SalesOptim.Activities.Task

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.BrightLink.SalesOptim.Adapters.TaskListAdapter
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.Constants.WrapContentLinearLayoutManager
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.ResponseModel.TaskListModelItem
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.ActivityTaskListBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class TaskListFragment : Fragment() {

    private var _binding: ActivityTaskListBinding? = null
    private lateinit var adapter: TaskListAdapter
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ActivityTaskListBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.actionBar.activityName.text = "Task List"
        binding.actionBar.back.setOnClickListener {
            requireActivity().onBackPressed()
        }
        binding.actionBar.option.visibility = View.INVISIBLE

        binding.taskListRecyclerView.layoutManager = WrapContentLinearLayoutManager(context)
        binding.addTaskBtn.setOnClickListener{
            // Add log statement to check if the click listener is triggered
            Log.d("TaskListFragment", "Add task button clicked")
            findNavController().navigate(R.id.addTask)
        }


        val jwtToken = gettoken(context)
        if (jwtToken != null)
        {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            CallApi(service, jwtToken)
        } else {
            Constants.error(context, "Token is null Login Again")
        }
        return root
    }

    private fun CallApi(apiservice: ApiInterface, jwtToken: String) {
        val call = apiservice.tasklist("Bearer $jwtToken")
        call.enqueue(object : Callback<List<TaskListModelItem>> {
            override fun onResponse(
                call: Call<List<TaskListModelItem>>,
                response: Response<List<TaskListModelItem>>
            ) {
                if (isAdded) {
                    if (response.isSuccessful) {
                        val data = response.body()
                        if (data != null) {
                            val customerMap = HashMap<String, String>()

                            data.forEach { item ->
                                customerMap[item.id.toString()] = item.customerName.toString()
                            }

                            adapter = TaskListAdapter(requireContext(), data, customerMap)
                            adapter.setOnItemClickListener(object : TaskListAdapter.onItemClickListener
                            {
                                override fun onItemClick(position: Int) {
                                    val selectedItem = data[position]
                                    val bundle = Bundle().apply {
                                        putSerializable("selectedItem",selectedItem)
                                    }
                                    findNavController().navigate(R.id.updateTask,bundle)
                                }
                            })
                            // Set adapter to RecyclerView
                            binding.taskListRecyclerView.adapter = adapter


                            binding.taskListRecyclerView.adapter = adapter
                        } else {
                            Constants.error(requireContext(), "Response body is null")
                        }
                    } else {
                        Constants.error(requireContext(), "Unsuccessful response: ${response.code()}")
                    }
                }
            }

            override fun onFailure(call: Call<List<TaskListModelItem>>, t: Throwable) {
                if (isAdded) { // Check if the fragment is added to the activity
                    Constants.error(requireContext(), "Error: ${t.message}")
                }
            }
        })
    }

    private fun gettoken(context: Context?): String? {
        val sharedPreferences = requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
        return sharedPreferences.getString("jwtToken", null)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}

