package com.BrightLink.SalesOptim.Activities.AssignLead.Contact

import android.content.Context
import android.graphics.PorterDuff
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.RequestModel.UpdateLeadInfoPutModel
import com.BrightLink.SalesOptim.ResponseModel.CityModelItem
import com.BrightLink.SalesOptim.ResponseModel.CountryModelItem
import com.BrightLink.SalesOptim.ResponseModel.LeadDetailsModel
import com.BrightLink.SalesOptim.ResponseModel.StateModelItem
import com.BrightLink.SalesOptim.ResponseModel.UpdateLeadInfoResponseModel
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.AssignLeadInfoBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class InfoFragment : Fragment() {

    private var _binding: AssignLeadInfoBinding? = null
    private val binding get() = _binding!!

    private lateinit var leadTypeSpinner: Spinner
    private lateinit var requirementSpinner: Spinner
    private lateinit var leadSourceSpinner: Spinner

    private lateinit var countrySpinner: Spinner
    private lateinit var countryAdapter: ArrayAdapter<String>
    private var countryNames : ArrayList<String> = ArrayList()

    private lateinit var countryList: List<CountryModelItem>
    private lateinit var stateList: List<StateModelItem>
    private lateinit var cityList: List<CityModelItem>

    private lateinit var stateSpinner: Spinner
    private lateinit var stateAdapter: ArrayAdapter<String>
    private var stateNames : ArrayList<String> = ArrayList()

    private lateinit var citySpinner: Spinner
    private lateinit var cityAdapter: ArrayAdapter<String>
    private var cityNames : ArrayList<String> = ArrayList()
    private var leadStatus : String = ""


    private var CountryID : String? = null
    private var StateID : String? = null
    private var CityID : String? = null



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = AssignLeadInfoBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.actionBar.activityName.text = "Assign Lead"
        binding.actionBar.back.setOnClickListener{
            requireActivity().onBackPressed()
        }

        val sharedPreferences = requireContext().getSharedPreferences("LeadPreferences", Context.MODE_PRIVATE)
        val leadId = sharedPreferences.getInt("leadId", -1)
        leadStatus = sharedPreferences.getString("leadStatus",leadStatus).toString()

        if (leadStatus!= null){

            when(leadStatus)
            {
                "New Lead"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)

                }

                "Contacted"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Email Sent"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Not Contacted"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Qualified"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Not Qualified"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Proposal Sent"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Lead Won"->{
                    binding.actionBar.contactIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Lead Lost"->{
                    binding.actionBar.contactIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }
            }
        }

        leadTypeSpinner = binding.leadType
        requirementSpinner = binding.requirement
        leadSourceSpinner = binding.leadSource

        val leadTypeAdapter = ArrayAdapter.createFromResource(requireContext(), R.array.Lead_Type, R.layout.simple_spinner_item_layout)
        leadTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        leadTypeSpinner.adapter = leadTypeAdapter

        val requirementAdapter = ArrayAdapter.createFromResource(requireContext(), R.array.Requirement, R.layout.simple_spinner_item_layout)
        requirementAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        requirementSpinner.adapter = requirementAdapter

        val leadSourceAdapter = ArrayAdapter.createFromResource(requireContext(), R.array.Lead_source, R.layout.simple_spinner_item_layout)
        leadSourceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        leadSourceSpinner.adapter = leadSourceAdapter



        val lid = leadId.toString()

        if (leadId != null) {
            fetchLeadDetailsById(leadId)
        }

        binding.update.setOnClickListener {
            updateLead(leadId)
        }

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?){
        super.onViewCreated(view, savedInstanceState)

        countrySpinner = binding.country
        countryAdapter = ArrayAdapter(requireContext(), R.layout.simple_spinner_item_layout, countryNames).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
        countrySpinner.adapter = countryAdapter

        stateSpinner = binding.state
        stateAdapter = ArrayAdapter(requireContext(), R.layout.simple_spinner_item_layout, stateNames).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
        stateSpinner.adapter = stateAdapter

        citySpinner = binding.city
        cityAdapter = ArrayAdapter(requireContext(), R.layout.simple_spinner_item_layout, cityNames).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
        citySpinner.adapter = cityAdapter

        callCountryApi()
    }

    private fun fetchLeadDetailsById(lid: Int) {
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null){
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val  call = service.getLeadDetailsById("Bearer $jwtToken", lid)
            call.enqueue(object : Callback<LeadDetailsModel> {
                override fun onResponse(
                    call: Call<LeadDetailsModel>,
                    response: Response<LeadDetailsModel>
                ) {
                    if (isAdded) {
                        if (response.isSuccessful) {
                            val data = response.body()
                            if (data != null) {
                                val lead = data

                                binding.leadOwnerName.text = lead.leadCreatedBy
                                binding.customerName.setText(lead.customerName)
                                binding.leadType.setSelection(
                                    getIndex(
                                        binding.leadType,
                                        lead.leadType
                                    )
                                )
                                binding.requirement.setSelection(
                                    getIndex(
                                        binding.requirement,
                                        lead.requirement
                                    )
                                )
                                binding.leadRevenue.setText(lead.leadRevenue)
                                binding.leadSource.setSelection(
                                    getIndex(
                                        binding.leadSource,
                                        lead.leadSource
                                    )
                                )
                                binding.companyName.setText(lead.companyName)
                                binding.website.setText(lead.website)
                                binding.email.setText(lead.email)
                                binding.address.setText(lead.address)
                                binding.followUpdate.text = lead.followUpdate
                                binding.mobileNumber.setText(lead.mobileNumber)
                                binding.alternateMobileNumber.setText(lead.alternateMobileNumber)
//                                binding.country.setSelection(getIndex(binding.country, lead.country))
//                                binding.state.setSelection(getIndex(binding.state, lead.state))
//                                binding.city.setSelection(getIndex(binding.city, lead.city))
                                binding.zip.setText(lead.zip.toString())
                                binding.description.setText(lead.description)

                                lead.country?.let { country ->
                                    val accountIndex = countryNames.indexOf(country)
                                    if (accountIndex != -1) {
                                        binding.country.setSelection(accountIndex)
                                    }
                                }
                                // Set billing state
                                lead.state?.let { state ->
                                    val accountIndex = stateNames.indexOf(state)
                                    if (accountIndex != -1) {
                                        binding.state.setSelection(accountIndex)
                                    }
                                }
                                // Set billing city
                                lead.city?.let { city ->
                                    val accountIndex = cityNames.indexOf(city)
                                    if (accountIndex != -1) {
                                        binding.city.setSelection(accountIndex)
                                    }
                                }




                                Log.d("Response For city","$data")

                            } else {
                                Constants.error(requireContext(), "Lead details not found")
                            }
                        } else {
                            Constants.error(
                                requireContext(),
                                "Unsuccessful response: ${response.code()}"
                            )
                        }
                    }
                }

                override fun onFailure(call: Call<LeadDetailsModel>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }
            })
        }
    }

    private fun callCountryApi() {
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null){
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.getCountry("Bearer $jwtToken")
            call.enqueue(object : Callback<List<CountryModelItem>> {
                override fun onResponse(
                    call: Call<List<CountryModelItem>>,
                    response: Response<List<CountryModelItem>>
                ) {
                    if (isAdded) {
                        if (response.isSuccessful) {
                            val data = response.body()
                            if (data != null) {
                                for (item in data) {
                                    item.name?.let { countryNames.add(it) }
                                }
                                countryAdapter.notifyDataSetChanged()

                                countrySpinner.onItemSelectedListener =
                                    object : AdapterView.OnItemSelectedListener {
                                        override fun onItemSelected(
                                            parent: AdapterView<*>?,
                                            view: View?,
                                            position: Int,
                                            id: Long
                                        ) {
                                            val selectedCountryName = countryNames[position]
                                            val selectedCountry =
                                                data.find { it.name == selectedCountryName }
                                            selectedCountry?.let {
                                                callStateApi(it.id)
                                                CountryID = it.id.toString()
                                            } ?: run {
                                                Constants.warning(
                                                    requireContext(),
                                                    "Country ID not found"
                                                )
                                            }
                                        }

                                        override fun onNothingSelected(p0: AdapterView<*>?) {
                                            Constants.warning(requireContext(), "Select Country")
                                        }

                                    }

                            } else {
                                Constants.error(requireContext(), "Response body is null")
                            }
                        } else {
                            Constants.error(
                                requireContext(),
                                "Unsuccessful response: ${response.code()}"
                            )
                        }
                    }
                }

                override fun onFailure(call: Call<List<CountryModelItem>>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }

            })
        }
    }

    private fun callStateApi(id: Int) {
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null){
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.getState("Bearer $jwtToken", id)
            call.enqueue(object : Callback<List<StateModelItem>> {
                override fun onResponse(
                    call: Call<List<StateModelItem>>,
                    response: Response<List<StateModelItem>>
                ) {
                    if (isAdded) {
                        if (response.isSuccessful) {
                            val data = response.body()
                            if (data != null) {
                                stateNames.clear()
                                for (item in data) {
                                    item.name?.let { stateNames.add(it) }
                                }
                                stateAdapter.notifyDataSetChanged()

                                stateSpinner.onItemSelectedListener =
                                    object : AdapterView.OnItemSelectedListener {
                                        override fun onItemSelected(
                                            parent: AdapterView<*>?,
                                            view: View?,
                                            position: Int,
                                            id: Long
                                        ) {
                                            val selectedStateName = stateNames[position]
                                            val selectedState =
                                                data.find { it.name == selectedStateName }
                                            selectedState?.let {
                                                callCityApi(it.id)
                                                StateID = it.id.toString()
                                            } ?: run {
                                                Constants.warning(
                                                    requireContext(),
                                                    "State ID not found"
                                                )
                                            }
                                        }

                                        override fun onNothingSelected(p0: AdapterView<*>?) {
                                            Constants.warning(requireContext(), "Select State")
                                        }

                                    }
                            } else {
                                Constants.error(requireContext(), "Response body is null")
                            }
                        } else {
                            Constants.error(
                                requireContext(),
                                "Unsuccessful response: ${response.code()}"
                            )
                        }
                    }
                }

                override fun onFailure(call: Call<List<StateModelItem>>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }

            })
        }
    }

    private fun callCityApi(id: Int) {
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null){
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.getCity("Bearer $jwtToken", id)
            call.enqueue(object : Callback<List<CityModelItem>> {
                override fun onResponse(
                    call: Call<List<CityModelItem>>,
                    response: Response<List<CityModelItem>>
                ) {
                    if (isAdded) {
                        if (response.isSuccessful) {
                            val data = response.body()
                            if (data != null) {
                                cityNames.clear()
                                for (item in data) {
                                    item.name?.let { cityNames.add(it) }
                                }
                                cityAdapter.notifyDataSetChanged()

                                citySpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                                    override fun onItemSelected(
                                        parent: AdapterView<*>?,
                                        view: View?,
                                        position: Int,
                                        id: Long
                                    ) {
                                        val selectedCityName = cityNames[position]
                                        val selectedCity = data.find { it.name == selectedCityName }
                                        selectedCity?.let {
                                            CityID = it.id.toString()
                                        } ?: run {
                                            Constants.warning(requireContext(), "City ID not found")
                                        }
                                    }

                                    override fun onNothingSelected(parent: AdapterView<*>?) {
                                        Constants.warning(requireContext(), "Select the city")
                                    }
                                }

                            } else {
                                Constants.error(requireContext(), "Response body is null")
                            }
                        } else {
                            Constants.error(
                                requireContext(),
                                "Unsuccessful response: ${response.code()}"
                            )
                        }
                    }
                }

                override fun onFailure(call: Call<List<CityModelItem>>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }

            })
        }
    }

    private fun updateLead(leadID: Int) {
        val leadCreatedBy = binding.leadOwnerName.text.toString()
        val customerName = binding.customerName.text.toString()
        val leadType = binding.leadType.selectedItem.toString()
        val requirement = binding.requirement.selectedItem.toString()
        val leadRevenue = binding.leadRevenue.text.toString()
        val leadSource = binding.leadSource.selectedItem.toString()
        val leadStatus = leadStatus
        val companyName = binding.companyName.text.toString()
        val website = binding.website.text.toString()
        val email = binding.email.text.toString()
        val address = binding.address.text.toString()
        val followUpdate = binding.followUpdate.text.toString()
        val mobileNumber = binding.mobileNumber.text.toString()
        val alternateMobileNumber = binding.alternateMobileNumber.text.toString()
        val country = binding.country.selectedItem.toString()
        val state = binding.state.selectedItem.toString()
        val city = binding.city.selectedItem.toString()
//        val country = countryList[countrySpinner.selectedItemPosition].id.toString()
//        val state = stateList[stateSpinner.selectedItemPosition].id.toString()
//        val city = cityList[citySpinner.selectedItemPosition].id.toString()
        val zip = binding.zip.text.toString()
        val description = binding.description.text.toString()



        if (CountryID == null) {
            Constants.error(requireContext(), "Country is Null.")
            return
        }
        if (StateID == null) {
            Constants.error(requireContext(), "State is Null.")
            return
        }

        if (CityID == null) {
            Constants.error(requireContext(), "City is Null.")
            return
        }

        val requestBody = UpdateLeadInfoPutModel(address, alternateMobileNumber, city, companyName,
            country, customerName, description, email, followUpdate, leadCreatedBy, leadID, leadRevenue,
            leadSource, leadStatus, leadType, mobileNumber, requirement, state, website, zip)

        Log.d("Update Lead","$requestBody")

        val jwtToken = gettoken(requireContext())
        if (jwtToken != null) {
            // Create Retrofit service instance
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            // Make PUT API call
            val call = service.leadInfoUpdate("Bearer $jwtToken", requestBody)
            call.enqueue(object : Callback<UpdateLeadInfoResponseModel> {
                override fun onResponse(
                    call: Call<UpdateLeadInfoResponseModel>,
                    response: Response<UpdateLeadInfoResponseModel>
                ) {
                    if (isAdded) {
                        if (response.isSuccessful) {
                            // Handle successful response
                            Constants.success(
                                requireContext(),
                                "Account updated successfully"
                            )
                            findNavController().popBackStack() // Go back to previous fragment
                        } else {
                            Constants.error(
                                requireContext(),
                                "Unsuccessful response: ${response.code()}"
                            )
                        }
                    }
                }

                override fun onFailure(
                    call: Call<UpdateLeadInfoResponseModel>,
                    t: Throwable
                ) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }
            })


        } else {
            Constants.error(requireContext(), "Token is null. Please log in again.")
        }
    }

    private fun gettoken(context: Context?): String? {
        val sharedPreferences = requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
        return sharedPreferences.getString("jwtToken", null)
    }

    private fun getIndex(spinner: Spinner, myString: String): Int {
        for (i in 0 until spinner.count) {
            if (spinner.getItemAtPosition(i).toString() == myString) {
                return i
            }
        }
        return 0
    }

}
