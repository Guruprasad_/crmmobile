package com.BrightLink.SalesOptim.Activities.AssignLead.Popups

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.ColorMatrix
import android.graphics.ColorMatrixColorFilter
import android.graphics.Paint
import android.os.Bundle
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.BrightLink.SalesOptim.Adapters.GetProductAdapter
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.Constants.SharedPreferecneManager
import com.BrightLink.SalesOptim.Constants.WrapContentLinearLayoutManager
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.databinding.ProposalInvoiceBinding
import com.itextpdf.text.Document
import com.itextpdf.text.Image
import com.itextpdf.text.PageSize
import com.itextpdf.text.pdf.PdfWriter
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.UUID

class ProposalInvoice: Fragment() {

    fun Context.isPermissionGranted(permission: String): Boolean {
        return ContextCompat.checkSelfPermission(
            this,
            permission
        ) == android.content.pm.PackageManager.PERMISSION_GRANTED
    }

    private lateinit var binding : ProposalInvoiceBinding
    private val OWNER_PREF = "OwnerPref"
    private val CUSTOMER_PREF = "CustomerPrefs"
    private val PRODUCT_PREF = "ProductPrefs"
    private val PRODUCT_DETAILS = "product_details"
    private val T_AND_C_PREF = "TandCPrefs"
    private lateinit var adapter : GetProductAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = ProposalInvoiceBinding.inflate(layoutInflater)

        binding.recyclerView.layoutManager = WrapContentLinearLayoutManager(context)

        val OwnerDetails = requireContext().getSharedPreferences(OWNER_PREF, Context.MODE_PRIVATE)
        getCompanyDetails(OwnerDetails)

        val CustomerDetails = requireContext().getSharedPreferences(CUSTOMER_PREF, Context.MODE_PRIVATE)
        getCustomerDetails(CustomerDetails)

        val product =  requireContext().getSharedPreferences(PRODUCT_PREF, Context.MODE_PRIVATE)
        val product_details =  requireContext().getSharedPreferences(PRODUCT_DETAILS, Context.MODE_PRIVATE)
        getProducts(product , product_details)

        val terms_and_conditions = requireContext().getSharedPreferences(T_AND_C_PREF, Context.MODE_PRIVATE)
        getTermsAndConditions(terms_and_conditions)

        binding.download.setOnClickListener {

            layoutToImage(binding.root)
        }


        return binding.root
    }

    fun layoutToImage(view: View) {
        // Get the view group using reference
        val relativeLayout = view.findViewById<View>(R.id.print) as LinearLayout
        // Convert view group to bitmap
        relativeLayout.isDrawingCacheEnabled = true
        relativeLayout.buildDrawingCache()
        val drawingCache = relativeLayout.drawingCache
        if (drawingCache != null) {
            val bitmap = Bitmap.createBitmap(drawingCache)
            val brightnessValue = 20 // Adjust brightness value as needed
            val contrastValue = 1.2f // Adjust contrast value as needed
            val adjustedBitmap = applyBrightnessAndContrast(bitmap, brightnessValue, contrastValue)

            // Save adjusted bitmap as image file
            val file = File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + File.separator + "${UUID.randomUUID().toString()}.jpg"
            )
            try {
                val fos = FileOutputStream(file)
                adjustedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos)
                fos.flush()
                fos.close()
                // Convert the adjusted image to PDF
                imageToPDF(file)
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        else
        {
            Constants.error(context,"Failed to create bitmap from drawing cache")
        }
    }

    private fun applyBrightnessAndContrast(bitmap: Bitmap, brightnessValue: Int, contrastValue: Float): Bitmap {
        val cm = ColorMatrix()
        cm.set(floatArrayOf(
            contrastValue, 0f, 0f, 0f, brightnessValue.toFloat(),
            0f, contrastValue, 0f, 0f, brightnessValue.toFloat(),
            0f, 0f, contrastValue, 0f, brightnessValue.toFloat(),
            0f, 0f, 0f, 1f, 0f
        ))
        val paint = Paint()
        paint.colorFilter = ColorMatrixColorFilter(cm)

        val adjustedBitmap = Bitmap.createBitmap(bitmap.width, bitmap.height, bitmap.config)
        val canvas = Canvas(adjustedBitmap)
        val paint2 = Paint()
        paint2.color = Color.WHITE
        canvas.drawBitmap(bitmap, 0f, 0f, paint2)
        canvas.drawBitmap(adjustedBitmap, 0f, 0f, paint)
        return adjustedBitmap
    }

    fun imageToPDF(imageFile: File) {
        try {
            val document = Document()
            val dirPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).toString() + File.separator + "CRM/PDF"
            val directory = File(dirPath)
            directory.mkdirs()
            val pdfFile = File(directory, "${UUID.randomUUID()}.pdf")
            PdfWriter.getInstance(
                document,
                FileOutputStream(pdfFile)
            )
            document.open()

            val img = Image.getInstance(imageFile.absolutePath)
            img.scaleToFit(PageSize.A4.width, PageSize.A4.height)
            // img.scaleAbsolute(document.pageSize.width - document.leftMargin() - document.rightMargin(), document.pageSize.height - document.topMargin() - document.bottomMargin())
            img.alignment = Image.ALIGN_CENTER
            document.add(img)
            document.close()
            Constants.success(context, "PDF generated successfully!")
        } catch (e: Exception) {
            e.printStackTrace()
            Constants.error(context, "Failed to generate PDF")
        }
    }

    private fun getTermsAndConditions(termsAndConditions: SharedPreferences?) {

        val deliveryPeriod = termsAndConditions!!.getString("deliveryPeriod","")
        val advancePayment = termsAndConditions.getString("advancePayment","")
        val T_and_C = termsAndConditions.getString("termsandConditions","")
        val note = termsAndConditions.getString("note","")
        val senderName = termsAndConditions.getString("senderName","")
        val senderNumber = termsAndConditions.getString("senderNumber","")

        binding.deliveryCharges.text = deliveryPeriod
        binding.advancePayment.text = advancePayment
        binding.termsandConditions.text = T_and_C
        binding.notes.text = note
        binding.sendername.text = senderName
        binding.senderphone.text = senderNumber
    }

    private fun getProducts(product: SharedPreferences? , product_details : SharedPreferences?) {

        val product =  SharedPreferecneManager.getItems(requireContext())
        binding.recyclerView.layoutManager = WrapContentLinearLayoutManager(context)
        adapter = GetProductAdapter(requireContext() , product)
        binding.recyclerView.adapter = adapter

        val tax = product_details!!.getString("tax", "")
        val shippingCharges = product_details.getString("shippingCharges","" )

        binding.tax.text = tax
        binding.applicableTax.text = tax
        binding.shippingCharges.text = shippingCharges
        binding.shipppingCharges.text = shippingCharges
    }

    private fun getCustomerDetails(customerDetails: SharedPreferences?) {
        val companyName = customerDetails!!.getString("companyName", "")
        val customerName = customerDetails.getString("customerName","" )
        val customerAddress = customerDetails.getString("address", "")
        val customerPhone = customerDetails.getString("phone", "")
        val subject = customerDetails.getString("subject", "")
        val message = customerDetails.getString("message", "")

        binding.customerName.text = customerName
        binding.customerAddress.text = customerAddress
        binding.customerCompany.text = companyName
        binding.customerPhone.text = customerPhone
        binding.subject.text = subject
        binding.message.text = message
    }
    private fun getCompanyDetails(ownerDetails: SharedPreferences?) {

        val CompanyName = ownerDetails!!.getString("companyName","");
        val address = ownerDetails.getString("address", "")
        val phone =  ownerDetails.getString("phone", "")
        val email =   ownerDetails.getString("email", "")
        val date =   ownerDetails.getString("date", "")
        val quotationId=  ownerDetails.getString("quotationId", "")
        val backgroundclr = ownerDetails.getString("background","")
        val headerbckground = ownerDetails.getString("header","")
        val text = ownerDetails.getString("text","")

        binding.companyName.text = CompanyName
        binding.companyaddress.text = address
        binding.companyphone.text =phone
        binding.companyemail.text = email
        binding.date.text = date
        binding.quatationId.text = quotationId

        if (text != null && text.isNotEmpty()) {
            binding.dateText.setTextColor(android.graphics.Color.parseColor(text))
            binding.date.setTextColor(android.graphics.Color.parseColor(text))
            binding.quatationId.setTextColor(android.graphics.Color.parseColor(text))
            binding.quotationIdText.setTextColor(android.graphics.Color.parseColor(text))
            binding.to.setTextColor(android.graphics.Color.parseColor(text))
            binding.customerName.setTextColor(android.graphics.Color.parseColor(text))
            binding.customerCompany.setTextColor(android.graphics.Color.parseColor(text))
            binding.customerAddress.setTextColor(android.graphics.Color.parseColor(text))
            binding.customerPhone.setTextColor(android.graphics.Color.parseColor(text))
            binding.sub.setTextColor(android.graphics.Color.parseColor(text))
            binding.subject.setTextColor(android.graphics.Color.parseColor(text))
            binding.sirMadam.setTextColor(android.graphics.Color.parseColor(text))
            binding.message.setTextColor(android.graphics.Color.parseColor(text))
            binding.ref.setTextColor(android.graphics.Color.parseColor(text))
            binding.termsandConditions.setTextColor(android.graphics.Color.parseColor(text))
            binding.applicableTaxexText.setTextColor(android.graphics.Color.parseColor(text))
            binding.applicableTax.setTextColor(android.graphics.Color.parseColor(text))
            binding.shippingCharges.setTextColor(android.graphics.Color.parseColor(text))
            binding.shippingCharges1Text.setTextColor(android.graphics.Color.parseColor(text))
            binding.deliveryCharges.setTextColor(android.graphics.Color.parseColor(text))
            binding.delieveryPeriodText.setTextColor(android.graphics.Color.parseColor(text))
           // binding.weekText.setTextColor(android.graphics.Color.parseColor(text))
            binding.advancePayment.setTextColor(android.graphics.Color.parseColor(text))
            binding.advancePaymentText.setTextColor(android.graphics.Color.parseColor(text))
            binding.inAdvanceText.setTextColor(android.graphics.Color.parseColor(text))
            binding.notes.setTextColor(android.graphics.Color.parseColor(text))
            binding.notesText.setTextColor(android.graphics.Color.parseColor(text))
            binding.sendername.setTextColor(android.graphics.Color.parseColor(text))
            binding.senderphone.setTextColor(android.graphics.Color.parseColor(text))
        }

        //Header
        if (headerbckground != null && headerbckground.isNotEmpty()) {
            binding.companyName.setTextColor(android.graphics.Color.parseColor(headerbckground))
            binding.companyaddress.setTextColor(android.graphics.Color.parseColor(headerbckground))
            binding.companyphone.setTextColor(android.graphics.Color.parseColor(headerbckground))
            binding.companyemail.setTextColor(android.graphics.Color.parseColor(headerbckground))
            binding.srno.setTextColor(android.graphics.Color.parseColor(headerbckground))
            binding.productDescription.setTextColor(android.graphics.Color.parseColor(headerbckground))
            binding.qty.setTextColor(android.graphics.Color.parseColor(headerbckground))
            binding.unitPrice.setTextColor(android.graphics.Color.parseColor(headerbckground))
            binding.totalPrice.setTextColor(android.graphics.Color.parseColor(headerbckground))
            binding.subTotaltext.setTextColor(android.graphics.Color.parseColor(headerbckground))
            binding.subtotal.setTextColor(android.graphics.Color.parseColor(headerbckground))
            binding.tax.setTextColor(android.graphics.Color.parseColor(headerbckground))
            binding.taxText.setTextColor(android.graphics.Color.parseColor(headerbckground))
            binding.shippingChargesText.setTextColor(android.graphics.Color.parseColor(headerbckground))
            binding.shipppingCharges.setTextColor(android.graphics.Color.parseColor(headerbckground))
            binding.grandTotalText.setTextColor(android.graphics.Color.parseColor(headerbckground))
            binding.grandTotal.setTextColor(android.graphics.Color.parseColor(headerbckground))
        }

        //Background
        if (backgroundclr != null && backgroundclr.isNotEmpty()) {
            binding.background1.setBackgroundColor(android.graphics.Color.parseColor(backgroundclr))
            binding.background2.setBackgroundColor(android.graphics.Color.parseColor(backgroundclr))
            binding.background3.setBackgroundColor(android.graphics.Color.parseColor(backgroundclr))
        }


    }

}