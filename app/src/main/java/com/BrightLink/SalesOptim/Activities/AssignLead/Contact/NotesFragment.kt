package com.BrightLink.SalesOptim.Activities.AssignLead.Contact

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.OpenableColumns
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.MimeTypeMap
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.BrightLink.SalesOptim.Activities.Contact.UpdateContact
import com.BrightLink.SalesOptim.Adapters.ALNotesAdapter
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.Constants.WrapContentLinearLayoutManager
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.RequestModel.AddNoteRequest
import com.BrightLink.SalesOptim.ResponseModel.AddNoteModel
import com.BrightLink.SalesOptim.ResponseModel.LeadNotesModelItem
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.NotesAssignLeadBinding
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class NotesFragment : Fragment() {

    private var _binding: NotesAssignLeadBinding? = null
    private val binding get() = _binding!!

    private val PICKFILE_RESULT_CODE = 1
    private var filePath: String? = null
    private var dialog: Dialog? = null
    private  var path : String= ""
    private lateinit var Desc : String
    private var leadStatus : String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = NotesAssignLeadBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val sharedPreferences = requireContext().getSharedPreferences("LeadPreferences", Context.MODE_PRIVATE)
        val leadId = sharedPreferences.getInt("leadId", -1)
        leadStatus = sharedPreferences.getString("leadStatus",leadStatus)

        if (leadStatus!= null){

            when(leadStatus)
            {
                "New Lead"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)

                }

                "Contacted"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Email Sent"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Not Contacted"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Qualified"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Not Qualified"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Proposal Sent"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Lead Won"->{
                    binding.actionBar.contactIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Lead Lost"->{
                    binding.actionBar.contactIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }
            }
        }


        binding.actionBar.activityName.text = "Assign Lead"
        binding.actionBar.back.setOnClickListener{
            requireActivity().onBackPressed()
        }

        binding.recyclerView.layoutManager = WrapContentLinearLayoutManager(requireContext())

        refreshNotesList(leadId)

        binding.leadNoteBtn.setOnClickListener {
            showDialog(R.layout.account_add_notes)
            val fileChooser = dialog!!.findViewById<Button>(R.id.noteFileOneChoose)
            fileChooser.setOnClickListener {
                showFileChooser()
            }
            val noteDescription = dialog!!.findViewById<EditText>(R.id.fileNoteDesEditText)

            val addNote = dialog!!.findViewById<Button>(R.id.addNote)

            addNote.setOnClickListener {
                Desc = noteDescription.text.toString()
                addNotes(leadId)

            }



        }

        return root
    }

    private fun refreshNotesList(leadId: Int) {
        getToken()?.let { jwtToken ->
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            callApi(service, jwtToken, leadId)
        } ?: Constants.error(requireContext(), "Token is null. Please log in again.")

    }

    private fun showDialog(layoutResId: Int) {

        if (dialog == null) {
            dialog = Dialog(requireActivity())
            dialog!!.setCancelable(false)
            dialog!!.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        dialog!!.setContentView(layoutResId)
        dialog!!.show()

        val closeBtn = dialog!!.findViewById<ImageButton>(R.id.closeBtn)
        closeBtn.setOnClickListener {
            dialog!!.dismiss()
        }

    }

    private fun showFileChooser() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
            intent.type = "*/*"
            startActivityForResult(intent, PICKFILE_RESULT_CODE)
        } else {
            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.type = "*/*"
            intent.addCategory(Intent.CATEGORY_OPENABLE)
            startActivityForResult(
                Intent.createChooser(intent, "Select File"),
                PICKFILE_RESULT_CODE
            )
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == PICKFILE_RESULT_CODE && resultCode == Activity.RESULT_OK && data != null && data.data != null) {
            val uri: Uri = data.data!!
            path = getPathFromUri(requireContext(),uri)!!
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                updateTextViewWithFileName(uri)
            } else {
                // Handle for lower versions
            }
        }
    }
    @SuppressLint("SetTextI18n")
    private fun updateTextViewWithFileName(uri: Uri) {
        val fileName = getFileNameFromUri(uri)
        filePath = uri.toString()
        if (fileName!= null){
            val selectedFile = dialog!!.findViewById<TextView>(R.id.selectedFile)
            selectedFile.text = Html.fromHtml("<b><font color='#7FFF00'>$fileName</font></b>")
        }
    }

    private fun getFileNameFromUri(uri: Uri): String? {
        var fileName: String? = null
        val cursor =requireContext().contentResolver.query(uri, null, null, null, null)
        if (cursor!= null){
            if (cursor.moveToFirst()) {
                val displayNameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
                if (displayNameIndex != -1) {
                    fileName = cursor.getString(displayNameIndex)
                }
            }
            cursor.close()
        }
        return fileName
    }
    @SuppressLint("Range")
    private fun getPathFromUri(context: Context, uri: Uri): String? {
        var path: String? = null
        if ("content".equals(uri.scheme, ignoreCase = true)) {
            // Using ACTION_OPEN_DOCUMENT
            val cursor = context.contentResolver.query(uri, null, null, null, null)
            cursor?.use {
                if (it.moveToFirst()) {
                    val displayName = it.getString(it.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                    val type = it.getString(it.getColumnIndex("mime_type"))
                    val file = File(context.cacheDir, displayName)
                    try {
                        val inputStream = context.contentResolver.openInputStream(uri)
                        inputStream?.use { input ->
                            FileOutputStream(file).use { output ->
                                input.copyTo(output)
                            }
                        }
                        path = file.absolutePath
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
            }
        } else if ("file".equals(uri.scheme, ignoreCase = true)) {
            path = uri.path
        }
        return path
    }

    private fun addNotes(leadId: Int) {
        val note = AddNoteRequest(0, 0, leadId, Desc,0)

        if (path != null && path.isNotEmpty()){
        val file = File(path)
        val content: ByteArray = file.readBytes()
        val fileExtension = MimeTypeMap.getFileExtensionFromUrl(file.absolutePath)
        val mediaType = UpdateContact.FileTypeHelper.getMediaTypeFromFileExtension(fileExtension)

        if (mediaType != null){
            val requestBody = RequestBody.create(mediaType, content)
            val jwtToken = getToken()
            if (jwtToken != null) {
                val service = ApiUtilities.getinstance().create(ApiInterface::class.java)

                val body = MultipartBody.Part.createFormData("file", file.name, requestBody)
                val call = service.addnote("Bearer $jwtToken", body,note)
                call.enqueue(object : Callback<AddNoteModel> {
                    override fun onResponse(
                        call: Call<AddNoteModel>,
                        response: Response<AddNoteModel>
                    ) {
                        if (isAdded) {
                            if (response.isSuccessful) {
                                val responseBody = response.body()
                                dialog!!.dismiss()
                                Constants.success(requireContext(), "Note Added Successfully")
                                refreshNotesList(leadId)
                            } else {
                                Constants.error(requireContext(), "Unsuccessful")
                            }
                        }
                    }
                    override fun onFailure(call: Call<AddNoteModel>, t: Throwable) {
                        Constants.error(requireContext(), "Error: ${t.message}")

                    }
                })
            } else {
                Constants.error(requireContext(), "Token Not Found")
            }
        } else {
            Constants.error(requireContext(), "Unsupported file type")
        }
        }else{
            Constants.error(requireContext(), "File not Selected, Please Select a file")
        }
    }

    private fun callApi(service: ApiInterface, jwtToken: String, leadId: Int) {
        val call = service.getNoteListLead("Bearer $jwtToken", leadId)
        call.enqueue(object : Callback<List<LeadNotesModelItem>> {
            override fun onResponse(
                call: Call<List<LeadNotesModelItem>>,
                response: Response<List<LeadNotesModelItem>>
            ) {
                if (isAdded) {
                    if (response.isSuccessful) {
                        val resp = response.body()
                        response.body()?.let { data ->
                            if (resp.isNullOrEmpty()){
                                binding.noNotesFound.visibility = View.VISIBLE
                            }else {
                                binding.noNotesFound.visibility = View.INVISIBLE
                            }
                            val adapter = ALNotesAdapter(requireContext(), data)
                            binding.recyclerView.adapter = adapter
                        } ?: Constants.error(requireContext(), "Response body is null")
                    }
                }
            }
            override fun onFailure(call: Call<List<LeadNotesModelItem>>, t: Throwable) {
                Constants.error(requireContext(), "Error: ${t.message}")
            }

        })
    }

    private fun getToken(): String? {
        return requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
            .getString("jwtToken", null)
    }
}