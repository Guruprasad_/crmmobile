package com.BrightLink.SalesOptim.Activities.ZoomSDK

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.databinding.ActivityZoomBinding
import us.zoom.sdk.JoinMeetingOptions
import us.zoom.sdk.JoinMeetingParams
import us.zoom.sdk.MeetingService
import us.zoom.sdk.ZoomError
import us.zoom.sdk.ZoomSDK
import us.zoom.sdk.ZoomSDKInitParams
import us.zoom.sdk.ZoomSDKInitializeListener

class ZoomActivity : AppCompatActivity() {

    private lateinit var binding : ActivityZoomBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        binding = ActivityZoomBinding.inflate(layoutInflater)
        setContentView(binding.root)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }


        val sdk: ZoomSDK = ZoomSDK.getInstance()
        val params: ZoomSDKInitParams = ZoomSDKInitParams().apply {
            jwtToken = "eyJhbGciOiJIUzI1NiJ9.eyJ0b2tlbkV4cCI6MTcxOTkyNzU5NywiYXBwS2V5IjoiOVQxNGVWZU9UcFJUQjd6Zzh2cXV3IiwiZXhwIjoxNzE5ODQ0Nzk3LCJpYXQiOjE3MTk4NDExOTd9.DSIc98vkWildvoRtA6qtMxNW0aw3UFVfPTI8UaQ3mmE"
            domain = "zoom.us"
            enableLog = true
        }

        val listener: ZoomSDKInitializeListener = object : ZoomSDKInitializeListener {
            override fun onZoomSDKInitializeResult(errorCode: Int, internalErrorCode: Int) {
                if (errorCode != ZoomError.ZOOM_ERROR_SUCCESS) {
                    // Initialization failed
                    Log.e(
                        "ZoomSDK",
                        "Failed to initialize Zoom SDK. Error: $errorCode, internalError: $internalErrorCode"
                    )
                } else {
                    // Initialization successful
                    Log.i("ZoomSDK", "Zoom SDK initialized successfully")
                }
            }

            override fun onZoomAuthIdentityExpired() {
                // Handle Zoom authentication identity expired
                Log.i("ZoomSDK", "Zoom authentication identity expired")
            }
        }

        sdk.initialize(this@ZoomActivity, listener, params)

        binding.submit.setOnClickListener{
            val name = binding.name.text.toString()
            val meetingNo = binding.meetingNo.text.toString()
            val meetingPassword = binding.meetingPassword.text.toString()

            if (meetingNo.trim().isNotEmpty() && meetingPassword.trim().isNotEmpty() && name.trim().isNotEmpty())
            {
                JoinMeeting(name,meetingNo,meetingPassword)
            }
            else
            {
                Constants.warning(this@ZoomActivity,"Please enter all fields")
            }
        }



    }

    private fun JoinMeeting(name: String, meetingNo: String, meetingPassword: String) {

        val meetingServie : MeetingService = ZoomSDK.getInstance().meetingService
        val options : JoinMeetingOptions = JoinMeetingOptions()
        val params : JoinMeetingParams = JoinMeetingParams()
        params.displayName = name
        params.meetingNo = meetingNo
        params.password = meetingPassword
        meetingServie.joinMeetingWithParams(this@ZoomActivity,params,options)

    }
}