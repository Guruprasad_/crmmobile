package com.BrightLink.SalesOptim.Activities.ui.Profile


import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Parcel
import android.provider.OpenableColumns
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.ResponseModel.ProfileResponseModel
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.ProfileBinding
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.MaterialDatePicker
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import android.util.Base64
import android.util.Log
import android.webkit.MimeTypeMap
import com.BrightLink.SalesOptim.Activities.Contact.UpdateContact
import com.BrightLink.SalesOptim.Constants.CustomDialog
import com.BrightLink.SalesOptim.RequestModel.ProfileUpdateRequest
import com.BrightLink.SalesOptim.RequestModel.UpdatePassReqModel
import com.BrightLink.SalesOptim.ResponseModel.PasswordResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.Date
import java.util.Locale


class ProfileFragment : Fragment() {
    private var _binding: ProfileBinding? = null
    private val binding get() = _binding!!
    private lateinit var materialDatePicker: MaterialDatePicker<Long>
    private lateinit var apiService: ApiInterface
    private val PICKFILE_RESULT_CODE = 1
    private var path : String = ""

    companion object {
        private const val REQUEST_CODE_PICK_IMAGE = 100
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ProfileBinding.inflate(inflater, container, false)
        val root: View = binding.root

        apiService = ApiUtilities.getinstance().create(ApiInterface::class.java)
        val userId = getUserId()
        Log.d("omar","$userId")
        if (userId != 0) {
            getProfileInformation(apiService, userId)
            binding.save.setOnClickListener {
                setProfileInformation(apiService, userId)
            }

            binding.reset.setOnClickListener {
                binding.empNameView.text = null
                binding.empDesignationView.text = null
                binding.profileName.text = null
                binding.DOB.text = null
                binding.profileEmail.text = null
                binding.profileGender.text = null
                binding.profilePhone.text = null
                binding.profileAddress.text = null
                binding.profileRole.text = null
                binding.profileJobTitle.text = null
                binding.profileDeparment.text = null
                binding.profileEmploymentStatus.text = null
            }

            binding.changePass.setOnClickListener {
                changePassword(apiService, userId)

            }
        } else {
            Constants.error(requireContext(), "User-Id is null")
        }

        val builder = MaterialDatePicker.Builder.datePicker()
            .setTitleText("Select Date of Birth")
            .setSelection(MaterialDatePicker.todayInUtcMilliseconds())

        val constraintsBuilder = CalendarConstraints.Builder()
        val dateValidator = object : CalendarConstraints.DateValidator {
            override fun describeContents(): Int {
                TODO("Not yet implemented")
            }

            override fun writeToParcel(dest: Parcel, flags: Int) {
                TODO("Not yet implemented")
            }

            override fun isValid(date: Long): Boolean {
                return date <= MaterialDatePicker.todayInUtcMilliseconds()
            }
        }
        constraintsBuilder.setValidator(dateValidator)
        builder.setCalendarConstraints(constraintsBuilder.build())

        materialDatePicker = builder.build()

        // Handle date selection
        materialDatePicker.addOnPositiveButtonClickListener { selection ->
            val selectedDate = Date(selection)
            val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            binding.DOB.setText(dateFormat.format(selectedDate))
        }


        binding.DOB.setOnClickListener {
            materialDatePicker.show(
                requireActivity().supportFragmentManager,
                "MATERIAL_DATE_PICKER"
            )
        }


        binding.profilePic.setOnClickListener {
            showFileChooser()
        }

        return root
    }

    private fun showFileChooser() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
            intent.type = "image/*"
            startActivityForResult(intent, PICKFILE_RESULT_CODE)
        } else {
            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.type = "image/*"
            intent.addCategory(Intent.CATEGORY_OPENABLE)
            startActivityForResult(
                Intent.createChooser(intent, "Select File"),
                PICKFILE_RESULT_CODE
            )
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == PICKFILE_RESULT_CODE && resultCode == Activity.RESULT_OK && data != null && data.data != null) {
            val uri: Uri = data.data!!
            path = getPathFromUri(requireContext(),uri)!!
            val inputStream = requireActivity().contentResolver.openInputStream(uri!!)
            val bitmap = BitmapFactory.decodeStream(inputStream)
            binding.profilePic.setImageBitmap(bitmap)
        }
    }

    @SuppressLint("Range")
    private fun getPathFromUri(context: Context, uri: Uri): String? {
        var path: String? = null
        if ("content".equals(uri.scheme, ignoreCase = true)) {
            // Using ACTION_OPEN_DOCUMENT
            val cursor = context.contentResolver.query(uri, null, null, null, null)
            cursor?.use {
                if (it.moveToFirst()) {
                    val displayName = it.getString(it.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                    val type = it.getString(it.getColumnIndex("mime_type"))
                    val file = File(context.cacheDir, displayName)
                    try {
                        val inputStream = context.contentResolver.openInputStream(uri)
                        inputStream?.use { input ->
                            FileOutputStream(file).use { output ->
                                input.copyTo(output)
                            }
                        }
                        path = file.absolutePath
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
            }
        } else if ("file".equals(uri.scheme, ignoreCase = true)) {
            path = uri.path
        }
        return path
    }

    private fun validatePassword(oldPassword: String, newpass: String): Boolean {
        if (oldPassword.isEmpty() || newpass.isEmpty()) {
            Constants.error(context, "Please enter the password")
            return false
        }

        return true
    }

    private fun changePassword(apiService: ApiInterface, userId: Int) {
        val loading = CustomDialog(requireContext())
        loading.dismiss()
        val oldPassword = binding.profileOldPassword.text.toString()
        val newpass = binding.profileNewPassword.text.toString()

        if (validatePassword(oldPassword, newpass)) {
            val model = UpdatePassReqModel(newpass, oldPassword)

            val jwtToken = gettoken(requireContext())
            if (jwtToken != null) {
                val call = apiService.updatePassword("Bearer $jwtToken", model)
                call.enqueue(object : Callback<PasswordResponse> {
                    override fun onResponse(
                        call: Call<PasswordResponse>,
                        response: Response<PasswordResponse>
                    ) {
                        if (response.isSuccessful) {
                            val data = response.body()
                            if (data != null) {
                                loading.dismiss()
                                Constants.success(context, data.success)
                            } else {
                                Constants.error(requireContext(), response.message().toString())
                                loading.dismiss()
                            }
                        } else {
                            Constants.error(
                                requireContext(),
                                "Old password does not match ${response.code()}"
                            )
                            loading.dismiss()
                        }
                    }

                    override fun onFailure(call: Call<PasswordResponse>, t: Throwable) {
                        Constants.error(requireContext(), "Error: ${t.message}")
                        loading.dismiss()
                    }
                })

            }
        }

    }

    private fun setProfileInformation(apiService: ApiInterface, userId: Int) {

        val name = binding.profileName.text.toString()
        val dateofbirth = binding.DOB.text.toString()
        val gender = binding.profileGender.text.toString()
        val email = binding.profileEmail.text.toString()
        val phone = binding.profilePhone.text.toString()
        val role = binding.profileRole.text.toString()
        val address = binding.profileAddress.text.toString()
        val jobtitle = binding.profileJobTitle.text.toString()
        val department = binding.profileDeparment.text.toString()
        val employeeStatus = binding.profileEmploymentStatus.text.toString()

        if (validateInput(
                name,
                dateofbirth,
                gender,
                email,
                phone,
                role,
                address,
                jobtitle,
                department,
                employeeStatus)) {
            updateProfileInfo(apiService, userId, name, dateofbirth, gender, email, phone, role, address, jobtitle, department, employeeStatus)
            }
    }

    private fun updateProfileInfo(
        apiService: ApiInterface,
        userId: Int,
        name: String,
        dateofbirth: String,
        gender: String,
        email: String,
        phone: String,
        role: String,
        address: String,
        jobtitle: String,
        department: String,
        employeeStatus: String
    ) {

        val loadingProgressBar = CustomDialog(requireContext())
        loadingProgressBar.show()

        val model = ProfileUpdateRequest(dateofbirth,department,employeeStatus,gender,userId,jobtitle,name,address,phone)
        if (path != null && path.isNotEmpty()){
            val file = File(path)
            val content: ByteArray = file.readBytes()
            val fileExtension = MimeTypeMap.getFileExtensionFromUrl(file.absolutePath)
            val mediatype = UpdateContact.FileTypeHelper.getMediaTypeFromFileExtension(fileExtension)

            if (mediatype != null){
                val requestBody = RequestBody.create(mediatype,content)
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null) {
            val body = MultipartBody.Part.createFormData("file", file.name, requestBody)
            val call = apiService.updateProfile("Bearer $jwtToken" ,body, model)
            call.enqueue(object : Callback<ProfileResponseModel> {
                override fun onResponse(
                    call: Call<ProfileResponseModel>,
                    response: Response<ProfileResponseModel>
                ) {
                    if (response.isSuccessful) {
                        val data = response.body()
                        if (data != null) {
                            loadingProgressBar.dismiss()
                            Constants.success(context, "Profile Updated Successfully")
                        } else {
                            Constants.error(requireContext(), "Employee data is null")
                            loadingProgressBar.dismiss()
                        }
                    } else {
                        Constants.error(requireContext(), "Unsuccessful response: ${response.code()}")
                        loadingProgressBar.dismiss()
                    }
                }

                override fun onFailure(call: Call<ProfileResponseModel>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                    loadingProgressBar.dismiss()
                }
            })
        } else {
            Constants.error(requireContext(), "Token Not Found")
            loadingProgressBar.dismiss()
        }
            } else {
                Constants.error(requireContext(), "Unsupported file type")
                loadingProgressBar.dismiss()
            }
        }else{
            Constants.error(requireContext(), "File not Selected, Please Select a file")
            loadingProgressBar.dismiss()
        }
    }

        private fun validateInput(
            name: String,
            dateofbirth: String,
            gender: String,
            email: String,
            phone: String,
            role: String,
            address: String,
            jobtitle: String,
            department: String,
            employeeStatus: String
        ): Boolean {

            if (name.isEmpty() || dateofbirth.isEmpty() || gender.isEmpty() || email.isEmpty() || phone.isEmpty() || role.isEmpty() || address.isEmpty() || jobtitle.isEmpty() ||
                department.isEmpty() || employeeStatus.isEmpty()
            ) {
                Constants.error(context, "Please fill all the details properly")
                return false
            }

            if (phone.length != 10) {
                Constants.error(context, "Please enter the valid phone Number")
                return false
            }

            if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                Constants.error(context, "Please enter the valid email address")
                return false
            }


            return true
        }


        private fun getProfileInformation(apiService: ApiInterface, userId: Int?) {
            val loadingProgressBar = CustomDialog(requireContext())
            loadingProgressBar.show()

            val jwtToken = gettoken(requireContext())
            if (jwtToken != null) {
                val call = apiService.getEmployeeProfile("Bearer $jwtToken", userId!!)
                call.enqueue(object : Callback<ProfileResponseModel> {
                    override fun onResponse(
                        call: Call<ProfileResponseModel>,
                        response: Response<ProfileResponseModel>
                    ) {
                        if (response.isSuccessful) {
                            val data = response.body()
                            if (data != null) {
                                loadingProgressBar.dismiss()
                                binding.empNameView.text = data.name
                                binding.empDesignationView.text = data.role
                                binding.profileName.setText(data.name)
                                binding.DOB.text = formatDate(data.dateOfBirh)
                                binding.profileEmail.setText(data.email)
                                binding.profileGender.setText(data.gender)
                                binding.profilePhone.setText(data.phone)
                                binding.profileAddress.setText(data.permanantAddr)
                                binding.profileRole.setText(data.role)
                                binding.profileJobTitle.setText(data.jobTitle)
                                binding.profileDeparment.setText(data.dept)
                                binding.profileEmploymentStatus.setText(data.empStatus)
                                val bitmap: Bitmap? = decodeBase64ToBitmap(data.images)
                                Glide.with(requireContext())
                                    .asBitmap()
                                    .load(bitmap)
                                    .transition(BitmapTransitionOptions.withCrossFade())
                                    .into(binding.profilePic)
                            } else {
                                loadingProgressBar.dismiss()
                                Constants.error(requireContext(), "Employee data is null")
                            }
                        } else {
                            Constants.error(
                                requireContext(),
                                "Unsuccessful response: ${response.code()}"
                            )
                            loadingProgressBar.dismiss()
                        }
                    }

                    override fun onFailure(call: Call<ProfileResponseModel>, t: Throwable) {
                        Constants.error(requireContext(), "Error: ${t.message}")
                        loadingProgressBar.dismiss()
                    }
                })
            }
        }
    private fun formatDate(date: String?): String? {
        if (date.isNullOrEmpty()) return null // Return empty string if date is null or empty

        val inputFormat = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
        val outputFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()) // Change the output format here
        val parsedDate = inputFormat.parse(date.toString())
        return outputFormat.format(parsedDate ?: Date())
    }


        private fun getUserId(): Int {
            val sharedPreferences =
                requireContext().getSharedPreferences("User", Context.MODE_PRIVATE)
            return sharedPreferences.getInt("id", 0)
        }

        private fun gettoken(context: Context?): String? {
            val sharedPreferences =
                requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
            return sharedPreferences.getString("jwtToken", null)
        }

        override fun onDestroyView() {
            super.onDestroyView()
            _binding = null
        }

    private fun decodeBase64ToBitmap(base64String: String?): Bitmap? {
        if (base64String != null) {
            val decodedBytes: ByteArray = Base64.decode(base64String, Base64.DEFAULT)
            return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.size)
        } else {
            // Handle the case where base64String is null
            return null
        }
    }

    suspend fun encodeBitmapToBase64Async(bitmap: Bitmap): String = withContext(Dispatchers.Default) {
        return@withContext encodeBitmapToBase64(bitmap)
    }

    fun encodeBitmapToBase64(bitmap: Bitmap): String {
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
        val byteArray = byteArrayOutputStream.toByteArray()
        return android.util.Base64.encodeToString(byteArray, android.util.Base64.DEFAULT)
    }
    }
