package com.BrightLink.SalesOptim.Activities.quotation

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager2.widget.ViewPager2
import com.BrightLink.SalesOptim.databinding.FragmentQuotationBinding
import com.google.android.material.tabs.TabLayout
import com.BrightLink.SalesOptim.Adapters.QuotationAdapter


class QuotationFragment : Fragment() {

    private var _binding: FragmentQuotationBinding? = null
    private lateinit var adapter : QuotationAdapter
    private val binding get() = _binding!!
    private lateinit var tabLayout: TabLayout
    private lateinit var viewPager2: ViewPager2

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentQuotationBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.actionBar.activityName.text = "Quotation"
        binding.actionBar.back.setOnClickListener{
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }

        binding.actionBar.option.visibility = View.INVISIBLE

        tabLayout = binding.tabs
        viewPager2 = binding.viewPager

        adapter = QuotationAdapter(requireActivity().supportFragmentManager, lifecycle)

        binding.tabs.addTab(tabLayout.newTab().setText("Own"))
        binding.tabs.addTab(tabLayout.newTab().setText("Customer"))
        binding.tabs.addTab(tabLayout.newTab().setText("Product"))
        binding.tabs.addTab(tabLayout.newTab().setText("T & C"))

        binding.viewPager.adapter = adapter

        tabLayout.addOnTabSelectedListener(object: TabLayout.OnTabSelectedListener{
            override fun onTabSelected(tab: TabLayout.Tab?) {
                if (tab != null) {
                    viewPager2.currentItem = tab.position
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {}

            override fun onTabReselected(tab: TabLayout.Tab?) {}

        })

        viewPager2.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback(){
            override fun onPageSelected(position: Int){
                super.onPageSelected(position)
                tabLayout.selectTab(tabLayout.getTabAt(position))
            }
        })

        return root

    }

    override fun onDestroy() {
        super.onDestroy()
        val prefs = requireActivity().getSharedPreferences("ProductPrefs", Context.MODE_PRIVATE)
        val editor = prefs.edit()
        editor.clear()
        editor.apply()

        val Detailprefs = requireActivity().getSharedPreferences("product_details", Context.MODE_PRIVATE)
        val Detaileditor = Detailprefs.edit()
        Detaileditor.clear()
        Detaileditor.apply()

        val removeCustomer = requireActivity().getSharedPreferences("CustomerPrefs", Context.MODE_PRIVATE)
        val removeCus = removeCustomer.edit()
        removeCus.clear()
        removeCus.apply()

        val removeOwner = requireActivity().getSharedPreferences("OwnerPref", Context.MODE_PRIVATE)
        val own = removeOwner.edit()
        own.clear()
        own.apply()

        val t_and_c = requireActivity().getSharedPreferences("TandCPrefs", Context.MODE_PRIVATE)
        val TC = t_and_c.edit()
        TC.clear()
        TC.apply()
    }
}