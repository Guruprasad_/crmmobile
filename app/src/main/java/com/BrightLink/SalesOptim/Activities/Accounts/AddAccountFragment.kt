package com.BrightLink.SalesOptim.Activities.Accounts

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.RequestModel.AddAccountPostModel
import com.BrightLink.SalesOptim.ResponseModel.AddAccountResponseModel
import com.BrightLink.SalesOptim.ResponseModel.CityModelItem
import com.BrightLink.SalesOptim.ResponseModel.CountryModelItem
import com.BrightLink.SalesOptim.ResponseModel.ShowEmployeelistModelItem
import com.BrightLink.SalesOptim.ResponseModel.StateModelItem
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.ActivityAddAccountBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddAccountFragment : Fragment() {

    private var _binding: ActivityAddAccountBinding? = null
    private val binding get() = _binding!!
    private lateinit var accountAdapter: ArrayAdapter<String>
    private lateinit var accountSpinner: Spinner
    private var accountNames: ArrayList<String> = ArrayList()



    private lateinit var countrySpinner: Spinner
    private lateinit var countryAdapter: ArrayAdapter<String>
    private var countryNames: ArrayList<String> = ArrayList()

    private lateinit var stateSpinner: Spinner
    private lateinit var stateAdapter: ArrayAdapter<String>
    private var stateNames: ArrayList<String> = ArrayList()

    private lateinit var citySpinner: Spinner
    private lateinit var cityAdapter: ArrayAdapter<String>
    private var cityNames: ArrayList<String> = ArrayList()

    private lateinit var countryList: List<CountryModelItem>
    private lateinit var stateList: List<StateModelItem>
    private lateinit var cityList: List<CityModelItem>


    private lateinit var countrySpinner1: Spinner
    private lateinit var countryAdapter1: ArrayAdapter<String>
    private var countryNames1: ArrayList<String> = ArrayList()

    private lateinit var stateSpinner1: Spinner
    private lateinit var stateAdapter1: ArrayAdapter<String>
    private var stateNames1: ArrayList<String> = ArrayList()

    private lateinit var citySpinner1: Spinner
    private lateinit var cityAdapter1: ArrayAdapter<String>
    private var cityNames1: ArrayList<String> = ArrayList()

    private lateinit var countryList1: List<CountryModelItem>
    private lateinit var stateList1: List<StateModelItem>
    private lateinit var cityList1: List<CityModelItem>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ActivityAddAccountBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.actionBar.activityName.text = "Add Account"
        binding.actionBar.back.setOnClickListener {
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }
        binding.actionBar.option.visibility = View.INVISIBLE

        binding.addToAccount.setOnClickListener {
            // Call the method to perform the POST API call
            performAddAccount()
        }
        binding.resetToAccount.setOnClickListener {
            // Call the method to perform the POST API call
            resetFields()
        }

        countrySpinner = binding.accountBillingCountry
        countryAdapter = ArrayAdapter(
            requireContext(),
            R.layout.spinner_item_layout,
            countryNames
        ).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
        countrySpinner.adapter = countryAdapter

        stateSpinner = binding.accountBillingState
        stateAdapter = ArrayAdapter(
            requireContext(),
            R.layout.spinner_item_layout,
            stateNames
        ).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
        stateSpinner.adapter = stateAdapter

        citySpinner = binding.accountBillingCity
        cityAdapter = ArrayAdapter(
            requireContext(),
            R.layout.spinner_item_layout,
            cityNames
        ).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
        citySpinner.adapter = cityAdapter





        countrySpinner1 = binding.accountShippingCountry
        countryAdapter1 = ArrayAdapter(
            requireContext(),
            R.layout.spinner_item_layout,
            countryNames1
        ).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
        countrySpinner1.adapter = countryAdapter1

        stateSpinner1 = binding.accountShippingState
        stateAdapter1 = ArrayAdapter(
            requireContext(),
            R.layout.spinner_item_layout,
            stateNames1
        ).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
        stateSpinner1.adapter = stateAdapter1

        citySpinner1 = binding.accountShippingCity
        cityAdapter1 = ArrayAdapter(
            requireContext(),
            R.layout.spinner_item_layout,
            cityNames1
        ).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
        citySpinner1.adapter = cityAdapter1







        callCountryApi()
        callCountryApi1()

        fetchEmployeeList()
        return root
    }

    private fun callCountryApi() {
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null) {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.getCountry("Bearer $jwtToken")
            call.enqueue(object : Callback<List<CountryModelItem>> {
                override fun onResponse(
                    call: Call<List<CountryModelItem>>,
                    response: Response<List<CountryModelItem>>
                ) {
                    if (response.isSuccessful) {
                        val data = response.body()
                        if (data != null) {
                            countryList = data
                            for (item in data) {
                                item.name.let { countryNames.add(it) }
                            }
                            countryAdapter.notifyDataSetChanged()

                            countrySpinner.onItemSelectedListener =
                                object : AdapterView.OnItemSelectedListener {
                                    override fun onItemSelected(
                                        parent: AdapterView<*>?,
                                        view: View?,
                                        position: Int,
                                        id: Long
                                    ) {
                                        val selectedCountryName = countryNames[position]
                                        val selectedCountry =
                                            data.find { it.name == selectedCountryName }
                                        selectedCountry?.let {
                                            callStateApi(it.id)
                                        } ?: run {
                                            Constants.warning(
                                                requireContext(),
                                                "Country ID not found"
                                            )
                                        }
                                    }

                                    override fun onNothingSelected(p0: AdapterView<*>?) {
                                        Constants.warning(requireContext(), "Select Country")
                                    }

                                }

                        } else {
                            Constants.error(requireContext(), "Response body is null")
                        }
                    } else {
                        Constants.error(
                            requireContext(),
                            "Unsuccessful response: ${response.code()}"
                        )
                    }
                }

                override fun onFailure(call: Call<List<CountryModelItem>>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }

            })
        }
    }

    private fun callStateApi(id: Int) {
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null) {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.getState("Bearer $jwtToken", id)
            call.enqueue(object : Callback<List<StateModelItem>> {
                override fun onResponse(
                    call: Call<List<StateModelItem>>,
                    response: Response<List<StateModelItem>>
                ) {
                    if (response.isSuccessful) {
                        val data = response.body()
                        if (data != null) {
                            stateList = data
                            stateNames.clear()
                            for (item in data) {
                                item.name.let { stateNames.add(it) }
                            }
                            stateAdapter.notifyDataSetChanged()

                            stateSpinner.onItemSelectedListener =
                                object : AdapterView.OnItemSelectedListener {
                                    override fun onItemSelected(
                                        parent: AdapterView<*>?,
                                        view: View?,
                                        position: Int,
                                        id: Long
                                    ) {
                                        val selectedStateName = stateNames[position]
                                        val selectedState =
                                            data.find { it.name == selectedStateName }
                                        selectedState?.let {
                                            callCityApi(it.id)
                                        } ?: run {
                                            Constants.warning(
                                                requireContext(),
                                                "State ID not found"
                                            )
                                        }
                                    }

                                    override fun onNothingSelected(p0: AdapterView<*>?) {
                                        Constants.warning(requireContext(), "Select State")
                                    }

                                }
                        } else {
                            Constants.error(requireContext(), "Response body is null")
                        }
                    } else {
                        Constants.error(
                            requireContext(),
                            "Unsuccessful response: ${response.code()}"
                        )
                    }
                }

                override fun onFailure(call: Call<List<StateModelItem>>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }

            })
        }
    }

    private fun callCityApi(id: Int) {
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null) {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.getCity("Bearer $jwtToken", id)
            call.enqueue(object
                : Callback<List<CityModelItem>> {
                override fun onResponse(
                    call: Call<List<CityModelItem>>,
                    response: Response<List<CityModelItem>>
                ) {
                    if (response.isSuccessful) {
                        val data = response.body()
                        if (data != null) {
                            cityList = data
                            cityNames.clear()
                            for (item in data) {
                                item.name.let { cityNames.add(it) }
                            }
                            cityAdapter.notifyDataSetChanged()
                        } else {
                            Constants.error(requireContext(), "Response body is null")
                        }
                    } else {
                        Constants.error(
                            requireContext(),
                            "Unsuccessful response: ${response.code()}"
                        )
                    }
                }

                override fun onFailure(call: Call<List<CityModelItem>>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }

            })
        }
    }


    private fun callCountryApi1() {
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null) {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.getCountry("Bearer $jwtToken")
            call.enqueue(object : Callback<List<CountryModelItem>> {
                override fun onResponse(
                    call: Call<List<CountryModelItem>>,
                    response: Response<List<CountryModelItem>>
                ) {
                    if (response.isSuccessful) {
                        val data = response.body()
                        if (data != null) {
                            countryList1 = data
                            for (item in data) {
                                item.name.let { countryNames1.add(it) }
                            }
                            countryAdapter1.notifyDataSetChanged()

                            countrySpinner1.onItemSelectedListener =
                                object : AdapterView.OnItemSelectedListener {
                                    override fun onItemSelected(
                                        parent: AdapterView<*>?,
                                        view: View?,
                                        position: Int,
                                        id: Long
                                    ) {
                                        val selectedCountryName = countryNames1[position]
                                        val selectedCountry =
                                            data.find { it.name == selectedCountryName }
                                        selectedCountry?.let {
                                            callStateApi1(it.id)
                                        } ?: run {
                                            Constants.warning(
                                                requireContext(),
                                                "Country ID not found"
                                            )
                                        }
                                    }

                                    override fun onNothingSelected(p0: AdapterView<*>?) {
                                        Constants.warning(requireContext(), "Select Country")
                                    }

                                }

                        } else {
                            Constants.error(requireContext(), "Response body is null")
                        }
                    } else {
                        Constants.error(
                            requireContext(),
                            "Unsuccessful response: ${response.code()}"
                        )
                    }
                }

                override fun onFailure(call: Call<List<CountryModelItem>>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }

            })
        }
    }

    private fun callStateApi1(id: Int) {
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null) {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.getState("Bearer $jwtToken", id)
            call.enqueue(object : Callback<List<StateModelItem>> {
                override fun onResponse(
                    call: Call<List<StateModelItem>>,
                    response: Response<List<StateModelItem>>
                ) {
                    if (response.isSuccessful) {
                        val data = response.body()
                        if (data != null) {
                            stateList1 = data
                            stateNames1.clear()
                            for (item in data) {
                                item.name.let { stateNames1.add(it) }
                            }
                            stateAdapter1.notifyDataSetChanged()

                            stateSpinner1.onItemSelectedListener =
                                object : AdapterView.OnItemSelectedListener {
                                    override fun onItemSelected(
                                        parent: AdapterView<*>?,
                                        view: View?,
                                        position: Int,
                                        id: Long
                                    ) {
                                        val selectedStateName = stateNames1[position]
                                        val selectedState =
                                            data.find { it.name == selectedStateName }
                                        selectedState?.let {
                                            callCityApi1(it.id)
                                        } ?: run {
                                            Constants.warning(
                                                requireContext(),
                                                "State ID not found"
                                            )
                                        }
                                    }

                                    override fun onNothingSelected(p0: AdapterView<*>?) {
                                        Constants.warning(requireContext(), "Select State")
                                    }

                                }
                        } else {
                            Constants.error(requireContext(), "Response body is null")
                        }
                    } else {
                        Constants.error(
                            requireContext(),
                            "Unsuccessful response: ${response.code()}"
                        )
                    }
                }

                override fun onFailure(call: Call<List<StateModelItem>>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }

            })
        }
    }

    private fun callCityApi1(id: Int) {
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null) {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.getCity("Bearer $jwtToken", id)
            call.enqueue(object
                : Callback<List<CityModelItem>> {
                override fun onResponse(
                    call: Call<List<CityModelItem>>,
                    response: Response<List<CityModelItem>>
                ) {
                    if (response.isSuccessful) {
                        val data = response.body()
                        if (data != null) {
                            cityList1 = data
                            cityNames1.clear()
                            for (item in data) {
                                item.name.let { cityNames1.add(it) }
                            }
                            cityAdapter1.notifyDataSetChanged()
                        } else {
                            Constants.error(requireContext(), "Response body is null")
                        }
                    } else {
                        Constants.error(
                            requireContext(),
                            "Unsuccessful response: ${response.code()}"
                        )
                    }
                }

                override fun onFailure(call: Call<List<CityModelItem>>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }

            })
        }
    }










    private fun resetFields() {
        listOf(
            binding.accountName, binding.accountSite,
            binding.accountNumber, binding.accountPhone,
            binding.accountWebsite ,binding.accountEmployee, binding.accountBillingStreet, binding.accountBillingCode,
            binding.accountShippingStreet, binding.accountShippingCode, binding.accountDescription,

            ).forEach { it.text = null }

        listOf(
            binding.accountTypeSpinner, binding.accountIndustrySpinner, binding.accountRating,
            binding.accountOwnershipSpinner, binding.accountBillingCountry, binding.accountBillingState, binding.accountBillingCity,
            binding.accountShippingCountry, binding.accountShippingState, binding.accountShippingCity
        ).forEach { it.setSelection(0) }

        // Clear any errors that might be shown
        listOf(
            binding.accountName, binding.accountPhone
            // Add other fields with error handling here
        ).forEach { it.error = null }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // Set up spinner with custom adapter
        setupSpinner(binding.accountOwnershipSpinner, R.array.Ownership)
        setupSpinner(binding.accountTypeSpinner, R.array.Account_Type)
        setupSpinner(binding.accountIndustrySpinner, R.array.Industry)
        setupSpinner(binding.accountRating, R.array.Rating)
    }

    private fun setupSpinner(spinner: Spinner, arrayResourceId: Int) {
        val adapter = ArrayAdapter.createFromResource(
            requireContext(),
            arrayResourceId,
            R.layout.spinner_item_layout_currency
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = adapter
    }

    private fun fetchEmployeeList() {
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null) {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val callEmployeelist = service.employeelist("Bearer $jwtToken")
            callEmployeelist.enqueue(object : Callback<ArrayList<ShowEmployeelistModelItem>> {
                override fun onResponse(
                    call: Call<ArrayList<ShowEmployeelistModelItem>>,
                    response: Response<ArrayList<ShowEmployeelistModelItem>>
                ) {
                    if (response.isSuccessful) {
                        val employeelist = response.body()
                        if (employeelist != null && employeelist.isNotEmpty()) {
                            // For simplicity, let's just take the first employee from the list
                            val employee = employeelist[0]
                            // Set the employee name to the TextView
                            binding.accountOwnerName.text = employee.name
                        } else {
                            Constants.error(requireContext(), "Employee list is null or empty")
                        }
                    } else {
                        Constants.error(
                            requireContext(),
                            "Unsuccessful response: ${response.code()}"
                        )
                    }
                }

                override fun onFailure(
                    call: Call<ArrayList<ShowEmployeelistModelItem>>,
                    t: Throwable
                ) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }
            })
        } else {
            Constants.error(requireContext(), "Token is null. Please log in again.")
        }
    }

    private fun performAddAccount() {
        val accountOwner = binding.accountOwnerName.text.toString()
        val accountName = binding.accountName.text.toString()
        val customerName = binding.CustomerName.text.toString()
        val accountSite = binding.accountSite.text.toString()
        val mobileNo = binding.mobileNo.text.toString()
        val phone = binding.accountPhone.text.toString()
        val gstin = binding.gstinNo.text.toString()
        val email = binding.email.text.toString()
        val accountNumber = binding.accountNumber.text.toString()
        val accountType = binding.accountTypeSpinner.selectedItem.toString()
        val rating = binding.accountRating.selectedItem.toString()
        val industry = binding.accountIndustrySpinner.selectedItem.toString()
        val revenue =
            if (binding.accountAnnualRevenue.text.isNotBlank()) {
                binding.accountAnnualRevenue.text.toString()
            } else {
                "0"
            }
        val employees = binding.accountEmployee.text.toString()
        val ownership = binding.accountOwnershipSpinner.selectedItem.toString()
        val website = binding.accountWebsite.text.toString()
        val billingStreet = binding.accountBillingStreet.text.toString()
        val billingCode = binding.accountBillingCode.text.toString()
        val shippingStreet = binding.accountShippingStreet.text.toString()
        val shippingCode = binding.accountShippingCode.text.toString()
        val description = binding.accountDescription.text.toString()
        val billingCountryId = countryList[countrySpinner.selectedItemPosition].id
        val billingStateId = stateList[stateSpinner.selectedItemPosition].id
        val billingCityId = cityList[citySpinner.selectedItemPosition].id
        val shippingCountryId = countryList1[countrySpinner1.selectedItemPosition].id
        val shippingStateId = stateList1[stateSpinner1.selectedItemPosition].id
        val shippingCityId = cityList1[citySpinner1.selectedItemPosition].id

        if (InputValidation(accountName,customerName,accountSite,mobileNo,gstin,email,accountNumber,accountType,industry,revenue,rating,phone,website,ownership,employees,billingStreet,billingCode,shippingStreet,
            shippingCode,description,billingCountryId,billingStateId,shippingCityId,shippingCountryId,shippingStateId,shippingCityId)){

            val requestBody = AddAccountPostModel(accountName,accountNumber,accountOwner,accountSite,accountType,email,billingCityId.toString(),billingCode, billingCountryId.toString(),billingStateId.toString()
            ,billingStreet,customerName,description,employees,gstin,industry,mobileNo,ownership,phone,rating,revenue,shippingCityId.toString(),shippingCode,shippingCountryId.toString(),shippingStateId.toString()
            ,shippingStreet,website)

            val jwtToken = gettoken(requireContext())
            if (jwtToken != null) {
                val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
                val call = service.addaccount("Bearer $jwtToken", requestBody)
                call.enqueue(object : Callback<AddAccountResponseModel> {
                    override fun onResponse(
                        call: Call<AddAccountResponseModel>,
                        response: Response<AddAccountResponseModel>
                    ) {
                        if (response.isSuccessful) {
                            Constants.success(
                                requireContext(),
                                "Created Successfully"
                            )
                            // Handle successful response
                            findNavController().popBackStack() // Go back to previous fragment
                        } else {
                            Constants.error(
                                requireContext(),
                                "Unsuccessful response: ${response.code()}"
                            )
                        }
                    }

                    override fun onFailure(call: Call<AddAccountResponseModel>, t: Throwable) {
                        Constants.error(requireContext(), "Error: ${t.message}")
                    }
                })
            } else {
                Constants.error(requireContext(), "Token is null. Please log in again.")
            }

        }
    }

    private fun InputValidation(
        accountName: String,
        customerName: String,
        accountSite: String,
        mobileNo: String,
        gstin: String,
        email: String,
        accountNumber: String,
        accountType: String,
        industry: String,
        revenue: String,
        rating: String,
        phone: String,
        website: String,
        ownership: String,
        employees: String,
        billingStreet: String,
        billingCode: String,
        shippingStreet: String,
        shippingCode: String,
        description: String,
        billingCountryId: Int,
        billingStateId: Int,
        shippingCityId: Int,
        shippingCountryId: Int,
        shippingStateId: Int,
        shippingCityId1: Int
    ): Boolean {

        if (accountName.length >255) {
        Constants.error(requireContext(),"Please enter the account name under 255 characters")
        return false
    }

    if (!accountName.matches(Regex("[a-zA-Z]+"))) {
        Constants.error(requireContext(), "Please enter only alphabetic characters for the account name")
        return false
    }

        if (customerName.length >255) {
            Constants.error(requireContext(),"Please enter the Customer name under 255 characters")
            return false
        }

        if (!customerName.matches(Regex("[a-zA-Z]+"))) {
            Constants.error(requireContext(), "Please enter only alphabetic characters for the Customer name")
            return false
        }

        if (accountSite.length >255) {
            Constants.error(requireContext(),"Please enter the Account Site under 255 characters")
            return false
        }

        if (phone.length >15) {
            Constants.error(requireContext(),"Please enter the Valid Phone Number")
            return false
        }

        if (mobileNo.length >15) {
            Constants.error(requireContext(),"Please enter the Valid Mobile Number")
            return false
        }

        if (gstin.length >15) {
            Constants.error(requireContext(),"Please enter the Valid GSTIN No")
            return false
        }

        if (accountNumber.length >100) {
            Constants.error(requireContext(),"Please enter the Valid Account Number")
            return false
        }

        if (revenue.length >10) {
            Constants.error(requireContext(),"Please enter the Valid Annual Revenue")
            return false
        }

        if (employees.length >10) {
            Constants.error(requireContext(),"Please enter the Valid Employee Number")
            return false
        }

        if (website.length >255) {
            Constants.error(requireContext(),"Please enter the Website Url under 255 characters")
            return false
        }

        if (billingStreet.length >255) {
            Constants.error(requireContext(),"Please enter the Billing Street under 255 characters")
            return false
        }

        if (billingCode.length >15) {
            Constants.error(requireContext(),"Please enter the Billing Code under 15 characters")
            return false
        }

        if (shippingStreet.length >255) {
            Constants.error(requireContext(),"Please enter the Shipping Street under 255 characters")
            return false
        }


        if (shippingCode.length >15) {
            Constants.error(requireContext(),"Please enter the Shipping Code under 15 characters")
            return false
        }


        if (description.length >255) {
            Constants.error(requireContext(),"Please enter the Description under 255 characters")
            return false
        }

        return true

    }


    private fun gettoken(context: Context?): String? {
        val sharedPreferences = requireContext().getSharedPreferences(
            "Token",
            Context.MODE_PRIVATE
        )
        return sharedPreferences.getString("jwtToken", null)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }





}