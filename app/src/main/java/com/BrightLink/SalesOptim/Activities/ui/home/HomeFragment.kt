package com.BrightLink.SalesOptim.Activities.ui.home

import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.Location.LocationService
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.databinding.FragmentHomeBinding

@Suppress("DEPRECATION")
class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root


        binding.leadDasboard.setOnClickListener{
            findNavController().navigate(R.id.leadDashboard)
        }
        binding.taskDashboard.setOnClickListener{
            findNavController().navigate(R.id.taskDashboard)
        }
        binding.salesDashboard.setOnClickListener{
            findNavController().navigate(R.id.salesDashboard)
        }
        binding.piplineDashboard.setOnClickListener{
            findNavController().navigate(R.id.pipelineDashboard)
        }

        binding.showlead.setOnClickListener {
            findNavController().navigate(R.id.showleads)
        }

        binding.meeting.setOnClickListener {
            findNavController().navigate(R.id.MeetingList)
        }

        binding.showtasks.setOnClickListener {
            findNavController().navigate(R.id.taskList)
        }

        binding.showaccount.setOnClickListener {
            findNavController().navigate(R.id.accountList)
        }

        binding.showcontacts.setOnClickListener {
            findNavController().navigate(R.id.contactList)
        }

        binding.showVendor.setOnClickListener {
            findNavController().navigate(R.id.vendorList)
        }

        binding.quotation.setOnClickListener{
            findNavController().navigate(R.id.quotation)
        }

        binding.purchaseOrder.setOnClickListener{
            findNavController().navigate(R.id.purchaseorderlist)
        }

        binding.SalesOrderList.setOnClickListener {
            findNavController().navigate(R.id.salesorderlist)
        }

        binding.deals.setOnClickListener {
            findNavController().navigate(R.id.lead_deals_frg)
        }

        binding.marketingHome.setOnClickListener {
            findNavController().navigate(R.id.marketing)
        }

        binding.templateHome.setOnClickListener {
            findNavController().navigate(R.id.template)
        }

        binding.products.setOnClickListener {
            findNavController().navigate(R.id.showProduct)
        }

        binding.map.setOnClickListener {
            val intent = Intent(requireContext(), LocationService::class.java)
            if (isLocationServiceRunning()) {
                // Service is running, stop it
                intent.action = LocationService.ACTION_STOP
                requireContext().startService(intent)
                binding.map.setBackgroundResource(R.drawable.locationoff)
                Constants.success(context, "Gps Tracking Stopped")
            } else {
                // Service is not running, start it
                intent.action = LocationService.ACTION_START
                requireContext().startService(intent)
                binding.map.setBackgroundResource(R.drawable.locationon)
                Constants.success(context, "Gps Tracking Started")
            }
        }



        return root
    }
    private fun isLocationServiceRunning(): Boolean {
        val manager = requireContext().getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
            if (LocationService::class.java.name == service.service.className) {
                return true
            }
        }
        return false
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}