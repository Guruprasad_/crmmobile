package com.BrightLink.SalesOptim.Activities.Leads

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Spinner
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.RequestModel.AddLeadPostModel
import com.BrightLink.SalesOptim.RequestModel.LeadAddAccountPostModel
import com.BrightLink.SalesOptim.ResponseModel.AccountDetailsByIdModel
import com.BrightLink.SalesOptim.ResponseModel.AddLeadModel
import com.BrightLink.SalesOptim.ResponseModel.CityModelItem
import com.BrightLink.SalesOptim.ResponseModel.CountryModelItem
import com.BrightLink.SalesOptim.ResponseModel.LeadAddAccountResponseModel
import com.BrightLink.SalesOptim.ResponseModel.ShowAccountsModelItem
import com.BrightLink.SalesOptim.ResponseModel.ShowEmployeelistModelItem
import com.BrightLink.SalesOptim.ResponseModel.StateModelItem
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.AddLeadBinding
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.DateValidatorPointForward
import com.google.android.material.datepicker.MaterialDatePicker
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale

class AddLeadFragment : Fragment() {

    private var _binding:AddLeadBinding ? = null
    private val binding get() = _binding!!

    private lateinit var leadTypeSpinner: Spinner
    private lateinit var requirementSpinner: Spinner
    private lateinit var leadSourceSpinner: Spinner

    private lateinit var companyNameSpinner: Spinner
    private lateinit var companyNameAdapter: ArrayAdapter<String>
    private var companyNames: ArrayList<String> = ArrayList()

    private val companyNameToAccountIdMap: MutableMap<String, String> = mutableMapOf()

    private lateinit var countrySpinner: Spinner
    private lateinit var countryAdapter: ArrayAdapter<String>
    private var countryNames : ArrayList<String> = ArrayList()

    private lateinit var stateSpinner: Spinner
    private lateinit var stateAdapter: ArrayAdapter<String>
    private var stateNames : ArrayList<String> = ArrayList()

    private lateinit var citySpinner: Spinner
    private lateinit var cityAdapter: ArrayAdapter<String>
    private var cityNames : ArrayList<String> = ArrayList()

    private var CountryID : String? = null
    private var StateID : String? = null
    private var CityID : String? = null

    private var selectedCityName : String? = null
    private var selectedCountryName : String? = null
    private var selectedStateName : String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = AddLeadBinding.inflate(inflater, container, false)
        val root: View = binding.root


        binding.actionBar.activityName.text = "Add Lead"
        binding.actionBar.back.setOnClickListener{
            requireActivity().onBackPressed()
        }
        binding.actionBar.option.visibility = View.INVISIBLE

        binding.date.setOnClickListener {
            showMaterialCalendar()
        }

        leadTypeSpinner = binding.leadTypeSpinner
        requirementSpinner = binding.requirementSpinner
        leadSourceSpinner = binding.leadSourceSpinner

        val leadTypeAdapter = ArrayAdapter.createFromResource(requireContext(), R.array.Lead_Type, R.layout.simple_spinner_item_layout)
        leadTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        leadTypeSpinner.adapter = leadTypeAdapter

        val requirementAdapter = ArrayAdapter.createFromResource(requireContext(), R.array.Requirement, R.layout.simple_spinner_item_layout)
        requirementAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        requirementSpinner.adapter = requirementAdapter

        val leadSourceAdapter = ArrayAdapter.createFromResource(requireContext(), R.array.Lead_source, R.layout.simple_spinner_item_layout)
        leadSourceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        leadSourceSpinner.adapter = leadSourceAdapter

        companyNameSpinner = binding.companyName
        companyNameAdapter = ArrayAdapter(requireContext(), R.layout.simple_spinner_item_layout, companyNames).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
        companyNameSpinner.adapter = companyNameAdapter

        binding.addCompany.setOnClickListener {
            val dialogView = layoutInflater.inflate(R.layout.dialog_add_company, null)
            val dialogBuilder = AlertDialog.Builder(requireContext())
                .setView(dialogView)
                .setTitle("Quick Create: Account")

            val companyNameEditText = dialogView.findViewById<EditText>(R.id.companyNameEditText)
            val emailEditText = dialogView.findViewById<EditText>(R.id.emailEditText)
            val phoneEditText = dialogView.findViewById<EditText>(R.id.phoneEditText)
            val accountTypespinner = dialogView.findViewById<Spinner>(R.id.accountType)

            val accountTypeAdapter = ArrayAdapter.createFromResource(requireContext(), R.array.Account_Type, R.layout.spinner_item_layout)
            accountTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            accountTypespinner.adapter = accountTypeAdapter

            dialogBuilder.setPositiveButton("Submit") { dialog, _ ->
                val newCompanyName = companyNameEditText.text.toString()

                val emailEditText = emailEditText.text.toString()
                val phoneEditText = phoneEditText.text.toString()
                val accountTypespinner = accountTypespinner.selectedItem.toString()

                if (newCompanyName.isNotBlank()) {
                    if (newCompanyName.matches("[a-zA-Z]{1,100}".toRegex())) {

                        val requestBody = LeadAddAccountPostModel(
                            emailEditText,
                            newCompanyName,
                            accountTypespinner,
                            phoneEditText
                        )
                        performAddCompany(requestBody)

                    } else {
                        Toast.makeText(
                            requireContext(),
                            "No Special characters Allowed",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
                else {
                    Toast.makeText(
                        requireContext(),
                        "Please enter a company name",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
            dialogBuilder.setNegativeButton("Close"){ dialog, _ ->
                dialog.dismiss()
            }

            val dialog = dialogBuilder.create()
            dialog.show()
        }

        fetchCompanyNames()

        companyNameSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val selectedCompanyName = companyNames[position]
                val accountId = companyNameToAccountIdMap[selectedCompanyName]
                if (!accountId.isNullOrBlank()) {
                    fetchCompanyDetails(accountId.toInt())
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                // Handle case when nothing is selected (if needed)
            }
        }

        countrySpinner = binding.countrySpinner
        countryAdapter = ArrayAdapter(requireContext(), R.layout.simple_spinner_item_layout, countryNames).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
        countrySpinner.adapter = countryAdapter

        stateSpinner = binding.stateSpinner
        stateAdapter = ArrayAdapter(requireContext(), R.layout.simple_spinner_item_layout, stateNames).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
        stateSpinner.adapter = stateAdapter

        citySpinner = binding.citySpinner
        cityAdapter = ArrayAdapter(requireContext(), R.layout.simple_spinner_item_layout, cityNames).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
        citySpinner.adapter = cityAdapter

        callCountryApi()

        binding.addLead.setOnClickListener{
            CallApi()
        }
        binding.reset.setOnClickListener {
            resetAll()
        }

        return root
    }

    private fun resetAll(){
        listOf(
            binding.customerName,binding.leadRevenue,binding.email,
            binding.date,binding.website,binding.mobileNo,
            binding.alternateMobileNo,binding.address,binding.zip,
            binding.description
        ).forEach{it.text = null}

        listOf(
            binding.companyName,binding.requirementSpinner,binding.leadTypeSpinner,
            binding.leadSourceSpinner,binding.countrySpinner,binding.stateSpinner,
            binding.citySpinner
        ).forEach { it.setSelection(0) }
    }

    private fun callCountryApi() {
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null){
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.getCountry("Bearer $jwtToken")
            call.enqueue(object : Callback<List<CountryModelItem>> {
                override fun onResponse(
                    call: Call<List<CountryModelItem>>,
                    response: Response<List<CountryModelItem>>
                ) {
                    if (response.isSuccessful){
                        val data = response.body()
                        if (data != null){
                            countryNames.clear()
                            countryNames.add("Select Country")
                            for (item in data){
                                item.name?.let { countryNames.add(it) }
                            }
                            countryAdapter.notifyDataSetChanged()

                            countrySpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                                    selectedCountryName = countryNames[position]
                                    val selectedCountry = data.find { it.name == selectedCountryName }
                                    selectedCountry?.let {
                                        callStateApi(it.id)
                                        CountryID = it.id.toString()
                                    }
                                }

                                override fun onNothingSelected(p0: AdapterView<*>?) {
                                    Constants.warning(requireContext(), "Select Country")
                                }

                            }

                        } else {
                            Constants.error(requireContext(), "Response body is null")
                        }
                    } else {
                        Constants.error(requireContext(), "Unsuccessful response: ${response.code()}")
                    }
                }

                override fun onFailure(call: Call<List<CountryModelItem>>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }

            })
        }
    }

    private fun callStateApi(id: Int) {
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null){
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.getState("Bearer $jwtToken", id)
            call.enqueue(object : Callback<List<StateModelItem>> {
                override fun onResponse(
                    call: Call<List<StateModelItem>>,
                    response: Response<List<StateModelItem>>
                ) {
                    if (response.isSuccessful){
                        val data = response.body()
                        if (data != null){
                            stateNames.clear()
                            stateNames.add("Select State")
                            for (item in data){
                                item.name?.let { stateNames.add(it) }
                            }
                            stateAdapter.notifyDataSetChanged()

                            stateSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                                    selectedStateName = stateNames[position]
                                    val selectedState = data.find { it.name == selectedStateName }
                                    if (selectedStateName != "Select State") {
                                        selectedState?.let {
                                            callCityApi(it.id)
                                            StateID = it.id.toString()
                                        } ?: run {
                                            Constants.warning(
                                                requireContext(),
                                                "State ID not found"
                                            )
                                        }
                                    }
                                }

                                override fun onNothingSelected(p0: AdapterView<*>?) {
                                    Constants.warning(requireContext(), "Select State")
                                }

                            }
                        } else {
                            Constants.error(requireContext(), "Response body is null")
                        }
                    } else {
                        Constants.error(requireContext(), "Unsuccessful response: ${response.code()}")
                    }
                }

                override fun onFailure(call: Call<List<StateModelItem>>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }

            })
        }
    }

    private fun callCityApi(id: Int) {
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null){
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.getCity("Bearer $jwtToken", id)
            call.enqueue(object : Callback<List<CityModelItem>> {
                override fun onResponse(
                    call: Call<List<CityModelItem>>,
                    response: Response<List<CityModelItem>>
                ) {
                    if (response.isSuccessful){
                        val data = response.body()
                        if (data != null){
                            cityNames.clear()
                            cityNames.add("Select City")
                            for (item in data){
                                item.name?.let { cityNames.add(it)
                                }
                            }
                            cityAdapter.notifyDataSetChanged()
                            citySpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                                override fun onItemSelected(
                                    parent: AdapterView<*>?,
                                    view: View?,
                                    position: Int,
                                    id: Long
                                ) {
                                    selectedCityName = cityNames[position]
                                    val selectedCity = data.find { it.name == selectedCityName }
                                    if (selectedCityName != "Select City") {
                                        selectedCity?.let {
                                            CityID = it.id.toString()
                                        } ?: run {
                                            Constants.warning(requireContext(), "City ID not found")
                                        }
                                    }
                                }

                                override fun onNothingSelected(parent: AdapterView<*>?) {
                                    Constants.warning(requireContext(), "Select the city")
                                }
                            }
                        } else {
                            Constants.error(requireContext(), "Response body is null")
                        }
                    } else {
                        Constants.error(requireContext(), "Unsuccessful response: ${response.code()}")
                    }
                }

                override fun onFailure(call: Call<List<CityModelItem>>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }

            })
        }
    }

    private fun performAddCompany(requestBody: LeadAddAccountPostModel) {
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null) {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.leadAddAccount("Bearer $jwtToken", requestBody)
            call.enqueue(object : Callback<LeadAddAccountResponseModel> {
                override fun onResponse(
                    call: Call<LeadAddAccountResponseModel>,
                    response: Response<LeadAddAccountResponseModel>
                ) {
                    if (response.isSuccessful) {
                        Constants.success(
                            requireContext(),
                            "Company Added Successfully"
                        )
                        // Handle successful response
                        val addedCompany = response.body()
                        if (addedCompany != null) {
                            // Update the company dropdown list
                            companyNames.add(addedCompany.accountName)
                            companyNameToAccountIdMap[addedCompany.accountName] = addedCompany.accountId.toString()
//                            companyNameToAccountIdMap[addedCompany.accountName] = addedCompany.accountEmail
//                            companyNameToAccountIdMap[addedCompany.accountName] = addedCompany.phone
                            companyNameAdapter.notifyDataSetChanged()
                        }
                    } else {
                        Constants.error(
                            requireContext(),
                            "Unsuccessful response: ${response.code()}"
                        )
                    }
                }

                override fun onFailure(call: Call<LeadAddAccountResponseModel>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }
            })
        } else {
            Constants.error(requireContext(), "Token is null. Please log in again.")
        }
    }

    private fun fetchCompanyNames() {
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null){
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val callAccounts = service.showaccounts("Bearer $jwtToken")
            val callEmployeeList = service.employeelist("Bearer $jwtToken")
            callAccounts.enqueue(object : Callback<ArrayList<ShowAccountsModelItem>>{
                override fun onResponse(
                    call: Call<ArrayList<ShowAccountsModelItem>>,
                    response: Response<ArrayList<ShowAccountsModelItem>>
                ) {
                    if (response.isSuccessful){
                        val data = response.body()
                        if (data != null){
                            companyNames.clear()
                            companyNames.add("Select Company Name")
                            for (item in data){
                                item.accountName?.let { companyNames.add(it) }
                                item.accountId?.let { accountId ->
                                    companyNameToAccountIdMap[item.accountName ?: ""] = accountId.toString()
                                }
                            }
                            companyNameAdapter.notifyDataSetChanged()
                        } else {
                            Constants.error(requireContext(), "Response body is null")
                        }
                    } else {
                        Constants.error(requireContext(), "Unsuccessful response: ${response.code()}")
                    }
                }

                override fun onFailure(call: Call<ArrayList<ShowAccountsModelItem>>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }

            })

            callEmployeeList.enqueue(object : Callback<ArrayList<ShowEmployeelistModelItem>>{
                override fun onResponse(
                    call: Call<ArrayList<ShowEmployeelistModelItem>>,
                    response: Response<ArrayList<ShowEmployeelistModelItem>>
                ) {
                    if (response.isSuccessful){
                        val employeelist = response.body()
                        if (employeelist != null && employeelist.isNotEmpty()){
                            val employee = employeelist[0]
                            binding.leadOwnerName.text = employee.name
                        } else {
                            Constants.error(requireContext(), "Employee List is null or empty")
                        }
                    } else {
                        Constants.error(requireContext(), "Unsuccessful response: ${response.code()}")
                    }
                }

                override fun onFailure(
                    call: Call<ArrayList<ShowEmployeelistModelItem>>,
                    t: Throwable
                ) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }

            })
        }
    }


    private fun CallApi() {

        val leadCreatedBy = binding.leadOwnerName.text.toString()

        val companyName = binding.companyName.selectedItem.toString()
        if (companyName == "Select Company Name") {
            Constants.error(requireContext(), "Company Name is  required.")
            return
        }

        val customerName = binding.customerName.text.toString()
        if (customerName.isBlank()) {
            Constants.error(requireContext(), "Customer name is Required")
            return
        }
        if (!customerName.matches("[A-Za-z ]{1,100}".toRegex())) {
            Constants.error(
                requireContext(),
                "Customer name contains only alphabetical characters and could not exceed 100 characters."
            )
            return
        }

        val requirement = binding.requirementSpinner.selectedItem.toString()
        if (requirement == "Select Requirement") {
            Constants.error(requireContext(), "Requirement is required.")
            return
        }

        val leadType = binding.leadTypeSpinner.selectedItem.toString()
        if (leadType == "Select Lead Type") {
            Constants.error(requireContext(), "Lead type is required.")
            return
        }

        val leadRevenue = binding.leadRevenue.text.toString()
        if (leadRevenue.isBlank()) {
            Constants.error(requireContext(), "Lead revenue is required.")
            return
        }
        if (leadRevenue.length > 10) {
            Constants.error(requireContext(), "Lead revenue should be up to 10 digits.")
            return
        }

        val leadSource = binding.leadSourceSpinner.selectedItem.toString()
        if (leadSource == "Select Lead Source") {
            Constants.error(requireContext(), "Lead source is required.")
            return
        }

        val leadStatus = "New Lead"

        val accountId = companyNameToAccountIdMap[companyName]
        val accountid : String = accountId.toString()
        if (accountid != null) {
            // The companyName exists in the map, and accountid is non-null
            Log.d("AccountID", "Account ID: $accountid")
        } else {
            // Handle the case where accountid is null
            Constants.error(requireContext(), "Account ID not found for the selected company name.")
        }

        val followUpdate = binding.date.text.toString()
        if (followUpdate.isBlank()) {
            Constants.error(requireContext(), "Follow update is required.")
            return
        }

        val website = binding.website.text.toString()
        if (website.isNotBlank()) {
            val urlRegex = "^(https?://)?([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}(/[a-zA-Z0-9-_.~!*'();:@&=+$,/?#[%]]*)*$".toRegex()

            if (!website.matches(urlRegex) && website.length !in 4.. 100) {
                Constants.error(
                    requireContext(),
                    "Please enter a valid website URL."
                )
                return
            }
        }

        val mobileNo = binding.mobileNo.text.toString()
        if (mobileNo.isBlank()) {
            Constants.error(requireContext(), "Mobile number is required.")
            return
        }
        if (mobileNo.length !in 6..16) {
            Constants.error(
                requireContext(),
                "Mobile number should contain upto 15 digits and optionally start with '+'."
            )
            return
        }

        val alternateMobileNumber = binding.alternateMobileNo.text.toString()
        if (alternateMobileNumber.isNotBlank()) {
            if (alternateMobileNumber.length !in 6.. 16) {
                Constants.error(
                    requireContext(),
                    "Mobile number should start with '+' and contain upto 15 digits."
                )
                return
            }
        }

        val email = binding.email.text.toString()
        if (email.isBlank()) {
            Constants.error(requireContext(), "Email is required.")
            return
        }
        if (email.length !in 1 .. 100) {
            Constants.error(requireContext(), "Email should be up to 100 characters.")
            return
        }

        val address = binding.address.text.toString()
        if (address.length !in 1 .. 255) {
            Constants.error(requireContext(), "Address should be upto 255 characters only.")
            return
        }

        val zip = binding.zip.text.toString()
        if (zip.isBlank()) {
            Constants.error(requireContext(), "ZIP code is required.")
            return
        }
        if (!zip.matches("[A-Za-z0-9]{1,15}".toRegex())) {
            Constants.error(
                requireContext(),
                "ZIP code should contain up to 10 alphanumeric characters."
            )
            return
        }

        if (CountryID == null && selectedCountryName == "Select Country" ) {
            Constants.error(requireContext(), "Please select country name")
            return
        }
        if (StateID == null && selectedStateName == "Select State") {
            Constants.error(requireContext(), "Please select state name")
            return
        }
        if (CityID == null && selectedCityName == "Select City") {
            Constants.error(requireContext(), "Please select city name")
            return  
        }

        val description = binding.description.text.toString()
        if( description.isNotBlank()) {
            if (description.length !in 5..255) {
                Constants.error(requireContext(), "Description should be 5 to 255 characters only.")
                return
            }
        }

        val requestBody = AddLeadPostModel(accountid, address, alternateMobileNumber, CityID.toString(), companyName, CountryID.toString(), customerName, description, email, followUpdate, leadCreatedBy, leadRevenue, leadSource, leadStatus, leadType, mobileNo, requirement, StateID.toString(), website, zip)

        Log.d("GURU",requestBody.toString())

        val jwtToken = gettoken(requireContext())
        if (jwtToken!=null)
        {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.addlead("Bearer $jwtToken" , requestBody)
            call.enqueue( object :Callback<AddLeadModel>{
                override fun onResponse(call: Call<AddLeadModel>, response: Response<AddLeadModel>) {
                    if (response.isSuccessful)
                    {
                        val data = response.body()
                        if (data!=null)
                        {
                            Constants.success(context," Lead Added successfully ${data.leadId}")
                            parentFragmentManager.popBackStack()
                        }
                    }
                    else
                    {
                        Constants.error(context,"Response is unsuccessful: ${response.code()}")
                    }

                }
                override fun onFailure(call: Call<AddLeadModel>, t: Throwable) {
                    Constants.error(context ,"Failed to add the lead ${t.message}")
                }

            })
        }
        else{
            Constants.error(requireContext(),"Token is null Login Again")
        }


    }

    private fun fetchCompanyDetails(aid: Int) {
        Log.d("account id","$aid")
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null) {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val callAccounts = service.getAccountDetailsById("Bearer $jwtToken", aid)
            callAccounts.enqueue(object : Callback<AccountDetailsByIdModel> {
                override fun onResponse(
                    call: Call<AccountDetailsByIdModel>,
                    response: Response<AccountDetailsByIdModel>
                ) {
                    if (response.isSuccessful) {
                        val data = response.body()
                        if (data != null) {
                            binding.email.setText(data.accountEmail)
                            binding.mobileNo.setText(data.phone)
                        } else {
                            Constants.error(requireContext(), "Response body is null")
                        }
                    } else {
                        Constants.error(
                            requireContext(),
                            "Unsuccessful response: ${response.code()}"
                        )
                    }
                }

                override fun onFailure(call: Call<AccountDetailsByIdModel>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }

            })
        }
    }

    private fun gettoken(context: Context?): String? {
        val sharedPreferences = requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
        return sharedPreferences.getString("jwtToken", null)
    }


    private fun showMaterialCalendar() {
        val today = Calendar.getInstance().timeInMillis
        val datePickerBuilder = MaterialDatePicker.Builder.datePicker()
        datePickerBuilder.setSelection(today)

        val constraintsBuilder = CalendarConstraints.Builder()
        constraintsBuilder.setValidator(DateValidatorPointForward.now())

        datePickerBuilder.setCalendarConstraints(constraintsBuilder.build())

        val datePicker = datePickerBuilder.build()

        datePicker.addOnPositiveButtonClickListener {selectedDate->
            val formatedDate = SimpleDateFormat("yyyy/dd/MM", Locale.getDefault()).format(Date(selectedDate))
            binding.date.setText(formatedDate.toString())
        }
        datePicker.show(parentFragmentManager, datePicker.toString())

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}