package com.BrightLink.SalesOptim.Activities.ui.notifications

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.BrightLink.SalesOptim.Adapters.NotificationAdapter
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.Constants.WrapContentLinearLayoutManager
import com.BrightLink.SalesOptim.ResponseModel.ShowLeadsModelItem
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.FragmentNotificationsBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NotificationsFragment : Fragment() {

    private var _binding: FragmentNotificationsBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentNotificationsBinding.inflate(inflater, container, false)
        val root: View = binding.root
        binding.recyclerView.layoutManager = WrapContentLinearLayoutManager(context)



        val jwtToken = gettoken(context)
        if (jwtToken!=null)
        {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            CallApi(service , jwtToken)
        }
        else{
            Constants.error(requireContext(),"Token is null Login Again")
        }
        // Assuming you have the adapter instantiated and attached to your RecyclerView
        val savedLeadId = context?.let { SharedPreferencesHelper.getLeadId(it) }


        return root
    }

    object SharedPreferencesHelper {
        fun getLeadId(context: Context): Int {
            val sharedPreferences = context.getSharedPreferences("your_shared_prefs_name", Context.MODE_PRIVATE)
            return sharedPreferences.getInt("leadId", -1) // Default value -1 if leadId is not found
        }

        fun saveLeadId(context: Context, leadId: Int) {
            val sharedPreferences = context.getSharedPreferences("your_shared_prefs_name", Context.MODE_PRIVATE)
            sharedPreferences.edit().putInt("leadId", leadId).apply()
        }
    }



    private fun CallApi(apiservice: ApiInterface, jwtToken: String) {
        val call = apiservice.notification("Bearer $jwtToken")
        call.enqueue(object : Callback<ArrayList<ShowLeadsModelItem>> {
            override fun onResponse(
                call: Call<ArrayList<ShowLeadsModelItem>>,
                response: Response<ArrayList<ShowLeadsModelItem>>
            ) {
                if (response.isSuccessful && isAdded) {
                    val data = response.body()
                    if (data != null) {
                        val adapter = NotificationAdapter(requireContext(), data)
                        binding.recyclerView.adapter = adapter
                    } else {
                        Constants.error(requireContext(), "Response body is null")
                    }
                } else {
                    Constants.error(requireContext(), "Unsuccessful response: ${response.code()}")
                }
            }

            override fun onFailure(call: Call<ArrayList<ShowLeadsModelItem>>, t: Throwable) {
                if (isAdded) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }
            }
        })
    }

    private fun gettoken(context: Context?): String? {
        val sharedPreferences = requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
        return sharedPreferences.getString("jwtToken", null)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}