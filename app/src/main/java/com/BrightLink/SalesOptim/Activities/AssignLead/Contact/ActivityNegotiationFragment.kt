package com.BrightLink.SalesOptim.Activities.AssignLead.Contact

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.BrightLink.SalesOptim.databinding.NegotiationAssignLeadBinding


class ActivityNegotiationFragment : Fragment() {
    private var _binding: NegotiationAssignLeadBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = NegotiationAssignLeadBinding.inflate(inflater, container, false)
        val root: View = binding.root

        return root
    }

}