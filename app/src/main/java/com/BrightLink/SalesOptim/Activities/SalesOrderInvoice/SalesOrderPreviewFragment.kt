package com.BrightLink.SalesOptim.Activities.SalesOrderInvoice

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.ColorMatrix
import android.graphics.ColorMatrixColorFilter
import android.graphics.Paint
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import com.BrightLink.SalesOptim.Adapters.SalesOrderGetProductAdapter
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.Constants.SharedPreferecneManager
import com.BrightLink.SalesOptim.Constants.WrapContentLinearLayoutManager
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.databinding.FragmentSalesOrderInvoiceBinding
import com.itextpdf.text.Document
import com.itextpdf.text.Image
import com.itextpdf.text.PageSize
import com.itextpdf.text.pdf.PdfWriter
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.UUID

class SalesOrderPreviewFragment : Fragment() {

    private var _binding: FragmentSalesOrderInvoiceBinding? = null
    private val binding get() = _binding!!
    private val PREF_NAME = "SalesPrefs"

    private lateinit var adapter : SalesOrderGetProductAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentSalesOrderInvoiceBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.actionBar.activityName.text = "PDF Preview"
        binding.actionBar.back.setOnClickListener{
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }

        binding.recyclerView.layoutManager = WrapContentLinearLayoutManager(context)

        val sharedPreferences = requireContext().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)

        //own
        val companyName = sharedPreferences.getString("companyName", "")
        val address = sharedPreferences.getString("address", "")
        val phone = sharedPreferences.getString("phone", "")
        val email = sharedPreferences.getString("email", "")
        val date = sharedPreferences.getString("date", "")
        val quotationId = sharedPreferences.getString("quotationId", "")
        val imageUriString = sharedPreferences.getString("imageUri", "")

        val backgroundclr = sharedPreferences.getString("background","")
        val headerbckground = sharedPreferences.getString("header","")
        val text = sharedPreferences.getString("text","")

        //Text
        if (text != null && text.isNotEmpty()) {
            binding.dateText.setTextColor(android.graphics.Color.parseColor(text))
            binding.date.setTextColor(android.graphics.Color.parseColor(text))
            binding.quotationId.setTextColor(android.graphics.Color.parseColor(text))
            binding.quotationIdText.setTextColor(android.graphics.Color.parseColor(text))
            binding.to.setTextColor(android.graphics.Color.parseColor(text))
            binding.customerName.setTextColor(android.graphics.Color.parseColor(text))
            binding.addressCompanyName.setTextColor(android.graphics.Color.parseColor(text))
            binding.customerAddress.setTextColor(android.graphics.Color.parseColor(text))
            binding.customerPhone.setTextColor(android.graphics.Color.parseColor(text))
            binding.sub.setTextColor(android.graphics.Color.parseColor(text))
            binding.subject.setTextColor(android.graphics.Color.parseColor(text))
            binding.sirMadam.setTextColor(android.graphics.Color.parseColor(text))
            binding.message.setTextColor(android.graphics.Color.parseColor(text))
            binding.ref.setTextColor(android.graphics.Color.parseColor(text))
            binding.tAndC.setTextColor(android.graphics.Color.parseColor(text))
            binding.applicableTaxexText.setTextColor(android.graphics.Color.parseColor(text))
            binding.tax1.setTextColor(android.graphics.Color.parseColor(text))
            binding.shippingCharges1.setTextColor(android.graphics.Color.parseColor(text))
            binding.shippingCharges1Text.setTextColor(android.graphics.Color.parseColor(text))
            binding.delieveryPeriod.setTextColor(android.graphics.Color.parseColor(text))
            binding.delieveryPeriodText.setTextColor(android.graphics.Color.parseColor(text))
            binding.weekText.setTextColor(android.graphics.Color.parseColor(text))
            binding.advancePayment.setTextColor(android.graphics.Color.parseColor(text))
            binding.advancePaymentText.setTextColor(android.graphics.Color.parseColor(text))
            binding.inAdvanceText.setTextColor(android.graphics.Color.parseColor(text))
            binding.notes.setTextColor(android.graphics.Color.parseColor(text))
            binding.notesText.setTextColor(android.graphics.Color.parseColor(text))
            binding.senderName.setTextColor(android.graphics.Color.parseColor(text))
            binding.senderNumber.setTextColor(android.graphics.Color.parseColor(text))
        }

        //Header
        if (headerbckground != null && headerbckground.isNotEmpty()) {
            binding.companyName.setTextColor(android.graphics.Color.parseColor(headerbckground))
            binding.address.setTextColor(android.graphics.Color.parseColor(headerbckground))
            binding.phone.setTextColor(android.graphics.Color.parseColor(headerbckground))
            binding.email.setTextColor(android.graphics.Color.parseColor(headerbckground))
            binding.srno.setTextColor(android.graphics.Color.parseColor(headerbckground))
            binding.productDescription.setTextColor(android.graphics.Color.parseColor(headerbckground))
            binding.qty.setTextColor(android.graphics.Color.parseColor(headerbckground))
            binding.unitPrice.setTextColor(android.graphics.Color.parseColor(headerbckground))
            binding.totalPrice.setTextColor(android.graphics.Color.parseColor(headerbckground))
            binding.subTotal.setTextColor(android.graphics.Color.parseColor(headerbckground))
            binding.subTotal1.setTextColor(android.graphics.Color.parseColor(headerbckground))
            binding.tax.setTextColor(android.graphics.Color.parseColor(headerbckground))
            binding.taxText.setTextColor(android.graphics.Color.parseColor(headerbckground))
            binding.shippingChargesText.setTextColor(android.graphics.Color.parseColor(headerbckground))
            binding.shippingCharges.setTextColor(android.graphics.Color.parseColor(headerbckground))
            binding.grandTotalText.setTextColor(android.graphics.Color.parseColor(headerbckground))
            binding.grandTotal.setTextColor(android.graphics.Color.parseColor(headerbckground))
        }

        //Background
        if (backgroundclr != null && backgroundclr.isNotEmpty()) {
            binding.background1.setBackgroundColor(android.graphics.Color.parseColor(backgroundclr))
            binding.background2.setBackgroundColor(android.graphics.Color.parseColor(backgroundclr))
            binding.background3.setBackgroundColor(android.graphics.Color.parseColor(backgroundclr))
        }

        //customer
        val customerName = sharedPreferences.getString("customerName", "")
        val addressCompanyName = sharedPreferences.getString("addressCompanyName", "")
        val customerAddress = sharedPreferences.getString("customerAddress", "")
        val customerPhone = sharedPreferences.getString("customerPhone", "")
        val subject = sharedPreferences.getString("subject", "")
        val message = sharedPreferences.getString("message", "")

        // shipping

        val ship_customerName = sharedPreferences.getString("ship_customerName", "")
        val ship_customerCompanyName = sharedPreferences.getString("ship_companyName", "")
        val ship_customerAddress = sharedPreferences.getString("ship_companyAddress", "")
        val ship_customerPhone = sharedPreferences.getString("ship_customer_phone", "")


        //product
        val product =  requireContext().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        getProducts(product)

        val tax = sharedPreferences.getString("tax", "")
        val shippingCharges = sharedPreferences.getString("shippingCharges", "")

        //terms and conditions
        val delieveryPeriod = sharedPreferences.getString("delieveryPeriod", "")
        val advancePayment = sharedPreferences.getString("advancePayment", "")
        val tAndC = sharedPreferences.getString("tAndC", "")
        val notes = sharedPreferences.getString("notes", "")
        val senderName = sharedPreferences.getString("senderName", "")
        val senderNumber = sharedPreferences.getString("senderNumber", "")

        // Set the retrieved strings to the appropriate TextViews

        binding.companyName.text = companyName
        binding.address.text = address
        binding.phone.text = phone
        binding.email.text = email
        binding.date.text = date
        binding.quotationId.text = quotationId
        imageUriString?.let { uriString ->
            val imageUri = Uri.parse(uriString)
            if (imageUri == null){
                Constants.error(context, "Image not selected")
            }
            binding.logo.setImageURI(imageUri)
        }

        binding.customerName.text = customerName
        binding.addressCompanyName.text = addressCompanyName
        binding.customerAddress.text = customerAddress
        binding.customerPhone.text = customerPhone
        binding.subject.text = subject
        binding.message.text = message

        binding.tax.text = tax
        binding.shippingCharges.text = shippingCharges

        binding.delieveryPeriod.text = delieveryPeriod
        binding.advancePayment.text = advancePayment
        binding.tAndC.text = tAndC
        binding.notes.text = notes
        binding.senderName.text = senderName
        binding.senderNumber.text = senderNumber
        binding.tax1.text = tax
        binding.shippingCharges1.text = shippingCharges
        binding.shippingCustomerName.text = ship_customerName
        binding.shippingCompanyName.text = ship_customerCompanyName
        binding.shippingAddress.text = ship_customerAddress
        binding.shippingPhone.text = ship_customerPhone

        binding.downloadPdf.setOnClickListener {
            layoutToImage(binding.root)
        }

        return root
    }

    fun layoutToImage(view: View) {
        // Get the view group using reference
        val relativeLayout = view.findViewById<View>(R.id.printSales) as LinearLayout
        // Convert view group to bitmap
        relativeLayout.isDrawingCacheEnabled = true
        relativeLayout.buildDrawingCache()
        val drawingCache = relativeLayout.drawingCache
        if (drawingCache != null) {
            val bitmap = Bitmap.createBitmap(drawingCache)
            val brightnessValue = 20 // Adjust brightness value as needed
            val contrastValue = 1.2f // Adjust contrast value as needed
            val adjustedBitmap = applyBrightnessAndContrast(bitmap, brightnessValue, contrastValue)

            // Save adjusted bitmap as image file
            val file = File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + File.separator + "${UUID.randomUUID().toString()}.jpg"
            )
            try {
                val fos = FileOutputStream(file)
                adjustedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos)
                fos.flush()
                fos.close()
                // Convert the adjusted image to PDF
                imageToPDF(file)
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        else
        {
            Constants.error(context,"Failed to create bitmap from drawing cache")
        }
    }

    private fun applyBrightnessAndContrast(bitmap: Bitmap, brightnessValue: Int, contrastValue: Float): Bitmap {
        val cm = ColorMatrix()
        cm.set(floatArrayOf(
            contrastValue, 0f, 0f, 0f, brightnessValue.toFloat(),
            0f, contrastValue, 0f, 0f, brightnessValue.toFloat(),
            0f, 0f, contrastValue, 0f, brightnessValue.toFloat(),
            0f, 0f, 0f, 1f, 0f
        ))
        val paint = Paint()
        paint.colorFilter = ColorMatrixColorFilter(cm)

        val adjustedBitmap = Bitmap.createBitmap(bitmap.width, bitmap.height, bitmap.config)
        val canvas = Canvas(adjustedBitmap)
        val paint2 = Paint()
        paint2.color = Color.WHITE
        canvas.drawBitmap(bitmap, 0f, 0f, paint2)
        canvas.drawBitmap(adjustedBitmap, 0f, 0f, paint)
        return adjustedBitmap
    }

    fun imageToPDF(imageFile: File) {
        try {
            val document = Document()
            val dirPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).toString() + File.separator + "CRM/PDF"
            val directory = File(dirPath)
            directory.mkdirs()
            val pdfFile = File(directory, "${UUID.randomUUID()}.pdf")
            PdfWriter.getInstance(
                document,
                FileOutputStream(pdfFile)
            )
            document.open()

            val img = Image.getInstance(imageFile.absolutePath)
            img.scaleToFit(PageSize.A4.width, PageSize.A4.height)
            // img.scaleAbsolute(document.pageSize.width - document.leftMargin() - document.rightMargin(), document.pageSize.height - document.topMargin() - document.bottomMargin())
            img.alignment = Image.ALIGN_CENTER
            document.add(img)
            document.close()
            Constants.success(context, "PDF generated successfully!")
        } catch (e: Exception) {
            e.printStackTrace()
            Constants.error(context, "Failed to generate PDF")
        }

    }

    private fun getProducts(product: SharedPreferences?) {

        val product =  SharedPreferecneManager.getSalesItems(requireContext())
        binding.recyclerView.layoutManager = WrapContentLinearLayoutManager(context)
        adapter = SalesOrderGetProductAdapter(requireContext() , product)
        binding.recyclerView.adapter = adapter

    }


}