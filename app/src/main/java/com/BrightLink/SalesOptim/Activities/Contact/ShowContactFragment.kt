package com.BrightLink.SalesOptim.Activities.Contact

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.BrightLink.SalesOptim.Adapters.ContactAdapter
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.Constants.WrapContentLinearLayoutManager
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.ResponseModel.ShowContactModelItem
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.ContactListBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ShowContactFragment: Fragment() {
    private var binding: ContactListBinding? = null
    private lateinit var adapter: ContactAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = ContactListBinding.inflate(inflater, container, false)
        val root: View = binding!!.root

        binding!!.actionBar.back.setOnClickListener{
            requireActivity().finish()
        }
        binding!!.actionBar.activityName.text = "Contact List"
        binding!!.actionBar.back.setOnClickListener{
            requireActivity().onBackPressed()
        }
        binding!!.actionBar.option.visibility = View.INVISIBLE

        binding!!.recyclerView.layoutManager = WrapContentLinearLayoutManager(context)

        val jwtToken = gettoken(context)
        if (jwtToken != null) {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            callApi(service, jwtToken)
        } else {
            Constants.error(context, "Token is null Login Again")
        }
        binding!!.addcontactsbtn.setOnClickListener{
            findNavController().navigate(R.id.addcontacts)
        }

        binding!!.search.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                adapter.filter(s.toString())
            }
        })

        return root
    }

    private fun callApi(apiService: ApiInterface, jwtToken: String) {
        val call = apiService.showcontact("Bearer $jwtToken")
        call.enqueue(object : Callback<List<ShowContactModelItem>> {
            override fun onResponse(
                call: Call<List<ShowContactModelItem>>,
                response: Response<List<ShowContactModelItem>>
            ) {
                if (isAdded) { // Check if the fragment is added to the activity
                    if (response.isSuccessful) {
                        val data = response.body()
                        if (data != null) {
                            adapter = ContactAdapter(requireContext(), data)
                            adapter.setOnItemClickListener(object : ContactAdapter.onItemClickListener {
                                override fun onItemClick(position: Int) {
                                    val selectedItem = data[position] // Get the selected item
                                    val bundle = Bundle().apply {
                                        putSerializable("selectedItem", selectedItem) // Pass the selected item as a Serializable
                                    }
                                    findNavController().navigate(R.id.updateConatct, bundle)
                                }
                            })
                            binding!!.recyclerView.adapter = adapter // Set the adapter to the RecyclerView
                        } else {
                            Constants.error(context, "Response is null")
                        }
                    }
                }
            }

            override fun onFailure(call: Call<List<ShowContactModelItem>>, t: Throwable) {
                Constants.error(context, "Error : ${t.message}")
            }
        })
    }

    private fun gettoken(context: Context?): String? {
        val sharedPreferences = requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
        return sharedPreferences.getString("jwtToken", null)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}
