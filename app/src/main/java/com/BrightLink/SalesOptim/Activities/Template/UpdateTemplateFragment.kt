package com.BrightLink.SalesOptim.Activities.Template

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.RequestModel.UpdateTemplatePutModel
import com.BrightLink.SalesOptim.ResponseModel.TemplateDetailsByIdModel
import com.BrightLink.SalesOptim.ResponseModel.UpdateTemplateResponseModel
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.UpdateTemplateBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class UpdateTemplateFragment : Fragment() {

    private var _binding: UpdateTemplateBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = UpdateTemplateBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.actionBar.activityName.text = "Template Info"
        binding.actionBar.back.setOnClickListener {
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }

        val tid = arguments?.getInt("tid") ?: 0

        if(tid != null){
            fetchTemplateDetailsById(tid)
        }

        val tempId = tid.toString()

        binding.update.setOnClickListener {
            callUpdateApi(tempId)
        }

        binding.reset.setOnClickListener {
            binding.title.text.clear()
            binding.description.text.clear()
        }

        return root
    }

    private fun fetchTemplateDetailsById(tid: Int) {
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null){
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val  call = service.getTemplateDetailsById("Bearer $jwtToken", tid)
            call.enqueue(object : Callback<TemplateDetailsByIdModel> {
                override fun onResponse(
                    call: Call<TemplateDetailsByIdModel>,
                    response: Response<TemplateDetailsByIdModel>
                ) {
                    if (response.isSuccessful) {
                        val data = response.body()
                        if (data != null) {

                            binding.createdBy.text = data.createdBy
                            binding.title.setText(data.title)
                            binding.type.text = data.type
                            binding.description.setText(data.description)

                        } else {
                            Constants.error(requireContext(), "Template details not found")
                        }
                    } else {
                        Constants.error(
                            requireContext(),
                            "Something went wrong, Try again"
                        )
                    }
                }

                override fun onFailure(call: Call<TemplateDetailsByIdModel>, t: Throwable) {
                    Constants.error(requireContext(), "Failed to Fetch Template Details")
                }
            })
        }
    }

    private fun callUpdateApi(tempId: String) {
        val title = binding.title.text.toString()
        val type = binding.type.text.toString()
        val description = binding.description.text.toString()

        val requestBody = UpdateTemplatePutModel(description, tempId, title, type)

        val jwtToken = gettoken(requireContext())

        if (jwtToken != null){
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.templateUpdate("Bearer $jwtToken", requestBody)
            call.enqueue(object : Callback<UpdateTemplateResponseModel> {
                override fun onResponse(
                    call: Call<UpdateTemplateResponseModel>,
                    response: Response<UpdateTemplateResponseModel>
                ) {
                    if (response.isSuccessful){
                        Constants.success(requireContext(), "Template updated successfully")
                        findNavController().popBackStack()
                    } else {
                        Constants.error(requireContext(), "Something went wrong, Try again")
                    }
                }

                override fun onFailure(call: Call<UpdateTemplateResponseModel>, t: Throwable) {
                    Constants.error(requireContext(), "Failed to Update Template. Try again")
                }

            })
        } else {
            Constants.error(requireContext(), "Please try to Login Again")
        }
    }

    private fun gettoken(context: Context?): String? {
        val sharedPreferences = requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
        return sharedPreferences.getString("jwtToken", null)
    }

}