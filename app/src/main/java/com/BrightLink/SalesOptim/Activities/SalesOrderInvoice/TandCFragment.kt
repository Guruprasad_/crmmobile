package com.BrightLink.SalesOptim.Activities.SalesOrderInvoice

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.databinding.FragmentInvoiceTandcBinding


class TandCFragment : Fragment() {
    private var _binding: FragmentInvoiceTandcBinding? = null
    private val binding get() = _binding!!
    private val PREF_NAME = "SalesPrefs"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentInvoiceTandcBinding.inflate(inflater, container, false)
        val root: View = binding.root



        binding.generate.setOnClickListener {
            val delieveryPeriod = binding.delieveryPeriod.text.toString()
            val advancePayment = binding.advancePayment.text.toString()
            val tAndC = binding.tAndC.text.toString()
            val notes = binding.notes.text.toString()
            val senderName = binding.senderName.text.toString()
            val senderNumber = binding.senderNumber.text.toString()

            val sharedPreferences =
                requireContext().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putString("delieveryPeriod", delieveryPeriod)
            editor.putString("advancePayment", advancePayment)
            editor.putString("tAndC", tAndC)
            editor.putString("notes", notes)
            editor.putString("senderName", senderName)
            editor.putString("senderNumber", senderNumber)
            editor.apply()
            findNavController().navigate(R.id.generatePdfSales)
        }

        return root
    }


    private fun showToast(message: String) {
        // Example method to show a toast message
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }

}