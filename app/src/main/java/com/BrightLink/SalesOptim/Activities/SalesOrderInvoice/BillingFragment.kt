package com.BrightLink.SalesOptim.Activities.SalesOrderInvoice

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.databinding.FragmentInvoiceCustomerBinding


class BillingFragment : Fragment() {
    private var _binding: FragmentInvoiceCustomerBinding? = null
    private val binding get() = _binding!!
    private val PREF_NAME = "SalesPrefs"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentInvoiceCustomerBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.save.setOnClickListener {

            val customerName = binding.customerName.text.toString()
            val addressCompanyName = binding.addressCompanyName.text.toString()
            val customerAddress = binding.customerAddress.text.toString()
            val customerPhone = binding.customerPhone.text.toString()
            val subject = binding.subject.text.toString()
            val message = binding.message.text.toString()


            val sharedPreferences =
                requireContext().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putString("customerName", customerName)
            editor.putString("addressCompanyName", addressCompanyName)
            editor.putString("customerAddress", customerAddress)
            editor.putString("customerPhone", customerPhone)
            editor.putString("subject", subject)
            editor.putString("message", message)
            editor.apply()

            Constants.success(context, "Customer details saved successfully")
        }

        return root
    }

    private fun showToast(message: String) {
        // Example method to show a toast message
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }

}