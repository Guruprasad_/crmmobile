package com.BrightLink.SalesOptim.Activities.SalesOrderInvoice

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.databinding.FragmentInvoiceOwnBinding
import com.google.android.material.datepicker.MaterialDatePicker
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class OwnFragment : Fragment() {

    private var _binding: FragmentInvoiceOwnBinding? = null
    private val binding get() = _binding!!
    private val PREF_NAME = "SalesPrefs"
    private var uri : Uri? = null

    private val imagePickerLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            val data: Intent? = result.data
            uri = data?.data
            uri?.let { selectedUri ->
                // Handle the selected image URI here
                showToast("Image selected")
                // You can save the selected image URI to SharedPreferences or perform any other operation
                saveImageUriToPreferences(selectedUri)
            }
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentInvoiceOwnBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.date.setOnClickListener{
            openDatePicker()
        }

        binding.linkFileOneChoose.setOnClickListener {
            ImagePicker()
        }

        binding.save.setOnClickListener {

            val companyName = binding.companyName.text.toString()
            val address = binding.address.text.toString()
            val phone = binding.phone.text.toString()
            val email = binding.email.text. toString()
            val date = binding.date.text.toString()
            val quotationId = binding.quotationId.text.toString()



            val sharedPreferences = requireContext().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putString("companyName", companyName)
            editor.putString("address", address)
            editor.putString("phone", phone)
            editor.putString("email", email)
            editor.putString("date", date)
            editor.putString("quotationId", quotationId)
            editor.apply()

            Constants.success(context , "Owner details saved successfully")
        }

        return root
    }

    private fun openDatePicker() {
        val builder = MaterialDatePicker.Builder.datePicker().build()

        builder.addOnPositiveButtonClickListener { selection ->
            val dateFormat = SimpleDateFormat("MM/dd/yyyy", Locale.US)
            val selectedDate = dateFormat.format(Date(selection))
            binding.date.text = selectedDate
        }
        builder.show(requireActivity().supportFragmentManager, builder.toString())
    }

    private fun ImagePicker() {
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        imagePickerLauncher.launch(intent)
    }

    private fun saveImageUriToPreferences(uri: Uri) {
        // Save the selected image URI to SharedPreferences or perform any other operation
        val sharedPreferences = requireContext().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("imageUri", uri.toString())
        editor.apply()
    }

    fun color(colorr : String): ColorPickerDialogFragment {
        val colorPickerDialogFragment = ColorPickerDialogFragment()
        colorPickerDialogFragment.setColorPickerListener(object :
            ColorPickerDialogFragment.ColorPickerListener {
            override fun onColorSelected(color: Int) {

                val hexColor = String.format("#%06X", 0xFFFFFF and color)

                val sharedPreferences =
                    requireContext().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
                val editor = sharedPreferences.edit()
                editor.putString(colorr, hexColor)
                editor.apply()

                when (colorr) {
                    "background" -> binding.background.setBackgroundColor(color)
                    "header" -> binding.head.setBackgroundColor(color)
                    "text" -> binding.text.setBackgroundColor(color)
                }
            }
        })
        return colorPickerDialogFragment
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.backgroundclr.setOnClickListener {
            color("background").show(childFragmentManager, "colorPicker")
            // binding.background.setBackgroundColor(android.graphics.Color.parseColor("background"))
        }
        binding.headingsclr.setOnClickListener {
            color("header").show(childFragmentManager,"colorPicker")
        }
        binding.textClr.setOnClickListener {
            color("text").show(childFragmentManager,"colorPicker")
        }
    }

    private fun showToast(message: String) {
        // Example method to show a toast message
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }

}