package com.BrightLink.SalesOptim.Activities.quotation

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.databinding.FragmentTermsAndConditionsBinding

class TermsAndConditionsFragment : Fragment() {


    private var _binding: FragmentTermsAndConditionsBinding? = null
    private val binding get() = _binding!!
    private val PREF_NAME = "TandCPrefs"
    private var visibility : Boolean = true

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentTermsAndConditionsBinding.inflate(inflater, container, false)

        binding.visible.setOnClickListener {
            binding.noteLayout.visibility = View.VISIBLE
            visibility = true
            binding.visible.setBackgroundResource(R.color.statusbar)
            val color = ContextCompat.getColor(requireContext(), R.color.white)
            binding.visible.setColorFilter(color)
            val default = ContextCompat.getColor(requireContext(), R.color.black)
            binding.invisible.setColorFilter(default)
            binding.invisible.setBackgroundResource(android.R.color.transparent)
        }

        binding.invisible.setOnClickListener {
            binding.noteLayout.visibility = View.INVISIBLE
            visibility = false
            binding.invisible.setBackgroundResource(R.color.statusbar)
            val color = ContextCompat.getColor(requireContext(), R.color.white)
            binding.invisible.setColorFilter(color)
            val default = ContextCompat.getColor(requireContext(), R.color.black)
            binding.visible.setColorFilter(default)
            binding.visible.setBackgroundResource(android.R.color.transparent)
        }


        binding.generatePDF.setOnClickListener {

            val deliveryPeriod = binding.delieveryPeriod.text.toString()
            val advancePayment = binding.advancePayment.text.toString()
            val termsandConditions = binding.tAndC.text.toString()
            val note = binding.notes.text.toString()
            val senderName = binding.senderName.text.toString()
            val senderNumber = binding.senderNumber.text.toString()
            val senderDesignation = binding.senderDesignation.text.toString()

            val sharedPreferences = requireContext().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putString("deliveryPeriod", deliveryPeriod)
            editor.putString("advancePayment", advancePayment)
            editor.putString("termsandConditions", termsandConditions)
            editor.putString("note", note)
            editor.putString("senderName", senderName)
            editor.putString("senderNumber", senderNumber)
            editor.putBoolean("visibility", visibility)
            editor.putString("senderDesignation", senderDesignation)
            editor.apply()

            Constants.success(context,"Generating PDF")

            findNavController().navigate(R.id.quotationPreview)
        }


        return binding.root
    }

}