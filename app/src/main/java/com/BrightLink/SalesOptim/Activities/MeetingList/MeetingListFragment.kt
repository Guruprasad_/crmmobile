package com.BrightLink.SalesOptim.Activities.MeetingList

import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TimePickerDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.ImageButton
import androidx.fragment.app.Fragment
import com.BrightLink.SalesOptim.Adapters.MeetingListAdapter
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.Constants.WrapContentLinearLayoutManager
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.ResponseModel.MeetingListModelItem
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.ActivityMeetingListBinding
import com.BrightLink.SalesOptim.databinding.AddMeetingPopupBinding
import com.BrightLink.SalesOptim.RequestModel.AddMeetingPostModel
import com.BrightLink.SalesOptim.ResponseModel.AddMeetingResponseModel
import com.BrightLink.SalesOptim.ResponseModel.ShowContactModelItem
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale

class MeetingListFragment : Fragment() {

    private var _binding: ActivityMeetingListBinding? = null
    private val binding get() = _binding!!
    private var popupWindow: AddMeetingPopupBinding? = null
    private lateinit var contactList: List<ShowContactModelItem>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ActivityMeetingListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupActionBar()
        setupRecyclerView()
        setupAddMeetingButton()
        fetchContactNames()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setupActionBar() {
        with(binding.actionBar) {
            activityName.text = "Meeting List"
            back.setOnClickListener {
                requireActivity().onBackPressed()
            }
            option.visibility = View.INVISIBLE
        }
    }

    private fun setupRecyclerView() {
        binding.recyclerView.layoutManager = WrapContentLinearLayoutManager(context)
        refreshMeetingList()
    }

    private fun setupAddMeetingButton() {
        binding.addmeeting.setOnClickListener {
            showAddMeetingPopup()
        }
    }

    private fun refreshMeetingList() {
        getToken()?.let { jwtToken ->
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            callApi(service, jwtToken)
        } ?: Constants.error(requireContext(), "Token is null. Please log in again.")
    }

    private fun showAddMeetingPopup() {
        val dialog = Dialog(requireContext())
        val popupBinding = AddMeetingPopupBinding.inflate(layoutInflater)
        popupWindow = popupBinding
        dialog.setContentView(popupBinding.root)
        dialog.setCancelable(false)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window?.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        fetchContactNames()
        setupDatePickers()
        popupBinding.addmeet.setOnClickListener {
            val dateFrom = popupBinding.dateFrom.text.toString()
            val dateTo = popupBinding.dateTo.text.toString()
            if (dateFrom.isNotEmpty() && dateTo.isNotEmpty()) {
                if (isDateValid(dateFrom, dateTo)) {
                    addMeeting(popupBinding)
                    dialog.dismiss()
                    refreshMeetingList()
                } else {
                    Constants.error(requireContext(), "Meeting To date should be after Meeting From date")
                }
            } else {
                Constants.error(requireContext(), "Please select Meeting From and Meeting To dates")
            }
        }
        dialog.findViewById<ImageButton>(R.id.closeButton)?.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun fetchContactNames() {
        getToken()?.let { jwtToken ->
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val callContacts = service.showcontact("Bearer $jwtToken")
            callContacts.enqueue(object : Callback<List<ShowContactModelItem>> {
                override fun onResponse(
                    call: Call<List<ShowContactModelItem>>,
                    response: Response<List<ShowContactModelItem>>
                ) {
                    if (isAdded  && response.isSuccessful) {
                        response.body()?.let { data ->
                            contactList = data
                            val contactNames = contactList.map { it.fullName }
                            val adapter = ArrayAdapter(requireContext(), R.layout.spinner_item_layout_currency, contactNames)
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                            popupWindow?.contactName?.adapter = adapter
                        } ?: Constants.error(requireContext(), "Response body is null")
                    } else {
                        Constants.error(requireContext(), "Unsuccessful response: ${response.code()}")
                    }
                }

                override fun onFailure(call: Call<List<ShowContactModelItem>>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }
            })
        } ?: Constants.error(requireContext(), "Token is null. Please log in again.")
    }

    private fun setupDatePickers() {
        popupWindow?.let { popupBinding ->
            popupBinding.dateFrom.setOnClickListener {
                showDatePicker(popupBinding.dateFrom)
            }
            popupBinding.dateTo.setOnClickListener {
                showDatePicker(popupBinding.dateTo)
            }
        }
    }

    private fun showDatePicker(editText: EditText) {
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH)

        val datePickerDialog = DatePickerDialog(requireContext(), R.style.MyDatePickerDialogTheme, { _, year, month, day ->
            val formattedMonth = String.format("%02d", month + 1)
            val formattedDay = String.format("%02d", day)
            val selectedDate = "$year-$formattedMonth-$formattedDay"
            showTimePicker(editText, selectedDate)
        }, year, month, dayOfMonth)

        datePickerDialog.show()
    }

    private fun showTimePicker(editText: EditText, selectedDate: String) {
        val calendar = Calendar.getInstance()
        val hourOfDay = calendar.get(Calendar.HOUR_OF_DAY)
        val minute = calendar.get(Calendar.MINUTE)

        val timePickerDialog = TimePickerDialog(requireContext(), R.style.MyDatePickerDialogTheme, { _, hourOfDay, minute ->
            val formattedHour = String.format("%02d", hourOfDay)
            val formattedMinute = String.format("%02d", minute)
            val t = "T"
            val selectedTime = "$formattedHour:$formattedMinute"
            val dateTime = "$selectedDate$t$selectedTime"
            editText.setText(dateTime)
        }, hourOfDay, minute, false)

        timePickerDialog.show()
    }

    private fun addMeeting(binding: AddMeetingPopupBinding) {
        val title = binding.title.text.toString()
        val meetingFrom = binding.dateFrom.text.toString()
        val meetingTo = binding.dateTo.text.toString()
        val contactName = binding.contactName.selectedItem.toString()

        val selectedContact = contactList.find { it.fullName == contactName }
        val contactId = selectedContact?.contactid ?: 0

        val clientEmail = binding.clientEmail.text.toString()
        val locationType = getLocationType(binding)
        val meetingLinkOrLocation = binding.meetingLink.text.toString()

        if (title.isEmpty() || meetingFrom.isEmpty() || meetingTo.isEmpty() || clientEmail.isEmpty() || locationType.isEmpty() || meetingLinkOrLocation.isEmpty()) {
            Constants.error(requireContext(), "Please fill in all fields")
            return
        }

        if (meetingFrom >= meetingTo) {
            Constants.error(requireContext(), "Meeting To date should be after Meeting From date")
            return
        }

        val requestBody = AddMeetingPostModel(
            title, meetingFrom, meetingTo, contactName, clientEmail, locationType, meetingLinkOrLocation, contactId
        )

        getToken()?.let { jwtToken ->
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.addmeeting("Bearer $jwtToken", requestBody)
            call.enqueue(object : Callback<AddMeetingResponseModel> {
                override fun onResponse(
                    call: Call<AddMeetingResponseModel>,
                    response: Response<AddMeetingResponseModel>
                ) {
                    if (isAdded && response.isSuccessful) {
                        Constants.success(requireContext(), "Meeting added successfully")
                        refreshMeetingListDelayed()
                    } else {
                        Constants.error(requireContext(), "Unsuccessful response: ${response.code()}")
                    }
                }

                override fun onFailure(call: Call<AddMeetingResponseModel>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }
            })
        } ?: Constants.error(requireContext(), "Token is null. Please log in again.")
    }

    private fun getLocationType(binding: AddMeetingPopupBinding): String {
        return when (binding.location.checkedRadioButtonId) {
            R.id.videoConference -> "videoConference"
            R.id.onsite -> "onsite"
            else -> ""
        }
    }

    private fun callApi(apiservice: ApiInterface, jwtToken: String) {
        val call = apiservice.meetinglist("Bearer $jwtToken")
        call.enqueue(object : Callback<List<MeetingListModelItem>> {
            override fun onResponse(
                call: Call<List<MeetingListModelItem>>,
                response: Response<List<MeetingListModelItem>>
            ) {
                if (isAdded && response.isSuccessful) {
                    response.body()?.let { data ->
                        val adapter = MeetingListAdapter(requireContext(), data)
                        binding.recyclerView.adapter = adapter
                    } ?: Constants.error(requireContext(), "Response body is null")
                } else {
                    Constants.error(requireContext(), "Unsuccessful response: ${response.code()}")
                }
            }

            override fun onFailure(call: Call<List<MeetingListModelItem>>, t: Throwable) {
                if (isAdded) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }
            }
        })
    }

    private fun refreshMeetingListDelayed() {
        Handler().postDelayed({
            refreshMeetingList()
        }, 3000)
    }

    private fun isDateValid(meetingFrom: String, meetingTo: String): Boolean {
        val calendarFrom = Calendar.getInstance().apply {
            timeInMillis = parseDateString(meetingFrom)
        }
        val calendarTo = Calendar.getInstance().apply {
            timeInMillis = parseDateString(meetingTo)
        }
        return calendarTo.after(calendarFrom)
    }

    private fun parseDateString(dateString: String): Long {
        val pattern = "yyyy-MM-dd'T'HH:mm"
        val dateFormat = SimpleDateFormat(pattern, Locale.getDefault())
        return dateFormat.parse(dateString)?.time ?: 0
    }

    private fun getToken(): String? {
        return requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
            .getString("jwtToken", null)
    }
}
