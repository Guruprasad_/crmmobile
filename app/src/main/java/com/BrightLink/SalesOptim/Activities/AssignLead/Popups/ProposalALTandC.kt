package com.BrightLink.SalesOptim.Activities.AssignLead.Popups

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.databinding.ProposalTandcBinding

class ProposalALTandC: Fragment() {

    private var _binding: ProposalTandcBinding? = null
    private val binding get() = _binding!!
    private val PREF_NAME = "TandCPrefs"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = ProposalTandcBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.generatePdf.setOnClickListener {

            val deliveryPeriod = binding.delivery.text.toString()
            val advancePayment = binding.advPament.text.toString()
            val termsandConditions = binding.terms.text.toString()
            val note = binding.note.text.toString()
            val senderName = binding.senderName.text.toString()
            val senderNumber = binding.senderNumber.text.toString()

            val sharedPreferences = requireContext().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putString("deliveryPeriod", deliveryPeriod)
            editor.putString("advancePayment", advancePayment)
            editor.putString("termsandConditions", termsandConditions)
            editor.putString("note", note)
            editor.putString("senderName", senderName)
            editor.putString("senderNumber", senderNumber)
            editor.apply()

            Constants.success(context,"Generating PDF")

            findNavController().navigate(R.id.proposalInvoice)
        }


        return root
    }
}