package com.BrightLink.SalesOptim.Activities.LeadDeals

import android.content.Context
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.PopupMenu
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.BrightLink.SalesOptim.Adapters.LeadDealsFragmentAdapter.DealsUntouchAdapter
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.Constants.WrapContentLinearLayoutManager
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.ResponseModel.LeadDeal.DealModel
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.ActivityLeadDealsBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LeadDealsFragment : Fragment() {

    private var _binding: ActivityLeadDealsBinding? = null
    private val binding get() = _binding!!
    private lateinit var untouchAdapter: DealsUntouchAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ActivityLeadDealsBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.leadDealsActionbar.activityName.text = "Lead Deals"
        binding.leadDealsActionbar.back.setOnClickListener{
        requireActivity().onBackPressedDispatcher.onBackPressed()
        }

        binding.leadDealsActionbar.option.setOnClickListener{
            showPopupMenu(it)
        }

        binding.recview.layoutManager = WrapContentLinearLayoutManager(context)

        val jwtToken = getToken()
        if (jwtToken!=null)
        {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            CallApi(service , jwtToken)
        }
        else
        {
            Constants.error(context,"Token is empty Login Again")
        }

        return root
    }

    private fun getToken(): String? {
        val sharedPreferences = requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
        return sharedPreferences.getString("jwtToken", null)
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun CallApi(service: ApiInterface , token : String) {

        service.leadDeals("Bearer $token").enqueue(object : Callback<DealModel>{

            override fun onFailure(call: Call<DealModel>, t: Throwable) {
                Constants.error(context,"Failed to call API ${t.message}")
            }

            override fun onResponse(call: Call<DealModel>, response: Response<DealModel>) {
                if (isAdded)
                {
                    if (response.isSuccessful)
                    {
                        val dataList = response.body()
                        untouchAdapter = DealsUntouchAdapter(requireContext(),dataList)
                        binding.recview.adapter = untouchAdapter
                        binding.untouchPercentage.text = dataList!!.untouchPercentage.toString()
                        binding.untouchAmount.text = dataList.untouchedRevenue.toString()
                        binding.totalLead.text = dataList.untouchedCount.toString()

                    }
                    else
                    {
                        Constants.error(context,"Response is unsuccessful ${response.code()}")
                    }

                }
            }

        })

    }

    private fun showPopupMenu(view: View) {
        val popupMenu = PopupMenu(context, view, Gravity.END, 0, R.style.MenuItemStyle)
        popupMenu.inflate(R.menu.lead_deals_menu) // Inflate your menu resource file here
        popupMenu.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {

                R.id.untouch_lead -> {
                    findNavController().navigate(R.id.lead_deals_frg)
                    true
                }

                R.id.qualified_lead -> {
                    findNavController().navigate(R.id.qualified_deals)
                    true
                }

                R.id.proposal_lead -> {
                    findNavController().navigate(R.id.proposal_deal)
                    true
                }

                R.id.loss_lead -> {
                    findNavController().navigate(R.id.loss_deal)
                    true
                }

                R.id.won_lead -> {
                    findNavController().navigate(R.id.won_deal)
                    true
                }
                else -> false
            }
        }

        popupMenu.setOnDismissListener {
            val animation = AnimationUtils.loadAnimation(context, R.anim.popup_animation)
            view.startAnimation(animation)
        }
        popupMenu.show()
    }

}