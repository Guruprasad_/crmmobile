package com.BrightLink.SalesOptim.Activities.PurchaseOrder

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.BrightLink.SalesOptim.Adapters.PurchaseListAdapter
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.Constants.CustomDialog
import com.BrightLink.SalesOptim.Constants.WrapContentLinearLayoutManager
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.ResponseModel.PurchaseOrderModelItem
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.ActivityPurchaselistBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PurchaseOrderList: Fragment() {

    private var _binding: ActivityPurchaselistBinding? = null
    private lateinit var adapter: PurchaseListAdapter
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = ActivityPurchaselistBinding.inflate(inflater, container, false)
        val root: View = binding.root


        binding.actionBar.activityName.text = "Purchase Order List"
        binding.actionBar.back.setOnClickListener {
            requireActivity().onBackPressed()
        }

        binding.search.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                adapter.filter(s.toString())
            }
        })



        binding.actionBar.saveButton.setOnClickListener {
            Navigation.findNavController(it).navigate(R.id.add_purchaseOrder)
        }


        binding.actionBar.salesOrderInvoice.setOnClickListener {
            findNavController().navigate(R.id.purchaseOrderList)
        }

        binding.purchaseOrderRView.layoutManager = WrapContentLinearLayoutManager(context)

        val jwtToken = gettoken(context)
        if (jwtToken!=null)
        {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            CallApi(service, jwtToken)
        }
        else
        {
            Constants.error(context, "Token is Null Login Again")
        }
        return root
    }
    private fun CallApi(apiservice: ApiInterface, jwtToken: String) {

        val loading = CustomDialog(requireContext())
        loading.show()

        apiservice.purchaseorderdata("Bearer $jwtToken").enqueue(object : Callback<List<PurchaseOrderModelItem>>{
            override fun onResponse(
                call: Call<List<PurchaseOrderModelItem>>,
                response: Response<List<PurchaseOrderModelItem>>
            ) {
                if (isAdded)
                {
                    if(response.isSuccessful)
                    {
                        val data = response.body()
                        if (data!=null)
                        {
                            loading.dismiss()
                            adapter = PurchaseListAdapter(requireContext(), data)
                            binding.purchaseOrderRView.adapter = adapter
                        }
                        else
                        {
                            Constants.error(requireContext(), "Response body is null")
                            loading.dismiss()
                        }
                    }
                    else
                    {
                        Constants.error(requireContext(), "Unsuccessful response: ${response.code()}")
                        loading.dismiss()
                    }
                }
            }

            override fun onFailure(call: Call<List<PurchaseOrderModelItem>>, t: Throwable) {
                if (isAdded) { // Check if the fragment is added to the activity
                    Constants.error(requireContext(), "Error: ${t.message}")
                    loading.dismiss()
                }
            }

        })
    }
    private fun gettoken(context: Context?): String? {
        val sharedPreferences = requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
        return sharedPreferences.getString("jwtToken", null)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}