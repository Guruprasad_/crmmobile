package com.BrightLink.SalesOptim.Activities.ui.Reminder

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.BrightLink.SalesOptim.Adapters.ReminderAdapter
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.Constants.WrapContentLinearLayoutManager
import com.BrightLink.SalesOptim.ResponseModel.TaskListModelItem
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.FragmentReminderBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ReminderFragment : Fragment() {

    private var _binding: FragmentReminderBinding? = null
    private lateinit var adapter : ReminderAdapter
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentReminderBinding.inflate(inflater, container, false)
        val root: View = binding.root
        binding.recyclerView.layoutManager = WrapContentLinearLayoutManager(context)

        val jwtToken = gettoken(context)
        if (jwtToken!=null)
        {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            CallApi(service , jwtToken)
        }
        else{
            Constants.error(requireContext(),"Token is null Login Again")
        }

        // Assuming you have the adapter instantiated and attached to your RecyclerView

        return root
    }

    private fun CallApi(apiservice: ApiInterface, jwtToken: String) {
        val call = apiservice.taskreminder("Bearer $jwtToken")
        call.enqueue(object : Callback<ArrayList<TaskListModelItem>> {
            override fun onResponse(
                call: Call<ArrayList<TaskListModelItem>>,
                response: Response<ArrayList<TaskListModelItem>>
            ) {
                if (isAdded) { // Check if the fragment is added to the activity
                    if (response.isSuccessful) {
                        val data = response.body()
                        if (data != null) {

                            val adapter = ReminderAdapter(requireContext(), data)
                            binding.recyclerView.adapter = adapter
                        } else {
                            Constants.error(requireContext(), "Response body is null")
                        }
                    } else {
                        Constants.error(requireContext(), "Unsuccessful response: ${response.code()}")
                    }
                }
            }

            override fun onFailure(call: Call<ArrayList<TaskListModelItem>>, t: Throwable) {
                if (isAdded) { // Check if the fragment is added to the activity
                    Constants.error(requireContext(), "Error: ${t.message}")
                }
            }
        })
    }

    private fun gettoken(context: Context?): String? {
        val sharedPreferences = requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
        return sharedPreferences.getString("jwtToken", null)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}