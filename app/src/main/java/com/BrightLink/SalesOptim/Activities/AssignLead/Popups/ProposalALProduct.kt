package com.BrightLink.SalesOptim.Activities.AssignLead.Popups

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import com.BrightLink.SalesOptim.Adapters.ProductAdapter
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.Constants.SharedPreferecneManager
import com.BrightLink.SalesOptim.Constants.WrapContentLinearLayoutManager
import com.BrightLink.SalesOptim.Model.ProductModel
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.databinding.ProposalProductBinding

class ProposalALProduct: Fragment() {
    private var _binding: ProposalProductBinding? = null
    private val binding get() = _binding!!
    private val items = mutableListOf<ProductModel>()
    private lateinit var adapter : ProductAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = ProposalProductBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val currencyAdapter = ArrayAdapter.createFromResource(requireContext(),
            R.array.currencies_array, R.layout.spinner_dropdown_layout)
        currencyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.prdCurrency.adapter = currencyAdapter

        adapter = ProductAdapter(items)
        binding.productview.adapter  = adapter
        binding.productview.layoutManager = WrapContentLinearLayoutManager(context)

        binding.addProduct.setOnClickListener {
            val productName = binding.prdName.text.toString()
            val quantity = binding.prdQuantity.text.toString()
            val price = binding.prdPrice.text.toString()
            val tax = binding.prdTax.text.toString()
            val currency = binding.prdCurrency.selectedItem.toString()
            val shippingCharges = binding.prdShipping.text.toString()

            if (validateInput(productName,quantity,price,tax,shippingCharges))
            {
                val item = ProductModel(productName,quantity,price)
                items.add(item)
                adapter.notifyDataSetChanged()
                SharedPreferecneManager.saveItems(requireContext(),items)
                val sharedPreferences = requireContext().getSharedPreferences("product_details", Context.MODE_PRIVATE)
                val editor = sharedPreferences.edit()
                editor.putString("currency", currency)
                editor.putString("tax", tax)
                editor.putString("shippingCharges", shippingCharges)
                editor.apply()
                Constants.success(context,"Product saved successfully")

            }

        }
        binding.removeProduct.setOnClickListener {
            adapter.removeLastItem(requireContext())
            Constants.success(requireContext(),"Product has successfully removed")
        }

        return root
    }
    private fun validateInput(productName: String, quantity: String, price: String, tax: String, shippingCharges: String): Boolean {

        if (productName.isEmpty())
        {
            Constants.error(context,"Product Name Required")
            return false
        }
        if (quantity.isEmpty())
        {
            Constants.error(context,"Quantity Required")
            return false
        }
        if (price.isEmpty())
        {
            Constants.error(context,"Price Required")
            return false
        }
        if (tax.isEmpty())
        {
            Constants.error(context,"Tax is required")
            return false
        }
        if (shippingCharges.isEmpty())
        {
            Constants.error(context,"Shipping charges required")
            return false
        }

        return true

    }
}