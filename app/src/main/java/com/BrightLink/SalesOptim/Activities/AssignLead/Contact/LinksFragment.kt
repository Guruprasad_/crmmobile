package com.BrightLink.SalesOptim.Activities.AssignLead.Contact

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import androidx.fragment.app.Fragment
import com.BrightLink.SalesOptim.Adapters.ALLinkAdapter
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.Constants.WrapContentLinearLayoutManager
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.RequestModel.AddLinkReqModel
import com.BrightLink.SalesOptim.ResponseModel.AddLinkResponseModel
import com.BrightLink.SalesOptim.ResponseModel.LeadLinksModelItem
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.LinksAssignLeadBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LinksFragment : Fragment() {

    private var _binding: LinksAssignLeadBinding? = null
    private val binding get() = _binding!!
    private var dialog: Dialog? = null
    private var leadStatus : String? = null

    @SuppressLint("SuspiciousIndentation")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = LinksAssignLeadBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.actionBar.activityName.text = "Assign Lead"
        binding.actionBar.back.setOnClickListener{
            requireActivity().onBackPressed()
        }
        val sharedPreferences = requireContext().getSharedPreferences("LeadPreferences", Context.MODE_PRIVATE)
        val leadId = sharedPreferences.getInt("leadId", -1)
        leadStatus = sharedPreferences.getString("leadStatus",leadStatus)

        if (leadStatus!= null){

            when(leadStatus)
            {
                "New Lead"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)

                }

                "Contacted"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Email Sent"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Not Contacted"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Qualified"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Not Qualified"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Proposal Sent"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Lead Won"->{
                    binding.actionBar.contactIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Lead Lost"->{
                    binding.actionBar.contactIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }
            }
        }

        binding.recyclerView.layoutManager = WrapContentLinearLayoutManager(requireContext())

        refreshLinksList(leadId)

        binding.addLink.setOnClickListener {
            showDialog(R.layout.account_add_links)

            val addLinkPop = dialog!!.findViewById<Button>(R.id.addLinkPop)
            val addLabel = dialog!!.findViewById<EditText>(R.id.addLabel)
            val addDesc = dialog!!.findViewById<EditText>(R.id.linkDesc)
            val addLinkUrl = dialog!!.findViewById<EditText>(R.id.linkUrl)

                addLinkPop.setOnClickListener {
                    val label = addLabel.text.toString().trim()
                    val desc = addDesc.text.toString().trim()
                    val linkUrl = addLinkUrl.text.toString().trim()

                    if (linkUrl.isEmpty()) {
                        Constants.error(requireContext(), "Link URL cannot be empty")
                    } else {
                        addLinks(label, desc, linkUrl, leadId)
                    }
                }
        }

        return root
    }
    private fun refreshLinksList(leadId: Int) {
        getToken()?.let { jwtToken ->
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            callApi(service, jwtToken, leadId)
        } ?: Constants.error(requireContext(), "Token is null. Please log in again.")

    }

    private fun showDialog(layoutResId: Int) {

        if (dialog == null) {
            dialog = Dialog(requireActivity())
            dialog!!.setCancelable(false)
            dialog!!.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        dialog!!.setContentView(layoutResId)
        dialog!!.show()

        val closeBtn = dialog!!.findViewById<ImageButton>(R.id.closeBtn)
        closeBtn.setOnClickListener {
            dialog!!.dismiss()
        }
    }

    private fun addLinks(label : String, desc : String, linkUrl : String, leadId: Int) {
        val requestBody = AddLinkReqModel(0,0,label,leadId,desc,0,linkUrl)

            val jwtToken = getToken()
            if (jwtToken != null) {
                val service = ApiUtilities.getinstance().create(ApiInterface::class.java)

                val call = service.addLink("Bearer $jwtToken", requestBody)
                call.enqueue(object : Callback<AddLinkResponseModel> {
                    override fun onResponse(
                        call: Call<AddLinkResponseModel>,
                        response: Response<AddLinkResponseModel>
                    ) {
                        if (isAdded) {
                            if (response.isSuccessful) {
                                val responseBody = response.body()
                                Log.d("ResponseBody", "$responseBody")
                                dialog!!.dismiss()
                                Constants.success(requireContext(), "Link added Successfully")
                                refreshLinksList(leadId)
                            } else {
                                Constants.error(requireContext(), "Unsuccessful")
                            }
                        }
                    }
                    override fun onFailure(call: Call<AddLinkResponseModel>, t: Throwable) {
                        Constants.error(requireContext(), "Error: ${t.message}")
                    }
                })
            } else {
                Constants.error(requireContext(), "Token Not Found")
            }
    }


    private fun callApi(service: ApiInterface, jwtToken: String, leadId: Int) {
        val call = service.getLinkListLead("Bearer $jwtToken", leadId)
        call.enqueue(object : Callback<List<LeadLinksModelItem>> {
            override fun onResponse(
                call: Call<List<LeadLinksModelItem>>,
                response: Response<List<LeadLinksModelItem>>
            ) {
                if (isAdded) {
                    if (response.isSuccessful) {
                        val resp = response.body()
                        response.body()?.let { data ->
                            if (resp.isNullOrEmpty()){
                                binding.noLinksFound.visibility = View.VISIBLE
                            }else {
                                binding.noLinksFound.visibility = View.INVISIBLE
                            }
                            val linkClickListener = object : ALLinkAdapter.LinkClickListener {
                                override fun onLinkClick(url: String) {
                                    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                                    context!!.startActivity(intent)
                                }
                            }

                            val adapter = ALLinkAdapter(requireContext(), data, linkClickListener)
                            binding.recyclerView.adapter = adapter
                        } ?: Constants.error(requireContext(), "Response body is null")
                    }
                }
            }

            override fun onFailure(call: Call<List<LeadLinksModelItem>>, t: Throwable) {
                Constants.error(requireContext(), "Error: ${t.message}")
            }

        })
    }

    private fun getToken(): String? {
        return requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
            .getString("jwtToken", null)
    }

}