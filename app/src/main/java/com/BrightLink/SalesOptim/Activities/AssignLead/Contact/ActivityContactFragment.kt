package com.BrightLink.SalesOptim.Activities.AssignLead.Contact

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.BrightLink.SalesOptim.Adapters.ActivityAssignLead.ActivityContactAdapter
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.Constants.WrapContentLinearLayoutManager
import com.BrightLink.SalesOptim.ResponseModel.ActivityStatus.ContactStatus
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.ActivityContactLeadBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ActivityContactFragment : Fragment() {

    private var _binding: ActivityContactLeadBinding? = null
    private val binding get() = _binding!!
    private var leadId : Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = ActivityContactLeadBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.ContactInfoRec.layoutManager = WrapContentLinearLayoutManager(context)


        val sharedPreferences = requireContext().getSharedPreferences("LeadPreferences", Context.MODE_PRIVATE)
        leadId = sharedPreferences.getInt("leadId", -1)
        Log.d("Lead ID","$leadId")


        val jwttoken = getToken()
        if (jwttoken != null){
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            getAllContactStatus(service,jwttoken)
        }else{
            Constants.error(context, "Token is null Login Again")
        }

        return root
    }
    private fun getAllContactStatus(service: ApiInterface, jwtToken: String) {
        val call = service.getAssignContactStatus("Bearer $jwtToken", leadId)
        call.enqueue(object : Callback<ContactStatus> {
            override fun onResponse(
                call: Call<ContactStatus>,
                response: Response<ContactStatus>
            ) {
                if (response.isSuccessful) {
                    response.body()?.let { data ->
                        val adapter = ActivityContactAdapter(requireContext(),data)
                        binding.ContactInfoRec.adapter = adapter
                    }?:Constants.error(requireContext(), "Response body is null")
                } else {
                    Constants.error(requireContext(), "Unsuccessful response: ${response.code()}")
                }
            }
            override fun onFailure(call: Call<ContactStatus>, t: Throwable) {
                Constants.error(requireContext(), "Error: ${t.message}")
                Log.d("omar","${t.message}")
            }

        })
    }

    private fun getToken(): String? {
        return requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
            .getString("jwtToken", null)
    }
}