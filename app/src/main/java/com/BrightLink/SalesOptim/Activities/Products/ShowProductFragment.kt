package com.BrightLink.SalesOptim.Activities.Products

import android.app.Dialog
import android.content.Context
import android.media.Image
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.RequestModel.AddProductPostModel
import com.BrightLink.SalesOptim.RequestModel.AddProductRequestModel
import com.BrightLink.SalesOptim.ResponseModel.AddProductResponseModel
import com.BrightLink.SalesOptim.ResponseModel.ProductResponseModelX
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.FragmentShowProductBinding
import com.google.android.material.bottomsheet.BottomSheetDialog
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ShowProductFragment : Fragment() {

    private lateinit var binding: FragmentShowProductBinding
    private lateinit var productAdapter: ProductAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentShowProductBinding.inflate(inflater, container, false)
        binding.productListRecyclerView.layoutManager = LinearLayoutManager(requireContext())

        binding.actionBar.back.setOnClickListener {
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }

        binding.actionBar.activityName.text = "Products"
        binding.actionBar.option.visibility = View.INVISIBLE


        binding.addProductBtn.setOnClickListener {
            openDialog()
        }

        binding.searchProduct.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                productAdapter.filter(s.toString())
            }
        })

        getAllProduct()

        return binding.root
    }

    private fun openDialog() {
        val dialogView = layoutInflater.inflate(R.layout.fragment_add_product, null)
        val productName = dialogView.findViewById<EditText>(R.id.productName)
        val productQuan = dialogView.findViewById<EditText>(R.id.productQuantity)
        val productPrice = dialogView.findViewById<EditText>(R.id.productUnitPrice)
        val add = dialogView.findViewById<Button>(R.id.addProducts)
        val reset = dialogView.findViewById<Button>(R.id.reset)
        val close = dialogView.findViewById<ImageButton>(R.id.closeButton)

        val dialog = AlertDialog.Builder(requireContext())
            .setView(dialogView)
            .create()
        

        add.setOnClickListener{
            val pname = productName.text.toString()
            val pquan = productQuan.text.toString()
            val pprice = productPrice.text.toString()

            if (TextUtils.isEmpty(pname) || TextUtils.isEmpty(pquan) || TextUtils.isEmpty(pprice))
            {
                Constants.error(requireContext(),"Please fill all the fields")
                return@setOnClickListener
            }
            else
            {
            AddProduct(pname,pquan,pprice , dialog)
            }
        }

        close.setOnClickListener{
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun AddProduct(name: String, quan: String, price: String,dialog: AlertDialog ) {
        val token = gettoken(requireContext())
        val model = AddProductRequestModel(getEmp().toString(), name,quan,price)
        val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
        service.AddProducts("Bearer $token" ,model ).enqueue(object :
            Callback<ProductResponseModelX> {
            override fun onResponse(call: Call<ProductResponseModelX>, response: Response<ProductResponseModelX>) {
                if (response.isSuccessful) {
                    val data = response.body()
                    if (data != null) {
                        Constants.success(requireActivity(), "Product Successfully Added ${data.productName}")
                        dialog.dismiss()
                    } else {
                        Constants.error(requireContext(), "Failed to Add the product ")
                    }
                } else {
                    Constants.error(requireContext(), "Failed to Add the product : ${response.code()}")
                }
            }

            override fun onFailure(call: Call<ProductResponseModelX>, t: Throwable) {
                Constants.error(requireContext(), "${t.message}")
            }
        })
    }




    private fun getAllProduct() {
        val jwtToken = gettoken(requireContext())

        if (jwtToken != null) {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.getAllProducts("Bearer $jwtToken")

            call.enqueue(object : Callback<List<AddProductResponseModel>?> {
                override fun onResponse(
                    call: Call<List<AddProductResponseModel>?>,
                    response: Response<List<AddProductResponseModel>?>
                ) {
                    val responseBody = response.body() ?: emptyList()
                    Log.e("ShowProductFragmentList", responseBody.toString())
                    productAdapter = ProductAdapter(requireContext(), responseBody)
                    binding.productListRecyclerView.adapter = productAdapter
                }

                override fun onFailure(call: Call<List<AddProductResponseModel>?>, t: Throwable) {
                    Log.e("ShowProductFragment", "Error: ${t.message}")
                }
            })
        }
    }
    private fun gettoken(context: Context?): String? {
        val sharedPreferences =
            context?.getSharedPreferences("Token", Context.MODE_PRIVATE)
        return sharedPreferences?.getString("jwtToken", null)
    }

    private fun getEmp(): Int {
        val sharedPreferences = requireContext().getSharedPreferences("UserDetails", Context.MODE_PRIVATE)
        return sharedPreferences.getInt("empID", -1)
    }

}
