package com.BrightLink.SalesOptim.Activities.quotation

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.databinding.FragmentCustomerBinding

class CustomerFragment : Fragment() {

    private var _binding: FragmentCustomerBinding? = null
    private val binding get() = _binding!!
    private val PREF_NAME = "CustomerPrefs"
    private var visibility : Boolean = true

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentCustomerBinding.inflate(inflater, container, false)
        val root: View = binding.root


        binding.visible.setOnClickListener {
            visibility = true
            binding.messageLayout.visibility = View.VISIBLE
            binding.visible.setBackgroundResource(R.color.statusbar)
            val color = ContextCompat.getColor(requireContext(), R.color.white)
            binding.visible.setColorFilter(color)
            val default = ContextCompat.getColor(requireContext(), R.color.black)
            binding.invisible.setColorFilter(default)
            binding.invisible.setBackgroundResource(android.R.color.transparent)
        }

        binding.invisible.setOnClickListener {
            visibility = false
            binding.messageLayout.visibility = View.INVISIBLE
            binding.invisible.setBackgroundResource(R.color.statusbar)
            val color = ContextCompat.getColor(requireContext(), R.color.white)
            binding.invisible.setColorFilter(color)
            val default = ContextCompat.getColor(requireContext(), R.color.black)
            binding.visible.setColorFilter(default)
            binding.visible.setBackgroundResource(android.R.color.transparent)
        }


        binding.save.setOnClickListener {

            val companyName = binding.companyName.text.toString()
            val customerName = binding.customerName.text.toString()
            val address = binding.address.text.toString()
            val phone = binding.phone.text.toString()
            val subject = binding.subject.text.toString()
            val message  = binding.message.text.toString()


            val sharedPreferences = requireContext().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putString("companyName", companyName)
            editor.putString("customerName", customerName)
            editor.putString("address", address)
            editor.putString("phone", phone)
            editor.putString("subject", subject)
            editor.putString("message", message)
            editor.putBoolean("visibility", visibility)
            editor.apply()

            Constants.success(context , "Customer details saved successfully")
        }

        return root
    }

}