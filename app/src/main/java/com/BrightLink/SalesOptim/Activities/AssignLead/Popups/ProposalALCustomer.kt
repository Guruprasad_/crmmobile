package com.BrightLink.SalesOptim.Activities.AssignLead.Popups

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.ResponseModel.LeadDetailResponseModel
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.ProposalCustomerBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProposalALCustomer : Fragment() {
    private var _binding: ProposalCustomerBinding? = null
    private val binding get() = _binding!!
    private val PREF_NAME = "CustomerPrefs"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = ProposalCustomerBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.saveCustomer.setOnClickListener {
            val companyName = binding.companyName.text.toString()
            val customerName = binding.customerName.text.toString()
            val address = binding.customerAddress.text.toString()
            val phone = binding.customerPhone.text.toString()
            val subject = binding.customerSub.text.toString()
            val message  = binding.customerMsg.text.toString()

            val sharedPreferences = requireContext().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putString("companyName", companyName)
            editor.putString("customerName", customerName)
            editor.putString("address", address)
            editor.putString("phone", phone)
            editor.putString("subject", subject)
            editor.putString("message", message)
            editor.apply()

            Constants.success(context , "Customer details saved successfully")
        }

        return root
    }

    override fun onStart() {
        super.onStart()
        val leadId = getLead()
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null) {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.getLeadDetails("Bearer $jwtToken" , leadId)
            call.enqueue(object : Callback<LeadDetailResponseModel> {
                override fun onResponse(
                    call: Call<LeadDetailResponseModel>,
                    response: Response<LeadDetailResponseModel>
                ) {
                    if (isAdded) {
                        if (response.isSuccessful && response.body() !=null) {
                            val data = response.body()
                            if (data != null) {
                                binding.customerName.setText(data.customerName)
                                binding.companyName.setText(data.companyName)
                                binding.customerAddress.setText(data.address)
                                binding.customerPhone.setText(data.mobileNumber)
                            }
                        } else {
                            Constants.error(requireContext(), "Unsuccessful Response ${response.code()}")
                        }
                    }
                }

                override fun onFailure(call: Call<LeadDetailResponseModel>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }
            })
        } else {
            Constants.error(requireContext(), "Token Not Found")
        }
    }
    private fun gettoken(context: Context?): String? {
        val sharedPreferences = requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
        return sharedPreferences.getString("jwtToken", null)
    }

    private fun getLead() : Int{
        val sharedPreferences = requireContext().getSharedPreferences("LeadPreferences", Context.MODE_PRIVATE)
        return sharedPreferences.getInt("leadId",-1)
    }
}
