package com.BrightLink.SalesOptim.Activities.Products

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.RequestModel.AddProductRequestModel
import com.BrightLink.SalesOptim.RequestModel.UpdateProductRequestModel
import com.BrightLink.SalesOptim.ResponseModel.AddProductResponseModel
import com.BrightLink.SalesOptim.ResponseModel.ProductResponseModelX
import com.BrightLink.SalesOptim.ResponseModel.ShowLeadsModelItem
import com.BrightLink.SalesOptim.ResponseModel.StateModelItem
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.HeadingsProductListBinding
import com.google.android.material.bottomsheet.BottomSheetDialog
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.Locale


class ProductAdapter(private val context: Context, private var products: List<AddProductResponseModel>) :
    RecyclerView.Adapter<ProductAdapter.ViewHolder>() {

    inner class ViewHolder(val binding: HeadingsProductListBinding) : RecyclerView.ViewHolder(binding.root)

    private var filteredList: List<AddProductResponseModel> = products

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            HeadingsProductListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return filteredList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentItem = filteredList[position]
        holder.binding.ProductName.text = currentItem.productName
        holder.binding.ProductQuantity.text = currentItem.quantity
        holder.binding.UnitPrice.text = currentItem.unitPrice

        holder.binding.layout.startAnimation(
            AnimationUtils.loadAnimation(
                holder.itemView.context,
                R.anim.recycler_view_anime_2
            )
        )

        holder.binding.updateProduct.setOnClickListener {
            showProductDetailsDialog(currentItem)
        }

        holder.binding.deleteProduct.setOnClickListener {
            deleteProduct(currentItem.productId)
        }
    }


    fun filter(text: String) {
        val searchText = text.lowercase(Locale.getDefault())
        filteredList = if (searchText.isEmpty()) {
            products
        } else {
            products.filter { item ->
                item.productName.lowercase(Locale.getDefault()).contains(searchText) ||
                        item.unitPrice.lowercase(Locale.getDefault()).contains(searchText)

            }
        }
        notifyDataSetChanged()
    }

    private fun showProductDetailsDialog(currentItem: AddProductResponseModel) {
        val dialogView = LayoutInflater.from(context).inflate(R.layout.fragment_update_product, null)
        val productName = dialogView.findViewById<EditText>(R.id.productNameUpdate)
        val productQuan = dialogView.findViewById<EditText>(R.id.productQuantityUpdate)
        val productPrice = dialogView.findViewById<EditText>(R.id.productUnitPriceUpdate)
        val add = dialogView.findViewById<Button>(R.id.updateProduct)
        val reset = dialogView.findViewById<Button>(R.id.reset)
        val close = dialogView.findViewById<ImageButton>(R.id.imageButton)

        val dialog = AlertDialog.Builder(context)
            .setView(dialogView)
            .create()
        
        productName.setText(currentItem.productName)
        productQuan.setText(currentItem.quantity)
        productPrice.setText(currentItem.unitPrice)

        add.setOnClickListener {
            val pname = productName.text.toString()
            val pquan = productQuan.text.toString()
            val pprice = productPrice.text.toString()
            
            if (TextUtils.isEmpty(pname) || TextUtils.isEmpty(pquan) || TextUtils.isEmpty(pprice)) {
               Constants.error(context,"please fill all the fields")
                return@setOnClickListener
            } else {
                UpdateProduct(pname, pquan, pprice, dialog,currentItem.productId)
            }
        }

        close.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun UpdateProduct(pname: String, pquan: String, pprice: String, dialog: AlertDialog , productId: Int) {

        val token = gettoken(context)
        val model = UpdateProductRequestModel(getEmp(),productId,pname,pquan,pprice.toDouble())
        val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
        service.updateProduct("Bearer $token" ,model ).enqueue(object :
            Callback<UpdateProductRequestModel> {
            override fun onResponse(call: Call<UpdateProductRequestModel>, response: Response<UpdateProductRequestModel>) {
                if (response.isSuccessful) {
                    val data = response.body()
                    if (data != null) {
                        Constants.success(context, "Product Updated Successfully Added ${data.productName}")
                        dialog.dismiss()
                        refreshAllProduct()
                    } else {
                        Constants.error(context, "Failed to Update the product ")
                    }
                } else {
                    Constants.error(context, "Failed to update the product : ${response.code()}")
                }
            }

            override fun onFailure(call: Call<UpdateProductRequestModel>, t: Throwable) {
                Constants.error(context, "${t.message}")
            }
        })

    }


    private fun deleteProduct(productId: Int) {
        Log.d("Delete Product for id :", productId.toString())

        val jwtToken = gettoken(context)

        if (jwtToken != null) {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.deleteProduct("Bearer $jwtToken", productId)

            call.enqueue(object : Callback<StateModelItem?> {
                override fun onResponse(call: Call<StateModelItem?>, response: Response<StateModelItem?>) {
                    if (response.isSuccessful) {
                        Constants.success(context, "Product Deleted Successfully")
                        refreshAllProduct()
                    } else {
                        Constants.error(context, "Unsuccessful Response: ${response.code()}")
                    }
                }

                override fun onFailure(call: Call<StateModelItem?>, t: Throwable) {
                    Constants.error(context, "Error : ${t.message}")
                }
            })
        }
    }

    fun refreshAllProduct() {
        val jwtToken = gettoken(context)

        if (jwtToken != null) {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.getAllProducts("Bearer $jwtToken")

            call.enqueue(object : Callback<List<AddProductResponseModel>?> {
                override fun onResponse(
                    call: Call<List<AddProductResponseModel>?>,
                    response: Response<List<AddProductResponseModel>?>
                ) {
                    val responseBody = response.body() ?: emptyList()
                    products = responseBody
                    filteredList = responseBody
                    notifyDataSetChanged()
                }

                override fun onFailure(call: Call<List<AddProductResponseModel>?>, t: Throwable) {
                    Constants.error(context, "Error: ${t.message}")
                }
            })
        }
    }

    private fun gettoken(context: Context?): String? {
        // Get SharedPreferences instance
        val sharedPreferences = context?.getSharedPreferences("Token", Context.MODE_PRIVATE)
        // Retrieve the token from SharedPreferences
        return sharedPreferences?.getString("jwtToken", null)
    }

    private fun getEmp(): Int {
        val sharedPreferences = context.getSharedPreferences("UserDetails", Context.MODE_PRIVATE)
        return sharedPreferences.getInt("empID", -1)
    }
}
