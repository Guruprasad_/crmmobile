package com.BrightLink.SalesOptim.Activities.AssignLead.Popups

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.BrightLink.SalesOptim.Activities.SalesOrderInvoice.ColorPickerDialogFragment
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.databinding.ProposalOwnBinding
import com.google.android.material.datepicker.MaterialDatePicker
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class ProposalALOwn: Fragment()  {
    private var _binding: ProposalOwnBinding? = null
    private val binding get() = _binding!!

    private val PREF_NAME = "OwnerPref"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = ProposalOwnBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.date.setOnClickListener {
            openDatePicker()
        }
        binding.saveOwn.setOnClickListener {

            val companyName = binding.comapnyName.text.toString()
            val address = binding.ownAddress.text.toString()
            val phone = binding.ownPhone.text.toString()
            val email = binding.ownEmail.text. toString()
            val date = binding.date.text.toString()
            val quotationId = binding.owmQuotationId.text.toString()

            val sharedPreferences = requireContext().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()

            editor.putString("companyName", companyName)
            editor.putString("address", address)
            editor.putString("phone", phone)
            editor.putString("email", email)
            editor.putString("date", date)
            editor.putString("quotationId", quotationId)
            editor.apply()

            Constants.success(context , "Owner details saved successfully")
        }


        return root
    }
    fun color(colorr : String): ColorPickerDialogFragment {
        val colorPickerDialogFragment = ColorPickerDialogFragment()
        colorPickerDialogFragment.setColorPickerListener(object :
            ColorPickerDialogFragment.ColorPickerListener {
            override fun onColorSelected(color: Int) {

                val hexColor = String.format("#%06X", 0xFFFFFF and color)

                val sharedPreferences =
                    requireContext().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
                val editor = sharedPreferences.edit()
                editor.putString(colorr, hexColor)
                editor.apply()

                when (colorr) {
                    "background" -> binding.background.setBackgroundColor(color)
                    "header" -> binding.head.setBackgroundColor(color)
                    "text" -> binding.text.setBackgroundColor(color)
                }
            }
        })
        return colorPickerDialogFragment
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.backgroundclr.setOnClickListener {
            color("background").show(childFragmentManager, "colorPicker")
            // binding.background.setBackgroundColor(android.graphics.Color.parseColor("background"))
        }
        binding.headingsclr.setOnClickListener {
            color("header").show(childFragmentManager,"colorPicker")
        }
        binding.textClr.setOnClickListener {
            color("text").show(childFragmentManager,"colorPicker")
        }

    }
    private fun openDatePicker() {
        val builder = MaterialDatePicker.Builder.datePicker().build()

        builder.addOnPositiveButtonClickListener { selection ->
            val dateFormat = SimpleDateFormat("MM/dd/yyyy", Locale.US)
            val selectedDate = dateFormat.format(Date(selection))
            binding.date.text = selectedDate
        }
        builder.show(requireActivity().supportFragmentManager, builder.toString())
    }

    override fun onStart() {
        super.onStart()
        val sharedPreferences = requireContext().getSharedPreferences("UserDetails", Context.MODE_PRIVATE)
        val companyName = sharedPreferences.getString("companyName", "")
        val companyPhone = sharedPreferences.getString("companyPhone", "")
        val companyAddress = sharedPreferences.getString("companyAddress", "")
        val email = sharedPreferences.getString("companyEmail", "")

        binding.comapnyName.setText(companyName)
        binding.ownAddress.setText(companyAddress)
        binding.ownPhone.setText(companyPhone)
        binding.ownEmail.setText(email)
    }



}