package com.BrightLink.SalesOptim.Activities.quotation

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import androidx.fragment.app.Fragment
import com.BrightLink.SalesOptim.Adapters.ProductAdapter
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.Constants.SharedPreferecneManager
import com.BrightLink.SalesOptim.Constants.WrapContentLinearLayoutManager
import com.BrightLink.SalesOptim.Model.ProductModel
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.RequestModel.AddProductRequestModel
import com.BrightLink.SalesOptim.ResponseModel.ProductResponseModel
import com.BrightLink.SalesOptim.ResponseModel.ProductResponseModelX
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.FragmentProductBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProductFragment : Fragment() {

    private var _binding: FragmentProductBinding? = null
    private val binding get() = _binding!!

    private val items = mutableListOf<ProductModel>()
    private lateinit var adapter : ProductAdapter




    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentProductBinding.inflate(inflater, container, false)
        val root: View = binding.root


        val currencyAdapter = ArrayAdapter.createFromResource(requireContext(),
            R.array.currencies_array,R.layout.spinner_item_layout_currency)
        currencyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.currency.adapter = currencyAdapter

        val taxAdapter = ArrayAdapter.createFromResource(requireContext(),
            R.array.Tax_Type,R.layout.spinner_item_layout_currency)
        taxAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.taxType.adapter = taxAdapter


        adapter = ProductAdapter(items)
        binding.productview.adapter  = adapter
        binding.productview.layoutManager = WrapContentLinearLayoutManager(context)

        binding.productAdd.setOnClickListener {
            val dialogView = layoutInflater.inflate(R.layout.product_dialog_layout, null)

            val dialogBuilder = AlertDialog.Builder(requireContext())
                .setView(dialogView)
            val dialog = dialogBuilder.create()

            val closeButton : ImageButton = dialogView.findViewById(R.id.closeButton)
            closeButton.setOnClickListener {
                dialog.dismiss()
            }

            val productName : EditText = dialogView.findViewById(R.id.productName)
            val productQuan : EditText = dialogView.findViewById(R.id.productQuantity)
            val productPrice : EditText = dialogView.findViewById(R.id.productUnitPrice)
            val reset : Button = dialogView.findViewById(R.id.reset)
            val add : Button = dialogView.findViewById(R.id.addProducts)

            reset.setOnClickListener{
                productName.text = null
                productQuan.text = null
                productPrice.text = null
            }

            add.setOnClickListener{

                val productName = productName.text.toString()
                val productQuantity = productQuan.text.toString()
                val productPrice = productPrice.text.toString()

                if (productName.isEmpty() || productQuantity.isEmpty() || productPrice.isEmpty())
                {
                    Constants.error(requireContext(),"Please fill all the credentials")
                    return@setOnClickListener
                }
                else
                {
                    AddProduct(gettoken() , productName, productQuantity,productPrice , dialog)
                }

            }

            dialog.show()

        }

        binding.addproduct.setOnClickListener {

            val productName = binding.productName.selectedItem.toString()
            val quantity = binding.quantity.text.toString()
            val price = binding.price.text.toString()
            val tax = binding.tax.text.toString()
            val selectTax = binding.taxType.selectedItem.toString()
            val currency = binding.currency.selectedItem.toString()
            val shippingCharges = binding.shippingCharges.text.toString()
            val discountValue = binding.discountValue.text.toString()

             if (validateInput(productName,quantity,price,tax,shippingCharges , discountValue))
             {
                 val item = ProductModel(productName,quantity,price)
                 items.add(item)
                 adapter.notifyDataSetChanged()
                 SharedPreferecneManager.saveItems(requireContext(),items)
                 val sharedPreferences = requireContext().getSharedPreferences("product_details", Context.MODE_PRIVATE)
                 val editor = sharedPreferences.edit()
                 editor.putString("currency", currency)
                 editor.putString("tax", tax)
                 editor.putString("shippingCharges", shippingCharges)
                 editor.putString("selectedTax", selectTax)
                 editor.putString("discountValue", discountValue)
                 editor.apply()
                 Constants.success(context,"Product saved successfully")
             }

        }

        binding.remove.setOnClickListener {
            adapter.removeLastItem(requireContext())
            Constants.success(requireContext(),"Product has successfully removed")
        }

        return root
    }

    private fun AddProduct(token: String? , productName : String , productQuan : String , productPrice : String , dialog: AlertDialog) {

        val model = AddProductRequestModel(getEmp().toString(), productName,productQuan,productPrice)
        val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
        service.AddProducts("Bearer $token" ,model ).enqueue(object :
            Callback<ProductResponseModelX> {
            override fun onResponse(call: Call<ProductResponseModelX>, response: Response<ProductResponseModelX>) {
                if (response.isSuccessful) {
                    val data = response.body()
                    if (data != null) {
                        Constants.success(requireActivity(), "Product Successfully Added ${data.productName}")
                        dialog.dismiss()
                    } else {
                        Constants.error(requireContext(), "Failed to Add the product ")
                    }
                } else {
                    Constants.error(requireContext(), "Failed to Add the product : ${response.code()}")
                }
            }

            override fun onFailure(call: Call<ProductResponseModelX>, t: Throwable) {
                Constants.error(requireContext(), "${t.message}")
            }
        })
    }

    private fun validateInput(productName: String, quantity: String, price: String, tax: String, shippingCharges: String , discountValue : String): Boolean {

        if (productName.isEmpty())
        {
            Constants.error(context,"Product Name Required")
            return false
        }
        if (quantity.isEmpty())
        {
            Constants.error(context,"Quantity Required")
            return false
        }
        if (price.isEmpty())
        {
            Constants.error(context,"Price Required")
            return false
        }
        if (tax.isEmpty())
        {
            Constants.error(context,"Tax is required")
            return false
        }
        if (shippingCharges.isEmpty())
        {
            Constants.error(context,"Shipping charges required")
            return false
        }
        if (discountValue.isEmpty())
        {
            Constants.error(context,"Discount Value Required")
            return false
        }

        return true

    }

    private fun gettoken(): String? {
        val sharedPreferences = requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
        return sharedPreferences.getString("jwtToken", null)
    }

    private fun getEmp(): Int {
        val sharedPreferences = requireContext().getSharedPreferences("UserDetails", Context.MODE_PRIVATE)
        return sharedPreferences.getInt("empID", -1)
    }

    override fun onStart() {
        super.onStart()

        val token = gettoken()
        if (token!=null)
        {
            FetchProducts(token)
        }
        else
        {
            Constants.error(requireContext(),"Token is Null")
        }

    }

    private fun FetchProducts(token: String) {
        val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
        service.getProducts("Bearer $token").enqueue(object :
            Callback<ProductResponseModel> {
            override fun onResponse(call: Call<ProductResponseModel>, response: Response<ProductResponseModel>) {
                if (response.isSuccessful) {
                    val vendorList = response.body()
                    if (vendorList != null) {
                        // Extract vendor names from the list of vendor objects
                        val ProductNames = vendorList.map { it.productName }

                        // Create an ArrayAdapter with the vendor names
                        val adapter = ArrayAdapter(
                            requireContext(),
                            android.R.layout.simple_spinner_dropdown_item,
                            ProductNames
                        )
                        binding.productName.setAdapter(adapter)
                    } else {
                        Constants.error(requireContext(), "Product list is null")
                    }
                } else {
                    Constants.error(requireContext(), "Failed to fetch Product list: ${response.code()}")
                }
            }

            override fun onFailure(call: Call<ProductResponseModel>, t: Throwable) {
                Constants.error(requireContext(), "Error fetching Product list: ${t.message}")
            }
        })
    }


}