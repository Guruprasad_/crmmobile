package com.BrightLink.SalesOptim.Activities.SalesOrderInvoice

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.databinding.FragmentShippingBinding

class ShippingFragment : Fragment() {

    private lateinit var binding: FragmentShippingBinding
    private val PREF_NAME = "SalesPrefs"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentShippingBinding.inflate(layoutInflater)

        binding.save.setOnClickListener {

            val customerName = binding.customerName.text.toString()
            val companyName = binding.companyName.text.toString()
            val companyAddress = binding.companyAddress.text.toString()
            val customerPhone = binding.customerPhone.text.toString()

            val sharedPreferences =
                requireContext().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putString("ship_customerName", customerName)
            editor.putString("ship_companyName", companyName)
            editor.putString("ship_companyAddress", companyAddress)
            editor.putString("ship_customer_phone", customerPhone)
            editor.apply()


            Constants.success(context , "Shipping details saved successfully")
        }



        return binding.root
    }

}