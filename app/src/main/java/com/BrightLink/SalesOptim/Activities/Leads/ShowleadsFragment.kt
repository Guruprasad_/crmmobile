package com.BrightLink.SalesOptim.Activities.Leads

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.BrightLink.SalesOptim.Adapters.LeadAdapter
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.Constants.WrapContentLinearLayoutManager
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.ResponseModel.ShowLeadsModelItem
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.ActivityLeadBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ShowleadsFragment : Fragment(){
    private var _binding: ActivityLeadBinding? = null
    private lateinit var adapter : LeadAdapter
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ActivityLeadBinding.inflate(inflater, container, false)
        val root: View = binding.root


        binding.addlead.setOnClickListener{
            findNavController().navigate(R.id.addLead)
        }

        binding.actionBar.activityName.text = "Lead"
        binding.actionBar.back.setOnClickListener{
           requireActivity().onBackPressedDispatcher.onBackPressed()
        }

        binding.actionBar.option.setOnClickListener{view->
            showPopupMenu(view)
        }

        binding.addlead.setOnClickListener{
            findNavController().navigate(R.id.addLead)
        }

        binding.search.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                if (::adapter.isInitialized) {
                    adapter.filter(s.toString())
                }
            }
        })

        binding.recyclerView.layoutManager = WrapContentLinearLayoutManager(context)

       val jwtToken = gettoken(context)
        if (jwtToken!=null)
        {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            CallApi(service , jwtToken , context)
        }
        else{
            Constants.error(context,"Login Again")
        }


        return root
    }


    private fun CallApi(apiservice: ApiInterface, jwtToken: String, context: Context?) {
        context ?: return
        val call = apiservice.showleads("Bearer $jwtToken")
        call.enqueue(object : Callback<List<ShowLeadsModelItem>> {
            override fun onResponse(
                call: Call<List<ShowLeadsModelItem>>,
                response: Response<List<ShowLeadsModelItem>>
            ) {
                if (isAdded) {
                    if (response.isSuccessful) {
                        val data = response.body()
                        if (data != null && data.isNotEmpty()) {
                            adapter = LeadAdapter(context, data)
                            binding.recyclerView.adapter = adapter
                        } else {
                            binding.noLeadsFound.visibility = View.VISIBLE
                        }
                    } else {
                        Constants.error(context, "Unsuccessful Response,\n Try again later")
                    }
                }
            }

            override fun onFailure(call: Call<List<ShowLeadsModelItem>>, t: Throwable) {
                Constants.error(context, "Failed to get Leads")
            }
        })
    }

    private fun gettoken(context: Context?): String? {
        val sharedPreferences = requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
        return sharedPreferences.getString("jwtToken", null)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun showPopupMenu(view: View) {
        val popupMenu = PopupMenu(context, view, Gravity.END, 0, R.style.MenuItemStyle)
        popupMenu.inflate(R.menu.menu_main) // Inflate your menu resource file here

        popupMenu.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.leadreport -> {
                   findNavController().navigate(R.id.leadreport)
                    true
                }
                else -> false
            }
        }
        popupMenu.show()
    }



}