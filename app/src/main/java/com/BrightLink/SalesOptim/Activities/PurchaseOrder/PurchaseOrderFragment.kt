package com.BrightLink.SalesOptim.Activities.PurchaseOrder

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.BrightLink.SalesOptim.Adapters.PurchaseOrderAdapter
import com.BrightLink.SalesOptim.databinding.FragmentPurchaseOrderInvoiceBinding
import com.google.android.material.tabs.TabLayout


class PurchaseOrderFragment : Fragment() {

    private var _binding: FragmentPurchaseOrderInvoiceBinding? = null
    private lateinit var adapter : PurchaseOrderAdapter
    private val binding get() = _binding!!
    private lateinit var tabLayout: TabLayout
    private lateinit var viewPager2: ViewPager2
    private val PREF_NAME = "MyPrefs"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentPurchaseOrderInvoiceBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.actionBar.activityName.text = "Purchase Order"
        binding.actionBar.back.setOnClickListener{
           requireActivity().onBackPressed()
        }

        tabLayout = binding.tabs
        viewPager2 = binding.viewPager

        adapter = PurchaseOrderAdapter(requireActivity().supportFragmentManager, lifecycle)

        binding.tabs.addTab(tabLayout.newTab().setText("Own"))
        binding.tabs.addTab(tabLayout.newTab().setText("Billing"))
        binding.tabs.addTab(tabLayout.newTab().setText("Shipping"))
        binding.tabs.addTab(tabLayout.newTab().setText("Product"))
        binding.tabs.addTab(tabLayout.newTab().setText("T & C"))

        binding.viewPager.adapter = adapter

        tabLayout.addOnTabSelectedListener(object: TabLayout.OnTabSelectedListener{
            override fun onTabSelected(tab: TabLayout.Tab?) {
                if (tab != null) {
                    viewPager2.currentItem = tab.position
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {}

            override fun onTabReselected(tab: TabLayout.Tab?) {}

        })

        viewPager2.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback(){
            override fun onPageSelected(position: Int){
                super.onPageSelected(position)
                tabLayout.selectTab(tabLayout.getTabAt(position))
            }
        })

        return root


    }
    override fun onDestroyView() {
        super.onDestroyView()
        val prefs = requireActivity().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        val editor = prefs.edit()
        editor.clear()
        editor.apply()

            val Productprefs = requireActivity().getSharedPreferences("PurchaseOrderProduct", Context.MODE_PRIVATE)
            val producteditor = Productprefs.edit()
        producteditor.clear()
        producteditor.apply()

        val ProductDetailprefs = requireActivity().getSharedPreferences("Purchase_Product_Details", Context.MODE_PRIVATE)
        val productDetaileditor = ProductDetailprefs.edit()
        productDetaileditor.clear()
        productDetaileditor.apply()


    }
}