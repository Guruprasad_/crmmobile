package com.BrightLink.SalesOptim.Activities.Vendors

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.BrightLink.SalesOptim.Adapters.VendorAdapter
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.Constants.WrapContentLinearLayoutManager
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.ResponseModel.ShowVendorModelItem
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.VendorListBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ShowVendorsFragment : Fragment() {

    private var bindingg  : VendorListBinding?= null
    private val binding get() = bindingg!!

    private lateinit var adapter : VendorAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        bindingg = VendorListBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.actionBar.back.setOnClickListener{
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }

        binding.AddVendorBtn.setOnClickListener{
            findNavController().navigate(R.id.add_vendor)
        }

        binding.actionBar.activityName.text = "Vendor List"
        binding.actionBar.option.visibility = View.INVISIBLE

        binding.searchvendor.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                adapter.filter(s.toString())
            }
        })

        binding.recyclerViewvendor.layoutManager = WrapContentLinearLayoutManager(context)

        val jwtToken = gettoken(context)
        if (jwtToken!=null)
        {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            CallApi(service , jwtToken)
        }
        else{
            Constants.error(context,"Please try to Login Again")
        }
        return root
    }
    private fun CallApi(apiservice : ApiInterface, jwtToken : String ) {
        val call = apiservice.showvendor("Bearer $jwtToken")
        call.enqueue(object : Callback<List<ShowVendorModelItem>> {
            override fun onResponse(
                call: Call<List<ShowVendorModelItem>>,
                response: Response<List<ShowVendorModelItem>>
            ) {
                if (isAdded)
                {
                    if (response.isSuccessful){
                        val data = response.body()
                        if (data!=null)
                        {
                            adapter = VendorAdapter(requireContext(), data)
                            adapter.setOnItemClickListener(object : VendorAdapter.onItemClickListener
                            {
                                override fun onItemClick(position: Int)
                                {
                                    val selectedItem = data[position]
                                    val bundle = Bundle().apply {
                                        putSerializable("selectedItem", selectedItem)
                                    }
                                    findNavController().navigate(R.id.update_Vendor, bundle)
                                }
                            })
                            binding.recyclerViewvendor.adapter = adapter
                        }
                        else
                        {
                            Constants.error(requireContext(), "Vendor List is Empty")
                        }
                    }else
                    {
                        Constants.error(requireContext(), "Something went wrong, Try again")

                    }

                }
            }
            override fun onFailure(call: Call<List<ShowVendorModelItem>>, t: Throwable) {
                Constants.error(context,"Failed to Fetch Vendor List")
            }

        })
    }
    private fun gettoken(context: Context?): String? {
        val sharedPreferences = requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
        return sharedPreferences.getString("jwtToken", null)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        bindingg = null
    }
}