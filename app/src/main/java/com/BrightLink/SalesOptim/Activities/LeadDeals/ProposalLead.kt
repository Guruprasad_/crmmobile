package com.BrightLink.SalesOptim.Activities.LeadDeals

import android.content.Context
import android.os.Bundle
import android.view.Gravity
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.PopupMenu
import androidx.navigation.fragment.findNavController
import com.BrightLink.SalesOptim.Adapters.LeadDealsFragmentAdapter.DealsProposalAdapter
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.Constants.WrapContentLinearLayoutManager
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.ResponseModel.LeadDeal.DealModel
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.FragmentProposalLeadBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ProposalLead : Fragment() {

    private var _binding: FragmentProposalLeadBinding? = null
    private val binding get() = _binding!!
    private lateinit var adapter: DealsProposalAdapter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?

    ): View? {
        _binding =FragmentProposalLeadBinding.inflate(inflater, container, false)
        val root: View = binding.root
        binding.actionBar.activityName.text = "Proposal Leads"
        binding.actionBar.back.setOnClickListener{
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }

        binding.actionBar.option.setOnClickListener{
            showPopupMenu(it)
        }



        binding.recview.layoutManager = WrapContentLinearLayoutManager(context)

        val jwtToken = getToken()
        if (jwtToken!=null)
        {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            CallApi(service , jwtToken)
        }
        else
        {
            Constants.error(context,"Token is empty Login Again")
        }



        return root
    }

    private fun getToken(): String? {
        val sharedPreferences = requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
        return sharedPreferences.getString("jwtToken", null)
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun CallApi(service: ApiInterface , token : String) {

        service.leadDeals("Bearer $token").enqueue(object : Callback<DealModel> {
            override fun onResponse(
                call: Call<DealModel>,
                response: Response<DealModel>
            ) {
                if (isAdded) {
                    if (response.isSuccessful) {
                        val data = response.body()
                        adapter = DealsProposalAdapter(requireContext(), data)
                        binding.recview.adapter = adapter
                        binding.percentage.text = data!!.proposalPercentage.toString()
                        binding.amount.text = data.proposalReveune.toString()
                        binding.totalLead.text = data.proposalCount.toString()

                    } else {
                        Constants.error(context, "Response is unsuccessful ${response.code()}")
                    }

                }
            }

            override fun onFailure(call: Call<DealModel>, t: Throwable) {
                Constants.error(context, "Failed to call API ${t.message}")
            }

        })

    }

    private fun showPopupMenu(view: View) {
        val popupMenu = PopupMenu(context, view, Gravity.END, 0, R.style.MenuItemStyle)
        popupMenu.inflate(R.menu.lead_deals_menu) // Inflate your menu resource file here
        popupMenu.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {

                R.id.untouch_lead -> {
                    findNavController().navigate(R.id.lead_deals_frg)
                    true
                }

                R.id.qualified_lead -> {
                    findNavController().navigate(R.id.qualified_deals)
                    true
                }

                R.id.proposal_lead -> {
                    findNavController().navigate(R.id.proposal_deal)
                    true
                }

                R.id.loss_lead -> {
                    findNavController().navigate(R.id.loss_deal)
                    true
                }

                R.id.won_lead -> {
                    findNavController().navigate(R.id.won_deal)
                    true
                }
                else -> false
            }
        }

        popupMenu.setOnDismissListener {
            val animation = AnimationUtils.loadAnimation(context, R.anim.popup_animation)
            view.startAnimation(animation)
        }
        popupMenu.show()
    }



}