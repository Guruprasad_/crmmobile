package com.BrightLink.SalesOptim.Activities.Sales

import android.R
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.OpenableColumns
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.MimeTypeMap
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.Constants.CustomDialog
import com.BrightLink.SalesOptim.RequestModel.CreatePurchaseModel
import com.BrightLink.SalesOptim.ResponseModel.SalesOrderResponseModel
import com.BrightLink.SalesOptim.ResponseModel.VendorListResponseModelItem
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.AddSalesBinding
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class AddSalesFragment : Fragment() {

    object FileTypeHelper {

        fun getMediaTypeFromFileExtension(extension: String): MediaType? {
            val mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
            return mimeType?.let { it.toMediaTypeOrNull() }
        }
    }


    private var _binding: AddSalesBinding? = null
    private val binding get() = _binding!!
    private lateinit var path : String
    private var filePath: String? = null
    private val PICKFILE_RESULT_CODE = 1
    private  lateinit var selectedVendorId : String
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = AddSalesBinding.inflate(inflater, container, false)
        val root: View = binding.root
        binding.actionBar.activityName.text = "Purchase Order List"
        binding.actionBar.back.setOnClickListener {
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }
        val token = gettoken(requireContext())
        if (token!=null)
        {
            fetchVendorListFromApi(token)
        }
        else
        {
            Constants.error(requireContext(),"Token is Null")
        }

        val name  = getUserName().toString()
        binding.createdBy.setText(name)



        binding.submitOrder.setOnClickListener {
            val createdBy = binding.createdBy.text.toString()
            val sendingTo = binding.sendingTo.text.toString()
            if (createdBy.isEmpty()  || sendingTo.isEmpty())
            {
                Constants.error(requireContext(),"Please Fill All the Credentials")
                return@setOnClickListener
            }
            AddAttachment(createdBy,sendingTo, selectedVendorId,)
        }


        binding.resetOrder.setOnClickListener {
            binding.sendingTo.text = null
        }

        binding.linkFileOneChoose.setOnClickListener {
            showFileChooser()
        }

        return root
    }

    private fun AddAttachment(createdBy : String , sendingTo : String ,  vendorName : String ) {

        val loading = CustomDialog(requireContext())
        loading.show()

        val attach = CreatePurchaseModel(createdBy,sendingTo,vendorName)

        val file = File(path)
        val content: ByteArray = file.readBytes()

        val fileExtension = MimeTypeMap.getFileExtensionFromUrl(file.absolutePath)
        val mediaType = FileTypeHelper.getMediaTypeFromFileExtension(fileExtension)

        if (mediaType != null){
            val requestBody = RequestBody.create(mediaType, content)
            val jwtToken = gettoken(requireContext())
            if (jwtToken != null) {
                val service = ApiUtilities.getinstance().create(ApiInterface::class.java)

                val body = MultipartBody.Part.createFormData("file", file.name, requestBody)
                val call = service.addSalesOrder("Bearer $jwtToken", body,attach)
                call.enqueue(object : Callback<SalesOrderResponseModel> {
                    override fun onResponse(
                        call: Call<SalesOrderResponseModel>,
                        response: Response<SalesOrderResponseModel>
                    ) {
                        if (isAdded)
                        {
                            if (response.isSuccessful) {
                                val responseBody = response.body()
                                Log.d("ResponseBody", "$responseBody")
                                loading.dismiss()
                                Constants.success(requireContext(),"File Attached Successfully")
                            }else{
                                Constants.error(requireContext(),"Unsuccessful")
                                loading.dismiss()
                            }
                        }
                    }
                    override fun onFailure(call: Call<SalesOrderResponseModel>, t: Throwable) {
                        Constants.error(requireContext(), "Error: ${t.message}")
                        loading.dismiss()

                    }
                })
            } else {
                Constants.error(requireContext(), "Token Not Found")
                loading.dismiss()
            }
        } else {
            Constants.error(requireContext(), "Unsupported file type")
            loading.dismiss()
        }
    }

    private fun getFileNameFromUri(uri: Uri): String? {
        var fileName: String? = null
        val cursor =requireContext().contentResolver.query(uri, null, null, null, null)
        if (cursor!= null){
            if (cursor.moveToFirst()) {
                val displayNameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
                if (displayNameIndex != -1) {
                    fileName = cursor.getString(displayNameIndex)
                }
            }
            cursor.close()
        }
        return fileName
    }
    @SuppressLint("Range")
    private fun getPathFromUri(context: Context, uri: Uri): String? {
        var path: String? = null
        if ("content".equals(uri.scheme, ignoreCase = true)) {
            // Using ACTION_OPEN_DOCUMENT
            val cursor = context.contentResolver.query(uri, null, null, null, null)
            cursor?.use {
                if (it.moveToFirst()) {
                    val displayName = it.getString(it.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                    val type = it.getString(it.getColumnIndex("mime_type"))
                    val file = File(context.cacheDir, displayName)
                    try {
                        val inputStream = context.contentResolver.openInputStream(uri)
                        inputStream?.use { input ->
                            FileOutputStream(file).use { output ->
                                input.copyTo(output)
                            }
                        }
                        path = file.absolutePath
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
            }
        } else if ("file".equals(uri.scheme, ignoreCase = true)) {
            path = uri.path
        }
        return path
    }

    private fun showFileChooser() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
            intent.type = "*/*"
            startActivityForResult(intent, PICKFILE_RESULT_CODE)
        } else {
            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.type = "*/*"
            intent.addCategory(Intent.CATEGORY_OPENABLE)
            startActivityForResult(
                Intent.createChooser(intent, "Select File"), PICKFILE_RESULT_CODE
            )
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == PICKFILE_RESULT_CODE && resultCode == Activity.RESULT_OK && data != null && data.data != null) {
            val uri: Uri = data.data!!
            path = getPathFromUri(requireContext(), uri)!!
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                updateTextViewWithFileName(uri)
            } else {
                // Handle for lower versions
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun updateTextViewWithFileName(uri: Uri) {
        val fileName = getFileNameFromUri(uri)
        filePath = uri.toString()
        if (fileName != null) {
            binding.orderFilePath.text = Html.fromHtml("<b><font color='#7FFF00'>$fileName</font></b>")
        }
    }


    fun fetchVendorListFromApi(jwtToken: String) {
        val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
        service.getVendorList("Bearer $jwtToken").enqueue(object : Callback<List<VendorListResponseModelItem>> {
            override fun onResponse(call: Call<List<VendorListResponseModelItem>>, response: Response<List<VendorListResponseModelItem>>) {
                if (response.isSuccessful) {
                    val vendorList = response.body()
                    if (vendorList != null) {
                        // Extract vendor names from the list of vendor objects
                        val vendorNames = vendorList.map { it.vendorName }

                        // Create an ArrayAdapter with the vendor names
                        val adapter = ArrayAdapter(
                            requireContext(),
                            R.layout.simple_spinner_dropdown_item,
                            vendorNames
                        )

                        binding.vendorName.setAdapter(adapter)

                        binding.vendorName.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                                val selectedVendorName = vendorNames[position]
                                val selectedVendor = vendorList[position]
                                selectedVendorId = selectedVendor.vendorId.toString()
                            }
                            override fun onNothingSelected(parent: AdapterView<*>?) {
                            }
                        }
                    } else {
                        Constants.error(requireContext(), "Vendor list is null")
                    }
                } else {
                    Constants.error(requireContext(), "Failed to fetch vendor list: ${response.code()}")
                }
            }

            override fun onFailure(call: Call<List<VendorListResponseModelItem>>, t: Throwable) {
                Constants.error(requireContext(), "Error fetching vendor list: ${t.message}")
            }
        })
    }

    private fun gettoken(context: Context?): String? {
        val sharedPreferences = requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
        return sharedPreferences.getString("jwtToken", null)
    }

    private fun getUserName() : String? {
        val sharedPreferences = requireActivity().getSharedPreferences("UserName", Context.MODE_PRIVATE)
        return sharedPreferences.getString("name", null)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}