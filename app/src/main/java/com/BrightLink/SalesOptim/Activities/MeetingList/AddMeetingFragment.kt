package com.BrightLink.SalesOptim.Activities.MeetingList

import android.content.Context
import androidx.fragment.app.Fragment
import com.BrightLink.SalesOptim.databinding.AddMeetingPopupBinding

class AddMeetingFragment : Fragment() {

    private var _binding: AddMeetingPopupBinding? = null

    private fun getToken(): String? {
        val sharedPreferences = requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
        return sharedPreferences.getString("jwtToken", null)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
