package com.BrightLink.SalesOptim.Activities.PurchaseOrder

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.ColorMatrix
import android.graphics.ColorMatrixColorFilter
import android.graphics.Paint
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import com.BrightLink.SalesOptim.Adapters.GetPurchaseProductAdapter
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.Constants.WrapContentLinearLayoutManager
import com.BrightLink.SalesOptim.Model.PurchaseOrderItem
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.databinding.PurchaseOrderInvoiceBinding
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.itextpdf.text.Document
import com.itextpdf.text.Image
import com.itextpdf.text.PageSize
import com.itextpdf.text.pdf.PdfWriter
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.UUID

class PurchaseOrderInvoice : Fragment () {
    private var _binding: PurchaseOrderInvoiceBinding? = null
    private val binding get() = _binding!!
    private val PREF_NAME = "MyPrefs"
    private val PRODUCT_PREF = "PurchaseOrderProduct"
    private val PRODUCT_DETAILS = "Purchase_Product_Details"
    private lateinit var adapter : GetPurchaseProductAdapter
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = PurchaseOrderInvoiceBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.actionBar.activityName.text = "PDF Preview"
        binding.actionBar.back.setOnClickListener{
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }


        binding.recyclerView.layoutManager = WrapContentLinearLayoutManager(context)

        // Retrieve strings from SharedPreferences
        val sharedPreferences = requireContext().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)

        //Own
        val companyName = sharedPreferences.getString("companyName", "")
        val address = sharedPreferences.getString("address", "")
        val phone = sharedPreferences.getString("phone", "")
        val email = sharedPreferences.getString("email", "")
        val date = sharedPreferences.getString("date", "")
        val Validdate = sharedPreferences.getString("validDate", "")
        val quotationId = sharedPreferences.getString("quotationId", "")
        val imageUriString = sharedPreferences.getString("imageUri", "")
        val gst=  sharedPreferences.getString("gst", "")
        val visibility = sharedPreferences.getBoolean("visibility",true)

        //Customer
        val customerName = sharedPreferences.getString("customerName", "")
        val customerCompanyName = sharedPreferences.getString("customerCompanyName", "")
        val customerAddress = sharedPreferences.getString("customerAddress", "")
        val customerPhone = sharedPreferences.getString("customerPhone", "")
        val customerSubject = sharedPreferences.getString("customerSubject", "")
        val customerMessage = sharedPreferences.getString("customerMessage", "")

        //Product & Price
        val product =  requireContext().getSharedPreferences(PRODUCT_PREF, Context.MODE_PRIVATE)
        val product_details =  requireContext().getSharedPreferences(PRODUCT_DETAILS, Context.MODE_PRIVATE)
        getProducts(product , product_details)

        // shipping

        val ship_customerName = sharedPreferences.getString("ship_customerName", "")
        val ship_customerCompanyName = sharedPreferences.getString("ship_companyName", "")
        val ship_customerAddress = sharedPreferences.getString("ship_companyAddress", "")
        val ship_customerPhone = sharedPreferences.getString("ship_customer_phone", "")

        //T&C
        val note = sharedPreferences.getString("note","")
        val sendorName = sharedPreferences.getString("sendorName","")
        val senderDesignation = sharedPreferences.getString("sendorDesignation","")
        val sendorNumber = sharedPreferences.getString("sendorNumber","")
        val deliveryPeriod = sharedPreferences.getString("deliveryPeriod","")
        val advancePayment = sharedPreferences.getString("advancePayment","")
        val tandc = sharedPreferences.getString("tandc","")

        val backgroundclr = sharedPreferences.getString("background","")
        val headerbckground = sharedPreferences.getString("header","")
        val text = sharedPreferences.getString("text","")


        imageUriString?.let { uriString ->
            val imageUri = Uri.parse(uriString)
            binding.logo.setImageURI(imageUri)
        }

        //Text
        if (text != null && text.isNotEmpty()) {
            binding.date.setTextColor(android.graphics.Color.parseColor(text))
            binding.validUntil.setTextColor(android.graphics.Color.parseColor(text))
            binding.quotationId.setTextColor(android.graphics.Color.parseColor(text))
            binding.to.setTextColor(android.graphics.Color.parseColor(text))
            binding.customerName.setTextColor(android.graphics.Color.parseColor(text))
            binding.customerCompanyName.setTextColor(android.graphics.Color.parseColor(text))
            binding.customerAddress.setTextColor(android.graphics.Color.parseColor(text))
            binding.customerPhone.setTextColor(android.graphics.Color.parseColor(text))
            binding.sub.setTextColor(android.graphics.Color.parseColor(text))
            binding.sub2.setTextColor(android.graphics.Color.parseColor(text))
            binding.dear.setTextColor(android.graphics.Color.parseColor(text))
            binding.ref.setTextColor(android.graphics.Color.parseColor(text))
            binding.refmessage.setTextColor(android.graphics.Color.parseColor(text))
            binding.termss.setTextColor(android.graphics.Color.parseColor(text))
            binding.taxx.setTextColor(android.graphics.Color.parseColor(text))
            binding.shipch.setTextColor(android.graphics.Color.parseColor(text))
            binding.delivper.setTextColor(android.graphics.Color.parseColor(text))
            binding.adv.setTextColor(android.graphics.Color.parseColor(text))
            binding.noteText.setTextColor(android.graphics.Color.parseColor(text))
            binding.yourname.setTextColor(android.graphics.Color.parseColor(text))
            binding.yourphone.setTextColor(android.graphics.Color.parseColor(text))
            binding.shippingPhone.setTextColor(android.graphics.Color.parseColor(text))
            binding.shippingAddress.setTextColor(android.graphics.Color.parseColor(text))
            binding.shippingCompanyName.setTextColor(android.graphics.Color.parseColor(text))
            binding.shippingCustomerName.setTextColor(android.graphics.Color.parseColor(text))
            binding.toShippingAddress.setTextColor(android.graphics.Color.parseColor(text))
            binding.tcText.setTextColor(android.graphics.Color.parseColor(text))
            binding.TaxText.setTextColor(android.graphics.Color.parseColor(text))
            binding.shippingChargesTextdown.setTextColor(android.graphics.Color.parseColor(text))
            binding.deliveryText.setTextColor(android.graphics.Color.parseColor(text))
            binding.advancepaymentText.setTextColor(android.graphics.Color.parseColor(text))
            binding.senderDesignation.setTextColor(android.graphics.Color.parseColor(text))
            binding.yourphone.setTextColor(android.graphics.Color.parseColor(text))
            binding.yourname.setTextColor(android.graphics.Color.parseColor(text))
            binding.dateText.setTextColor(android.graphics.Color.parseColor(text))
            binding.validUntilText.setTextColor(android.graphics.Color.parseColor(text))
            binding.quatationIdtext.setTextColor(android.graphics.Color.parseColor(text))
            binding.notee2.setTextColor(android.graphics.Color.parseColor(text))

        }


        //Header
        if (headerbckground != null && headerbckground.isNotEmpty()) {
            binding.companyName.setTextColor(android.graphics.Color.parseColor(headerbckground))
            binding.address.setTextColor(android.graphics.Color.parseColor(headerbckground))
            binding.phone.setTextColor(android.graphics.Color.parseColor(headerbckground))
            binding.email.setTextColor(android.graphics.Color.parseColor(headerbckground))
            binding.srnoo.setTextColor(android.graphics.Color.parseColor(headerbckground))
            binding.prdDesc.setTextColor(android.graphics.Color.parseColor(headerbckground))
            binding.qtyy.setTextColor(android.graphics.Color.parseColor(headerbckground))
            binding.unitprc.setTextColor(android.graphics.Color.parseColor(headerbckground))
            binding.totalprc.setTextColor(android.graphics.Color.parseColor(headerbckground))
        }


        //Background
        if (backgroundclr != null && backgroundclr.isNotEmpty()) {
            binding.bckground1.setBackgroundColor(android.graphics.Color.parseColor(backgroundclr))
            binding.bckground2.setBackgroundColor(android.graphics.Color.parseColor(backgroundclr))
            binding.bckground3.setBackgroundColor(android.graphics.Color.parseColor(backgroundclr))
        }

        // Set the retrieved strings to the appropriate TextViews
        binding.companyName.text = companyName
        binding.address.text = address
        binding.phone.text = phone
        binding.email.text = email
        binding.date.text = date
        binding.validUntil.text = Validdate
        binding.quotationId.text = quotationId

        binding.customerName.text = customerName
        binding.customerCompanyName.text= customerCompanyName
        binding.customerAddress.text = customerAddress
        binding.customerPhone.text = customerPhone
        binding.refmessage.text = customerMessage
        binding.sub2.text = customerSubject

        binding.termss.text = tandc
        binding.delivper.text = deliveryPeriod
        binding.adv.text = advancePayment
        binding.notee2.text = note
        binding.yourname.text = sendorName
        binding.yourphone.text = sendorNumber
        binding.shippingCustomerName.text = ship_customerName
        binding.shippingCompanyName.text = ship_customerCompanyName
        binding.shippingAddress.text = ship_customerAddress
        binding.shippingPhone.text = ship_customerPhone
        binding.senderDesignation.text = senderDesignation


        binding.downloadPdf.setOnClickListener {

            layoutToImage(binding.root)
        }


        return root
    }

    fun layoutToImage(view: View) {
        // Get the view group using reference
        val relativeLayout = view.findViewById<View>(R.id.print) as LinearLayout
        // Convert view group to bitmap
        relativeLayout.isDrawingCacheEnabled = true
        relativeLayout.buildDrawingCache()
        val drawingCache = relativeLayout.drawingCache
        if (drawingCache != null) {
            val bitmap = Bitmap.createBitmap(drawingCache)
            val brightnessValue = 20 // Adjust brightness value as needed
            val contrastValue = 1.2f // Adjust contrast value as needed
            val adjustedBitmap = applyBrightnessAndContrast(bitmap, brightnessValue, contrastValue)

            // Save adjusted bitmap as image file
            val file = File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + File.separator + "${UUID.randomUUID().toString()}.jpg"
            )
            try {
                val fos = FileOutputStream(file)
                adjustedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos)
                fos.flush()
                fos.close()
                // Convert the adjusted image to PDF
                imageToPDF(file)
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        else
        {
            Constants.error(context,"Failed to create bitmap from drawing cache")
        }
    }

    private fun applyBrightnessAndContrast(bitmap: Bitmap, brightnessValue: Int, contrastValue: Float): Bitmap {
        val cm = ColorMatrix()
        cm.set(floatArrayOf(
            contrastValue, 0f, 0f, 0f, brightnessValue.toFloat(),
            0f, contrastValue, 0f, 0f, brightnessValue.toFloat(),
            0f, 0f, contrastValue, 0f, brightnessValue.toFloat(),
            0f, 0f, 0f, 1f, 0f
        ))
        val paint = Paint()
        paint.colorFilter = ColorMatrixColorFilter(cm)

        val adjustedBitmap = Bitmap.createBitmap(bitmap.width, bitmap.height, bitmap.config)
        val canvas = Canvas(adjustedBitmap)
        val paint2 = Paint()
        paint2.color = Color.WHITE
        canvas.drawBitmap(bitmap, 0f, 0f, paint2)
        canvas.drawBitmap(adjustedBitmap, 0f, 0f, paint)
        return adjustedBitmap
    }

    fun imageToPDF(imageFile: File) {
        try {
            val document = Document()
            val dirPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).toString() + File.separator + "CRM/PDF"
            val directory = File(dirPath)
            directory.mkdirs()
            val pdfFile = File(directory, "${UUID.randomUUID()}.pdf")
            PdfWriter.getInstance(
                document,
                FileOutputStream(pdfFile)
            )
            document.open()

            val img = Image.getInstance(imageFile.absolutePath)
            img.scaleToFit(PageSize.A4.width, PageSize.A4.height)
            // img.scaleAbsolute(document.pageSize.width - document.leftMargin() - document.rightMargin(), document.pageSize.height - document.topMargin() - document.bottomMargin())
            img.alignment = Image.ALIGN_CENTER
            document.add(img)
            document.close()
            Constants.success(context, "PDF generated successfully!")
        } catch (e: Exception) {
            e.printStackTrace()
            Constants.error(context, "Failed to generate PDF")
        }
    }

    private fun getProducts(product: SharedPreferences?, product_details : SharedPreferences?) {

        val product = getItems(requireContext())
        binding.recyclerView.layoutManager = WrapContentLinearLayoutManager(context)
        adapter = GetPurchaseProductAdapter(requireContext() , product)
        binding.recyclerView.adapter = adapter

        val tax = product_details!!.getString("tax", "")
        val shippingCharges = product_details.getString("shippingCharges","" )

        binding.taxx.text = tax
        binding.taxper.text = tax
        binding.shipch.text = shippingCharges
        binding.shipchrg.text = shippingCharges
    }


    fun getItems(context: Context): List<PurchaseOrderItem> {
        val prefs: SharedPreferences = context.getSharedPreferences(PRODUCT_PREF, Context.MODE_PRIVATE)
        val gson = Gson()
        val json = prefs.getString("Product", null)
        val type = object : TypeToken<List<PurchaseOrderItem>>() {}.type
        return gson.fromJson(json, type) ?: emptyList()
    }

}