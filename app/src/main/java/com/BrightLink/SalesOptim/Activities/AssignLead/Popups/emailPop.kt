package com.BrightLink.SalesOptim.Activities.AssignLead.Popups

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.BrightLink.SalesOptim.databinding.EmailPopBinding

class emailPop : Fragment() {

    private var _binding: EmailPopBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        _binding = EmailPopBinding.inflate(inflater, container, false)
        val root: View = binding.root


        return root
    }

}