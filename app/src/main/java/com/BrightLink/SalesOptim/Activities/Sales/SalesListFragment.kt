package com.BrightLink.SalesOptim.Activities.Sales

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.BrightLink.SalesOptim.Adapters.SalesOrderAdapter
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.Constants.CustomDialog
import com.BrightLink.SalesOptim.Constants.WrapContentLinearLayoutManager
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.ResponseModel.SalesListModelItem
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.ActivitySalesOrderBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class SalesListFragment : Fragment() {
    private var _binding: ActivitySalesOrderBinding? = null
    private lateinit var adapter: SalesOrderAdapter
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ActivitySalesOrderBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.actionBar.activityName.text = "Sales Order"
        binding.actionBar.back.setOnClickListener {
            requireActivity().onBackPressed()
        }

        binding.search.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                adapter.filter(s.toString())
            }
        })


        binding.salesListRecyclerView.layoutManager = WrapContentLinearLayoutManager(context)
        binding.actionBar.saveButton.setOnClickListener{
            findNavController().navigate(R.id.add_salesOrder_frg)
        }

        binding.actionBar.salesOrderInvoice.setOnClickListener {
            findNavController().navigate(R.id.salesOrderInvoice)
        }

        val jwtToken = gettoken(context)
        if (jwtToken!=null)
        {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            CallApi(service, jwtToken)
        }
        else
        {
            Constants.error(context,"Token is null Login Again")
        }
        return root
    }

    private fun CallApi(apiservice: ApiInterface, jwtToken: String) {
        val loading = CustomDialog(requireContext())
        loading.show()
        val call = apiservice.salesOrderList("Bearer $jwtToken")
        call.enqueue(object : Callback<List<SalesListModelItem>> {
            override fun onResponse(
                call: Call<List<SalesListModelItem>>,
                response: Response<List<SalesListModelItem>>
            ) {
                if (isAdded) {
                    if (response.isSuccessful) {
                        val data = response.body()
                        if (data != null) {
                            loading.dismiss()
                            adapter = SalesOrderAdapter(requireContext(), data)
                            binding.salesListRecyclerView.adapter = adapter
                        } else {
                            loading.dismiss()
                            Constants.error(requireContext(), "Response body is null")
                        }
                    } else {
                        Constants.error(requireContext(), "Unsuccessful response: ${response.code()}")
                        loading.dismiss()
                    }
                }
            }

            override fun onFailure(call: Call<List<SalesListModelItem>>, t: Throwable) {
                if (isAdded) { // Check if the fragment is added to the activity
                    Constants.error(requireContext(), "Error: ${t.message}")
                    loading.dismiss()
                }
            }
        })
    }
    private fun gettoken(context: Context?): String? {
        val sharedPreferences = requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
        return sharedPreferences.getString("jwtToken", null)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}