package com.BrightLink.SalesOptim.Activities.Leads

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.BrightLink.SalesOptim.Adapters.LeadReportAdapter
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.Constants.WrapContentLinearLayoutManager
import com.BrightLink.SalesOptim.ResponseModel.LeadReportModelItem
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.ActivityLeadReportBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LeadReportFragment : Fragment() {

    private var _binding: ActivityLeadReportBinding? = null
    private lateinit var adapter : LeadReportAdapter
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = ActivityLeadReportBinding.inflate(inflater, container, false)
        val root : View = binding.root

        binding.actionBar.activityName.text = "Lead Report"
        binding.actionBar.back.setOnClickListener{
            requireActivity().onBackPressed()
        }

        binding.actionBar.option.visibility = View.INVISIBLE

        binding.recyclerView.layoutManager = WrapContentLinearLayoutManager(context)

        val jwtToken = gettoken(context)
        if (jwtToken!=null)
        {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            CallApi(service , jwtToken , context)
        }
        else{
            Constants.error(context,"Login Again")
        }


        binding.search.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                if (::adapter.isInitialized) {
                    adapter.filter(s.toString())
                }
            }
        })

        return root
    }

    private fun CallApi(apiservice : ApiInterface , jwtToken : String , context: Context? ) {
        context?:return
        val call = apiservice.showLeadReport("Bearer $jwtToken")
        call.enqueue(object : Callback<List<LeadReportModelItem>> {
            override fun onResponse(
                call: Call<List<LeadReportModelItem>>,
                response: Response<List<LeadReportModelItem>>
            ) {
                if (isAdded)
                {
                    if (response.isSuccessful)
                    {
                        val data  = response.body()
                        if (data!=null && data.isNotEmpty()) {
                            adapter = LeadReportAdapter(context, data)
                            binding.recyclerView.adapter = adapter
                        } else{
                            binding.noLeadsReportFound.visibility = View.VISIBLE
                        }
                    } else {
                        Constants.error(context, "Unsuccessful Response,\n Try again later")
                    }
                }
            }

            override fun onFailure(call: Call<List<LeadReportModelItem>>, t: Throwable) {
                Constants.error(context,"Failed to get Lead Report")
            }
        })
    }

    private fun gettoken(context: Context?): String? {
        val sharedPreferences = requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
        return sharedPreferences.getString("jwtToken", null)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}