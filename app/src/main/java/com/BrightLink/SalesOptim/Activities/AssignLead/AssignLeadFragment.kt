package com.BrightLink.SalesOptim.Activities.AssignLead

import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.OpenableColumns
import android.text.Html
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.MimeTypeMap
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.PopupMenu
import android.widget.RadioButton
import android.widget.Spinner
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2
import com.BrightLink.SalesOptim.Activities.Contact.UpdateContact
import com.BrightLink.SalesOptim.Adapters.ProposalALAdapter
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.RequestModel.AddContactReq
import com.BrightLink.SalesOptim.RequestModel.AddProposalReq
import com.BrightLink.SalesOptim.RequestModel.NegotiationReq
import com.BrightLink.SalesOptim.RequestModel.QualifyProposalReq
import com.BrightLink.SalesOptim.ResponseModel.AddContactRespo
import com.BrightLink.SalesOptim.ResponseModel.AddProposalRespo
import com.BrightLink.SalesOptim.ResponseModel.LeadDetailResponseModel
import com.BrightLink.SalesOptim.ResponseModel.NegotiationRespo
import com.BrightLink.SalesOptim.ResponseModel.QualifyProposalRespo
import com.BrightLink.SalesOptim.ResponseModel.TemplateModelItem
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.FragmentAssignLeadBinding
import com.google.android.material.tabs.TabLayout
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.Calendar

class AssignLeadFragment : Fragment() {

    private var _binding: FragmentAssignLeadBinding? = null
    private val binding get() = _binding!!

    private var dialog: Dialog? = null
    private var phoneNumber: String = ""
    private var leadId : Int? =  null
    private var qualifyStatus :String? = null
    private var LeadStatus :String? = null
    private var sendReason : String? = null
    private var negoReason : String?  = null
    private var sendReq : String? = null
    private var sendDesc : String? = ""
    private var revenue : String? = null
    private var leadStatus : String? = null
    private var callDuration : String? = "-"
    private var contactType : String? = null
    private val PICKFILE_RESULT_CODE = 1
    private var filePath: String? = null
    private var path : String = ""
    private var taskDate: String? = ""
    private var cntSubject: String = "-"
    private var cntDescription: String = "-"
    private var email : String = ""
    private var contacted : String? = null

    companion object {
        private const val REQUEST_PERMISSION_CODE = 123
    }

    @SuppressLint("SuspiciousIndentation", "QueryPermissionsNeeded", "ResourceType",
        "MissingInflatedId", "CutPasteId")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
     ): View? {
         email = arguments?.getString("email").toString()
        _binding = FragmentAssignLeadBinding.inflate(inflater, container, false)
        val root: View = binding.root

        phoneNumber = arguments?.getString("phone").toString()
        revenue = arguments?.getString("revenue") ?: ""
        leadStatus = arguments?.getString("leadStatus") ?: ""

        if (leadStatus!=null)
        {
            when(leadStatus)
            {
                "New Lead"->{

                    binding.qualifyBtn.visibility = View.INVISIBLE
                   // binding.qualifiedIcon.visibility = View.INVISIBLE
                    binding.qualifiedIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.proposalBtn.visibility = View.INVISIBLE
                    //binding.proposalIcon.visibility = View.INVISIBLE
                    binding.proposalIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.negotiationBtn.visibility = View.INVISIBLE
                    //binding.negotiationIcon.visibility = View.INVISIBLE
                    binding.negotiationIcon.setColorFilter(Color. argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)

                }

                "Contacted"->{
                    binding.qualifyBtn.visibility = View.VISIBLE
                    //binding.qualifiedIcon.visibility = View.VISIBLE
                    binding.qualifiedIcon.setColorFilter(Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.proposalBtn.visibility = View.INVISIBLE
                    binding.proposalIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.proposalIcon.visibility = View.INVISIBLE
                    binding.negotiationBtn.visibility = View.INVISIBLE
                    binding.negotiationIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.negotiationIcon.visibility = View.INVISIBLE
                }

                "Email Sent"->{
                    binding.qualifyBtn.visibility = View.VISIBLE
                    //binding.qualifiedIcon.visibility = View.VISIBLE
                    binding.qualifiedIcon.setColorFilter(Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.proposalBtn.visibility = View.INVISIBLE
                    binding.proposalIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.proposalIcon.visibility = View.INVISIBLE
                    binding.negotiationBtn.visibility = View.INVISIBLE
                    binding.negotiationIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.negotiationIcon.visibility = View.INVISIBLE
                }

                "Not Contacted"->{
                    binding.qualifyBtn.visibility = View.INVISIBLE
                    //binding.qualifiedIcon.visibility = View.INVISIBLE
                    binding.qualifiedIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.proposalBtn.visibility = View.INVISIBLE
                    binding.proposalIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.proposalIcon.visibility = View.INVISIBLE
                    binding.negotiationBtn.visibility = View.INVISIBLE
                    binding.negotiationIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.negotiationIcon.visibility = View.INVISIBLE
                }

                "Qualified"->{
                    binding.qualifyBtn.visibility = View.VISIBLE
                    binding.qualifiedIcon.setColorFilter(Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.qualifiedIcon.visibility = View.VISIBLE
                    binding.proposalBtn.visibility = View.VISIBLE
                    //binding.proposalIcon.visibility = View.VISIBLE
                    binding.proposalIcon.setColorFilter(Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.negotiationBtn.visibility = View.INVISIBLE
                    binding.negotiationIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.negotiationIcon.visibility = View.INVISIBLE
                }

                "Not Qualified"->{
                    binding.qualifyBtn.visibility = View.VISIBLE
                    binding.qualifiedIcon.setColorFilter(Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.qualifiedIcon.visibility = View.VISIBLE
                    binding.proposalBtn.visibility = View.INVISIBLE
                    binding.proposalIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.proposalIcon.visibility = View.INVISIBLE
                    binding.negotiationBtn.visibility = View.INVISIBLE
                    binding.negotiationIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.negotiationIcon.visibility = View.INVISIBLE
                }

                "Proposal Sent"->{
                    binding.qualifyBtn.visibility = View.VISIBLE
                    binding.qualifiedIcon.setColorFilter(Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.qualifiedIcon.visibility = View.VISIBLE
                    binding.proposalBtn.visibility = View.VISIBLE
                    binding.proposalIcon.setColorFilter(Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.proposalIcon.visibility = View.VISIBLE
                    binding.negotiationBtn.visibility = View.VISIBLE
                    binding.qualifiedIcon.setColorFilter(Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.negotiationIcon.visibility = View.VISIBLE
                }

                "Lead Won"->{
                    binding.callNow.visibility = View.INVISIBLE
                    binding.emailNow.visibility = View.INVISIBLE
                    binding.contactBtn.isEnabled = false
                    binding.contactBtn.setBackgroundColor(resources.getColor(R.color.FaintGrey))
                    binding.contactIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.contactIcon.visibility = View.VISIBLE
                    binding.qualifyBtn.isEnabled = false
                    binding.qualifyBtn.setBackgroundColor(resources.getColor(R.color.FaintGrey))
                    binding.qualifiedIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.qualifiedIcon.visibility = View.INVISIBLE
                    binding.proposalBtn.isEnabled = false
                    binding.proposalBtn.setBackgroundColor(resources.getColor(R.color.FaintGrey))
                    binding.proposalIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.proposalIcon.visibility = View.INVISIBLE
                    binding.negotiationBtn.isEnabled = false
                    binding.negotiationBtn.setBackgroundColor(resources.getColor(R.color.FaintGrey))
                    binding.negotiationIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.negotiationIcon.visibility = View.INVISIBLE
                }

                "Lead Lost"->{
                    binding.callNow.visibility = View.INVISIBLE
                    binding.emailNow.visibility = View.INVISIBLE
                    binding.contactBtn.isEnabled = false
                    binding.contactBtn.setBackgroundColor(resources.getColor(R.color.FaintGrey))
                    binding.contactIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.contactIcon.visibility = View.INVISIBLE
                    binding.qualifyBtn.isEnabled = false
                    binding.qualifyBtn.setBackgroundColor(resources.getColor(R.color.FaintGrey))
                    binding.qualifiedIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.qualifiedIcon.visibility = View.INVISIBLE
                    binding.proposalBtn.isEnabled = false
                    binding.proposalBtn.setBackgroundColor(resources.getColor(R.color.FaintGrey))
                    binding.proposalIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.proposalIcon.visibility = View.INVISIBLE
                    binding.negotiationBtn.isEnabled = false
                    binding.negotiationBtn.setBackgroundColor(resources.getColor(R.color.FaintGrey))
                    binding.negotiationIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.negotiationIcon.visibility = View.INVISIBLE
                }
            }
        }
        binding.actionBar.option.visibility = View.INVISIBLE
        binding.actionBar.activityName.text = "Assign Lead"
        binding.actionBar.back.setOnClickListener {
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }

        val sharedPreferences = requireContext().getSharedPreferences("LeadPreferences", Context.MODE_PRIVATE)
         leadId = sharedPreferences.getInt("leadId", -1)

        val bundle = Bundle()
        bundle.putInt("leadId", leadId!!)
        bundle.putString("phone",phoneNumber.toString())

        binding.task.setOnClickListener {
            findNavController().navigate(R.id.task , bundle)
        }

        binding.meeting.setOnClickListener {
            findNavController().navigate(R.id.meeting)
        }

        binding.activity.setOnClickListener {
            findNavController().navigate(R.id.activity)
        }
        binding.attachment.setOnClickListener {
            findNavController().navigate(R.id.attachment)
        }

        binding.notes.setOnClickListener {
            findNavController().navigate(R.id.note)
        }
        binding.links.setOnClickListener {
            findNavController().navigate(R.id.link)
        }
        binding.info.setOnClickListener {
            findNavController().navigate(R.id.info)
        }

         revenue = arguments?.getString("revenue") ?: ""

            binding.callNow.setOnClickListener {
                if (phoneNumber != null) {
                    val intent = Intent(Intent.ACTION_CALL)
                    intent.setData(Uri.parse("tel:" + phoneNumber));
                    startActivity(intent)
                    contactType = "call"
                    contactStatus( subject = "-", title = "-", desc = "-",phoneNumber)

                }
                Constants.error(context, "Phone Number not found")

        }

        binding.emailNow.setOnClickListener {
            contactType = "email"
            email?.let {
                val intent = Intent(Intent.ACTION_SENDTO).apply {
                    data = Uri.parse("mailto:$email")
                }
                startActivity(intent)
            } ?: run {
                // Handle the case where email is null
                Constants.error(requireContext(), "Email address not found")
            }
            contactStatus(subject = "-", title = "-", desc = "-",email)
        }



        binding.qualifyBtn.setOnClickListener {
            showDialog(R.layout.qualified)
            val qualifiedYes = inflater.inflate(R.layout.qualified_yes,null)
            val cont = dialog!!.findViewById<LinearLayout>(R.id.qualifiedContainer)
            qualifyStatus = "Qualified"
            updateAdditionalLayout(cont,qualifiedYes)

            val sub = dialog?.findViewById<Button>(R.id.submitYes)
            sub?.setOnClickListener {
                val title = dialog?.findViewById<EditText>(R.id.titleQualifyYes)
                val req = dialog?.findViewById<EditText>(R.id.reqQualify)
                val titleText = title?.text.toString().trim()
                val reqText = req?.text.toString().trim()
                    qualifiyProposal(titleText, sendReason ?: "", reqText)
            }

            val qualifiedyes = dialog!!.findViewById<RadioButton>(R.id.radioButtonYes)
            val qualifiedno = dialog!!.findViewById<RadioButton>(R.id.radioButtonNo)

            qualifiedyes.setOnClickListener {
                qualifyStatus = "Qualified"
                val cnt = dialog!!.findViewById<LinearLayout>(R.id.qualifiedContainer)
                val yes = inflater.inflate(R.layout.qualified_yes, null)
                updateAdditionalLayout(cnt,yes)


                val submit = dialog?.findViewById<Button>(R.id.submitYes)
                submit?.setOnClickListener {
                    val title = dialog?.findViewById<EditText>(R.id.titleQualifyYes)
                    val req = dialog?.findViewById<EditText>(R.id.reqQualify)
                    val titleText = title?.text.toString().trim()
                    val reqText = req?.text.toString().trim()
                        qualifiyProposal(titleText, sendReason ?: "", reqText)
                }
            }

            qualifiedno.setOnClickListener {
                qualifyStatus = "Not Qualified"
                val cnt = dialog!!.findViewById<LinearLayout>(R.id.qualifiedContainer)
                val no = inflater.inflate(R.layout.qualified_no, null)
                updateAdditionalLayout(cnt, no)

                val submt = dialog?.findViewById<Button>(R.id.submitQualifyy)
                submt?.setOnClickListener {
                    val titlee = dialog?.findViewById<EditText>(R.id.titleQualifyYes)
                    val reaso = dialog?.findViewById<EditText>(R.id.qualifyingReasonEditText)
                    val titleText = titlee?.text.toString().trim()
                    val reasonText = reaso?.text.toString().trim()
                        qualifiyProposal(titleText, reasonText, sendReq ?: "")
                }
            }
        }

        binding.proposalBtn.setOnClickListener {
            showDialog(R.layout.proposal_fragment)
            val quot = dialog!!.findViewById<Button>(R.id.custonquotation)
            val add = dialog!!.findViewById<Button>(R.id.addProposal)
            quot.setBackgroundColor((resources.getColor(R.color.white)))

            val cont = dialog!!.findViewById<LinearLayout>(R.id.proposalContainer)
            val quotation = inflater.inflate(R.layout.proposal_custom_quotatio, null)
            updateAdditionalLayout(cont, quotation)

            val tabLayout: TabLayout = dialog!!.findViewById(R.id.tabs)
            val viewPager2: ViewPager2 = dialog!!.findViewById(R.id.viewPagerp)
            val adapter = ProposalALAdapter(requireActivity().supportFragmentManager, lifecycle)

            initializeTabLayoutAndViewPager(tabLayout, viewPager2, adapter)


            quot.setOnClickListener {
                quot.setBackgroundColor((resources.getColor(R.color.white)))
                add.setBackgroundColor((resources.getColor(R.color.LightGrey)))

                val cnt = dialog!!.findViewById<LinearLayout>(R.id.proposalContainer)
                val yes = inflater.inflate(R.layout.proposal_custom_quotatio, null)
                updateAdditionalLayout(cnt, yes)
                val tabLayout: TabLayout = dialog!!.findViewById(R.id.tabs)
                val viewPager2: ViewPager2 = dialog!!.findViewById(R.id.viewPagerp)
                val adapter = ProposalALAdapter(requireActivity().supportFragmentManager, lifecycle)

                initializeTabLayoutAndViewPager(tabLayout, viewPager2, adapter)
            }

            add.setOnClickListener {
                add.setBackgroundColor(resources.getColor(R.color.white))
                quot.setBackgroundColor(resources.getColor(R.color.LightGrey))
                val cnt = dialog!!.findViewById<LinearLayout>(R.id.proposalContainer)
                val no = inflater.inflate(R.layout.add_proposal, null)
                updateAdditionalLayout(cnt, no)
                val tvEmail = dialog!!.findViewById<EditText>(R.id.emailtv)
                tvEmail.setText(email)

                val template = dialog!!.findViewById<Button>(R.id.template)
                val sub = dialog!!.findViewById<EditText>(R.id.subjectEmail)
                val desc = dialog!!.findViewById<EditText>(R.id.descriptionEmail)

                template.setOnClickListener {
                    addTempToEmail(sub, desc, template)
                }

                val choosefile = dialog!!.findViewById<Button>(R.id.proposalFile)
                choosefile.setOnClickListener {
                    showFileChooser()
                }

                val addProposalBtn = dialog!!.findViewById<Button>(R.id.addProposalBtn)
                addProposalBtn.setOnClickListener {
                    val contacted = tvEmail.text.toString()

                    val desc = dialog?.findViewById<EditText>(R.id.descriptionEmail)
                    val sub = dialog?.findViewById<EditText>(R.id.subjectEmail)
                    val descText = desc?.text.toString().trim()
                    val subText = sub?.text.toString().trim()
                    if (descText.isEmpty() || subText.isEmpty()){
                        Constants.error(requireContext(),"Subject or Description is Empty")
                    }else{
                        addProposal(descText, contacted, subText)
                    }
                }
            }
        }



        binding.negotiationBtn.setOnClickListener {
            showDialog(R.layout.negociation)
            val cont = dialog!!.findViewById<LinearLayout>(R.id.negoContainer)
            val accpt = inflater.inflate(R.layout.negociation_accept,null)
            val revenu = dialog!!.findViewById<TextView>(R.id.actualrevenu)
            revenu.text = revenue.toString()
            LeadStatus = "Accept"
            updateAdditionalLayout(cont,accpt)

            val submit  = dialog?.findViewById<Button>(R.id.submitNego)
            submit?.setOnClickListener {
                val desc = dialog?.findViewById<EditText>(R.id.negoDesc)
                val negotiableReve = dialog?.findViewById<EditText>(R.id.negotiableRevenue)
                val title = dialog?.findViewById<EditText>(R.id.negoTitle)
                val descText = desc?.text.toString().trim()
                val negoRevenue = negotiableReve?.text.toString().trim()
                val titleText = title?.text.toString().trim()
                if (descText.isEmpty() || negoRevenue.isEmpty() || titleText.isEmpty()){
                    Constants.error(requireContext(),"Please Fill the Given Fields")
                }else {
                    negotationStatus(descText, negoRevenue, negoReason ?: "", titleText)
                }
            }

            val accept = dialog!!.findViewById<RadioButton>(R.id.negoaccpt)
            val reject = dialog!!.findViewById<RadioButton>(R.id.negoreject)

            accept.setOnClickListener {
                LeadStatus = "Accept"
                val cnt = dialog!!.findViewById<LinearLayout>(R.id.negoContainer)
                val yes = inflater.inflate(R.layout.negociation_accept, null)
                updateAdditionalLayout(cnt, yes)

                val submit = dialog?.findViewById<Button>(R.id.submitNego)
                submit?.setOnClickListener {
                    val desc = dialog?.findViewById<EditText>(R.id.negoDesc)
                    val negotiableReve = dialog?.findViewById<EditText>(R.id.negotiableRevenue)
                    val title = dialog?.findViewById<EditText>(R.id.negoTitle)
                    val descText = desc?.text.toString().trim()
                    val negoRevenue = negotiableReve?.text.toString().trim()
                    val titleText = title?.text.toString().trim()
                    if (descText.isEmpty() || negoRevenue.isEmpty() || titleText.isEmpty()) {
                        Constants.error(requireContext(), "Please Fill the Given Fields")
                    } else {
                        negotationStatus(descText, negoRevenue, negoReason ?: "", titleText)
                    }
                }
            }

            reject.setOnClickListener {
                LeadStatus = "Reject"
                val cnt = dialog!!.findViewById<LinearLayout>(R.id.negoContainer)
                val no = inflater.inflate(R.layout.negociation_reject, null)
                updateAdditionalLayout(cnt,no)

                val submit  = dialog?.findViewById<Button>(R.id.negorejsubmit)
                submit?.setOnClickListener {
                    val reason = dialog?.findViewById<EditText>(R.id.negoReason)
                    val negotiableReve = dialog?.findViewById<EditText>(R.id.negotiableRevenue)
                    val title = dialog?.findViewById<EditText>(R.id.negoTitle)
                    val reasonText = reason?.text.toString().trim()
                    val negoRevenue = negotiableReve?.text.toString().trim()
                    val titleText = title?.text.toString().trim()
                    if (reasonText.isEmpty() || negoRevenue.isEmpty() || titleText.isEmpty()){
                        Constants.error(requireContext(),"Please Fill the Given Fields")
                    }else {
                        negotationStatus(sendDesc ?: "", negoRevenue, reasonText, titleText)
                    }
                }
            }
        }


        binding.contactBtn.setOnClickListener {
            showDialog(R.layout.contacts)
            val additionalLayoutContainer = dialog!!.findViewById<LinearLayout>(R.id.contactContainer)
            val additionalLayout = inflater.inflate(R.layout.call_pop,null)
            contactType = "call"
            updateAdditionalLayout(additionalLayoutContainer, additionalLayout)

            val tasktime = dialog!!.findViewById<TextView>(R.id.taskTime)
            tasktime.setOnClickListener {
                showDateTimePicker()
            }

            val tvPhone = dialog!!.findViewById<EditText>(R.id.phone)
            tvPhone.setText(phoneNumber)

            val contactBtn = dialog!!.findViewById<Button>(R.id.contactPrcd)
            contactBtn.setOnClickListener {

                val title = dialog!!.findViewById<EditText>(R.id.contactTitle)
                val titleText = title?.text.toString()
                contacted = tvPhone.text.toString()
                    contactStatus(subject = "-", titleText, desc = "-",contacted!!)

            }

            val radioButton1 = dialog!!.findViewById<RadioButton>(R.id.radioButtonCall)
            val radioButton2 = dialog!!.findViewById<RadioButton>(R.id.radioButtonEmail)

            radioButton1.setOnClickListener {
                val cnt = dialog!!.findViewById<LinearLayout>(R.id.contactContainer)
                val ad = inflater.inflate(R.layout.call_pop, null)
                contactType = "call"
                updateAdditionalLayout(cnt, ad)


                val tasktime = dialog!!.findViewById<TextView>(R.id.taskTime)
                tasktime.setOnClickListener {
                    showDateTimePicker()
                }

                val tvPhone = dialog!!.findViewById<EditText>(R.id.phone)
                tvPhone.setText(phoneNumber)

                val contactBtn = dialog!!.findViewById<Button>(R.id.contactPrcd)
                contactBtn.setOnClickListener {

                    val title = dialog!!.findViewById<EditText>(R.id.contactTitle)
                    val titleText = title?.text.toString()
                    contacted = tvPhone.text.toString()
                        contactStatus(subject = "-", titleText, desc = "-",contacted!!)

                }
            }

            radioButton2.setOnClickListener {
                val cont = dialog!!.findViewById<LinearLayout>(R.id.contactContainer)
                val add = inflater.inflate(R.layout.email_pop,null)
                contactType = "email"
                updateAdditionalLayout(cont, add)

                val emailFile = dialog!!.findViewById<Button>(R.id.contactEmailChoose)
                emailFile.setOnClickListener {
                    showFileChooser()
                }
                val tvEmail = dialog!!.findViewById<EditText>(R.id.contactEmail)
                tvEmail.setText(email)

                val template = dialog!!.findViewById<Button>(R.id.template)
                val sub = dialog!!.findViewById<EditText>(R.id.contactSubject)
                val desc = dialog!!.findViewById<EditText>(R.id.contactDesc)

                template.setOnClickListener {
                    addTempToEmail(sub, desc, template)
                }

                val sendEmail = dialog!!.findViewById<Button>(R.id.proceedEmail)
                sendEmail.setOnClickListener {
                    val title = dialog!!.findViewById<EditText>(R.id.contactTitle)
                    val cntSub = dialog!!.findViewById<EditText>(R.id.contactSubject)
                    val cntDesc = dialog!!.findViewById<EditText>(R.id.contactDesc)
                     cntSubject = cntSub?.text.toString()
                     cntDescription = cntDesc?.text.toString()
                    val emailtitleText = title?.text.toString()
                    val contacted = tvEmail.text.toString()

                    if (cntSubject.isNotEmpty() || emailtitleText.isNotEmpty() || cntDescription.isNotEmpty()) {
                        contactStatus(cntSubject, emailtitleText, cntDescription,contacted)
                           // ToGmail(cntSubject, email!!, cntDescription)
                    } else {
                        Constants.error(requireContext(), "Some Fields are Missing")
                    }
                }
            }
        }
        return root
    }

    private  fun iconsEffect(linearLayout: LinearLayout) {
        val drawable: Drawable? = linearLayout.background

        // Check if the background drawable is not null
        if (drawable!= null){
            val mutableDrawable = drawable.mutate()
            mutableDrawable.setColorFilter(Color.argb(20, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
            linearLayout.background = mutableDrawable
        }
    }



    private fun ToGmail( sub: String, Email : String,Desc : String) {

        if (Email.isEmpty()) {
            Constants.error(requireContext(), "Please Enter Email")
            return
        }
        if (sub.isEmpty()) {
            Constants.error(requireContext(), "Please Enter Subject")
            return
        }


        val intent = Intent(Intent.ACTION_SEND)
        intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(Email))
        intent.putExtra(Intent.EXTRA_SUBJECT, sub)
        intent.putExtra(Intent.EXTRA_TEXT, Desc)
        if (!filePath.isNullOrEmpty()){
            val uri = Uri.parse(filePath)
            intent.putExtra(Intent.EXTRA_STREAM,uri)
            intent.type = "*/*"
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        }else{
            Constants.info(requireContext(),"Sending Email Without Attachment")
            intent.type = "plain/text"
        }

        try {
            startActivity(Intent.createChooser(intent, "Send Email"))
        } catch (ex: Exception) {
            Constants.error(requireContext(), ex.message ?: "")
        }
    }

    private fun contactStatus(subject: String, title: String, desc: String,contacted: String) {
        val requestModel = AddContactReq(
            callDuration!!, contacted, contactType!!, leadId!!, desc, subject, taskDate!!, title
        )

        // Check if path is null or empty
        if (path.isNullOrEmpty()) {
            // Proceed with the API call without attaching a file
            performContactApiCall(requestModel, null)
        } else {
            // If path is not null or empty, proceed with file attachment and API call
            val file = File(path)
            val content: ByteArray = file.readBytes()

            val fileExtension = MimeTypeMap.getFileExtensionFromUrl(file.absolutePath)
            val mediaType = UpdateContact.FileTypeHelper.getMediaTypeFromFileExtension(fileExtension)

            if (mediaType != null) {
                val requestBody = RequestBody.create(mediaType, content)
                    val body = MultipartBody.Part.createFormData("file", file.name, requestBody)
                    // Proceed with the API call with file attachment
                    performContactApiCall(requestModel, body)
            } else {
                Constants.error(requireContext(), "Unsupported file type")
            }
        }
    }

    // Function to perform API call
    private fun performContactApiCall(requestModel: AddContactReq, fileBody: MultipartBody.Part? ) {
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null) {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)

            val call = service.addContact("Bearer $jwtToken", leadId, fileBody, requestModel)
            call.enqueue(object : Callback<AddContactRespo> {
                override fun onResponse(
                    call: Call<AddContactRespo>,
                    response: Response<AddContactRespo>
                ) {
                    if (isAdded) {
                        if (response.isSuccessful) {
                            val responseBody = response.body()
                            if (dialog != null) {
                                dialog!!.dismiss()
                            }
                            if (cntSubject != "-" && cntDescription != "-") {
                                ToGmail(cntSubject, email, cntDescription)
                            }
                                if (taskDate.isNullOrEmpty()){
                                    sendTo()
                                }
                            taskDate = ""
                            Log.d("Response Contact", "$responseBody")

                            GetStatus(getLeadId())

                            Constants.success(requireContext(), "Successfully Contacted")

                        } else {
                            Constants.error(requireContext(), "Unsuccessful")
                        }
                    }
                }

                override fun onFailure(call: Call<AddContactRespo>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }
            })
        } else {
            Constants.error(requireContext(), "Token Not Found")
        }
    }

    private fun GetStatus(leadId: Int) {
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null) {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.getLeadDetails("Bearer $jwtToken" , leadId)
            call.enqueue(object : Callback<LeadDetailResponseModel> {
                override fun onResponse(
                    call: Call<LeadDetailResponseModel>,
                    response: Response<LeadDetailResponseModel>
                ) {
                    if (isAdded) {
                        if (response.isSuccessful && response.body() !=null) {
                            val data = response.body()
                            if (data != null) {
                                setStatus(data.leadStatus)
                            }
                        } else {
                            Constants.error(requireContext(), "Unsuccessful Response ${response.code()}")
                        }
                    }
                }

                override fun onFailure(call: Call<LeadDetailResponseModel>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }
            })
        } else {
            Constants.error(requireContext(), "Token Not Found")
        }
    }

    private fun setStatus(leadStatus: String) {

        if (leadStatus!=null)
        {
            when(leadStatus)
            {
                "New Lead"->{

                    binding.qualifyBtn.visibility = View.INVISIBLE
                    // binding.qualifiedIcon.visibility = View.INVISIBLE
                    binding.qualifiedIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.proposalBtn.visibility = View.INVISIBLE
                    //binding.proposalIcon.visibility = View.INVISIBLE
                    binding.proposalIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.negotiationBtn.visibility = View.INVISIBLE
                    //binding.negotiationIcon.visibility = View.INVISIBLE
                    binding.negotiationIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)

                }

                "Contacted"->{
                    binding.qualifyBtn.visibility = View.VISIBLE
                    //binding.qualifiedIcon.visibility = View.VISIBLE
                    binding.qualifiedIcon.setColorFilter(Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.proposalBtn.visibility = View.INVISIBLE
                    binding.proposalIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.proposalIcon.visibility = View.INVISIBLE
                    binding.negotiationBtn.visibility = View.INVISIBLE
                    binding.negotiationIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.negotiationIcon.visibility = View.INVISIBLE
                }

                "Email Sent"->{
                    binding.qualifyBtn.visibility = View.VISIBLE
                    //binding.qualifiedIcon.visibility = View.VISIBLE
                    binding.qualifiedIcon.setColorFilter(Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.proposalBtn.visibility = View.INVISIBLE
                    binding.proposalIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.proposalIcon.visibility = View.INVISIBLE
                    binding.negotiationBtn.visibility = View.INVISIBLE
                    binding.negotiationIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.negotiationIcon.visibility = View.INVISIBLE
                }

                "Not Contacted"->{
                    binding.qualifyBtn.visibility = View.INVISIBLE
                    //binding.qualifiedIcon.visibility = View.INVISIBLE
                    binding.qualifiedIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.proposalBtn.visibility = View.INVISIBLE
                    binding.proposalIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.proposalIcon.visibility = View.INVISIBLE
                    binding.negotiationBtn.visibility = View.INVISIBLE
                    binding.negotiationIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.negotiationIcon.visibility = View.INVISIBLE
                }

                "Qualified"->{
                    binding.qualifyBtn.visibility = View.VISIBLE
                    binding.qualifiedIcon.setColorFilter(Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.qualifiedIcon.visibility = View.VISIBLE
                    binding.proposalBtn.visibility = View.VISIBLE
                    //binding.proposalIcon.visibility = View.VISIBLE
                    binding.proposalIcon.setColorFilter(Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.negotiationBtn.visibility = View.INVISIBLE
                    binding.negotiationIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.negotiationIcon.visibility = View.INVISIBLE
                }

                "Not Qualified"->{
                    binding.qualifyBtn.visibility = View.VISIBLE
                    binding.qualifiedIcon.setColorFilter(Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.qualifiedIcon.visibility = View.VISIBLE
                    binding.proposalBtn.visibility = View.INVISIBLE
                    binding.proposalIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.proposalIcon.visibility = View.INVISIBLE
                    binding.negotiationBtn.visibility = View.INVISIBLE
                    binding.negotiationIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.negotiationIcon.visibility = View.INVISIBLE
                }

                "Proposal Sent"->{
                    binding.qualifyBtn.visibility = View.VISIBLE
                    binding.qualifiedIcon.setColorFilter(Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.qualifiedIcon.visibility = View.VISIBLE
                    binding.proposalBtn.visibility = View.VISIBLE
                    binding.proposalIcon.setColorFilter(Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.proposalIcon.visibility = View.VISIBLE
                    binding.negotiationBtn.visibility = View.VISIBLE
                    binding.qualifiedIcon.setColorFilter(Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.negotiationIcon.visibility = View.VISIBLE
                }

                "Lead Won"->{
                    binding.callNow.visibility = View.INVISIBLE
                    binding.emailNow.visibility = View.INVISIBLE
                    binding.contactBtn.isEnabled = false
                    binding.contactBtn.setBackgroundColor(resources.getColor(R.color.FaintGrey))
                    binding.contactIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.contactIcon.visibility = View.VISIBLE
                    binding.qualifyBtn.isEnabled = false
                    binding.qualifyBtn.setBackgroundColor(resources.getColor(R.color.FaintGrey))
                    binding.qualifiedIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.qualifiedIcon.visibility = View.INVISIBLE
                    binding.proposalBtn.isEnabled = false
                    binding.proposalBtn.setBackgroundColor(resources.getColor(R.color.FaintGrey))
                    binding.proposalIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.proposalIcon.visibility = View.INVISIBLE
                    binding.negotiationBtn.isEnabled = false
                    binding.negotiationBtn.setBackgroundColor(resources.getColor(R.color.FaintGrey))
                    binding.negotiationIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.negotiationIcon.visibility = View.INVISIBLE
                }

                "Lead Lost"->{
                    binding.callNow.visibility = View.INVISIBLE
                    binding.emailNow.visibility = View.INVISIBLE
                    binding.contactBtn.isEnabled = false
                    binding.contactBtn.setBackgroundColor(resources.getColor(R.color.FaintGrey))
                    binding.contactIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.contactIcon.visibility = View.INVISIBLE
                    binding.qualifyBtn.isEnabled = false
                    binding.qualifyBtn.setBackgroundColor(resources.getColor(R.color.FaintGrey))
                    binding.qualifiedIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.qualifiedIcon.visibility = View.INVISIBLE
                    binding.proposalBtn.isEnabled = false
                    binding.proposalBtn.setBackgroundColor(resources.getColor(R.color.FaintGrey))
                    binding.proposalIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.proposalIcon.visibility = View.INVISIBLE
                    binding.negotiationBtn.isEnabled = false
                    binding.negotiationBtn.setBackgroundColor(resources.getColor(R.color.FaintGrey))
                    binding.negotiationIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    //binding.negotiationIcon.visibility = View.INVISIBLE
                }
            }
        }
    }

    private fun sendTo() {
            if (contacted != null) {
                val intent = Intent(Intent.ACTION_CALL)
                intent.setData(Uri.parse("tel:" + contacted));
                startActivity(intent)
                contactType = "call"

            } else {
                Constants.error(context, "Phone Number not found")
            }
    }

    private fun showDateTimePicker() {
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH)
        val hourOfDay = calendar.get(Calendar.HOUR_OF_DAY)
        val minute = calendar.get(Calendar.MINUTE)

        val datePickerDialog = DatePickerDialog(requireContext(), R.style.MyDatePickerDialogTheme, { _, year, month, dayOfMonth ->
            val formattedMonth = String.format("%02d", month + 1) // Month starts from 0
            val formattedDayOfMonth = String.format("%02d", dayOfMonth)
            val selectedDate = "$year-$formattedMonth-$formattedDayOfMonth"

            val timePickerDialog = TimePickerDialog(requireContext(), R.style.MyDatePickerDialogTheme, { _, hourOfDay, minute ->
                val formattedHour = String.format("%02d", hourOfDay)
                val formattedMinute = String.format("%02d", minute)
                val selectedTime = "$formattedHour:$formattedMinute"

                // Concatenate date and time with 'T' separator
                val dateTime = "$selectedDate"+"T"+"$selectedTime"

                // Set the formatted date and time to the TextView
                val taskDateTextView = dialog!!.findViewById<TextView>(R.id.taskTime)
                taskDateTextView.text = dateTime
                taskDate = dateTime
            }, hourOfDay, minute, false)

            timePickerDialog.show()
        }, year, month, dayOfMonth)

        datePickerDialog.show()
    }

    private fun negotationStatus(desc: String, negoRevenue: String, negoReason: String, titleText: String
    ) {

        val reqbody = NegotiationReq(revenue!!,desc,leadId!!,negoRevenue,LeadStatus!!,negoReason,titleText)
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null){
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.proposalNegotiation("Bearer $jwtToken",leadId!!,reqbody)
            call.enqueue(object : Callback<NegotiationRespo>{
                override fun onResponse(
                    call: Call<NegotiationRespo>,
                    response: Response<NegotiationRespo>
                ) {
                    if (isAdded) {
                        if (response.isSuccessful) {
                            val responsebodt = response.body()
                            dialog!!.dismiss()
                            Constants.success(requireContext(), "Negotiation Status Update")
                            GetStatus(getLeadId())
                            Log.d("NegotiationResponse", "$responsebodt")
                        } else {
                            Constants.error(requireContext(), "Unsuccessful")
                        }
                    }
                }

                override fun onFailure(call: Call<NegotiationRespo>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }
            })
        }else {
            Constants.error(requireContext(), "Token Not Found")
        }

    }

    private fun qualifiyProposal(sendTitle: String, sendReason: String, sendReq: String) {
        val reqbody = QualifyProposalReq(leadId!!,qualifyStatus,sendReason, sendReq,sendTitle)
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null){
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.proposalQualify("Bearer $jwtToken",leadId!!,reqbody)
            call.enqueue(object : Callback<QualifyProposalRespo>{
                override fun onResponse(
                    call: Call<QualifyProposalRespo>,
                    response: Response<QualifyProposalRespo>
                ) {
                    if (isAdded) {
                        if (response.isSuccessful) {
                            val responsebodt = response.body()
                            dialog!!.dismiss()
                            Log.d("QualifyResponse", "$responsebodt")
                            Constants.success(requireContext(), "Qualify Status Update")

                            GetStatus(getLeadId())


                        } else {
                            Constants.error(requireContext(), "Unsuccessful")
                        }
                    }
                }

                override fun onFailure(call: Call<QualifyProposalRespo>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }
            })
        }else {
            Constants.error(requireContext(), "Token Not Found")
        }

    }

    private fun showFileChooser() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
            intent.type = "*/*"
            startActivityForResult(intent, PICKFILE_RESULT_CODE)
        } else {
            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.type = "*/*"
            intent.addCategory(Intent.CATEGORY_OPENABLE)
            startActivityForResult(
                Intent.createChooser(intent, "Select File"),
                PICKFILE_RESULT_CODE
            )
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == PICKFILE_RESULT_CODE && resultCode == Activity.RESULT_OK && data != null && data.data != null) {
            val uri: Uri = data.data!!
            path = getPathFromUri(requireContext(),uri)!!
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                updateTextViewWithFileName(uri)
            } else {
                // Handle for lower versions
            }
        }
    }
    @SuppressLint("SetTextI18n")
    private fun updateTextViewWithFileName(uri: Uri) {
        val fileName = getFileNameFromUri(uri)
        filePath = uri.toString()
        if (fileName!= null){
            val selectedFile = dialog!!.findViewById<TextView>(R.id.proposalFileNme)
            selectedFile.text = Html.fromHtml("<b><font color='#7FFF00'>$fileName</font></b>")
        }
    }

    private fun getFileNameFromUri(uri: Uri): String? {
        var fileName: String? = null
        val cursor =requireContext().contentResolver.query(uri, null, null, null, null)
        if (cursor!= null){
            if (cursor.moveToFirst()) {
                val displayNameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
                if (displayNameIndex != -1) {
                    fileName = cursor.getString(displayNameIndex)
                }
            }
            cursor.close()
        }
        return fileName
    }
    @SuppressLint("Range")
    private fun getPathFromUri(context: Context, uri: Uri): String? {
        var path: String? = null
        if ("content".equals(uri.scheme, ignoreCase = true)) {
            // Using ACTION_OPEN_DOCUMENT
            val cursor = context.contentResolver.query(uri, null, null, null, null)
            cursor?.use {
                if (it.moveToFirst()) {
                    val displayName = it.getString(it.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                    val type = it.getString(it.getColumnIndex("mime_type"))
                    val file = File(context.cacheDir, displayName)
                    try {
                        val inputStream = context.contentResolver.openInputStream(uri)
                        inputStream?.use { input ->
                            FileOutputStream(file).use { output ->
                                input.copyTo(output)
                            }
                        }
                        path = file.absolutePath
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
            }
        } else if ("file".equals(uri.scheme, ignoreCase = true)) {
            path = uri.path
        }
        return path
    }

    private fun addTempToEmail(sub: EditText?, desc: EditText?, template: Button) {
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null) {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val callTemplates = service.templateList("Bearer $jwtToken")
            callTemplates.enqueue(object : Callback<MutableList<TemplateModelItem>> {
                override fun onResponse(
                    call: Call<MutableList<TemplateModelItem>>,
                    response: Response<MutableList<TemplateModelItem>>
                ) {
                    if (isAdded) {
                        if (response.isSuccessful) {
                            val data = response.body()
                            if (data != null) {
                                val popupMenu = PopupMenu(context, template)
                                data.forEach { templateItem ->
                                    val menuItem = popupMenu.menu.add(templateItem.title)
                                    menuItem.setOnMenuItemClickListener {
                                        sub?.setText(templateItem.title)
                                        desc?.setText(templateItem.description)
                                        true
                                    }
                                    // Set text color to black
                                    val textColor = Color.BLACK
                                    val spanString = SpannableString(templateItem.title)
                                    spanString.setSpan(
                                        ForegroundColorSpan(textColor),
                                        0,
                                        spanString.length,
                                        0
                                    )
                                    menuItem.title = spanString
                                }
                                popupMenu.show()
                            } else {
                                Constants.error(requireContext(), "Response body is null")
                            }
                        } else {
                            Constants.error(
                                requireContext(),
                                "Unsuccessful response: ${response.code()}"
                            )
                        }
                    }
                }

                override fun onFailure(call: Call<MutableList<TemplateModelItem>>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }

            })
        } else {
            Constants.error(requireContext(), "Token is null. Please log in again.")
        }
    }

    private fun addProposal(desc:String,mail : String,subject: String){
        val requestModel = AddProposalReq(desc,leadId!!,mail,subject)
        if (path != null && path.isNotEmpty()) {
            val file = File(path)
            val content: ByteArray = file.readBytes()

            val fileExtension = MimeTypeMap.getFileExtensionFromUrl(file.absolutePath)
            val mediaType = UpdateContact.FileTypeHelper.getMediaTypeFromFileExtension(fileExtension)
            if (mediaType != null){
                val requestBody = RequestBody.create(mediaType, content)
                val jwtToken = gettoken(requireContext())
                if (jwtToken != null) {
                    val service = ApiUtilities.getinstance().create(ApiInterface::class.java)

                    val uploadFile = MultipartBody.Part.createFormData("file", file.name, requestBody)
                    val call = service.addProposal("Bearer $jwtToken", leadId,uploadFile,requestModel)
                    call.enqueue(object : Callback<AddProposalRespo> {
                        override fun onResponse(
                            call: Call<AddProposalRespo>,
                            response: Response<AddProposalRespo>
                        ) {
                            if (isAdded)
                            {
                                if (response.isSuccessful) {
                                    val responseBody = response.body()
                                    Log.d("ResponseBody", "$responseBody")
                                    dialog!!.dismiss()
                                    Constants.success(requireContext(),"Proposal Sent Successfully")
                                    GetStatus(getLeadId())
                                }else{
                                    Constants.error(requireContext(),"Unsuccessful")
                                }
                            }
                        }
                        override fun onFailure(call: Call<AddProposalRespo>, t: Throwable) {
                            Constants.error(requireContext(), "Error: ${t.message}")
                        }
                    })
                } else {
                    Constants.error(requireContext(), "Token Not Found")
                }
            } else {
                Constants.error(requireContext(), "Unsupported file type")
            }
        }else{
            Constants.error(requireContext(), "File not Selected, Please Select a file")
        }
    }

    private fun initializeTabLayoutAndViewPager(
        tabLayout: TabLayout,
        viewPager2: ViewPager2,
        adapter: ProposalALAdapter
    ) {
        // Set the adapter for the ViewPager2
        viewPager2.adapter = adapter

        // Add tabs to the TabLayout
        tabLayout.addTab(tabLayout.newTab().setText("Own"))
        tabLayout.addTab(tabLayout.newTab().setText("Customer"))
        tabLayout.addTab(tabLayout.newTab().setText("Product"))
        tabLayout.addTab(tabLayout.newTab().setText("T & C"))

        // Set up TabLayout with ViewPager2
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                tab?.let {
                    viewPager2.currentItem = it.position
                }
            }
            override fun onTabUnselected(tab: TabLayout.Tab?) {}
            override fun onTabReselected(tab: TabLayout.Tab?) {}
        })

        viewPager2.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                tabLayout.getTabAt(position)?.select()
            }
        })
    }
    private fun showDialog(layoutResId: Int) {
        if (dialog == null) {
            dialog = Dialog(requireActivity()).apply {
                setCancelable(false)
                window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                setContentView(layoutResId)
                show()
            }
        } else {
            dialog?.setContentView(layoutResId)
            dialog?.show()
        }

        val closeBtn = dialog!!.findViewById<ImageButton>(R.id.closeBtn)
        closeBtn.setOnClickListener {
            dialog!!.dismiss()
        }
    }


    private fun updateAdditionalLayout( container: LinearLayout,layout: View) {
        container.removeAllViews()
        container.addView(layout)

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_PERMISSION_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            } else {
                Constants.error(context, "Permission denied to read call logs")
            }
        }
    }


    private fun gettoken(context: Context?): String? {
        val sharedPreferences = requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
        return sharedPreferences.getString("jwtToken", null)
    }

    private fun getLeadId(): Int {
        val sharedPreferences = requireContext().getSharedPreferences("LeadPreferences", Context.MODE_PRIVATE)
        return sharedPreferences.getInt("leadId", -1)
    }

    private fun getIndex(spinner: Spinner, myString: Any): Int {
        for (i in 0 until spinner.count) {
            if (spinner.getItemAtPosition(i).toString() == myString) {
                return i
            }
        }
        return 0 // Default to 0 if not found
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null

        val prefs = requireActivity().getSharedPreferences("ProductPrefs", Context.MODE_PRIVATE)
        val editor = prefs.edit()
        editor.clear()
        editor.apply()

        val Detailprefs = requireActivity().getSharedPreferences("product_details", Context.MODE_PRIVATE)
        val Detaileditor = Detailprefs.edit()
        Detaileditor.clear()
        Detaileditor.apply()

        val removeCustomer = requireActivity().getSharedPreferences("CustomerPrefs", Context.MODE_PRIVATE)
        val removeCus = removeCustomer.edit()
        removeCus.clear()
        removeCus.apply()

        val removeOwner = requireActivity().getSharedPreferences("OwnerPref", Context.MODE_PRIVATE)
        val own = removeOwner.edit()
        own.clear()
        own.apply()

        val t_and_c = requireActivity().getSharedPreferences("TandCPrefs", Context.MODE_PRIVATE)
        val TC = t_and_c.edit()
        TC.clear()
        TC.apply()
    }
}
