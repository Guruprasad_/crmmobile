package com.BrightLink.SalesOptim.OnBoard

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.BrightLink.SalesOptim.Authenthication.LoginActivity
import com.BrightLink.SalesOptim.databinding.ActivitySplashScreenBinding

class SplashScreen : AppCompatActivity() {

    private lateinit var binding : ActivitySplashScreenBinding
    private lateinit var sharedPreferences: SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivitySplashScreenBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)


        sharedPreferences = getSharedPreferences("onboarding_prefs", Context.MODE_PRIVATE)
        val hasSeenOnboarding = sharedPreferences.getBoolean("has_seen_onboarding", false)

        Handler().postDelayed(Runnable {
            if (hasSeenOnboarding) {
                startActivity(Intent(this, LoginActivity::class.java))
                finish()
            } else {
                startActivity(Intent(this, OnBoard1::class.java))
                finish()
            }
        },2000)

    }
}