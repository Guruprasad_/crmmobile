package com.BrightLink.SalesOptim.OnBoard

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.BrightLink.SalesOptim.Authenthication.LoginActivity
import com.BrightLink.SalesOptim.databinding.ActivityOnboard2Binding

class OnBoard2 : AppCompatActivity() {

    private lateinit var binding: ActivityOnboard2Binding
    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivityOnboard2Binding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        binding.start.setOnClickListener{
            startActivity(Intent(this@OnBoard2,LoginActivity::class.java))
            finish()
        }

        binding.skip.setOnClickListener {
            startActivity(Intent(this@OnBoard2,LoginActivity::class.java))
            finish()
        }




    }

}