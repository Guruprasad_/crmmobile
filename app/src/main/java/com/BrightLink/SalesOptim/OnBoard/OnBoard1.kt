package com.BrightLink.SalesOptim.OnBoard

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.BrightLink.SalesOptim.Authenthication.LoginActivity
import com.BrightLink.SalesOptim.databinding.ActivityOnboard1Binding

class OnBoard1 : AppCompatActivity() {

    private lateinit var binding : ActivityOnboard1Binding
    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivityOnboard1Binding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        val sharedPreferences = getSharedPreferences("onboarding_prefs", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()

        binding.skip.setOnClickListener{
            startActivity(Intent(this@OnBoard1,LoginActivity::class.java))
            finish()
            editor.putBoolean("has_seen_onboarding", true) // Change the value as needed
            editor.apply()
        }

        binding.next.setOnClickListener{
            startActivity(Intent(this@OnBoard1,OnBoard2::class.java))
            finish()
            editor.putBoolean("has_seen_onboarding", true) // Change the value as needed
            editor.apply()
        }

    }
}