package com.BrightLink.SalesOptim.Admin.AdminActivities.PipelineReport

import android.app.AlertDialog
import android.app.TimePickerDialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.ImageButton
import android.widget.Spinner
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminRequestModel.AddPipelinePostModel
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel.AddPipelineResponseModel
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel.PipelineReportModelItem
import com.BrightLink.SalesOptim.AdminAdapters.PipelineReportAdapter
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.Constants.WrapContentLinearLayoutManager
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.ResponseModel.Admin.ShowEmployeeModelItem
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.AdminFragmentPipelineReportBinding
import com.google.android.material.button.MaterialButton
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.DateValidatorPointForward
import com.google.android.material.datepicker.MaterialDatePicker
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale

class PipelineReportFragment : Fragment(), UpdatePipelineReportFragment.PipelineRefreshListener {

    private var _binding: AdminFragmentPipelineReportBinding? = null
    private val binding get() = _binding!!

    private lateinit var employeeNameSpinner: Spinner
    private lateinit var employeeNameAdapter: ArrayAdapter<String>
    private var employeeNames: ArrayList<String> = ArrayList()

    private val employeeMap: MutableMap<String, String> = mutableMapOf()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = AdminFragmentPipelineReportBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.actionBar.activityName.text = "Pipeline Report"
        binding.actionBar.back.setOnClickListener {
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }

        binding.recyclerView.layoutManager = WrapContentLinearLayoutManager(context)

        pipelineReportList()

        binding.actionBar.option.setOnClickListener {
            val dialogView = layoutInflater.inflate(R.layout.admin_pipeline_set_target, null)
            val dialogBuilder = AlertDialog.Builder(requireContext()).setView(dialogView)

            val empName = dialogView.findViewById<Spinner>(R.id.employeeName)
            val startDate = dialogView.findViewById<TextView>(R.id.startDatePipeline)
            val endDate = dialogView.findViewById<TextView>(R.id.endDatePipeline)
            val leadTarget = dialogView.findViewById<EditText>(R.id.leadTarget)
            val revenueTarget = dialogView.findViewById<EditText>(R.id.revenueTarget)
            val winTarget = dialogView.findViewById<EditText>(R.id.winTarget)
            val save = dialogView.findViewById<MaterialButton>(R.id.saveTargetBtn)
            val close = dialogView.findViewById<ImageButton>(R.id.setTargetClose)

            val dialog = dialogBuilder.create()
            dialog.show()

            employeeNameSpinner = empName
            employeeNameAdapter = ArrayAdapter(requireContext(), R.layout.spinner_item_layout, employeeNames).apply {
                setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            }
            employeeNameSpinner.adapter = employeeNameAdapter

            fetchEmployeeNames()

            startDate.setOnClickListener {
                showMaterialCalendar(startDate)
            }

            endDate.setOnClickListener {
                showMaterialCalendar(endDate)
            }

            save.setOnClickListener {
                val empName = empName.selectedItem.toString()
                val empId = employeeMap[empName]!!
                Log.d("empID","empId: $empId")

                val startDate = startDate.text.toString()
                val endDate = endDate.text.toString()
                val leadTarget = leadTarget.text.toString()
                val revenueTarget = revenueTarget.text.toString()
                val winTarget = winTarget.text.toString()

                if (empName.isEmpty() || startDate.isEmpty() || endDate.isEmpty() || leadTarget.isEmpty() || revenueTarget.isEmpty() || winTarget.isEmpty()){
                    Constants.warning(requireContext(), "All fields are required")
                    return@setOnClickListener
                }

//                if (!isDateValid(startDate, endDate)){
//                    Constants.warning(requireContext(), "End Date must be $startDate or later")
//                    return@setOnClickListener
//                }

                val reqBody = AddPipelinePostModel(empId, endDate, leadTarget, revenueTarget, startDate, winTarget)
                Log.d("reqBody", "reqBody: $reqBody")

                getToken()?.let { jwtToken ->
                    val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
                    setTarget(service, jwtToken, reqBody, dialog)
                } ?: Constants.error(requireContext(), "Token is null. Please log in again.")

            }

            close.setOnClickListener {
                dialog.dismiss()
            }
        }

        return root
    }

    private fun pipelineReportList() {
        getToken()?.let { jwtToken ->
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            callApi(service, jwtToken)
        } ?: Constants.error(requireContext(), "Please try to login again")
    }

    private fun fetchEmployeeNames() {
        getToken()?.let { jwtToken ->

            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.showsEmployeeAdmin("Bearer $jwtToken")
            call.enqueue(object : Callback<List<ShowEmployeeModelItem>> {
                override fun onResponse(
                    call: Call<List<ShowEmployeeModelItem>>,
                    response: Response<List<ShowEmployeeModelItem>>
                ) {
                    if (response.isSuccessful){
                        response.body()?.let { data ->

                            employeeNames.clear()
                            for (item in data){
                                item.name?.let {
                                    employeeNames.add(it)
                                }
                                item.cId?.let { empId ->
                                    employeeMap[item.name ?: ""] = empId.toString()
                                }
                            }
                            Log.d("emp", "Employee : $employeeNames")
                            employeeNameAdapter.notifyDataSetChanged()

                        } ?: Constants.error(requireContext(), "Employee list is empty")
                    }
                    else {
                        Constants.error(requireContext(), "Something went wrong, Try again")
                    }
                }

                override fun onFailure(call: Call<List<ShowEmployeeModelItem>>, t: Throwable) {
                    Constants.error(requireContext(), "Failed to fetch employee name. Try again")
                }

            })

        } ?: Constants.error(requireContext(), "Please try to Login Again")
    }

    private fun callApi(service: ApiInterface, jwtToken: String) {
        val call = service.pipelineReportAdmin("Bearer $jwtToken")
        call.enqueue(object : Callback<List<PipelineReportModelItem>> {
            override fun onResponse(
                call: Call<List<PipelineReportModelItem>>,
                response: Response<List<PipelineReportModelItem>>
            ) {
                if (response.isSuccessful){
                    response.body()?.let { data ->
                        val adapter = PipelineReportAdapter(requireContext(), data, childFragmentManager)
                        binding.recyclerView.adapter = adapter
                    } ?: Constants.error(requireContext(), "Pipeline report list is empty")
                } else {
                    Constants.error(requireContext(), "Something went wrong, Try again")
                }
            }

            override fun onFailure(call: Call<List<PipelineReportModelItem>>, t: Throwable) {
                Constants.error(requireContext(), "Failed to fetch Pipeline Target. Try again")
            }

        })
    }

    private fun setTarget(service: ApiInterface, jwtToken: String, reqBody: AddPipelinePostModel, dialog: AlertDialog) {
        val call = service.setTargetPipelineAdmin("Bearer $jwtToken", reqBody)
        call.enqueue(object : Callback<AddPipelineResponseModel>{
            override fun onResponse(
                call: Call<AddPipelineResponseModel>,
                response: Response<AddPipelineResponseModel>
            ) {
                if (response.isSuccessful){
                    Log.d("response", "Response: ${response.body()}")
                    Constants.success(requireContext(), "Monthly Target set successfully")
                    dialog.dismiss()
                    pipelineReportList()
                }
                else {
                    Log.d("response", "Response: ${response.body()}")
                    Constants.error(requireContext(), "Something went wrong. Try Again")
                }
            }

            override fun onFailure(call: Call<AddPipelineResponseModel>, t: Throwable) {
                Constants.error(requireContext(), "Failed to set monthly target. Try again")
            }

        })
    }

    private fun getToken(): String? {
        return requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
            .getString("jwtToken", null)
    }

    private fun showMaterialCalendar(textView: TextView) {
        val today = Calendar.getInstance().timeInMillis
        val datePickerBuilder = MaterialDatePicker.Builder.datePicker()
        datePickerBuilder.setSelection(today)

        val constraintsBuilder = CalendarConstraints.Builder()
        constraintsBuilder.setValidator(DateValidatorPointForward.now())

        datePickerBuilder.setCalendarConstraints(constraintsBuilder.build())

        val datePicker = datePickerBuilder.build()

        datePicker.addOnPositiveButtonClickListener {selectedDate->
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = selectedDate

            // Launch TimePickerDialog
            val timePickerDialog = TimePickerDialog(
                requireContext(),
                { _, hourOfDay, minute ->
                    // Combine date and time
                    calendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
                    calendar.set(Calendar.MINUTE, minute)

                    // Format the final date-time
                    val formattedDateTime = SimpleDateFormat("yyyy-MM-dd'T'HH:mm", Locale.getDefault()).format(calendar.time)
                    textView.text = formattedDateTime
                },
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                false
            )

            timePickerDialog.show()
        }
        datePicker.show(parentFragmentManager, datePicker.toString())

    }

    private fun isDateValid(startDate: String, endDate: String): Boolean {
        val calendarFrom = Calendar.getInstance().apply {
            timeInMillis = parseDateString(startDate)
        }
        val calendarTo = Calendar.getInstance().apply {
            timeInMillis = parseDateString(endDate)
        }
        return calendarTo.after(calendarFrom)
    }

    private fun parseDateString(dateString: String): Long {
        val pattern = "yyyy-MM-dd'T'HH:mm"
        val dateFormat = SimpleDateFormat(pattern, Locale.getDefault())
        return try {
            dateFormat.parse(dateString)?.time ?: 0
        } catch (e: ParseException) {
            Log.e("Parsing Error", "Error parsing date", e)
            0
        }
    }

    override fun onPipelineRefresh() {
        pipelineReportList()
    }

}