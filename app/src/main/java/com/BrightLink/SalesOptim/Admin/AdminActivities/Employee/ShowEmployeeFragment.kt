package com.BrightLink.SalesOptim.Admin.AdminActivities.Employee

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.BrightLink.SalesOptim.Admin.AdminAdapters.EmployeeAdapter
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.Constants.WrapContentLinearLayoutManager
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.ResponseModel.Admin.ShowEmployeeModelItem
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.AdminFragmentEmployeeBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ShowEmployeeFragment : Fragment() {

    private var _binding: AdminFragmentEmployeeBinding? = null
    private val binding get() = _binding!!

    private lateinit var adapter : EmployeeAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = AdminFragmentEmployeeBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.actionBar.activityName.text = "Employee"
        binding.actionBar.back.setOnClickListener {
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }

        binding.recyclerView.layoutManager = WrapContentLinearLayoutManager(context)

        refreshEmployeeList()

        binding.addEmployee.setOnClickListener {
            findNavController().navigate(R.id.adminAddEmployee)
        }

        binding.searchEmployee.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                adapter.filter(s.toString())
            }
        })

        return root
    }

    private fun refreshEmployeeList() {
        getToken()?.let { jwtToken ->
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            callApi(service, jwtToken)
        } ?: Constants.error(requireContext(), "Please try to Login Again")
    }

    private fun callApi(service: ApiInterface, jwtToken: String) {
        val call = service.showsEmployeeAdmin("Bearer $jwtToken")
        call.enqueue(object : Callback<List<ShowEmployeeModelItem>> {
            override fun onResponse(
                call: Call<List<ShowEmployeeModelItem>>,
                response: Response<List<ShowEmployeeModelItem>>
            ) {
                if (isAdded)
                {
                    if (response.isSuccessful){
                        response.body()?.let { data ->
                            adapter = EmployeeAdapter(requireContext(), data)
                            binding.recyclerView.adapter = adapter
                        } ?: Constants.error(requireContext(), "Employee list is empty")
                    }
                    else {
                        Constants.error(requireContext(), "Something went wrong, Try again")
                    }
                }
            }

            override fun onFailure(call: Call<List<ShowEmployeeModelItem>>, t: Throwable) {
                Constants.error(requireContext(), "Failed to Fetch Employee List")
            }

        })
    }

    private fun getToken(): String? {
        return requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
            .getString("jwtToken", null)
    }

}