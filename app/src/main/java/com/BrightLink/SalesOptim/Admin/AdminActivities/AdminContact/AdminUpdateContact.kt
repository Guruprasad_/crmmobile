package com.BrightLink.SalesOptim.Admin.AdminActivities.AdminContact

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.OpenableColumns
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.MimeTypeMap
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import android.widget.Spinner
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.BrightLink.SalesOptim.Adapters.ContactLANadapters.ContactAttachmentAdapter
import com.BrightLink.SalesOptim.Adapters.ContactLANadapters.ContactLinksAdapter
import com.BrightLink.SalesOptim.Adapters.ContactLANadapters.ContactNotesAdapter
import com.BrightLink.SalesOptim.Adapters.ContactMeetingDetailsAdapter
import com.BrightLink.SalesOptim.Adapters.ContactTaskDetailsAdapter
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.Constants.WrapContentLinearLayoutManager
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.RequestModel.AddLinkReqModel
import com.BrightLink.SalesOptim.RequestModel.AddNoteRequest
import com.BrightLink.SalesOptim.RequestModel.AdminUpdateContactPutModel
import com.BrightLink.SalesOptim.RequestModel.AttachmentDto
import com.BrightLink.SalesOptim.ResponseModel.AddLinkResponseModel
import com.BrightLink.SalesOptim.ResponseModel.AddNoteModel
import com.BrightLink.SalesOptim.ResponseModel.AttaModel
import com.BrightLink.SalesOptim.ResponseModel.ContactAttachmentsResModel
import com.BrightLink.SalesOptim.ResponseModel.ContactMeetingDetailsModelItem
import com.BrightLink.SalesOptim.ResponseModel.ContactTaskDetailsModelItem
import com.BrightLink.SalesOptim.ResponseModel.ShowContactModelItem
import com.BrightLink.SalesOptim.ResponseModel.ShowAccountsModelItem
import com.BrightLink.SalesOptim.ResponseModel.UpdateContactResponseModel
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.AdminActivityUpdateContactBinding
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class AdminUpdateContact : Fragment()  {

    object FileTypeHelper {
        fun getMediaTypeFromFileExtension(extension: String?): MediaType? {
            val mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
            return mimeType?.let { it.toMediaTypeOrNull() }
        }
    }

    private var _binding: AdminActivityUpdateContactBinding? = null
    private val binding get() = _binding!!
    private lateinit var accountSpinner: Spinner
    private lateinit var accountAdapter: ArrayAdapter<String>
    private var accountNames: ArrayList<String> = ArrayList()

    private val PICKFILE_RESULT_CODE = 1
    private var filePath: String? = null
    private var dialog: Dialog? = null
    private lateinit var path : String
    private var ContactId : Int = 0
    private lateinit var Desc : String

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = AdminActivityUpdateContactBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.actionBar.activityName.text = "Update Contact"
        binding.actionBar.back.setOnClickListener{
            requireActivity().onBackPressed()
        }
        binding.addNoteCnt.setOnClickListener {
            showDialog(R.layout.account_add_notes)

            val fileChooser = dialog!!.findViewById<Button>(R.id.noteFileOneChoose)
            fileChooser.setOnClickListener {
                showFileChooser()
            }
            val noteDescription = dialog!!.findViewById<EditText>(R.id.fileNoteDesEditText)

            val addNote = dialog!!.findViewById<Button>(R.id.addNote)

            addNote.setOnClickListener {
                Desc = noteDescription.text.toString()
                addNotes()

            }
        }

        binding.addAttachment.setOnClickListener {
            showDialog(R.layout.update_contact_add_attachment)
            val fileChooser = dialog!!.findViewById<Button>(R.id.filechooser)
            fileChooser.setOnClickListener {
                showFileChooser()
            }
            val sendAttachment = dialog!!.findViewById<Button>(R.id.addAtt)
            sendAttachment.setOnClickListener {
                addAttachmentContact()

            }
        }

        binding.contactAddLink.setOnClickListener {
            showDialog(R.layout.account_add_links)

            val addLinkPop = dialog!!.findViewById<Button>(R.id.addLinkPop)
            val addLabel = dialog!!.findViewById<EditText>(R.id.addLabel)
            val addDesc = dialog!!.findViewById<EditText>(R.id.linkDesc)
            val addLinkUrl = dialog!!.findViewById<EditText>(R.id.linkUrl)

            addLinkPop.setOnClickListener {
                val label = addLabel.text.toString().trim()
                val desc = addDesc.text.toString().trim()
                val linkUrl = addLinkUrl.text.toString().trim()

                if (linkUrl.isEmpty()) {
                    Constants.error(requireContext(), "Link URL cannot be empty")
                } else {
                    addLinks(label, desc, linkUrl, ContactId)
                }
            }
        }
        binding.actionBar.option.visibility = View.INVISIBLE

        // Initialize spinners and adapters
        accountSpinner = binding.accountName

        accountAdapter = ArrayAdapter(requireContext(), R.layout.spinner_item_layout_currency, accountNames)
        accountAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        accountSpinner.adapter = accountAdapter

        // Call fetchAccountAndVendorNames to populate spinners
        fetchAccountNames()

        // Fetch the selected contact item
        val selectedItem = arguments?.getSerializable("selectedItem") as? ShowContactModelItem
        if (selectedItem == null) {
            // Handle null case, perhaps show an error message
            Constants.error(requireContext(), "Selected item is null")
            return root
        }
        val contactId = selectedItem.contactid
//        if (contactId != null) {
//            Constants.info(requireContext(),"Contact Id fetched ${contactId}")
//            fetchAttachmentsLinksNotes(contactId)        }
//        else{
//            Constants.error(requireContext(),"Contact Id is Null")
//        }

        // Set up UI with data from the selected contact item
        setUpUIWithData(selectedItem)

        // Perform update on button click
        binding.update.setOnClickListener {
            performUpdateContact(contactId, selectedItem)
        }

        ContactId = (selectedItem.contactid ?: "") as Int

        storeInitialValues()
        binding.reset.setOnClickListener {
            // Call method to reset UI elements
            resetUI()
        }
        refreshAttachmentList(contactId)

        // Reset UI values when the view is created
        Handler(Looper.getMainLooper()).postDelayed({
            resetUI()
        }, 3000)

        return root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // Set up spinner with custom adapter
        setupSpinner(binding.leadSource, R.array.Lead_source)

        val selectedItem = arguments?.getSerializable("selectedItem") as? ShowContactModelItem
        if (selectedItem != null) {
            setUpUIWithData(selectedItem)

            val cid = selectedItem.contactid ?: 0

            binding.rvMeetingDetails.layoutManager = WrapContentLinearLayoutManager(context)
            meetingDetailsByContactId(cid)

            binding.rvTaskDetails.layoutManager = WrapContentLinearLayoutManager(context)
            taskDetailsByContactId(cid)
        }

        resetUI()
    }

    private fun meetingDetailsByContactId(cid: Int) {
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null){
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.meetingDetailsByContactId("Bearer $jwtToken", cid)
            call.enqueue(object : Callback<List<ContactMeetingDetailsModelItem>> {
                override fun onResponse(
                    call: Call<List<ContactMeetingDetailsModelItem>>,
                    response: Response<List<ContactMeetingDetailsModelItem>>
                ) {
                    if (response.isSuccessful){

                        response.body()?.let { data ->
                            Log.d("data", "data: $data")
                            val adapter = ContactMeetingDetailsAdapter(requireContext(), data)
                            binding.rvMeetingDetails.adapter = adapter
                        } ?: Constants.error(requireContext(), "Response body is null")

                    }
                    else {
                        //Constants.error(context,"Response is not successful")
                        showEmptyView(true, binding.emptyView1)
                    }
                }

                override fun onFailure(call: Call<List<ContactMeetingDetailsModelItem>>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }

            })
        }
    }

    private fun taskDetailsByContactId(cid: Int) {
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null){
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.taskDetailsByContactId("Bearer $jwtToken", cid)
            call.enqueue(object : Callback<List<ContactTaskDetailsModelItem>> {
                override fun onResponse(
                    call: Call<List<ContactTaskDetailsModelItem>>,
                    response: Response<List<ContactTaskDetailsModelItem>>
                ) {
                    if (response.isSuccessful){

                        response.body()?.let { data ->
                            Log.d("data", "data: $data")
                            val adapter = ContactTaskDetailsAdapter(requireContext(), data)
                            binding.rvTaskDetails.adapter = adapter
                        } ?: Constants.error(requireContext(), "Response body is null")

                    }
                    else {
                        //Constants.error(context,"Response is not successful")
                        showEmptyView(true, binding.emptyView2)
                    }
                }

                override fun onFailure(call: Call<List<ContactTaskDetailsModelItem>>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }

            })
        }
    }

    private fun showEmptyView(show: Boolean, view : View) {
        view.visibility = if (show) View.VISIBLE else View.GONE
    }

    private fun refreshAttachmentList(contactId: Int) {
        gettoken(requireContext())?.let { jwtToken ->
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            fetchAndBind(service, jwtToken, contactId,
                { data ->
                    binding.ContactAttachmentRecyclerView.apply {
                        layoutManager = LinearLayoutManager(requireContext())
                        adapter = ContactAttachmentAdapter(requireContext(), data)
                    }
                },
                { data ->
                    val linkClickListener = object : ContactLinksAdapter.LinkClickListener {
                        override fun onLinkClick(url: String) {
                            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
                        }
                    }
                    binding.ContactLinksRecyclerView.apply {
                        layoutManager = LinearLayoutManager(requireContext())
                        adapter = ContactLinksAdapter(requireContext(), data, linkClickListener)
                    }
                },
                { data ->
                    binding.ContactNotesRecyclerView.apply {
                        layoutManager = LinearLayoutManager(requireContext())
                        adapter = ContactNotesAdapter(requireContext(), data)
                    }
                }
            )
        } ?: Constants.error(requireContext(), "Token is null. Please log in again.")
    }

    private fun fetchAndBind(
        service: ApiInterface,
        jwtToken: String,
        accountId: Int,
        attachmentHandler: (List<ContactAttachmentsResModel>) -> Unit,
        linkHandler: (List<ContactAttachmentsResModel>) -> Unit,
        notesHandler: (List<ContactAttachmentsResModel>) -> Unit
    ) {
        val calls = listOf(
            service.getContactAttachments("Bearer $jwtToken", accountId),
            service.getContactLinks("Bearer $jwtToken", accountId),
            service.getContactNotes("Bearer $jwtToken", accountId)
        )

        calls.forEachIndexed { index, call ->
            call.enqueue(object : Callback<List<ContactAttachmentsResModel>> {
                override fun onResponse(call: Call<List<ContactAttachmentsResModel>>, response: Response<List<ContactAttachmentsResModel>>) {
                    if (!isAdded) return
                    if (response.isSuccessful) {
                        val data = response.body() ?: emptyList()
                        when (index) {
                            0 -> attachmentHandler(data)
                            1 -> linkHandler(data)
                            2 -> notesHandler(data)
                        }
                    } else {
                        Constants.error(requireContext(), "Unsuccessful response: ${response.code()}")
                        Log.d("oma", "${response.code()}")
                    }
                }

                override fun onFailure(call: Call<List<ContactAttachmentsResModel>>, t: Throwable) {
                    if (isAdded) {
                        Constants.error(requireContext(), "Error: ${t.message}")
                        Log.d("omar", "${t.message}")
                    }
                }
            })
        }
    }

    private fun gettoken(context: Context): String? {
        return context.getSharedPreferences("Token", Context.MODE_PRIVATE).getString("jwtToken", null)
    }







    private fun showDialog(layoutResId: Int) {

        if (dialog == null) {
            dialog = Dialog(requireActivity())
            dialog!!.setCancelable(false)
            dialog!!.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        dialog!!.setContentView(layoutResId)
        dialog!!.show()

        val closeBtn = dialog!!.findViewById<ImageButton>(R.id.closeBtn)
        closeBtn.setOnClickListener {
            dialog!!.dismiss()
        }
    }
    private fun showFileChooser() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
            intent.type = "*/*"
            startActivityForResult(intent, PICKFILE_RESULT_CODE)
        } else {
            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.type = "*/*"
            intent.addCategory(Intent.CATEGORY_OPENABLE)
            startActivityForResult(
                Intent.createChooser(intent, "Select File"),
                PICKFILE_RESULT_CODE
            )
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == PICKFILE_RESULT_CODE && resultCode == Activity.RESULT_OK && data != null && data.data != null) {
            val uri: Uri = data.data!!
            path = getPathFromUri(requireContext(),uri)!!
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                updateTextViewWithFileName(uri)
            } else {
                // Handle for lower versions
            }
        }
    }
    @SuppressLint("SetTextI18n")
    private fun updateTextViewWithFileName(uri: Uri) {
        val fileName = getFileNameFromUri(uri)
        filePath = uri.toString()
        if (fileName!= null){
            val selectedFile = dialog!!.findViewById<TextView>(R.id.selectedFile)
            selectedFile.text = Html.fromHtml("<b><font color='#7FFF00'>$fileName</font></b>")
        }
    }

    private fun getFileNameFromUri(uri: Uri): String? {
        var fileName: String? = null
        val cursor =requireContext().contentResolver.query(uri, null, null, null, null)
        if (cursor!= null){
            if (cursor.moveToFirst()) {
                val displayNameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
                if (displayNameIndex != -1) {
                    fileName = cursor.getString(displayNameIndex)
                }
            }
            cursor.close()
        }
        return fileName
    }
    @SuppressLint("Range")
    private fun getPathFromUri(context: Context, uri: Uri): String? {
        var path: String? = null
        if ("content".equals(uri.scheme, ignoreCase = true)) {
            // Using ACTION_OPEN_DOCUMENT
            val cursor = context.contentResolver.query(uri, null, null, null, null)
            cursor?.use {
                if (it.moveToFirst()) {
                    val displayName = it.getString(it.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                    val type = it.getString(it.getColumnIndex("mime_type"))
                    val file = File(context.cacheDir, displayName)
                    try {
                        val inputStream = context.contentResolver.openInputStream(uri)
                        inputStream?.use { input ->
                            FileOutputStream(file).use { output ->
                                input.copyTo(output)
                            }
                        }
                        path = file.absolutePath
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
            }
        } else if ("file".equals(uri.scheme, ignoreCase = true)) {
            path = uri.path
        }
        return path
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun addAttachmentContact() {
        val attach = AttachmentDto(0, 0, ContactId, 0)


        val file = File(path)
        val content: ByteArray = file.readBytes()

        val fileExtension = MimeTypeMap.getFileExtensionFromUrl(file.absolutePath)
        val mediaType = FileTypeHelper.getMediaTypeFromFileExtension(fileExtension)

        if (mediaType != null){
            val requestBody = RequestBody.create(mediaType, content)
            val jwtToken = gettoken(requireContext())
            if (jwtToken != null) {
                val service = ApiUtilities.getinstance().create(ApiInterface::class.java)

                val body = MultipartBody.Part.createFormData("file", file.name, requestBody)
                val call = service.addAttachment("Bearer $jwtToken", body,attach)
                call.enqueue(object : Callback<AttaModel> {
                    override fun onResponse(
                        call: Call<AttaModel>,
                        response: Response<AttaModel>
                    ) {
                        if (response.isSuccessful) {
                            val responseBody = response.body()
                            Log.d("ResponseBody", "$responseBody")
                            dialog!!.dismiss()
                            Constants.success(requireContext(),"File Attached Successfully")
                        }else{
                            Constants.error(requireContext(),"Unsuccessful")
                        }
                    }
                    override fun onFailure(call: Call<AttaModel>, t: Throwable) {
                        Constants.error(requireContext(), "Error: ${t.message}")

                    }
                })
            } else {
                Constants.error(requireContext(), "Token Not Found")
            }
        } else {
            Constants.error(requireContext(), "Unsupported file type")
        }
    }

    private fun addNotes() {
        val note = AddNoteRequest(0, ContactId, 0, Desc,0)
        if (path != null && path.isNotEmpty()) {
            val file = File(path)
            val content: ByteArray = file.readBytes()
            val fileExtension = MimeTypeMap.getFileExtensionFromUrl(file.absolutePath)
            val mediaType = FileTypeHelper.getMediaTypeFromFileExtension(fileExtension)

            if (mediaType != null){
                val requestBody = RequestBody.create(mediaType, content)
                val jwtToken = gettoken(requireContext())
                if (jwtToken != null) {
                    val service = ApiUtilities.getinstance().create(ApiInterface::class.java)

                    val body = MultipartBody.Part.createFormData("file", file.name, requestBody)
                    val call = service.addnote("Bearer $jwtToken", body,note)
                    call.enqueue(object : Callback<AddNoteModel> {
                        override fun onResponse(
                            call: Call<AddNoteModel>,
                            response: Response<AddNoteModel>
                        ) {
                            if (response.isSuccessful) {
                                val responseBody = response.body()
                                Log.d("ResponseBody", "$responseBody")
                                dialog!!.dismiss()
                                Constants.success(requireContext(),"Note Sent Successfully")
                            }else{
                                Constants.error(requireContext(),"Unsuccessful")
                            }
                        }
                        override fun onFailure(call: Call<AddNoteModel>, t: Throwable) {
                            Constants.error(requireContext(), "Error: ${t.message}")

                        }
                    })
                } else {
                    Constants.error(requireContext(), "Token Not Found")
                }
            } else {
                Constants.error(requireContext(), "Unsupported file type")
            }
        }else{
            Constants.error(requireContext(), "File not Selected, Please Select a file")
        }
    }

    private fun addLinks(label : String, desc : String, linkUrl : String, ContactId: Int) {
        val requestBody = AddLinkReqModel(0,ContactId,label,0,desc,0,linkUrl)

        val jwtToken = gettoken(requireContext())
        if (jwtToken != null) {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)

            val call = service.addLink("Bearer $jwtToken", requestBody)
            call.enqueue(object : Callback<AddLinkResponseModel> {
                override fun onResponse(
                    call: Call<AddLinkResponseModel>,
                    response: Response<AddLinkResponseModel>
                ) {
                    if (response.isSuccessful) {
                        val responseBody = response.body()
                        Log.d("ResponseBody", "$responseBody")
                        dialog!!.dismiss()
                        Constants.success(requireContext(),"Note sent Successfully")
                    }else{
                        Constants.error(requireContext(),"Unsuccessful")
                    }
                }
                override fun onFailure(call: Call<AddLinkResponseModel>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }
            })
        } else {
            Constants.error(requireContext(), "Token Not Found")
        }
    }


    private var initialValues: Map<String, String?> = emptyMap()

    private fun storeInitialValues() {
        // Null check for selectedItem
        val selectedItem = arguments?.getSerializable("selectedItem") as? ShowContactModelItem
        if (selectedItem == null) {
            // Handle null case, perhaps show an error message
            Constants.error(requireContext(), "Selected item is null")
            return
        }

        // Fetch selected item's values
        val accountNameValue = selectedItem.accountName ?: ""

        // Store initial values
        initialValues = mapOf(
            "leadOwnerName" to binding.leadOwnerName.text.toString(),
            "fullName" to binding.fullName.text.toString(),
            "leadSource" to binding.leadSource.selectedItem.toString(),
            "title" to binding.title.text.toString(),
            "phone" to binding.phone.text.toString(),
            "alternatePhone" to binding.alternatePhone.text.toString(),
            "department" to binding.department.text.toString(),
            "email" to binding.email.text.toString(),
            "twitter" to binding.twitter.text.toString(),
            "skypeId" to binding.skypeId.text.toString(),
            "assistance" to binding.assistance.text.toString(),
            "assistantPhone" to binding.assistantPhone.text.toString(),
            "fax" to binding.fax.text.toString(),
            "mailingStreet" to binding.mailingStreet.text.toString(),
            "otherStreet" to binding.otherStreet.text.toString(),
            "mailingCity" to binding.mailingCity.text.toString(),
            "otherCity" to binding.otherCity.text.toString(),
            "mailingCountry" to binding.mailingCountry.text.toString(),
            "otherCountry" to binding.otherCountry.text.toString(),
            "zipCode" to binding.zipCode.text.toString(),
            "otherZipCode" to binding.otherZipCode.text.toString(),
            "description" to binding.description.text.toString(),
            "accountName" to accountNameValue
        )
    }


    private fun resetUI() {
        initialValues.forEach { (elementId, value) ->
            when (elementId) {
                "leadOwnerName" -> binding.leadOwnerName.text = value
                "fullName" -> binding.fullName.setText(value)
                "leadSource" -> binding.leadSource.setSelection((binding.leadSource.adapter as? ArrayAdapter<String>)?.getPosition(value) ?: 0)
                "title" -> binding.title.setText(value)
                "phone" -> binding.phone.text = value
                "alternatePhone" -> binding.alternatePhone.setText(value)
                "department" -> binding.department.setText(value)
                "email" -> binding.email.setText(value)
                "twitter" -> binding.twitter.setText(value)
                "skypeId" -> binding.skypeId.setText(value)
                "assistance" -> binding.assistance.setText(value)
                "assistantPhone" -> binding.assistantPhone.setText(value)
                "fax" -> binding.fax.setText(value)
                "mailingStreet" -> binding.mailingStreet.setText(value)
                "otherStreet" -> binding.otherStreet.setText(value)
                "mailingCity" -> binding.mailingCity.setText(value)
                "otherCity" -> binding.otherCity.setText(value)
                "mailingCountry" -> binding.mailingCountry.setText(value)
                "otherCountry" -> binding.otherCountry.setText(value)
                "zipCode" -> binding.zipCode.setText(value)
                "otherZipCode" -> binding.otherZipCode.setText(value)
                "description" -> binding.description.setText(value)
                "accountName" -> binding.accountName.setSelection((binding.accountName.adapter as? ArrayAdapter<String>)?.getPosition(value) ?: 0)
            }
        }
    }

    private fun getIndex(spinner: Spinner, myString: String): Int {
        val adapter = spinner.adapter as? ArrayAdapter<String>
        adapter?.let {
            for (i in 0 until it.count) {
                if (it.getItem(i) == myString) {
                    return i
                }
            }
        }
        return 0 // Default to 0 if not found
    }

//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//        // Set up spinner with custom adapter
//        setupSpinner(binding.leadSource, R.array.Lead_source)
//
//        val selectedItem = arguments?.getSerializable("selectedItem") as? ShowContactModelItem
//        if (selectedItem != null) {
//            setUpUIWithData(selectedItem)
//        }
//        resetUI()
//    }

    private fun setupSpinner(spinner: Spinner, arrayResourceId: Int) {
        val adapter = ArrayAdapter.createFromResource(
            requireContext(),
            arrayResourceId,
            R.layout.spinner_item_layout_currency
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = adapter

    }

    private fun setUpUIWithData(selectedItem: ShowContactModelItem) {
        // Set UI fields with data from the selected contact item
        binding.leadOwnerName.setText(selectedItem.leadOwnerName ?: "")
//        binding.leadSource.setText(selectedItem.leadSource ?: "")
        binding.leadSource.setSelection(getIndex( binding.leadSource,selectedItem.leadSource ?: ""))
        binding.fullName.setText(selectedItem.fullName ?: "")
        binding.title.setText(selectedItem.title ?: "")
        binding.phone.setText(selectedItem.phone ?: "")
        binding.alternatePhone.setText(selectedItem.alternatePhone ?: "")
        binding.department.setText(selectedItem.department ?: "")
        binding.email.setText(selectedItem.email ?: "")
        binding.twitter.setText(selectedItem.twitter ?: "")
        binding.skypeId.setText(selectedItem.skypeId ?: "")
        binding.assistance.setText(selectedItem.assistance ?: "")
        binding.assistantPhone.setText(selectedItem.assistantPhone ?: "")
        binding.fax.setText(selectedItem.fax ?: "")
        binding.mailingStreet.setText(selectedItem.mailingStreet ?: "")
        binding.otherStreet.setText(selectedItem.otherStreet ?: "")
        binding.mailingCity.setText(selectedItem.mailingCity ?: "")
        binding.otherCity.setText(selectedItem.otherCity ?: "")
        binding.mailingCountry.setText(selectedItem.mailingCountry ?: "")
        binding.otherCountry.setText(selectedItem.otherCountry ?: "")
        binding.zipCode.setText(selectedItem.zipCode ?: "")
        binding.otherZipCode.setText(selectedItem.otherZipCode ?: "")
        binding.description.setText(selectedItem.description ?: "")
        binding.accountName.setSelection(getIndex( binding.accountName,selectedItem.accountName ?: ""))

        selectedItem.accountName?.let {
            val accountIndex = accountNames.indexOf(it)
            if (accountIndex != -1) {
                binding.accountName.setSelection(accountIndex)
            }
        }

    }

    private fun performUpdateContact(contactid: Int, selectedItem: ShowContactModelItem) {

        if (selectedItem == null) {
            // Handle null case, perhaps show an error message
            Constants.error(requireContext(), "Selected item is null")
            return
        }

        // Fetch input data from the UI fields
        val leadOwnerName = binding.leadOwnerName.text.toString()
        val leadSource = binding.leadSource.selectedItem.toString()
        val fullName = binding.fullName.text.toString()
        val title = binding.title.text.toString()
        val accountName = binding.accountName.selectedItem.toString()
        val phone = binding.phone.text.toString()
        val alternatePhone = binding.alternatePhone.text.toString()
        val department = binding.department.text.toString()
        val email = binding.email.text.toString()
        val twitter = binding.twitter.text.toString()
        val skypeId = binding.skypeId.text.toString()
        val assistance = binding.assistance.text.toString()
        val assistantPhone = binding.assistantPhone.text.toString()
        val fax = binding.fax.text.toString()
        val mailingStreet = binding.mailingStreet.text.toString()
        val otherStreet = binding.otherStreet.text.toString()
        val mailingCity = binding.mailingCity.text.toString()
        val otherCity = binding.otherCity.text.toString()
        val mailingCountry = binding.mailingCountry.text.toString()
        val otherCountry = binding.otherCountry.text.toString()
        val zipCode = binding.zipCode.text.toString()
        val otherZipCode = binding.otherZipCode.text.toString()
        val description = binding.description.text.toString()

        // Get account Id based on selected account name
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null) {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val callAccounts = service.adminshowaccounts("Bearer $jwtToken")
            callAccounts.enqueue(object : Callback<ArrayList<ShowAccountsModelItem>> {
                override fun onResponse(
                    call: Call<ArrayList<ShowAccountsModelItem>>,
                    response: Response<ArrayList<ShowAccountsModelItem>>
                ) {
                    if (response.isSuccessful) {
                        val accountData = response.body()
                        if (accountData != null) {
                            val selectedAccount = accountData.find { it.accountName == accountName }
                            val accountId = selectedAccount?.accountId ?: 0

                            // Create the request body
                            val requestBody = AdminUpdateContactPutModel(
                                leadOwnerName, leadSource, fullName, title, accountName, accountId,
                                phone, alternatePhone, department, email, twitter, skypeId, assistance,
                                assistantPhone, fax, mailingStreet, otherStreet, mailingCity,
                                otherCity, mailingCountry, otherCountry, zipCode, otherZipCode,
                                description, contactid
                            )

                            // Make PUT API call
                            val callUpdateContact = service.admincontactupdate("Bearer $jwtToken", requestBody)
                            callUpdateContact.enqueue(object : Callback<UpdateContactResponseModel> {
                                override fun onResponse(
                                    call: Call<UpdateContactResponseModel>,
                                    response: Response<UpdateContactResponseModel>
                                ) {
                                    if (response.isSuccessful) {
                                        // Handle successful response
                                        Constants.success(
                                            requireContext(),
                                            "Contact updated successfully"
                                        )
                                        findNavController().popBackStack() // Go back to previous fragment
                                    } else {
                                        Constants.error(
                                            requireContext(),
                                            "Unsuccessful response: ${response.code()}"
                                        )
                                    }
                                }


                                override fun onFailure(call: Call<UpdateContactResponseModel>, t: Throwable) {
                                    Constants.error(requireContext(), "Error: ${t.message}")
                                }
                            })
                        } else {
                            Constants.error(requireContext(), "Response body is null")
                        }
                    } else {
                        Constants.error(requireContext(), "Unsuccessful response: ${response.code()}")
                    }
                }

                override fun onFailure(call: Call<ArrayList<ShowAccountsModelItem>>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }
            })
        } else {
            Constants.error(requireContext(), "Token is null. Please log in again.")
        }
    }

    private var accountDataList: List<ShowAccountsModelItem> = emptyList()

    private fun fetchAccountNames() {
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null) {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val callAccounts = service.adminshowaccounts("Bearer $jwtToken")
            callAccounts.enqueue(object : Callback<ArrayList<ShowAccountsModelItem>> {
                override fun onResponse(
                    call: Call<ArrayList<ShowAccountsModelItem>>,
                    response: Response<ArrayList<ShowAccountsModelItem>>
                ) {
                    if (response.isSuccessful) {
                        val data = response.body()
                        if (data != null) {
                            accountNames.clear()
                            for (item in data) {
                                item.accountName?.let { accountNames.add(it) }
                            }
                            accountAdapter.notifyDataSetChanged()
                        } else {
                            Constants.error(requireContext(), "Response body is null")
                        }
                    } else {
                        Constants.error(requireContext(), "Unsuccessful response: ${response.code()}")
                    }
                }

                override fun onFailure(call: Call<ArrayList<ShowAccountsModelItem>>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }
            })
        } else {
            Constants.error(requireContext(), "Token is null. Please log in again.")
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
