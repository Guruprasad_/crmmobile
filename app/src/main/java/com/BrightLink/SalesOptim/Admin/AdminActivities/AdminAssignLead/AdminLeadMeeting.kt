package com.BrightLink.SalesOptim.Admin.AdminActivities.AdminAssignLead

import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.graphics.PorterDuff
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.BrightLink.SalesOptim.Adapters.ALMeetingListAdapter
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminRequestModel.AdminPostMeetingAL
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel.AdminMeetingPostRes
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.Constants.WrapContentLinearLayoutManager
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.ResponseModel.ALMeetingModelItem
import com.BrightLink.SalesOptim.ResponseModel.LeadDetailsModel
import com.BrightLink.SalesOptim.ResponseModel.ShowEmployeelistModelItem
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.AdminLeadMeetingFragmentBinding
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.DateValidatorPointForward
import com.google.android.material.datepicker.MaterialDatePicker
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale

class AdminLeadMeeting : Fragment() {
    private var _binding: AdminLeadMeetingFragmentBinding? = null
    private val binding get() = _binding!!
    private var leadId : Int =  0
    private var leadStatus : String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = AdminLeadMeetingFragmentBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val sharedPreferences = requireContext().getSharedPreferences("AdminLead", Context.MODE_PRIVATE)
        leadId = sharedPreferences.getInt("leadId", -1)
        leadStatus = sharedPreferences.getString("leadStatus", "")

        if (leadStatus!= null){

            when(leadStatus)
            {
                "New Lead"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)

                }

                "Contacted"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Email Sent"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Not Contacted"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Qualified"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Not Qualified"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Proposal Sent"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Lead Won"->{
                    binding.actionBar.contactIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Lead Lost"->{
                    binding.actionBar.contactIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }
            }
        }

        binding.actionBar.activityName.text = "Assign Lead"
        binding.actionBar.back.setOnClickListener{
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }

        binding.meetingDateFrom.setOnClickListener {
            showMaterialCalendar(binding.meetingDateFrom)
        }

        binding.meetingDateTo.setOnClickListener {
            showMaterialCalendar(binding.meetingDateTo)
        }

        binding.recyclerView.layoutManager = WrapContentLinearLayoutManager(context)


        fetchEmployee()

        if (leadId != null) {
            fetchLeadDetailsById(leadId)
        }
        refreshMeetingList(leadId)

        binding.update.setOnClickListener {
            getToken()?.let { jwtToken ->
                val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
                updateMeeting(service, jwtToken, leadId)
            } ?: Constants.error(requireContext(), "Token is null. Please log in again.")
        }


        binding.reset.setOnClickListener {
            binding.title.text.clear()
            binding.meetingDateFrom.setText("")
            binding.meetingDateTo.setText("")
            binding.location.clearCheck()
            binding.linkOrAddress.text.clear()
        }



        return  root
    }

    private fun refreshMeetingList(leadId: Int) {
        getToken()?.let { jwtToken ->
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            callApi(service, jwtToken, leadId)
        } ?: Constants.error(requireContext(), "Token is null. Please log in again.")

    }

    // Get Employee Details
    private fun fetchEmployee() {
        val jwtToken = getToken()
        if (jwtToken != null){
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val callEmployeeList = service.adminEmployeeList("Bearer $jwtToken")

            callEmployeeList.enqueue(object : Callback<ArrayList<ShowEmployeelistModelItem>> {
                override fun onResponse(
                    call: Call<ArrayList<ShowEmployeelistModelItem>>,
                    response: Response<ArrayList<ShowEmployeelistModelItem>>
                ) {
                    if (response.isSuccessful){
                        val employeelist = response.body()
                        if (employeelist != null && employeelist.isNotEmpty()){
                            val employee = employeelist[0]
                            binding.assignedTo.setText(employee.name)
                        } else {
                            Constants.error(requireContext(), "Employee List is null or empty")
                        }
                    } else {
                        Constants.error(requireContext(), "Unsuccessful response: ${response.code()}")
                    }
                }

                override fun onFailure(
                    call: Call<ArrayList<ShowEmployeelistModelItem>>,
                    t: Throwable
                ) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }

            })
        }
    }

    // Get Lead details by lead id
    private fun fetchLeadDetailsById(lid: Int) {
        val jwtToken = getToken()
        if (jwtToken != null){
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val  call = service.getAdminLeadDetails("Bearer $jwtToken", lid)
            call.enqueue(object : Callback<LeadDetailsModel> {
                override fun onResponse(
                    call: Call<LeadDetailsModel>,
                    response: Response<LeadDetailsModel>
                ) {
                    if (response.isSuccessful) {
                        val data = response.body()
                        if (data != null) {
                            val lead = data

                            binding.clientEmail.setText(lead.email)
                        } else {
                            Constants.error(requireContext(), "Lead details not found")
                        }
                    } else {
                        Constants.error(
                            requireContext(),
                            "Unsuccessful response: ${response.code()}"
                        )
                    }
                }

                override fun onFailure(call: Call<LeadDetailsModel>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }
            })
        }
    }

    // Get Meeting List By Lead Id
    private fun callApi(service: ApiInterface, jwtToken: String, leadId: Int) {
        val call = service.adminLeadMeetingList("Bearer $jwtToken", leadId)
        call.enqueue(object : Callback<List<ALMeetingModelItem>>{
            override fun onResponse(
                call: Call<List<ALMeetingModelItem>>,
                response: Response<List<ALMeetingModelItem>>
            ) {
                if (isAdded) {
                    if (response.isSuccessful) {
                        val meetingList = response.body()
                        if (meetingList.isNullOrEmpty()) {
                            binding.noMeetingStatus.visibility = View.VISIBLE
                        } else {
                            binding.noMeetingStatus.visibility = View.INVISIBLE

                            val linkClickListener = object : ALMeetingListAdapter.LinkClickListener {
                                override fun onLinkClick(url: String) {
                                    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                                    context!!.startActivity(intent)
                                }
                            }

                            val adapter = ALMeetingListAdapter(requireContext(), meetingList,linkClickListener)
                            binding.recyclerView.adapter = adapter
                        }
                    } else {
                        //Show toast indicating unsuccessful response
                        Constants.error(requireContext(), "Unsuccessful: ${response.code()}")
                    }
                }
            }

            override fun onFailure(call: Call<List<ALMeetingModelItem>>, t: Throwable) {
                Constants.error(requireContext(), "Error: ${t.message}")
            }

        })
    }

    // Post / Add Meetings
    private fun updateMeeting(service: ApiInterface, jwtToken: String, lid: Int) {
        val title = binding.title.text.toString()
        val meetingFrom = binding.meetingDateFrom.text.toString()
        val meetingTo = binding.meetingDateTo.text.toString()
        val clientEmail = binding.clientEmail.text.toString()
        val locationType = getLocationType()
        val meetingLinkOrLocation = binding.linkOrAddress.text.toString()


        if (title.isEmpty() || meetingFrom.isEmpty() || meetingTo.isEmpty() || clientEmail.isEmpty() || locationType.isEmpty() || meetingLinkOrLocation.isEmpty()) {
            Constants.error(requireContext(), "Please fill in all fields")
            return
        }

        if (!isDateValid(meetingFrom, meetingTo)) {
            Constants.error(requireContext(), "Meeting To date should be after Meeting From date")
            return
        }

        val reqBody = AdminPostMeetingAL( clientEmail, locationType, "2024-03-27T12:33", meetingLinkOrLocation, "2024-03-27T12:40", title)

        val call = service.adminAddMeeting("Bearer $jwtToken",leadId, reqBody)
        call.enqueue(object : Callback<AdminMeetingPostRes>{
            override fun onResponse(
                call: Call<AdminMeetingPostRes>,
                response: Response<AdminMeetingPostRes>
            ) {
                if (response.isSuccessful){
                    Constants.success(requireContext(), "Meeting added successfully")
                    refreshMeetingList(lid)
                } else {
                    Constants.error(requireContext(), "Unsuccessful Response: ${response.errorBody()}")
                }
            }

            override fun onFailure(call: Call<AdminMeetingPostRes>, t: Throwable) {
                Constants.error(requireContext(), "Error : ${t.message}")
            }

        })
    }

    private fun showMaterialCalendar(textView: TextView) {
        val today = Calendar.getInstance().timeInMillis
        val datePickerBuilder = MaterialDatePicker.Builder.datePicker()
        datePickerBuilder.setSelection(today)

        val constraintsBuilder = CalendarConstraints.Builder()
        constraintsBuilder.setValidator(DateValidatorPointForward.now())

        datePickerBuilder.setCalendarConstraints(constraintsBuilder.build())

        val datePicker = datePickerBuilder.build()

        datePicker.addOnPositiveButtonClickListener {selectedDate->
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = selectedDate

            // Launch TimePickerDialog
            val timePickerDialog = TimePickerDialog(
                requireContext(),
                { _, hourOfDay, minute ->
                    // Combine date and time
                    calendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
                    calendar.set(Calendar.MINUTE, minute)

                    // Format the final date-time
                    val formattedDateTime = SimpleDateFormat("yyyy-MM-dd'T'HH:mm", Locale.getDefault()).format(calendar.time)
                    textView.text = formattedDateTime
                },
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                true
            )

            timePickerDialog.show()
        }
        datePicker.show(parentFragmentManager, datePicker.toString())

    }

    private fun getLocationType(): String {
        return when (binding.location.checkedRadioButtonId) {
            R.id.videoConference -> "Video Conference"
            R.id.onsite -> "Site Visit"
            else -> ""
        }
    }
    private fun isDateValid(meetingFrom: String, meetingTo: String): Boolean {
        val calendarFrom = Calendar.getInstance().apply {
            timeInMillis = parseDateString(meetingFrom)
        }
        val calendarTo = Calendar.getInstance().apply {
            timeInMillis = parseDateString(meetingTo)
        }
        return calendarTo.after(calendarFrom)
    }

    private fun parseDateString(dateString: String): Long {
        val pattern = "yyyy-MM-dd'T'HH:mm"
        val dateFormat = SimpleDateFormat(pattern, Locale.getDefault())
        return try {
            dateFormat.parse(dateString)?.time ?: 0
        } catch (e: ParseException) {
            Log.e("Parsing Error", "Error parsing date", e)
            0
        }
    }

    private fun getToken(): String? {
        return requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
            .getString("jwtToken", null)
    }

}