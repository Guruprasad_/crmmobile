package com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel

data class AdminEmployeeDashboardResponseModel(
    val bottomEmployee: List<BottomEmployee>,
    val employee: List<Employee>,
    val topEmployee: List<TopEmployee>
)