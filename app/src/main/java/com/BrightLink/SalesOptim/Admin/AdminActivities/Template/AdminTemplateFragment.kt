package com.BrightLink.SalesOptim.Admin.AdminActivities.Template

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.BrightLink.SalesOptim.Admin.AdminAdapters.TemplateAdapter
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.Constants.WrapContentLinearLayoutManager
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.RequestModel.AddTemplatePostModel
import com.BrightLink.SalesOptim.ResponseModel.AddTemplateResponseModel
import com.BrightLink.SalesOptim.ResponseModel.TemplateModelItem
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.AdminFragmentTemplateBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AdminTemplateFragment : Fragment() {

    private var _binding: AdminFragmentTemplateBinding? = null
    private val binding get() = _binding!!

    private lateinit var adapter : TemplateAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = AdminFragmentTemplateBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.actionBar.activityName.text = "Template"
        binding.actionBar.back.setOnClickListener {
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }

        binding.recyclerView.layoutManager = WrapContentLinearLayoutManager(context)

        refreshTemplateList()

        binding.searchTemplate.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                adapter.filter(s.toString())
            }
        })

        binding.addTemplate.setOnClickListener {

            val dialogView = layoutInflater.inflate(R.layout.add_template, null)
            val dialogBuilder = AlertDialog.Builder(requireContext()).setView(dialogView)

            val title = dialogView.findViewById<EditText>(R.id.title)
            val type = dialogView.findViewById<TextView>(R.id.temptype)
            val desc = dialogView.findViewById<EditText>(R.id.tempDesc)
            val submit = dialogView.findViewById<Button>(R.id.sumbitBtn)
            val reset = dialogView.findViewById<Button>(R.id.resetBtn)
            val close = dialogView.findViewById<ImageButton>(R.id.tempClose)

            val dialog = dialogBuilder.create()
            dialog.show()

            submit.setOnClickListener {
                val title = title.text.toString()
                val type = type.text.toString()
                val desc = desc.text.toString()

                if(title.isBlank() || desc.isBlank()){
                    Constants.error(requireContext(), "All fields are required")
                    return@setOnClickListener
                }

                val reqBody = AddTemplatePostModel(desc, title, type)
                Log.d("reqBody", "Body: $reqBody")
                performAddTemplate(reqBody, dialog)
            }

            reset.setOnClickListener {
                title.text.clear()
                desc.text.clear()
            }

            close.setOnClickListener {
                dialog.dismiss()
            }

        }

        return root

    }

    private fun refreshTemplateList() {
        getToken()?.let { jwtToken ->
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            callApi(service, jwtToken)
        } ?: Constants.error(requireContext(), "Token is null. Please log in again.")
    }

    private fun performAddTemplate(reqBody: AddTemplatePostModel, dialog: AlertDialog) {
        getToken()?.let { jwtToken ->

            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.addTemplateAdmin("Bearer $jwtToken", reqBody)

            call.enqueue(object : Callback<AddTemplateResponseModel> {
                override fun onResponse(
                    call: Call<AddTemplateResponseModel>,
                    response: Response<AddTemplateResponseModel>
                ) {
                    if (response.isSuccessful){
                        Constants.success(requireContext(), "Template Added Successfully")
                        dialog.dismiss()
                        refreshTemplateList()
                    } else {
                        Constants.error(requireContext(), "Unsuccessful Response: ${response.code()}")
                    }
                }

                override fun onFailure(call: Call<AddTemplateResponseModel>, t: Throwable) {
                    Constants.error(requireContext(), "Error : ${t.message}")
                }

            })

        } ?: Constants.error(requireContext(), "Token is null. Please log in again.")

    }

    private fun callApi(service: ApiInterface, jwtToken: String) {

        val call = service.templateListAdmin("Bearer $jwtToken")
        call.enqueue(object : Callback<MutableList<TemplateModelItem>> {
            override fun onResponse(
                call: Call<MutableList<TemplateModelItem>>,
                response: Response<MutableList<TemplateModelItem>>
            ) {
                if (isAdded && response.isSuccessful) {
                    response.body()?.let { data ->
                        adapter = TemplateAdapter(requireContext(), data)
                        binding.recyclerView.adapter = adapter
                    } ?: Constants.error(requireContext(), "Response body is null")
                } else {
                    Constants.error(requireContext(), "Unsuccessful response: ${response.code()}")
                }
            }

            override fun onFailure(call: Call<MutableList<TemplateModelItem>>, t: Throwable) {
                if (isAdded) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }
            }
        })

    }

    private fun getToken(): String? {
        return requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
            .getString("jwtToken", null)
    }

}