package com.BrightLink.SalesOptim.Admin.AdminActivities.Marketing

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.OpenableColumns
import android.text.Editable
import android.text.Html
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.PopupMenu
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.ResponseModel.TemplateModelItem
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.AdminMarketingFragmentBinding
import com.BrightLink.SalesOptim.databinding.MarketingEmailBinding
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AdminMarketing : Fragment() {

    private var _binding: AdminMarketingFragmentBinding? = null
    private val binding get() = _binding!!
    private val PICKFILE_RESULT_CODE = 1
    private var filePath: String? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = AdminMarketingFragmentBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.marketingAB.activityName.text = "Marketing"
        binding.marketingAB.back.setOnClickListener {
            requireActivity().onBackPressed()
        }
        binding.mSendToAll.setOnClickListener {
            val jwtToken = getToken()
            if (jwtToken != null) {
                val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
                CallApi(service, jwtToken)
            } else {
                Constants.error(context, "Login Again")
            }
        }

        binding.mChooseFile.setOnClickListener {
            showFileChooser()
        }
        binding.mSend.setOnClickListener {
            sendEmail()
        }
        val subject = binding.mSubject
        val message = binding.mMessage
        val template = binding.adminTemplate

        template.setOnClickListener {
            addTempToEmail(subject, message, template)
        }



        return root
    }

    private fun addTempToEmail(sub: EditText?, desc: EditText?, template: Button) {
        val jwtToken = getToken()
        if (jwtToken != null) {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val callTemplates = service.templateListAdmin("Bearer $jwtToken")
            callTemplates.enqueue(object : Callback<MutableList<TemplateModelItem>> {
                override fun onResponse(
                    call: Call<MutableList<TemplateModelItem>>,
                    response: Response<MutableList<TemplateModelItem>>
                ) {
                    if (isAdded) {
                        if (response.isSuccessful) {
                            val data = response.body()
                            if (data != null && data.isNotEmpty()) {
                                val popupMenu = PopupMenu(context, template)
                                data.forEach { templateItem ->
                                    val menuItem = popupMenu.menu.add(templateItem.title)
                                    menuItem.setOnMenuItemClickListener {
                                        sub?.setText(templateItem.title)
                                        desc?.setText(templateItem.description)
                                        true
                                    }
                                    // Set text color to black
                                    val textColor = Color.BLACK
                                    val spanString = SpannableString(templateItem.title)
                                    spanString.setSpan(
                                        ForegroundColorSpan(textColor),
                                        0,
                                        spanString.length,
                                        0
                                    )
                                    menuItem.title = spanString
                                }
                                popupMenu.show()
                            } else {
                                Constants.error(requireContext(), "Template is Empty\n" +
                                        "Please add Template")
                            }
                        } else {
                            Constants.error(context, "Something went wrong,\nTry Again ")
                        }
                    }
                }

                override fun onFailure(call: Call<MutableList<TemplateModelItem>>, t: Throwable) {
                    Constants.error(requireContext(), "Failed to get Template")
                }

            })
        } else {
            Constants.error(requireContext(), "Please log in again.")
        }
    }

    private fun showFileChooser() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
            intent.type = "*/*"
            startActivityForResult(intent, PICKFILE_RESULT_CODE)
        } else {
            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.type = "*/*"
            intent.addCategory(Intent.CATEGORY_OPENABLE)
            startActivityForResult(
                Intent.createChooser(intent, "Select File"),
                PICKFILE_RESULT_CODE
            )
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == PICKFILE_RESULT_CODE && resultCode == Activity.RESULT_OK && data != null && data.data != null) {
            val uri: Uri = data.data!!
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                updateTextViewWithFileName(uri)
            } else {
                // Handle for lower versions
            }
        }
    }
    @SuppressLint("SetTextI18n")
    private fun updateTextViewWithFileName(uri: Uri) {
        val fileName = getFileNameFromUri(uri)
        filePath = uri.toString()
        //selectedFileUri = uri
        if (fileName!= null){
            binding.selectedFile.text = Html.fromHtml("<b><font color='#38E54D'>$fileName</font></b>")
        }
    }

    private fun getFileNameFromUri(uri: Uri): String? {
        var fileName: String? = null
        val cursor =requireContext().contentResolver.query(uri, null, null, null, null)
        if (cursor!= null){
            if (cursor.moveToFirst()) {
                val displayNameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
                if (displayNameIndex != -1) {
                    fileName = cursor.getString(displayNameIndex)
                }
            }
            cursor.close()
        }
        return fileName
    }

    private fun sendEmail() {
        val email = binding.mEmail.text.toString().trim()
        val subject = binding.mSubject.text.toString().trim()
        val message = binding.mMessage.text.toString().trim()

        if (email.isEmpty()) {
            Constants.error(requireContext(), "Please Enter Email")
            return
        }
        if (subject.isEmpty()) {
            Constants.error(requireContext(), "Please Enter Subject")
            return
        }


        val intent = Intent(Intent.ACTION_SEND)
        intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(email))
        intent.putExtra(Intent.EXTRA_SUBJECT, subject)
        intent.putExtra(Intent.EXTRA_TEXT, message)
        if (!filePath.isNullOrEmpty()){
            val uri = Uri.parse(filePath)
            intent.putExtra(Intent.EXTRA_STREAM,uri)
            intent.type = "*/*"
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        }else{
            Constants.info(requireContext(),"Sending Email Without Attachment")
            intent.type = "plain/text"
        }

        try {
            startActivity(Intent.createChooser(intent, "Send Email"))
            if (message.isEmpty()) {
                Constants.info(requireContext(), "Message is not present, Type in Gmail")
            }
        } catch (ex: Exception) {
            Constants.error(requireContext(), ex.message ?: "")
        }
    }


    private fun getToken(): String? {
        val sharedPreferences = requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
        return sharedPreferences.getString("jwtToken", null)
    }
    private fun CallApi(service: ApiInterface , token : String) {

        service.adminGetEmails("Bearer $token").enqueue(object : Callback<ResponseBody> {
            @SuppressLint("SuspiciousIndentation")
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (isAdded) {
                    if (response.isSuccessful) {
                        val responseBody = response.body()?.string()
                            responseBody?.let {
                                if (it.isEmpty()){
                                    Constants.error(context, "Email List is Empty")
                                }
                                processEmails(it) }
                        } else {
                            Constants.error(context, "Something went wrong,\nTry Again ")
                        }
                }
            }
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Constants.error(context, "Failed to get Emails")
            }
        })
    }
    private fun processEmails(emailString: String?) {
        // Process the list of emails here
        emailString?.let{
            val emaill = it.replace("[","").replace("]","")

            binding.mEmail.text = Editable.Factory.getInstance().newEditable(emaill)
            // Send email, etc
        }
    }
}