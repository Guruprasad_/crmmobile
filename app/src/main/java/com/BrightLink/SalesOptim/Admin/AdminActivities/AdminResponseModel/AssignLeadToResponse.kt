package com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel

data class AssignLeadToResponse(
    val data: String,
    val message: String,
    val status: String
)