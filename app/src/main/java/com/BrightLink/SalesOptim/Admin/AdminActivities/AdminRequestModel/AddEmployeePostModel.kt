package com.BrightLink.SalesOptim.Admin.AdminActivities.AdminRequestModel

data class AddEmployeePostModel(
    val Gender: String,
    val Hiredate: String,
    val Name: String,
    val department: String,
    val description: String,
    val email: String,
    val password: String,
    val phone: String,
    val secondName: String,
    val shiftTime: String,
    val timeZone: String,
    val work: String
)
//
//
//
//{
//    "Name": "Arati",
//    "secondName": "Potadar",
//    "work": "Developer",
//    "email": "aratir@gmail.com",
//    "phone": "8767876787",
//    "description": "backend",
//    "department": "IT",
//    "shiftTime": "8:00 AM to 6:00 PM",
//    "Gender": "female",
//    "Hiredate": "21-03-2024 01:45 PM",
//    "password": "arati@123",
//    "timeZone": "Asia/Kolkata"
//}
