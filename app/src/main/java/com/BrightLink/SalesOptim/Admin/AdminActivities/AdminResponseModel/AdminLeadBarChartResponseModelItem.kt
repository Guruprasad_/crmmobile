package com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel

data class AdminLeadBarChartResponseModelItem(
    val lostLeadsPercentage: Double,
    val proposalLeadsPercentage: Double,
    val qualifiedLeadsPercentage: Double,
    val wonLeadsPercentage: Double
)