package com.BrightLink.SalesOptim.Admin.AdminActivities.Task

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.OpenableColumns
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.BrightLink.SalesOptim.Adapters.TaskAttachmentsNotesLinksAdapters.TaskAttachmentsAdapter
import com.BrightLink.SalesOptim.Adapters.TaskAttachmentsNotesLinksAdapters.TaskLinksAdapter
import com.BrightLink.SalesOptim.Adapters.TaskAttachmentsNotesLinksAdapters.TaskNotesAdapter
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.RequestModel.UpdateTaskReqModel
import com.BrightLink.SalesOptim.ResponseModel.NAL_ResModels.TaskAttaResModelItem
import com.BrightLink.SalesOptim.ResponseModel.NAL_ResModels.TaskLinksResModelItem
import com.BrightLink.SalesOptim.ResponseModel.NAL_ResModels.TaskNotesResModelItem
import com.BrightLink.SalesOptim.ResponseModel.TaskListModelItem
import com.BrightLink.SalesOptim.ResponseModel.UpdateTaskResModel
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.AdminActivityUpdateTaskBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.Date

class AdminUpdatetaskFragment: Fragment() {
    private var _binding: AdminActivityUpdateTaskBinding? = null
    private val binding get() = _binding!!

    private lateinit var dueDateEditText: EditText
    private lateinit var reminderDateEditText: EditText
    private var dueDate: Date? = null
    private var reminderDate: Date? = null

    private val PICKFILE_RESULT_CODE = 1
    private var filePath: String? = null
    private var dialog: Dialog? = null
    private lateinit var path: String
    private var taskId: Int? = 0
    private lateinit var Desc : String

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = AdminActivityUpdateTaskBinding.inflate(inflater, container, false)
        val root: View = binding.root

        dueDateEditText = binding.updateDueDate
        reminderDateEditText = binding.updateRemdate


        binding.closeBtn.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.updateDueDate.setOnClickListener {
            AdminTaskUtils.showDatePickerDialog(
                requireContext(),
                dueDateEditText,
                reminderDate,
                dueDate,
                isDueDate = true,
                onDateSelected = { selectedDate ->
                    dueDate = selectedDate // Update the due date
                }
            )
        }

        binding.updateRemdate.setOnClickListener {
            AdminTaskUtils.showDatePickerDialog(
                requireContext(),
                reminderDateEditText,
                reminderDate,
                dueDate,
                isDueDate = false,
                onDateSelected = { selectedDate ->
                    reminderDate = selectedDate // Update the reminder date
                }
            )
        }

        AdminTaskUtils.setupSpinners(
            requireContext(),
            binding.taskStatusList,
            binding.taskPriorityList,
            binding.taskTypeList
        )

        val selectedStatus = AdminTaskUtils.getSelectedItem(binding.taskStatusList)
        val selectedPriority = AdminTaskUtils.getSelectedItem(binding.taskPriorityList)
        val selectedType = AdminTaskUtils.getSelectedItem(binding.taskTypeList)

        val selectedItem = arguments?.getSerializable("selectedItem") as? TaskListModelItem
        if (selectedItem == null)
        {
            Constants.error(requireContext(), "Selected Item is Null")
            return root
        }
        taskId = selectedItem.id
        if (taskId != null) {
            Constants.info(requireContext(),"Task Id fetched")
            fetchNotesList(taskId!!)
            fetchLinksList(taskId!!)
            fetchAttachmentsList(taskId!!)
        }
        else{
            Constants.error(requireContext(),"Task Id is Null")
        }

        //Fetching the LeadIdorAccountIdorContactId
        val leadId = selectedItem.leadIdOrAccountId?:0

        selectedItem?.let {
            binding.fullName.setText(it.taskOwner?:"")
            binding.taskSubject.setText(it.subject?:"")
            binding.updateDueDate.setText(it.duedate?:"")
            binding.updateRemdate.setText(it.reminderDate?:"")
            binding.addTaskDesEditText.setText(it.description?:"")

            // Find the index of the status in the status array
            val statusIndex = getStatusIndex(it.status.toString())
            if (statusIndex != -1) {
                // Set the selection if statusIndex is valid
                binding.taskStatusList.setSelection(statusIndex)
            }

            //Find the index of the type in the type array
            val typeIndex = getTypeIndex(it.leadOrAccountType.toString())
            if (typeIndex != -1)
            {
                binding.taskTypeList.setSelection(typeIndex)
            }
            // Find the index of the priority in the priority array
            val priorityIndex = getPriorityIndex(it.priority.toString())
            if (priorityIndex != -1) {
                // Set the selection if priorityIndex is valid
                binding.taskPriorityList.setSelection(priorityIndex)
            }
        }

        binding.taskUpdateBtn.setOnClickListener {
            updateTaskoperation(taskId!!,leadId,selectedItem)
        }

        return root
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == PICKFILE_RESULT_CODE && resultCode == Activity.RESULT_OK && data != null && data.data != null) {
            val uri: Uri = data.data!!
            path = getPathFromUri(requireContext(), uri)!!
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                updateTextViewWithFileName(uri)
            } else {
                // Handle for lower versions
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun updateTextViewWithFileName(uri: Uri) {
        val fileName = getFileNameFromUri(uri)
        filePath = uri.toString()
        if (fileName != null) {
            val selectedFile = dialog!!.findViewById<TextView>(R.id.selectedFile)
            selectedFile.text = Html.fromHtml("<b><font color='#7FFF00'>$fileName</font></b>")
        }
    }

    private fun getFileNameFromUri(uri: Uri): String? {
        var fileName: String? = null
        val cursor = requireContext().contentResolver.query(uri, null, null, null, null)
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                val displayNameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
                if (displayNameIndex != -1) {
                    fileName = cursor.getString(displayNameIndex)
                }
            }
            cursor.close()
        }
        return fileName
    }

    @SuppressLint("Range")
    private fun getPathFromUri(context: Context, uri: Uri): String? {
        var path: String? = null
        if ("content".equals(uri.scheme, ignoreCase = true)) {
            // Using ACTION_OPEN_DOCUMENT
            val cursor = context.contentResolver.query(uri, null, null, null, null)
            cursor?.use {
                if (it.moveToFirst()) {
                    val displayName = it.getString(it.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                    val type = it.getString(it.getColumnIndex("mime_type"))
                    val file = File(context.cacheDir, displayName)
                    try {
                        val inputStream = context.contentResolver.openInputStream(uri)
                        inputStream?.use { input ->
                            FileOutputStream(file).use { output ->
                                input.copyTo(output)
                            }
                        }
                        path = file.absolutePath
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
            }
        } else if ("file".equals(uri.scheme, ignoreCase = true)) {
            path = uri.path
        }
        return path
    }
    private fun fetchNotesList(taskId: Int) {
        val jwtToken = AdminTaskUtils.gettoken(requireContext())
        val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
        val callApi = service.getAdminTaskNotesList("Bearer $jwtToken", taskId)

        callApi.enqueue(object : Callback<List<TaskNotesResModelItem >>{
            override fun onResponse(
                call: Call<List<TaskNotesResModelItem>>,
                response: Response<List<TaskNotesResModelItem>>
            ) {
                if (isAdded&& response.isSuccessful)
                {
                    val data = response.body()
                    val Notesadapter = TaskNotesAdapter(requireContext(), data)


                    binding.taskNotesRecyclerView.adapter = Notesadapter

                    binding.taskNotesRecyclerView.apply {
                        layoutManager = LinearLayoutManager(requireContext())
                        adapter = Notesadapter
                    }

                }
                else {
                    Constants.error(requireContext(), "Unsuccessful response in fetching Notes List:  ${response.code()}")
                }
            }
            override fun onFailure(call: Call<List<TaskNotesResModelItem>>, t: Throwable) {
                Constants.error(requireContext(), "Error: ${t.message}")
            }

        })
    }

    private fun fetchAttachmentsList(taskId: Int) {

        val jwtToken = AdminTaskUtils.gettoken(requireContext())
        val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
        val callApi = service.getAdminTaskAttaList("Bearer $jwtToken", taskId)

        callApi.enqueue(object : Callback<List<TaskAttaResModelItem>>{
            override fun onResponse(
                call: Call<List<TaskAttaResModelItem>>,
                response: Response<List<TaskAttaResModelItem>>
            ) {
                if (response.isSuccessful && isAdded)
                {
                    val data = response.body()
                    val AttachmentAdapter = TaskAttachmentsAdapter(requireContext(),data!!)
                    binding.taskAttachmentsRecyclerView.adapter = AttachmentAdapter

                    binding.taskAttachmentsRecyclerView.apply {
                        layoutManager = LinearLayoutManager(requireContext())
                        adapter = AttachmentAdapter
                    }
                }
            }

            override fun onFailure(call: Call<List<TaskAttaResModelItem>>, t: Throwable) {
                TODO("Not yet implemented")
            }

        })

    }
    private fun fetchLinksList(taskId: Int) {
        val jwtToken = AdminTaskUtils.gettoken(requireContext())
        val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
        val callApi = service.getAdminTaskLinksList("Bearer $jwtToken", taskId)

        callApi.enqueue(object : Callback<List<TaskLinksResModelItem>>
        {
            override fun onResponse(
                call: Call<List<TaskLinksResModelItem>>,
                response: Response<List<TaskLinksResModelItem>>
            ) {
                if (response.isSuccessful && isAdded)
                {
                    val data = response.body()
                    val linkClickListener = object : TaskLinksAdapter.LinkClickListener {
                        override fun onLinkClick(url: String) {
                            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                            context!!.startActivity(intent)
                        }
                    }
                    val LinksAdapter = TaskLinksAdapter(requireContext(), data!!,linkClickListener)

                    binding.taskLinksRecyclerView.apply {
                        layoutManager = LinearLayoutManager(requireContext())
                        adapter = LinksAdapter
                    }
                }
                else {
                    Constants.error(requireContext(), "Unsuccessful response in fetching Links List: ${response.code()}")
                }
            }

            override fun onFailure(call: Call<List<TaskLinksResModelItem>>, t: Throwable) {
                Constants.error(requireContext(), "Error: ${t.message}")
            }

        })
    }

    private fun getTypeIndex(type: String): Int {
        val typeArray = resources.getStringArray(R.array.Task_type)
        return typeArray.indexOf(type)
    }

    // Function to get the index of the status in the array
    fun getStatusIndex(status: String): Int {
        val statusArray = resources.getStringArray(R.array.Lead_Status)
        return statusArray.indexOf(status)
    }

    // Function to get the index of the priority in the array
    fun getPriorityIndex(priority: String): Int {
        val priorityArray = resources.getStringArray(R.array.Task_Priority)
        return priorityArray.indexOf(priority)
    }

    private fun updateTaskoperation(taskId: Int,leadId:Int, selectedItem: TaskListModelItem) {
        val taskowner = binding.fullName.text.toString()
        val subject = binding.taskSubject.text.toString()
        val type = binding.taskTypeList.selectedItem.toString()
        val status = binding.taskStatusList.selectedItem.toString()
        val priority = binding.taskPriorityList.selectedItem.toString()
        val reminderDate = binding.updateRemdate.text.toString()
        val dueDate = binding.updateDueDate.text.toString()
        val desc = binding.addTaskDesEditText.text.toString()

        val requestBody = UpdateTaskReqModel(desc,dueDate,taskId.toString(),leadId.toString()
            ,type,priority,reminderDate,status,subject,taskowner)

        val jwtToken = AdminTaskUtils.gettoken(requireContext())
        if (jwtToken!=null)
        {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)

            val callUpdateTask = service.updateTaskAdmin("Bearer $jwtToken",requestBody)
            callUpdateTask.enqueue(object : Callback<UpdateTaskResModel>{
                override fun onResponse(
                    call: Call<UpdateTaskResModel>,
                    response: Response<UpdateTaskResModel>
                ) {
                    if (response.isSuccessful)
                    {
                        Constants.success(requireContext(),"Task Updated Successfully ")
                        findNavController().popBackStack()
                    }
                    else{
                        Constants.error(requireContext(),"Something went wrong: ${response.code()}")
                    }
                }
                override fun onFailure(call: Call<UpdateTaskResModel>, t: Throwable) {
                    Constants.error(requireContext(), "Token is null. Please log in again.")

                }
            })
        }
    }

    private fun gettoken(context: Context): String? {
        val sharedPreferences = context.getSharedPreferences(
            "Token", Context.MODE_PRIVATE
        )
        return sharedPreferences.getString("jwtToken", null)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}