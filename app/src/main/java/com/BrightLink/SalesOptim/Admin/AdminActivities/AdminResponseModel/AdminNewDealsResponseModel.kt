package com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel

data class AdminNewDealsResponseModel(
    val resultList: List<ResultX>,
    val totalLeadList: List<TotalLead>
)