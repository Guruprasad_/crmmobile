package com.BrightLink.SalesOptim.Admin.AdminActivities.AdminLeads

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel.AdminLeadReportModelItem
import com.BrightLink.SalesOptim.AdminAdapters.AdminLeadReportAdapter
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.Constants.WrapContentLinearLayoutManager
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.AdminLeadReportFragmentBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AdminLeadReport : Fragment() {
    private var _binding: AdminLeadReportFragmentBinding? = null
    private lateinit var adapter : AdminLeadReportAdapter
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = AdminLeadReportFragmentBinding.inflate(inflater, container, false)
        val root : View = binding.root

        binding.actionBar.activityName.text = "Lead Report"
        binding.actionBar.back.setOnClickListener {
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }

        binding.actionBar.option.visibility = View.INVISIBLE

        binding.recyclerView.layoutManager = WrapContentLinearLayoutManager(context)

        val jwtToken = gettoken(context)
        if (jwtToken!=null)
        {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            GetLeadReport(service , jwtToken , context)
        }
        else{
            Constants.error(context,"Token is null Login Again")
        }

        return root
    }

    private fun GetLeadReport(apiservice : ApiInterface , jwtToken : String , context: Context? ) {
        context?:return
        val call = apiservice.adminLeadReport("Bearer $jwtToken")
        call.enqueue(object : Callback<List<AdminLeadReportModelItem>> {
            override fun onResponse(
                call: Call<List<AdminLeadReportModelItem>>,
                response: Response<List<AdminLeadReportModelItem>>
            ) {
                if (isAdded)
                {
                    if (response.isSuccessful)
                    {
                        val data = response.body()
                        if (data!=null)
                        {
                            adapter = AdminLeadReportAdapter(context, data)
                            binding.recyclerView.adapter = adapter
                            Log.d("Report","$data")
                        }
                        else{
                            Constants.error(context, "Response body is null")
                        }
                    }
                    else
                    {
                        Constants.error(context,"Response is not successful")
                    }
                }

            }

            override fun onFailure(call: Call<List<AdminLeadReportModelItem>>, t: Throwable) {
                Constants.error(context,"Error : ${t.message}")
            }

        })
    }
    private fun gettoken(context: Context?): String? {
        val sharedPreferences = requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
        return sharedPreferences.getString("jwtToken", null)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}