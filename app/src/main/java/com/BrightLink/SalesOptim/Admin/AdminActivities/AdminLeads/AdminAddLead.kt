package com.BrightLink.SalesOptim.Admin.AdminActivities.AdminLeads

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel.AccountNameByEmpIdResponseItem
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.RequestModel.AddLeadPostModel
import com.BrightLink.SalesOptim.ResponseModel.AccountDetailsByIdModel
import com.BrightLink.SalesOptim.ResponseModel.AddLeadModel
import com.BrightLink.SalesOptim.ResponseModel.CityModelItem
import com.BrightLink.SalesOptim.ResponseModel.CountryModelItem
import com.BrightLink.SalesOptim.ResponseModel.ShowEmployeelistModelItem
import com.BrightLink.SalesOptim.ResponseModel.StateModelItem
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.AdminAddLeadFragmentBinding
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.DateValidatorPointForward
import com.google.android.material.datepicker.MaterialDatePicker
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale

class AdminAddLead : Fragment() {
    private var _binding: AdminAddLeadFragmentBinding? = null
    private val binding get() = _binding!!

    private lateinit var leadTypeSpinner: Spinner
    private lateinit var requirementSpinner: Spinner
    private lateinit var leadSourceSpinner: Spinner

    private lateinit var companyNameSpinner: Spinner
    private lateinit var companyNameAdapter: ArrayAdapter<String>
    private var companyNames: ArrayList<String> = ArrayList()
    private val companyNameToAccountIdMap: MutableMap<String, String> = mutableMapOf()

    private lateinit var countrySpinner: Spinner
    private lateinit var countryAdapter: ArrayAdapter<String>
    private var countryNames : ArrayList<String> = ArrayList()

    private lateinit var stateSpinner: Spinner
    private lateinit var stateAdapter: ArrayAdapter<String>
    private var stateNames : ArrayList<String> = ArrayList()

    private lateinit var citySpinner: Spinner
    private lateinit var cityAdapter: ArrayAdapter<String>
    private var cityNames : ArrayList<String> = ArrayList()

    private var CountryID : String? = null
    private var StateID : String? = null
    private var CityID : String? = null
    private var selectedLeadName : String = ""
    private var selectedEmpId : Int = 0
    private var accountNames : ArrayList<String>  = ArrayList()
    private var selectedAccountId : Int = 0
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = AdminAddLeadFragmentBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.actionBar.activityName.text = "Add Lead"
        binding.actionBar.back.setOnClickListener{
            requireActivity().onBackPressed()
        }
        binding.actionBar.option.visibility = View.INVISIBLE

        binding.date.setOnClickListener {
            showMaterialCalendar()
        }

        leadTypeSpinner = binding.leadTypeSpinner
        requirementSpinner = binding.requirementSpinner
        leadSourceSpinner = binding.leadSourceSpinner

        val leadTypeAdapter = ArrayAdapter.createFromResource(requireContext(), R.array.Lead_Type, R.layout.simple_spinner_item_layout)
        leadTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        leadTypeSpinner.adapter = leadTypeAdapter

        val requirementAdapter = ArrayAdapter.createFromResource(requireContext(), R.array.Requirement, R.layout.simple_spinner_item_layout)
        requirementAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        requirementSpinner.adapter = requirementAdapter

        val leadSourceAdapter = ArrayAdapter.createFromResource(requireContext(), R.array.Lead_source, R.layout.simple_spinner_item_layout)
        leadSourceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        leadSourceSpinner.adapter = leadSourceAdapter



        fetchLeadOwnerNames()
        //getAccountNmeByEmpId(selectedEmpId)


        countrySpinner = binding.countrySpinner
        countryAdapter = ArrayAdapter(requireContext(), R.layout.simple_spinner_item_layout, countryNames).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
        countrySpinner.adapter = countryAdapter

        stateSpinner = binding.stateSpinner
        stateAdapter = ArrayAdapter(requireContext(), R.layout.simple_spinner_item_layout, stateNames).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
        stateSpinner.adapter = stateAdapter

        citySpinner = binding.citySpinner
        cityAdapter = ArrayAdapter(requireContext(), R.layout.simple_spinner_item_layout, cityNames).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
        citySpinner.adapter = cityAdapter

        callCountryApi()

        binding.addLead.setOnClickListener{
            CallApi()
        }

        return root
    }

    private fun callCountryApi() {
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null){
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.getCountry("Bearer $jwtToken")
            call.enqueue(object : Callback<List<CountryModelItem>> {
                override fun onResponse(
                    call: Call<List<CountryModelItem>>,
                    response: Response<List<CountryModelItem>>
                ) {
                    if (response.isSuccessful){
                        val data = response.body()
                        if (data != null){
                            for (item in data){
                                item.name?.let { countryNames.add(it) }
                            }
                            countryAdapter.notifyDataSetChanged()

                            countrySpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                                    val selectedCountryName = countryNames[position]
                                    val selectedCountry = data.find { it.name == selectedCountryName }
                                    selectedCountry?.let {
                                        callStateApi(it.id)
                                        CountryID = it.id.toString()
                                    }
                                }

                                override fun onNothingSelected(p0: AdapterView<*>?) {
                                    Constants.warning(requireContext(), "Select Country")
                                }

                            }

                        } else {
                            Constants.error(requireContext(), "Response body is null")
                        }
                    } else {
                        Constants.error(requireContext(), "Unsuccessful response: ${response.code()}")
                    }
                }

                override fun onFailure(call: Call<List<CountryModelItem>>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }

            })
        }
    }

    private fun callStateApi(id: Int) {
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null){
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.getState("Bearer $jwtToken", id)
            call.enqueue(object : Callback<List<StateModelItem>> {
                override fun onResponse(
                    call: Call<List<StateModelItem>>,
                    response: Response<List<StateModelItem>>
                ) {
                    if (response.isSuccessful){
                        val data = response.body()
                        if (data != null){
                            stateNames.clear()
                            for (item in data){
                                item.name?.let { stateNames.add(it) }
                            }
                            stateAdapter.notifyDataSetChanged()

                            stateSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                                    val selectedStateName = stateNames[position]
                                    val selectedState = data.find { it.name == selectedStateName }
                                    selectedState?.let {
                                        callCityApi(it.id)
                                        StateID = it.id.toString()
                                    } ?: run {
                                        Constants.warning(requireContext(), "State ID not found")
                                    }
                                }

                                override fun onNothingSelected(p0: AdapterView<*>?) {
                                    Constants.warning(requireContext(), "Select State")
                                }

                            }
                        } else {
                            Constants.error(requireContext(), "Response body is null")
                        }
                    } else {
                        Constants.error(requireContext(), "Unsuccessful response: ${response.code()}")
                    }
                }

                override fun onFailure(call: Call<List<StateModelItem>>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }

            })
        }
    }

    private fun callCityApi(id: Int) {
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null){
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.getCity("Bearer $jwtToken", id)
            call.enqueue(object : Callback<List<CityModelItem>> {
                override fun onResponse(
                    call: Call<List<CityModelItem>>,
                    response: Response<List<CityModelItem>>
                ) {
                    if (response.isSuccessful){
                        val data = response.body()
                        if (data != null){
                            cityNames.clear()
                            for (item in data){
                                item.name?.let { cityNames.add(it)
                                }
                            }
                            cityAdapter.notifyDataSetChanged()
                            citySpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                                override fun onItemSelected(
                                    parent: AdapterView<*>?,
                                    view: View?,
                                    position: Int,
                                    id: Long
                                ) {
                                    val selectedCityName = cityNames[position]
                                    val selectedCity = data.find { it.name == selectedCityName }
                                    selectedCity?.let {
                                        CityID = it.id.toString()
                                    } ?: run {
                                        Constants.warning(requireContext(), "City ID not found")
                                    }
                                }

                                override fun onNothingSelected(parent: AdapterView<*>?) {
                                    Constants.warning(requireContext(), "Select the city")
                                }
                            }
                        } else {
                            Constants.error(requireContext(), "Response body is null")
                        }
                    } else {
                        Constants.error(requireContext(), "Unsuccessful response: ${response.code()}")
                    }
                }

                override fun onFailure(call: Call<List<CityModelItem>>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }

            })
        }
    }

    private fun fetchLeadOwnerNames() {
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null){
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val callEmployeeList = service.adminEmployeeList("Bearer $jwtToken")
            callEmployeeList.enqueue(object : Callback<ArrayList<ShowEmployeelistModelItem>> {
                override fun onResponse(
                    call: Call<ArrayList<ShowEmployeelistModelItem>>,
                    response: Response<ArrayList<ShowEmployeelistModelItem>>
                ) {
                    if (isAdded) {
                        if (response.isSuccessful) {
                            val employeelist = response.body()
                            if (employeelist != null && employeelist.isNotEmpty()) {
                                val employeeNames = employeelist.map { it.name }
                                leadOwnerSpinner(employeeNames)

                                binding.leadOwnerName.onItemSelectedListener =
                                    object : AdapterView.OnItemSelectedListener {
                                        override fun onItemSelected(
                                            parent: AdapterView<*>?,
                                            view: View?,
                                            position: Int,
                                            id: Long
                                        ) {
                                            selectedLeadName =
                                                parent?.getItemAtPosition(position).toString()
                                            val selectedLead =
                                                employeelist.find { it.name == selectedLeadName }
                                            if (selectedLead != null) {
                                                selectedEmpId = selectedLead.cId
                                                getAccountNmeByEmpId(selectedEmpId)
                                                Log.d("E,pID", "$selectedEmpId")
                                            }
                                            setCompanyNameEmpty(accountNames)
                                        }

                                        override fun onNothingSelected(parent: AdapterView<*>?) {
                                            TODO("Not yet implemented")
                                        }

                                    }
                            } else {
                                Constants.error(requireContext(), "Employee List is null or empty")
                            }
                        } else {
                            Constants.error(
                                requireContext(),
                                "Unsuccessful response: ${response.code()}"
                            )
                        }
                    }
                }

                override fun onFailure(
                    call: Call<ArrayList<ShowEmployeelistModelItem>>,
                    t: Throwable
                ) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }

            })
        }else {
            Constants.error(requireContext(), "Login Again")
        }
    }

    private fun leadOwnerSpinner(employeeNames: List<String>) {

        val defaultText = "Select Lead Owner"
        val defaultList =  mutableListOf(defaultText)
        defaultList.addAll(employeeNames)
        // Set up the spinner adapter
        val employeeAdapter = ArrayAdapter(requireContext(), R.layout.simple_spinner_item_layout, defaultList)
        employeeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        // Set the adapter to the spinner
        binding.leadOwnerName.adapter = employeeAdapter
    }
    private fun getAccountNmeByEmpId(EmpId : Int){
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null){
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.getAccountNameByEmpId("Bearer $jwtToken",EmpId)
            call.enqueue(object : Callback<ArrayList<AccountNameByEmpIdResponseItem>> {
                override fun onResponse(
                    call: Call<ArrayList<AccountNameByEmpIdResponseItem>>,
                    response: Response<ArrayList<AccountNameByEmpIdResponseItem>>
                ) {
                    if (isAdded){
                        if (response.isSuccessful){
                            val data = response.body()
                            if (data.isNullOrEmpty()){
                                setCompanyNameEmpty(accountNames)
                            }else {
                                val accountNames = data.map { it.accountName }

                                //Spinner Code Starts here
                                val defaultText = "Select Account Name"
                                val defaultList =  mutableListOf(defaultText)
                                defaultList.addAll(accountNames)
                                // Set up the spinner adapter
                                val companyNameAdapter =
                                    ArrayAdapter(requireContext(), R.layout.simple_spinner_item_layout, defaultList)
                                companyNameAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

                                // Set the adapter to the spinner
                                binding.companyName.adapter = companyNameAdapter
                                binding.companyName.onItemSelectedListener = object  : AdapterView.OnItemSelectedListener{

                                    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                                        val accountIds = data.map { it.accountId }
                                        selectedAccountId = accountIds[p2]
                                        fetchCompanyDetails(selectedAccountId)

                                    }

                                    override fun onNothingSelected(p0: AdapterView<*>?) {
                                        TODO("Not yet implemented")
                                    }

                                }

                            }
                        }
                    }
                }

                override fun onFailure(call: Call<ArrayList<AccountNameByEmpIdResponseItem>>, t: Throwable) {
                    TODO("Not yet implemented")
                }
            })
        }
    }



    private fun setCompanyNameEmpty(accountNames: List<String>) {
        // Set up the spinner adapter
        val defaultText = "No accounts available"
        val defaultList = listOf(defaultText)

        val defaultAdapter = ArrayAdapter(
            requireContext(),
            R.layout.simple_spinner_item_layout,
            defaultList
        )
        defaultAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.companyName.adapter = defaultAdapter

    }

    private fun fetchCompanyDetails(aid: Int) {
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null) {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val callAccounts = service.accountDetailsById("Bearer $jwtToken", aid)
            callAccounts.enqueue(object : Callback<AccountDetailsByIdModel> {
                override fun onResponse(
                    call: Call<AccountDetailsByIdModel>,
                    response: Response<AccountDetailsByIdModel>
                ) {
                    if (response.isSuccessful) {
                        val data = response.body()
                        if (data != null) {
                            binding.email.setText(data.accountEmail)
                            binding.mobileNo.setText(data.phone)
                            binding.website.setText(data.website)
                        } else {
                            Constants.error(requireContext(), "Response body is null")
                        }
                    } else {
                        Constants.error(
                            requireContext(),
                            "Unsuccessful response: ${response.code()}"
                        )
                    }
                }

                override fun onFailure(call: Call<AccountDetailsByIdModel>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }

            })
        }
    }

    private fun CallApi() {

        val leadCreatedBy = binding.leadOwnerName.selectedItem.toString()

        val customerName = binding.customerName.text.toString()
        if (customerName.isBlank()) {
            Constants.error(requireContext(), "Customer name is Required")
            return
        }
        if (!customerName.matches("[A-Za-z ]{1,100}".toRegex())) {
            Constants.error(
                requireContext(),
                "Customer name contains only alphabetical characters and could not exceed 100 characters."
            )
            return
        }

        val leadType = binding.leadTypeSpinner.selectedItem.toString()
        if (leadType.isBlank()) {
            Constants.error(requireContext(), "Lead type is required.")
            return
        }

        val requirement = binding.requirementSpinner.selectedItem.toString()
        if (requirement.isBlank()) {
            Constants.error(requireContext(), "Requirement is required.")
            return
        }

        val leadRevenue = binding.leadRevenue.text.toString()
        if (leadRevenue.isBlank()) {
            Constants.error(requireContext(), "Lead revenue is required.")
            return
        }
        if (leadRevenue.length > 10) {
            Constants.error(requireContext(), "Lead revenue should be up to 10 digits.")
            return
        }

        val leadSource = binding.leadSourceSpinner.selectedItem.toString()
        if (leadSource.isBlank()) {
            Constants.error(requireContext(), "Lead source is required.")
            return
        }

        val leadStatus = "New Lead"

        val companyName = binding.companyName.selectedItem.toString()
        if (companyName.isBlank()) {
            Constants.error(requireContext(), "Company Name is required.")
            return
        }

        val accountid : String = selectedAccountId.toString()
        if (accountid != null) {
            // The companyName exists in the map, and accountid is non-null
            Log.d("AccountID", "Account ID: $accountid")
        } else {
            // Handle the case where accountid is null
            Constants.error(requireContext(), "Account ID not found for the selected company name.")
        }

        val followUpdate = binding.date.text.toString()
        if (followUpdate.isBlank()) {
            Constants.error(requireContext(), "Follow update is required.")
            return
        }

        val website = binding.website.text.toString()
        if (website.isNotBlank()) {
            val urlRegex = "^(https?://)?([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}(/[a-zA-Z0-9-_.~!*'();:@&=+$,/?#[%]]*)*$".toRegex()

            if (!website.matches(urlRegex)) {
                Constants.error(
                    requireContext(),
                    "Please enter a valid website URL."
                )
                return
            }
        }

        val mobileNo = binding.mobileNo.text.toString()
        if (mobileNo.isBlank()) {
            Constants.error(requireContext(), "Mobile number is required.")
            return
        }
        if (!mobileNo.matches("""(\+?[0-9]{1,15})""".toRegex())) {
            Constants.error(
                requireContext(),
                "Mobile number should contain upto 15 digits and optionally start with '+'."
            )
            return
        }

        val alternateMobileNumber = binding.alternateMobileNo.text.toString()

        if (alternateMobileNumber.isNotEmpty()) {
            if (!alternateMobileNumber.matches("""(\+?[0-9]{1,15})""".toRegex())) {
                Constants.error(
                    requireContext(),
                    "Mobile number should start with '+' and contain up to 15 digits."
                )
                return
            }
        }

        val email = binding.email.text.toString()
        if (email.isBlank()) {
            Constants.error(requireContext(), "Email is required.")
            return
        }
        if (email.length > 100) {
            Constants.error(requireContext(), "Email should be up to 100 characters.")
            return
        }

        val address = binding.address.text.toString()
        if (address.length > 255) {
            Constants.error(requireContext(), "Address should be upto 255 characters only.")
            return
        }

        val zip = binding.zip.text.toString()
        if (zip.isBlank()) {
            Constants.error(requireContext(), "ZIP code is required.")
            return
        }
        if (!zip.matches("[A-Za-z0-9]{1,10}".toRegex())) {
            Constants.error(
                requireContext(),
                "ZIP code should contain up to 10 alphanumeric characters."
            )
            return
        }

        if (CountryID == null) {
            Constants.error(requireContext(), "Country is Null.")
            return
        }
        if (StateID == null) {
            Constants.error(requireContext(), "State is Null.")
            return
        }

        if (CityID == null) {
            Constants.error(requireContext(), "City is Null.")
            return
        }

        val description = binding.description.text.toString()
        if (description.length > 255) {
            Constants.error(requireContext(), "Description should be upto 255 characters only.")
            return
        }

        val requestBody = AddLeadPostModel(accountid, address, alternateMobileNumber, CityID.toString(), companyName, CountryID.toString(), customerName, description, email, followUpdate, leadCreatedBy, leadRevenue, leadSource, leadStatus, leadType, mobileNo, requirement, StateID.toString(), website, zip)

        Log.d("GURU",requestBody.toString())

        val jwtToken = gettoken(requireContext())
        if (jwtToken!=null)
        {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.adminAddLead("Bearer $jwtToken" , requestBody)
            call.enqueue( object :Callback<AddLeadModel>{
                override fun onResponse(call: Call<AddLeadModel>, response: Response<AddLeadModel>) {
                    if (response.isSuccessful)
                    {
                        val data = response.body()
                        if (data!=null)
                        {
                            Constants.success(context," Lead Added successfully ${data.leadId}")
                            parentFragmentManager.popBackStack()
                        }
                    }
                    else
                    {
                        Constants.error(context,"Response is unsuccessful: ${response.code()}")
                    }

                }
                override fun onFailure(call: Call<AddLeadModel>, t: Throwable) {
                    Constants.error(context ,"Failed to add the lead ${t.message}")
                }

            })
        }
        else{
            Constants.error(requireContext(),"Token is null Login Again")
        }


    }

    private fun gettoken(context: Context?): String? {
        val sharedPreferences = requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
        return sharedPreferences.getString("jwtToken", null)
    }


    private fun showMaterialCalendar() {
        val today = Calendar.getInstance().timeInMillis
        val datePickerBuilder = MaterialDatePicker.Builder.datePicker()
        datePickerBuilder.setSelection(today)

        val constraintsBuilder = CalendarConstraints.Builder()
        constraintsBuilder.setValidator(DateValidatorPointForward.now())

        datePickerBuilder.setCalendarConstraints(constraintsBuilder.build())

        val datePicker = datePickerBuilder.build()

        datePicker.addOnPositiveButtonClickListener {selectedDate->
            val formatedDate = SimpleDateFormat("yyyy/dd/MM", Locale.getDefault()).format(Date(selectedDate))
            binding.date.setText(formatedDate.toString())
        }
        datePicker.show(parentFragmentManager, datePicker.toString())

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}