package com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel

data class TopEmployee(
    val assignedTo: String,
    val assignedToLow: Any,
    val assignedToTop: Any,
    val cancelCount: Int,
    val completeCount: Int,
    val completeTasksLow: Int,
    val completeTasksTop: Int,
    val emplyoeeId: Int,
    val emplyoeeIdLow: Int,
    val emplyoeeIdTop: Int,
    val inProcessCount: Int,
    val lowResult: Double,
    val pendingCount: Int,
    val topResult: Double,
    val totalCount: Int,
    val totalTaskLow: Int,
    val totalTasksTop: Int
)