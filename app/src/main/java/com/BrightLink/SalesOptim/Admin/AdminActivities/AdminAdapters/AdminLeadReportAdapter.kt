package com.BrightLink.SalesOptim.AdminAdapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel.AdminLeadReportModelItem
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.databinding.LeadreportTableBinding

class AdminLeadReportAdapter(
    val context : Context,private var dataList: List<AdminLeadReportModelItem>
): RecyclerView.Adapter<AdminLeadReportAdapter.ViewHolder>() {

    private var filteredList: List<AdminLeadReportModelItem> = dataList
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdminLeadReportAdapter.ViewHolder {
        val binding = LeadreportTableBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: AdminLeadReportAdapter.ViewHolder, position: Int) {
        val currentItem = filteredList[position]
        holder.binding.taskTitle.text = currentItem.taskTitle
        holder.binding.taskType.text = currentItem.taskType
        holder.binding.costumerName.text = currentItem.customerName
        holder.binding.taskStatus.text = currentItem.status
        val status = currentItem.status
        statusColor(status , holder)

        holder.binding.layout.startAnimation(
            AnimationUtils.loadAnimation(
                holder.itemView.context,
                R.anim.recycler_view_anime_2
            )
        )
    }
    @SuppressLint("ResourceAsColor")
    private fun statusColor(status: String?, holder: AdminLeadReportAdapter.ViewHolder) {
        val colorResId = when (status) {
            "New Lead" -> R.color.newLead
            "Contacted" -> R.color.contacted
            "Email Sent" -> R.color.emailSent
            "Not Contacted" -> R.color.notContacted
            "Qualified" -> R.color.qualified
            "Not Qualified" -> R.color.notQualified
            "Proposal Sent" -> R.color.proposalSend
            "Lead Won" -> R.color.leadWon
            "Lead Lost" -> R.color.leadLost
            else -> R.color.primaryColor // Set a default color if status is null or doesn't match any condition
        }
        changeTextViewBackgroundTint(context, colorResId, holder)
    }
    fun changeTextViewBackgroundTint(context: Context, colorResId: Int, holder: AdminLeadReportAdapter.ViewHolder) {
        val color = ContextCompat.getColor(context, colorResId)
        val colorStateList = ColorStateList.valueOf(color)
        holder.binding.taskStatus.backgroundTintList = colorStateList
    }

    class ViewHolder (val binding: LeadreportTableBinding): RecyclerView.ViewHolder(binding.root){

    }

    override fun getItemCount(): Int {
        return filteredList.size
    }

}