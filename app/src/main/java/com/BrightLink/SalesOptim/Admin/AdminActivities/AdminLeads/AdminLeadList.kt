package com.BrightLink.SalesOptim.Admin.AdminActivities.AdminLeads

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.Gravity
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import androidx.navigation.fragment.findNavController
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel.AdminLeadListModelItem
import com.BrightLink.SalesOptim.AdminAdapters.AdminLeadListAdapter
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.Constants.WrapContentLinearLayoutManager
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.AdminLeadListFragmentBinding
import retrofit2.Call
import retrofit2.Response

class AdminLeadList : Fragment() {

    private var _binding: AdminLeadListFragmentBinding? = null
    private var adapter : AdminLeadListAdapter? = null

    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding =  AdminLeadListFragmentBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.actionBar.activityName.text = "Lead"
        binding.actionBar.back.setOnClickListener {
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }
        binding.actionBar.option.setOnClickListener{view->
            showPopupMenu(view)
        }
        binding.addlead.setOnClickListener {
            findNavController().navigate(R.id.adminAddLead)
        }
        binding.recyclerView.layoutManager = WrapContentLinearLayoutManager(context)

        val jwtToken = gettoken(context)
        if (jwtToken != null){
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            GetLeadList(service,jwtToken)
        }else{
            Constants.error(context,"Token is null Login Again")
        }

        binding.search.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                adapter?.filter(s.toString())
            }
        })

        return root
    }

    private fun GetLeadList(apiInterface: ApiInterface,jwtToken: String){
        val call = apiInterface.adminLeadList("Bearer $jwtToken")
        call.enqueue(object : retrofit2.Callback<List<AdminLeadListModelItem>>{
            override fun onResponse(
                call: Call<List<AdminLeadListModelItem>>,
                response: Response<List<AdminLeadListModelItem>>
            ) {
                if (isAdded){
                    if (response.isSuccessful){
                        val data = response.body()
                        if (data != null){
                            adapter = AdminLeadListAdapter(requireContext(),data)
                            binding.recyclerView.adapter = adapter
                        } else {
                            Constants.error(context, "Response body is null")
                        }
                    } else {
                        Constants.error(context, "Response is not successful")
                    }
                }
            }

            override fun onFailure(call: Call<List<AdminLeadListModelItem>>, t: Throwable) {
                Constants.error(context, "Error: ${t.message}")
                Log.d("Show Lead","${t.message}")
            }

        })
    }


    private fun gettoken(context: Context?): String? {
        val sharedPreferences = requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
        return sharedPreferences.getString("jwtToken", null)
    }

    private fun showPopupMenu(view: View) {
        val popupMenu = PopupMenu(context, view, Gravity.END, 0, R.style.MenuItemStyle)
        popupMenu.inflate(R.menu.menu_main) // Inflate your menu resource file here

        popupMenu.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.leadreport -> {
                    findNavController().navigate(R.id.adminleadreport)
                    true
                }
                else -> false
            }
        }
        popupMenu.show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}