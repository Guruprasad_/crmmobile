package com.BrightLink.SalesOptim.Admin.AdminAdapters

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.ResponseModel.DeleteTemplateModel
import com.BrightLink.SalesOptim.ResponseModel.TemplateModelItem
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.GetTemplateBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.Locale

class TemplateAdapter(val context : Context, private var dataList: MutableList<TemplateModelItem>) : RecyclerView.Adapter<TemplateAdapter.ViewHolder>()  {

    private var filteredList: List<TemplateModelItem> = dataList
    inner class ViewHolder( val binding: GetTemplateBinding) : RecyclerView.ViewHolder(binding.root) {
        init {
            binding.delete.setOnClickListener {
                val position = adapterPosition
                if(position != RecyclerView.NO_POSITION){
                    deleteTemplate(position)
                }
            }
        }

        private fun deleteTemplate(position: Int) {
            getToken()?.let { jwtToken ->

                val templateId = dataList[position].templateId
                val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
                val call = service.deleteTemplateAdmin("Bearer $jwtToken", templateId)
                call.enqueue(object : Callback<DeleteTemplateModel> {
                    override fun onResponse(
                        call: Call<DeleteTemplateModel>,
                        response: Response<DeleteTemplateModel>
                    ) {
                        if (response.isSuccessful){
                            Log.d("Response", "response: $response")
                            val templateMsg = response.body()
                            val message = templateMsg!!.message

                            // Remove the item from the dataset
                            dataList.removeAt(position)

                            // Notify adapter about the item being removed
                            notifyItemRemoved(position)

                            Constants.success(context, "$message")

                        } else {
                            Constants.error(context, "Unsuccessful Response: ${response.code()}")
                        }
                    }

                    override fun onFailure(call: Call<DeleteTemplateModel>, t: Throwable) {
                        try {
                            val errorBody = t.message
                            Log.e("Error", "Error response: $errorBody")
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }

                })

            } ?: Constants.error(context, "Token is null. Please log in again.")
        }

        private fun getToken(): String? {
            return context.getSharedPreferences("Token", Context.MODE_PRIVATE)
                .getString("jwtToken", null)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = GetTemplateBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return  dataList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentItem = filteredList[position]
        holder.binding.type.text = currentItem.type
        holder.binding.title.text = currentItem.title
        holder.binding.createdDate.text = currentItem.createdDate
        holder.binding.lastModified.text = currentItem.lastModified
        holder.binding.layout.setOnClickListener {
            val bundle = Bundle()
            bundle.putInt("templateId", dataList.get(position).templateId)
            Navigation.findNavController(it).navigate(R.id.updateTemplateAdmin, bundle)
        }
    }

    fun filter(text: String) {
        val searchText = text.lowercase(Locale.getDefault())
        filteredList = if (searchText.isEmpty()) {
            dataList
        } else {
            dataList.filter { item ->
                item.title!!.lowercase(Locale.getDefault()).contains(searchText)
            }
        }
        notifyDataSetChanged()
    }

}