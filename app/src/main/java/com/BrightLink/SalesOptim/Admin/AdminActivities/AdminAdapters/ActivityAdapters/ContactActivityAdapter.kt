package com.BrightLink.SalesOptim.AdminAdapters.ActivityAdapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.ResponseModel.ActivityStatus.ContactStatusItem
import com.BrightLink.SalesOptim.databinding.GetContactAssignLeadBinding

class ContactActivityAdapter (val context: Context, private var data: List<ContactStatusItem>): RecyclerView.Adapter<ContactActivityAdapter.ViewHolder>(){

    inner class ViewHolder (val binding: GetContactAssignLeadBinding) : RecyclerView.ViewHolder(binding.root){
        fun bind(contactItem: ContactStatusItem){
            binding.titleContact.text = contactItem.title
            binding.dataAndtime.text = contactItem.taskTime
            binding.subjectcontact.text = contactItem.subject
            binding.contactDesc.text = contactItem.modelDescription
            binding.contactedBy.text = contactItem.contactBy

            val contactTypeIcon = if (contactItem.contactType == "call"){
                R.drawable.phone_red
            }else{
                R.drawable.mail_icon_contact
            }
            binding.contactType.setImageResource(contactTypeIcon)
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = GetContactAssignLeadBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return  ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val contactItem = data[position]
        holder.bind(contactItem)
    }

}