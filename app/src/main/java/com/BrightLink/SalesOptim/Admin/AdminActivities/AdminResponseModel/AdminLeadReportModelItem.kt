package com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel

data class AdminLeadReportModelItem(
    val assignedTo: String,
    val city: String,
    val companyName: String,
    val contactId: Int,
    val createdDate: String,
    val customerName: String,
    val description: String,
    val emplyoeeId: Int,
    val leadDescription: String,
    val leadId: Int,
    val leadRevenue: String,
    val leadStatus: String,
    val leadcreatedDate: String,
    val negotiationId: Int,
    val proposalId: Int,
    val qualifyId: Int,
    val status: String,
    val task: List<Any>,
    val taskAssignedBy: String,
    val taskId: Int,
    val taskTitle: String,
    val taskType: String,
    val tasktimeDate: Any
)