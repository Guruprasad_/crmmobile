package com.BrightLink.SalesOptim.Admin.AdminActivities.AdminAdapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.ResponseModel.ThirdPartyIntegrationResModel
import com.BrightLink.SalesOptim.databinding.AdminThirdpartyTableBinding

class IntegrationAdapter(val context : Context, private var dataList: List<ThirdPartyIntegrationResModel>) : RecyclerView.Adapter<IntegrationAdapter.ViewHolder>() {

    private lateinit var mListener: onItemClickListener
    //
    interface onItemClickListener{
        fun onItemClick(position: Int)
    }
    fun setOnItemClickListener(listener: onItemClickListener){
        mListener = listener
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = AdminThirdpartyTableBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding,mListener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentItem = dataList[position]

        holder.binding.platform.text = dataList[position].platform
        holder.binding.appId.text = dataList[position].appId
        holder.binding.id.text  = dataList[position].id.toString()
        holder.binding.adsId.text=dataList[position].adsId

        val id = currentItem.id
        holder.binding.layout.startAnimation(
            AnimationUtils.loadAnimation(
                holder.itemView.context,
                R.anim.recycler_view_anime_2
            )
        )

    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    inner class ViewHolder( val binding:  AdminThirdpartyTableBinding, listener: onItemClickListener) : RecyclerView.ViewHolder(binding.root) {



        init {
            itemView.findViewById<Button>(R.id.update).setOnClickListener {
                listener.onItemClick(adapterPosition)
            }
        }
    }
}
