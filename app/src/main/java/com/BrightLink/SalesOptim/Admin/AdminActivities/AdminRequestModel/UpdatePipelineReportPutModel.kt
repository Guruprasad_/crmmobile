package com.BrightLink.SalesOptim.Admin.AdminActivities.AdminRequestModel

data class UpdatePipelineReportPutModel(
    val employeeId: String,
    val endDate: String,
    val id: String,
    val monthlyLeads: String,
    val monthlyRevenue: String,
    val startDate: String,
    val winLeads: String
)