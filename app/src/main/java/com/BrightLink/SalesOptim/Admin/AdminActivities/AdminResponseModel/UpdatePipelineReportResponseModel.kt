package com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel

data class UpdatePipelineReportResponseModel(
    val actualLeads: Int,
    val actualRevenue: Any,
    val actualWonLeads: Int,
    val empName: String,
    val endDate: String,
    val id: Int,
    val monthlyLeads: Int,
    val monthlyRevenue: Int,
    val startDate: String,
    val userId: Int,
    val winLeads: Int
)