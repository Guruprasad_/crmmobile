package com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel

data class Result(
    val month: Int,
    val totalTask: Int,
    val year: Int
)