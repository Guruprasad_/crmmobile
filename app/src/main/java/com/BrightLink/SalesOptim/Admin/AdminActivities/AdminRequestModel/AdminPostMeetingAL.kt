package com.BrightLink.SalesOptim.Admin.AdminActivities.AdminRequestModel

data class AdminPostMeetingAL(
    val clientEmail: String,
    val locationType: String,
    val meetingFrom: String,
    val meetingLinkOrLocation: String,
    val meetingTo: String,
    val title: String
)