package com.BrightLink.SalesOptim.Admin.AdminActivities.DashBoards

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.BrightLink.SalesOptim.Adapters.TaskDashBoardTopLeadAdapter
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel.AdminAcheiveLeadResponseModelItem
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel.AdminNewDealsResponseModel
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.Constants.CustomDialog
import com.BrightLink.SalesOptim.Constants.WrapContentLinearLayoutManager
import com.BrightLink.SalesOptim.ResponseModel.TaskDashBoardResponseModel
import com.BrightLink.SalesOptim.ResponseModel.TaskDashboardPieChartResponseModelItem
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.FragmentAdminTaskDashboardBinding
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.LegendEntry
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale
import kotlin.math.roundToInt

class AdminTaskDashboard : Fragment() {

    private lateinit var binding : FragmentAdminTaskDashboardBinding
    private lateinit var adapter : TaskDashBoardTopLeadAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View{
        binding = FragmentAdminTaskDashboardBinding.inflate(layoutInflater)
        binding.recyclerView.layoutManager = WrapContentLinearLayoutManager(context)
        binding.actionBar.activityName.text = "Task Dashboard"
        binding.actionBar.back.setOnClickListener{
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }

        val jwtToken = gettoken(context)
        if (jwtToken!=null)
        {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            CallTask(service , jwtToken , context)
            CallTaskPieChart(service , jwtToken , context)
            CallTaskNewDealsChart(service , jwtToken , context)
            CallTaskAcheiveChart(service , jwtToken , context)
        }
        else{
            Constants.error(context,"Token is null Login Again")
        }

        binding.actionBar.option.visibility = View.INVISIBLE
        return binding.root
    }

    private fun CallTaskAcheiveChart(service: ApiInterface, jwtToken: String, context: Context?) {
        val loading = CustomDialog(requireContext())
        loading.show()
        val call = service.getAdminTaskDashboardAcheiveChart("Bearer $jwtToken")
        call.enqueue(object : Callback<List<AdminAcheiveLeadResponseModelItem>> {
            override fun onResponse(
                call: Call<List<AdminAcheiveLeadResponseModelItem>>,
                response: Response<List<AdminAcheiveLeadResponseModelItem>>
            ) {
                if (isAdded) {
                    if (response.isSuccessful) {
                        val data = response.body()
                        if (data != null) {
                            loading.dismiss()
                            AcheiveChart(data)

                        } else {
                            Constants.error(context, "Response body is null or empty")
                            loading.dismiss()
                        }
                    } else {
                        Constants.error(context, "Response is not successful")
                        loading.dismiss()
                    }
                }
            }

            override fun onFailure(call: Call<List<AdminAcheiveLeadResponseModelItem>>, t: Throwable) {
                Constants.error(context, "Error : ${t.message}")
                Log.d("GURU", t.message.toString())
                loading.dismiss()
            }
        })
    }

    private fun AcheiveChart(data: List<AdminAcheiveLeadResponseModelItem>) {
        if (data.isNotEmpty()) {
            val entries = ArrayList<BarEntry>()
            val labels = ArrayList<String>()

            data.forEachIndexed { index, item ->
                val revenue = item.wonLeads.toFloat()
                entries.add(BarEntry(index.toFloat(), revenue))


                // Format the month and year
                val calendar = Calendar.getInstance()
                calendar.set(Calendar.MONTH, item.month - 1) // Adjust month index
                calendar.set(Calendar.YEAR, item.year)
                val formattedDate = SimpleDateFormat("MMM", Locale.getDefault()).format(calendar.time)
                labels.add(formattedDate)
            }

            val dataSet = BarDataSet(entries, "")
            dataSet.color = Color.rgb(1, 124, 217)
            dataSet.valueTextColor = Color.BLACK

            val barData = BarData(dataSet)
            binding.archiveLeads.data = barData

            val xAxis: XAxis = binding.archiveLeads.xAxis
            xAxis.position = XAxis.XAxisPosition.BOTTOM
            xAxis.valueFormatter = object : ValueFormatter() {
                override fun getFormattedValue(value: Float): String {
                    // Ensure value is within the labels array bounds
                    val index = value.toInt()
                    return if (index >= 0 && index < labels.size) {
                        labels[index]
                    } else {
                        ""
                    }
                }
            }
            val yAxisLeft = binding.archiveLeads.axisLeft
            yAxisLeft.setDrawGridLines(false)
            yAxisLeft.valueFormatter = object : ValueFormatter() {
                override fun getFormattedValue(value: Float): String {
                    return "${value.toInt()}" // Format value as currency
                }
            }

            binding.archiveLeads.axisRight.isEnabled = false
            binding.archiveLeads.description.isEnabled = false
            binding.archiveLeads.animateY(1000)
        } else {
            Log.e("SalesDashboardFragment", "Data list is empty")
        }
    }

    private fun CallTaskNewDealsChart(service: ApiInterface, jwtToken: String, context: Context?) {
        val loading = CustomDialog(requireContext())
        loading.show()
        val call = service.getAdminTaskDashboardNewDealsChart("Bearer $jwtToken")
        call.enqueue(object : Callback<AdminNewDealsResponseModel> {
            override fun onResponse(
                call: Call<AdminNewDealsResponseModel>,
                response: Response<AdminNewDealsResponseModel>
            ) {
                if (isAdded) {
                    if (response.isSuccessful) {
                        val data = response.body()
                        if (data != null) {
                            loading.dismiss()
                            callLineChart(data)
                        }
                    }
                }

            }

            override fun onFailure(call: Call<AdminNewDealsResponseModel>, t: Throwable) {
                Constants.error(context, "Error : ${t.message}")
                Log.d("GURU", t.message.toString())
                loading.dismiss()
            }
        })
    }

    private fun callLineChart(data: AdminNewDealsResponseModel) {
        val resultListEntries = ArrayList<Entry>()
        val totalLeadsEntries = ArrayList<Entry>()

        data.resultList.forEach { entry ->
            resultListEntries.add(Entry(entry.month.toFloat(), entry.actualCounts.toFloat()))
        }

        // Populate totalLeadsEntries from totalLeadList in API response
        data.totalLeadList.forEach { entry ->
            totalLeadsEntries.add(Entry(entry.month.toFloat(), entry.targetCount.toFloat()))
        }

        val resultListDataSet = LineDataSet(resultListEntries, "Result List")
        resultListDataSet.color = Color.BLUE
        resultListDataSet.setCircleColor(Color.BLUE)

        val totalLeadsDataSet = LineDataSet(totalLeadsEntries, "Total Leads")
        totalLeadsDataSet.color = Color.RED
        totalLeadsDataSet.setCircleColor(Color.RED)

        val dataSets = ArrayList<ILineDataSet>()
        dataSets.add(resultListDataSet)
        dataSets.add(totalLeadsDataSet)

        val xAxis: XAxis = binding.newdeals.xAxis
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.granularity = 1f // Set granularity to 1 to ensure all months are displayed
        xAxis.isGranularityEnabled = true
        xAxis.valueFormatter = object : ValueFormatter() {
            override fun getFormattedValue(value: Float): String {
                val monthNumber = value.toInt()

                // Ensure monthNumber is within bounds
                return if (monthNumber in 1..12) {
                    // Return the corresponding month name using the month number
                    getMonthName(monthNumber)
                } else {
                    "" // Return empty string for out-of-bounds month numbers
                }
            }
        }


        val yAxisLeft = binding.newdeals.axisLeft
        yAxisLeft.valueFormatter = object : ValueFormatter() {
            override fun getFormattedValue(value: Float): String {
                // Format the y-axis values as integers
                return value.roundToInt().toString()
            }
        }

        val yAxisRight = binding.newdeals.axisRight
        yAxisRight.isEnabled = false

        val lineData = LineData(dataSets)
        binding.newdeals.data = lineData

        val description = Description()
        description.text = "Monthly Sales"
        binding.newdeals.description = description
        binding.newdeals.animateX(1500)

        binding.newdeals.notifyDataSetChanged()
        binding.newdeals.invalidate()
    }
    private fun getMonthName(monthNumber: Int): String {
        return when (monthNumber) {
            1 -> "January"
            2 -> "February"
            3 -> "March"
            4 -> "April"
            5 -> "May"
            6 -> "June"
            7 -> "July"
            8 -> "August"
            9 -> "September"
            10 -> "October"
            11 -> "November"
            12 -> "December"
            else -> ""
        }
    }

    fun createBarData(actualEntries: List<Float>, targetEntries: List<Float>): BarData {
        val actualDataSet = BarDataSet(actualEntries.mapIndexed { index, value ->
            BarEntry(index.toFloat(), value)
        }, "Actual Count")
        actualDataSet.color = Color.rgb(54,66,80)

        val targetDataSet = BarDataSet(targetEntries.mapIndexed { index, value ->
            BarEntry(index.toFloat(), value)
        }, "Target Count")
        targetDataSet.color = Color.rgb(103,183,220)

        val dataSets: MutableList<IBarDataSet> = ArrayList()
        dataSets.add(actualDataSet)
        dataSets.add(targetDataSet)
        return BarData(dataSets)
    }
    private fun CallTaskPieChart(service: ApiInterface, jwtToken: String, context: Context?) {
        val loading = CustomDialog(requireContext())
        loading.show()
        val call = service.getAdminTaskDashboardPieChart("Bearer $jwtToken")
        call.enqueue(object : Callback<List<TaskDashboardPieChartResponseModelItem>> {
            override fun onResponse(
                call: Call<List<TaskDashboardPieChartResponseModelItem>>,
                response: Response<List<TaskDashboardPieChartResponseModelItem>>
            ) {
                if (isAdded)
                {
                    if (response.isSuccessful)
                    {
                        val data  = response.body()
                        if (data != null)
                        {
                            loading.dismiss()
                            LeadPieChart(data)
                        }
                        else{
                            Constants.error(context, "Response body is null or empty")
                            loading.dismiss()
                        }
                    }
                    else
                    {
                        Constants.error(context,"Response is not successful")
                        loading.dismiss()
                    }
                }

            }

            override fun onFailure(call: Call<List<TaskDashboardPieChartResponseModelItem>>, t: Throwable) {
                Constants.error(context,"Error : ${t.message}")
                Log.d("GURU",t.message.toString())
                loading.dismiss()
            }

        })
    }

    private fun LeadPieChart(data: List<TaskDashboardPieChartResponseModelItem>) {

        val colors = listOf(
            Color.rgb(251,146,60),
            Color.rgb(192,132,252),
            Color.rgb(32,99,235),
            Color.rgb(34,197,94)
        )

        val entries = mutableListOf<PieEntry>()
        data.forEach{item->
            entries.add(PieEntry(item.contactedLeadsPercentage.toFloat(), "Contact"))
            entries.add(PieEntry(item.qualifiedLeadPercentage.toFloat(), "Qualified"))
            entries.add(PieEntry(item.proposalLeadPercentage.toFloat(), "Proposal"))
            entries.add(PieEntry(item.negociationLeadCountPercentage.toFloat(), "Negociation"))
        }

        val dataSet = PieDataSet(entries, "Revenue By Lead")
        dataSet.colors = colors

        val legendEntries = mutableListOf<LegendEntry>()
        entries.forEachIndexed { index, entry ->
            legendEntries.add(LegendEntry(entry.label, Legend.LegendForm.DEFAULT, 10f, 2f, null, colors[index]))
        }

        val legend = binding.pieChart.legend
        legend.isWordWrapEnabled = true
        legend.setCustom(legendEntries)

        val data = PieData(dataSet)
        binding.pieChart.data = data
        binding.pieChart.description.isEnabled = false
        binding.pieChart.setUsePercentValues(true)
        binding.pieChart.setDrawEntryLabels(false)
        binding.pieChart.invalidate()

    }

    private fun CallTask(service: ApiInterface, jwtToken: String, context: Context?) {
        val loading = CustomDialog(requireContext())
        loading.show()
        val call = service.getAdminTaskDashboard("Bearer $jwtToken")
        call.enqueue(object : Callback<TaskDashBoardResponseModel> {
            override fun onResponse(
                call: Call<TaskDashBoardResponseModel>,
                response: Response<TaskDashBoardResponseModel>
            ) {
                if (isAdded)
                {
                    if (response.isSuccessful)
                    {
                        val data  = response.body()
                        if (data != null)
                        {
                            loading.dismiss()
                            binding.contact.text = data.callLeadCount.toString()
                            binding.email.text = data.emailLeadCount.toString()
                            binding.qualified.text = data.qualifiedLeadCount.toString()
                            binding.proposal.text = data.proposalLeadCount.toString()
                            binding.negotation.text = data.negociatationLeadCount.toString()
                            adapter = TaskDashBoardTopLeadAdapter(data.topLeads)
                            binding.recyclerView.adapter = adapter
                        }
                        else{
                            Constants.error(context, "Response body is null or empty")
                            loading.dismiss()
                        }
                    }
                    else
                    {
                        Constants.error(context,"Response is not successful")
                        loading.dismiss()
                    }
                }

            }

            override fun onFailure(call: Call<TaskDashBoardResponseModel>, t: Throwable) {
                Constants.error(context,"Error : ${t.message}")
                Log.d("GURU",t.message.toString())
                loading.dismiss()
            }

        })

    }


    private fun gettoken(context: Context?): String? {
        val sharedPreferences = requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
        return sharedPreferences.getString("jwtToken", null)
    }
}