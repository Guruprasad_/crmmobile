package com.BrightLink.SalesOptim.Admin.AdminActivities.ui.Home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.databinding.AdminHomeScreenBinding

class HomeFragment : Fragment() {

    private var _binding: AdminHomeScreenBinding? = null

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = AdminHomeScreenBinding.inflate(inflater, container, false)
        val root: View = binding.root


        binding.employee.setOnClickListener {
            findNavController().navigate(R.id.adminEmployee)
        }

        binding.template.setOnClickListener {
            findNavController().navigate(R.id.adminTemplate)
        }

        binding.empDashboard.setOnClickListener {
            findNavController().navigate(R.id.admin_EmployeeDashboard)
        }

        binding.leadDasboard.setOnClickListener {
            findNavController().navigate(R.id.admin_LeadDashboard)
        }

        binding.taskDashboard.setOnClickListener {
            findNavController().navigate(R.id.admin_TaskDashboard)
        }

        binding.showlead.setOnClickListener {
            findNavController().navigate(R.id.admin_Lead_List)
        }

        binding.task.setOnClickListener{
            findNavController().navigate(R.id.adminTaskList)
        }

        binding.meeting.setOnClickListener{
            findNavController().navigate(R.id.adminMeetingList)
        }
        binding.pipelineReport.setOnClickListener {
            findNavController().navigate(R.id.adminPipelineReport)
        }

        binding.showcontacts.setOnClickListener{
            findNavController().navigate(R.id.admin_showcontact)
        }

        binding.showaccount.setOnClickListener{
            findNavController().navigate(R.id.admin_showaccount)
        }
        binding.thirdPartyIntegration.setOnClickListener {
            findNavController().navigate(R.id.thirdPartyIntegration)
        }

        binding.tracking.setOnClickListener {
            findNavController().navigate(R.id.tracking)
        }


        binding.admarketing.setOnClickListener {
            findNavController().navigate(R.id.adminMarketing)
        }
        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}