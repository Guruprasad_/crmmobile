package com.BrightLink.SalesOptim.Admin.AdminActivities.Employee

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.OpenableColumns
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.MimeTypeMap
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminRequestModel.UpdateEmployeePostModel
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel.EmployeeDetailsByIdModel
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel.UpdateEmployeeResponseModel
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.AdminFragmentUpdateEmployeeBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions
import com.google.android.material.button.MaterialButton
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.MaterialDatePicker
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale

class UpdateEmployeeFragment : Fragment() {

    object FileTypeHelper {

        fun getMediaTypeFromFileExtension(extension: String): MediaType? {
            val mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
            return mimeType?.let { it.toMediaTypeOrNull() }
        }
    }

    private var _binding: AdminFragmentUpdateEmployeeBinding? = null
    private val binding get() = _binding!!

    private var filePath: String? = null
    private var path : String = ""
    private val PICKFILE_RESULT_CODE = 1

    private lateinit var departmentSpinner: Spinner

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = AdminFragmentUpdateEmployeeBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val cId = arguments?.getInt("cId") ?: 0

        binding.actionBar.activityName.text = "Employee Details"
        binding.actionBar.back.setOnClickListener {
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }
        binding.actionBar.option.visibility = View.INVISIBLE

        getToken()?.let { jwtToken ->
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            getDetails(service, jwtToken, cId)
        } ?: Constants.error(requireContext(), "Please try to Login Again")

        departmentSpinner = binding.department

        val departmentAdapter = ArrayAdapter.createFromResource(requireContext(), R.array.Department, R.layout.spinner_item_layout_currency)
        departmentAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        departmentSpinner.adapter = departmentAdapter

        binding.addShift.setOnClickListener {
            val dialogView = layoutInflater.inflate(R.layout.admin_employee_add_shift, null)
            val dialogBuilder = AlertDialog.Builder(requireContext())
                .setView(dialogView)

            val dialog = dialogBuilder.create()
            dialog.show()

            val from = dialogView.findViewById<TextView>(R.id.fromShift)
            val to = dialogView.findViewById<TextView>(R.id.toShift)
            val submit = dialogView.findViewById<MaterialButton>(R.id.submitBtn)

            from.setOnClickListener {
               showTimePickerDialog(from)
            }

            to.setOnClickListener {
                showTimePickerDialog(to)
            }

            submit.setOnClickListener {

                val fromTime = from.text.toString()
                val toTime = to.text.toString()

                if (fromTime.isNotBlank() && toTime.isNotBlank()) {
                    val shiftTime = "$fromTime to $toTime"
                    binding.shiftTime.setText(shiftTime)
                    dialog.dismiss()
                } else {
                    Constants.warning(requireContext(), "Select Time")
                }
            }
        }

        binding.hiredDate.setOnClickListener {
            showMaterialCalendar(binding.hiredDate)
        }

        binding.changeProfileButton.setOnClickListener {
            showFileChooser()
        }

        binding.update.setOnClickListener {
            updateEmployee(cId)
        }

        return root
    }

    private fun getDetails(service: ApiInterface, jwtToken: String, cId: Int) {
        val call = service.getEmployeeDetailsByIdAdmin("Bearer $jwtToken", cId)
        call.enqueue(object : Callback<EmployeeDetailsByIdModel> {
            override fun onResponse(
                call: Call<EmployeeDetailsByIdModel>,
                response: Response<EmployeeDetailsByIdModel>
            ) {
                if(response.isSuccessful){
                    val data = response.body()
                    if (data != null){

                        val bitmap: Bitmap? = decodeBase64ToBitmap(data.images)
                        if (!data.images.isNullOrEmpty()) {
                            Glide.with(requireContext())
                                .asBitmap()
                                .load(bitmap)
                                .transition(BitmapTransitionOptions.withCrossFade())
                                .into(binding.profilePic)
                        }

                        binding.description.setText(data.description)
                        binding.name.setText(data.name)
                        binding.jobTitle.setText(data.work)
                        binding.phoneNo.setText(data.phone)
                        binding.shiftTime.setText(data.shiftTime)
                        binding.nikName.setText(data.secondName)
                        binding.email.setText(data.email)
                        binding.department.setSelection(getIndex(binding.department, data.department))
                        binding.hiredDate.setText(data.hiredate)
                        binding.gender.setText(data.gender)
                    }
                    else {
                        Constants.error(requireContext(), "Employee Details not Found")
                    }
                }
                else {
                    Constants.error(requireContext(), "Something went wrong, Try again")
                }
            }

            override fun onFailure(call: Call<EmployeeDetailsByIdModel>, t: Throwable) {
                Constants.error(requireContext(), "Failed to Fetch Employee Details")
            }

        })
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun updateEmployee(cId: Int) {
        val name = binding.name.text.toString()
        val nikName = binding.nikName.text.toString()
        val work = binding.jobTitle.text.toString()
        val email = binding.email.text.toString()
        val phone = binding.phoneNo.text.toString()
        val department = binding.department.selectedItem.toString()
        val shiftTime = binding.shiftTime.text.toString()
        val hiredDate = binding.hiredDate.text.toString()
        val gender = binding.gender.text.toString()
        val description = binding.description.text.toString()
        val timeZone = "Asia/Kolkata"

        val attach = UpdateEmployeePostModel(gender, hiredDate, name, cId, department, description, email, phone, nikName, shiftTime, timeZone, work)

        Log.d("attach", "attach: $attach")

        if (path.isNullOrEmpty()) {
            // Proceed with the API call without attaching a file
            performUpdateEmployee(attach, null)
        } else {
            val file = File(path)
            val content: ByteArray = file.readBytes()

            val fileExtension = MimeTypeMap.getFileExtensionFromUrl(file.absolutePath)
            val mediaType = FileTypeHelper.getMediaTypeFromFileExtension(fileExtension)

            if (mediaType != null){
                val requestBody = RequestBody.create(mediaType, content)
                val body = MultipartBody.Part.createFormData("file", file.name, requestBody)
                Log.d("file", "body: $body")
                performUpdateEmployee(attach, body)
            } else {
                Constants.error(requireContext(), "Unsupported file type")
            }
        }

    }

    private fun performUpdateEmployee(attach: UpdateEmployeePostModel, fileBody: MultipartBody.Part?) {
        getToken()?.let { jwtToken ->
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.updateEmployeeAdmin("Bearer $jwtToken", fileBody, attach)
            call.enqueue(object : Callback<UpdateEmployeeResponseModel> {
                override fun onResponse(
                    call: Call<UpdateEmployeeResponseModel>,
                    response: Response<UpdateEmployeeResponseModel>
                ) {
                    if (isAdded)
                    {
                        if (response.isSuccessful) {
                            val responseBody = response.body()
                            Log.d("ResponseBody", "$responseBody")
                            Constants.success(requireContext(),"Employee Updated Successfully")
                            requireActivity().supportFragmentManager.popBackStack()
                        }else{
                            Log.d("ResponseBody", "${response.body()}")
                            Constants.error(requireContext(),"Something went wrong, Try again")
                        }
                    }
                }
                override fun onFailure(call: Call<UpdateEmployeeResponseModel>, t: Throwable) {
                    Constants.error(requireContext(), "Failed to Update Employee. Try again")

                }
            })
        } ?: Constants.error(requireContext(), "Please try to Login Again")
    }

    fun showTimePickerDialog(textView: TextView) {
        val calendar = Calendar.getInstance()
        val timePickerDialog = TimePickerDialog(
            requireContext(),
            { _, hourOfDay, minute ->
                // Combine date and time
                calendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
                calendar.set(Calendar.MINUTE, minute)

                // Format the final date-time with AM/PM indicator
                val formattedDateTime = SimpleDateFormat("hh:mm a", Locale.getDefault()).format(calendar.time)
                textView.text = formattedDateTime.uppercase(Locale.getDefault())
            },
            calendar.get(Calendar.HOUR_OF_DAY),
            calendar.get(Calendar.MINUTE),
            false // Set to false for 12-hour format with AM/PM
        )

        timePickerDialog.show()
    }

    private fun showMaterialCalendar(textView: TextView) {
        val today = Calendar.getInstance().timeInMillis
        val datePickerBuilder = MaterialDatePicker.Builder.datePicker()
        datePickerBuilder.setSelection(today)

        val constraintsBuilder = CalendarConstraints.Builder()
        //constraintsBuilder.setValidator(DateValidatorPointForward.now())

        datePickerBuilder.setCalendarConstraints(constraintsBuilder.build())

        val datePicker = datePickerBuilder.build()

        datePicker.addOnPositiveButtonClickListener { selectedDate ->
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = selectedDate

            // Launch TimePickerDialog
            val timePickerDialog = TimePickerDialog(
                requireContext(),
                { _, hourOfDay, minute ->
                    // Combine date and time
                    calendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
                    calendar.set(Calendar.MINUTE, minute)

                    // Format the final date-time with AM/PM indicator
                    val formattedDateTime = SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.getDefault()).format(calendar.time)
                    textView.text = formattedDateTime.uppercase(Locale.getDefault())
                },
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                false // Set to false for 12-hour format with AM/PM
            )

            timePickerDialog.show()
        }
        datePicker.show(parentFragmentManager, datePicker.toString())
    }

    private fun decodeBase64ToBitmap(base64String: String?): Bitmap? {
        if (base64String != null) {
            val decodedBytes: ByteArray = Base64.decode(base64String, Base64.DEFAULT)
            return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.size)
        } else {
            // Handle the case where base64String is null
            return null
        }
    }

    private fun getIndex(spinner: Spinner, myString: String): Int {
        for (i in 0 until spinner.count) {
            if (spinner.getItemAtPosition(i).toString() == myString) {
                return i
            }
        }
        return 0
    }

    private fun showFileChooser() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
            intent.type = "*/*"
            startActivityForResult(intent, PICKFILE_RESULT_CODE)
        } else {
            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.type = "*/*"
            intent.addCategory(Intent.CATEGORY_OPENABLE)
            startActivityForResult(
                Intent.createChooser(intent, "Select File"), PICKFILE_RESULT_CODE
            )
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == PICKFILE_RESULT_CODE && resultCode == Activity.RESULT_OK && data != null && data.data != null) {
            val uri: Uri = data.data!!
            path = getPathFromUri(requireContext(), uri)!!

            binding.profilePic.setImageURI(uri)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                updateTextViewWithFileName(uri)
            } else {
                // Handle for lower versions
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun updateTextViewWithFileName(uri: Uri) {
        val fileName = getFileNameFromUri(uri)
        filePath = uri.toString()
        if (fileName != null) {
            //binding.orderFilePath.text = Html.fromHtml("<b><font color='#7FFF00'>$fileName</font></b>")
        }
    }

    private fun getFileNameFromUri(uri: Uri): String? {
        var fileName: String? = null
        val cursor =requireContext().contentResolver.query(uri, null, null, null, null)
        if (cursor!= null){
            if (cursor.moveToFirst()) {
                val displayNameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
                if (displayNameIndex != -1) {
                    fileName = cursor.getString(displayNameIndex)
                }
            }
            cursor.close()
        }
        return fileName
    }

    @SuppressLint("Range")
    private fun getPathFromUri(context: Context, uri: Uri): String? {
        var path: String? = null
        if ("content".equals(uri.scheme, ignoreCase = true)) {
            // Using ACTION_OPEN_DOCUMENT
            val cursor = context.contentResolver.query(uri, null, null, null, null)
            cursor?.use {
                if (it.moveToFirst()) {
                    val displayName = it.getString(it.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                    val type = it.getString(it.getColumnIndex("mime_type"))
                    val file = File(context.cacheDir, displayName)
                    try {
                        val inputStream = context.contentResolver.openInputStream(uri)
                        inputStream?.use { input ->
                            FileOutputStream(file).use { output ->
                                input.copyTo(output)
                            }
                        }
                        path = file.absolutePath
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
            }
        } else if ("file".equals(uri.scheme, ignoreCase = true)) {
            path = uri.path
        }
        return path
    }

    private fun getToken(): String? {
        return requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
            .getString("jwtToken", null)
    }

}