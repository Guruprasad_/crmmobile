package com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel

data class AdminLeadLineChartResponseModel(
    val resultList: List<Result>,
    val totalLeadLists: List<TotalLeadLists>
)