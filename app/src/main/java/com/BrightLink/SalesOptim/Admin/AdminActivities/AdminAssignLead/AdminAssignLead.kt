package com.BrightLink.SalesOptim.Admin.AdminActivities.AdminAssignLead

import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel.AssignLeadToResponse
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.ResponseModel.ShowEmployeelistModelItem
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.AdminAssignLeadFragmentBinding
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AdminAssignLead : Fragment() {
    private var _binding: AdminAssignLeadFragmentBinding? = null
    private val binding get() = _binding!!
    private var leadId: Int? = null
    private var leadStatus: String? = null
    private lateinit var assignLeadSpinner: Spinner
    private lateinit var leadNamesAdapter: ArrayAdapter<String>
    private var leadNames : ArrayList<String> = ArrayList()
   private var selectedEmployeeEmail : String = ""
    private var selectedEmployeeName : String = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = AdminAssignLeadFragmentBinding.inflate(inflater,container,false)
        val  root: View = binding.root

        val sharedPreferences = requireContext().getSharedPreferences("AdminLead", Context.MODE_PRIVATE)
        leadId = sharedPreferences.getInt("leadId", -1)
        leadStatus = sharedPreferences.getString("leadStatus", "")

        binding.actionBar.option.visibility = View.INVISIBLE
        binding.actionBar.activityName.text = "Assign Lead"
        binding.actionBar.back.setOnClickListener {
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }

        if (leadStatus!=null)
        {
            when(leadStatus)
            {
                "New Lead"->{

                    binding.qualifiedIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.proposalIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.negotiationIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)

                }

                "Contacted"->{
                    binding.qualifiedIcon.setColorFilter(Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.proposalIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.negotiationIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Email Sent"->{
                    binding.qualifiedIcon.setColorFilter(Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.proposalIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.negotiationIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Not Contacted"->{
                    binding.qualifiedIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.proposalIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.negotiationIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Qualified"->{
                    binding.qualifiedIcon.setColorFilter(Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.proposalIcon.setColorFilter(Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.negotiationIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Not Qualified"->{
                    binding.qualifiedIcon.setColorFilter(Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.proposalIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.negotiationIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Proposal Sent"->{
                    binding.qualifiedIcon.setColorFilter(Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.proposalIcon.setColorFilter(Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.qualifiedIcon.setColorFilter(Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Lead Won"->{
                    binding.contactIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.qualifiedIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.proposalIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.negotiationIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Lead Lost"->{
                    binding.contactIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.qualifiedIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.proposalIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.negotiationIcon.setColorFilter(Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }
            }
        }

        binding.meeting.setOnClickListener {
            findNavController().navigate(R.id.adminLeadMeeting)
        }

        binding.activity.setOnClickListener {
            findNavController().navigate(R.id.adminLeadActivity)
        }

        binding.attachment.setOnClickListener {
            findNavController().navigate(R.id.adminLeadAttachments)
        }

        binding.notes.setOnClickListener {
            findNavController().navigate(R.id.adminLeadNotes)
        }
        binding.links.setOnClickListener {
            findNavController().navigate(R.id.adminLeadLinks)
        }
        binding.info.setOnClickListener {
            findNavController().navigate(R.id.adminLeadInfo)
        }

        assignLeadSpinner = binding.assignLeadSpinner
        leadNamesAdapter = ArrayAdapter(requireContext(),R.layout.spinner_assign_lead_admin,leadNames).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
        assignLeadSpinner.adapter = leadNamesAdapter

        getEmployeesToAssign()

        binding.assignToBtn.setOnClickListener {
            if (selectedEmployeeEmail != null && selectedEmployeeName != "Select Employee"){
                assignLeadToEmployee(selectedEmployeeEmail)
            }else{
                Constants.error(requireContext(),"Select a valid Employee")
            }
        }
        binding.assignReset.setOnClickListener {
            assignLeadSpinner.setSelection(0)
            selectedEmployeeEmail = ""
            selectedEmployeeName = ""
        }


        return root
    }

    private fun assignLeadToEmployee(email: String){
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null) {
                val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val emailRequestBody: RequestBody = RequestBody.create(
                "text/plain".toMediaTypeOrNull(),
                email
            )
                val call = service.leadAssignTo("Bearer $jwtToken", leadId, emailRequestBody)
                call.enqueue(object : Callback<AssignLeadToResponse>{
                    override fun onResponse(
                        call: Call<AssignLeadToResponse>,
                        response: Response<AssignLeadToResponse>
                    ) {
                        if (isAdded) {
                            if (response.isSuccessful) {
                                val resbdy = response.body()
                                Constants.success(requireContext(),"Lead is successfully transferred to Employee $selectedEmployeeName")

                            }
                        }
                    }

                    override fun onFailure(call: Call<AssignLeadToResponse>, t: Throwable) {
                        Constants.error(requireContext(), "Failed")
                        Log.d("Assign Lead To","${t.message}")
                    }

                })
            }else{
                Constants.error(requireContext(), "Employee not found")
            }
    }

    private fun getEmployeesToAssign(){
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null){
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val callEmployeeList = service.adminEmployeeList("Bearer $jwtToken")
            callEmployeeList.enqueue(object : Callback<ArrayList<ShowEmployeelistModelItem>> {
                override fun onResponse(
                    call: Call<ArrayList<ShowEmployeelistModelItem>>,
                    response: Response<ArrayList<ShowEmployeelistModelItem>>
                ) {
                    if (isAdded) {
                        if (response.isSuccessful) {
                            val employeelist = response.body()
                            if (employeelist != null && employeelist.isNotEmpty()) {
                                val employeeNames = employeelist.map { it.name }
                                leadNames.clear()
                                leadNames.add("Select Employee")
                                leadNames.addAll(employeeNames)
                                leadNamesAdapter.notifyDataSetChanged()

                                assignLeadSpinner.onItemSelectedListener =
                                    object : AdapterView.OnItemSelectedListener {
                                        override fun onItemSelected(
                                            parent: AdapterView<*>?,
                                            view: View?,
                                            position: Int,
                                            id: Long
                                        ) {
                                            selectedEmployeeName =
                                                parent?.getItemAtPosition(position).toString()
                                            Log.d("selected Emplu","$selectedEmployeeName"
                                            )
                                            val selectedEmployee =
                                                employeelist.find { it.name == selectedEmployeeName }
                                            selectedEmployeeEmail =
                                                selectedEmployee?.email.toString()
                                        }

                                        override fun onNothingSelected(parent: AdapterView<*>?) {
                                       //     TODO("Not yet implemented")
                                        }

                                    }
                            } else {
                                Constants.error(requireContext(), "Employee List is null or empty")
                            }
                        } else {
                            Constants.error(
                                requireContext(),
                                "Unsuccessful response: ${response.code()}"
                            )
                        }
                    }
                }

                override fun onFailure(
                    call: Call<ArrayList<ShowEmployeelistModelItem>>,
                    t: Throwable
                ) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }

            })
        }else{
            Constants.error(requireContext(),"Login Expired, Login Again")
        }
    }

    private fun gettoken(context: Context?): String? {
        val sharedPreferences = requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
        return sharedPreferences.getString("jwtToken", null)
    }
}