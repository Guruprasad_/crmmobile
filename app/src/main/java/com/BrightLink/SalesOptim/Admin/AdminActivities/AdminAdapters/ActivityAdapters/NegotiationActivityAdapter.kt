package com.BrightLink.SalesOptim.AdminAdapters.ActivityAdapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.ResponseModel.ActivityStatus.NegotiationStatusItem
import com.BrightLink.SalesOptim.databinding.GetNegotiationAssignLeadBinding

class NegotiationActivityAdapter (val context: Context, private var data: List<NegotiationStatusItem>): RecyclerView.Adapter<NegotiationActivityAdapter.ViewHolder>() {

    inner class ViewHolder (val binding: GetNegotiationAssignLeadBinding) : RecyclerView.ViewHolder(binding.root){
        fun bind (negotiationItem: NegotiationStatusItem){
            binding.expectedRevenue.text = negotiationItem.actualRevenue.toString()
            binding.StatusNego.text = negotiationItem.proposalStatus
            val combinedRemark = "${negotiationItem.reason}${negotiationItem.description}"
            binding.RemarkNego.text = combinedRemark
            binding.NegoRevenue.text = negotiationItem.negociableRevenue.toString()


            when(negotiationItem.proposalStatus){
                "Accept" -> binding.StatusNego.setTextColor(context.resources.getColor(R.color.blue))
                "Reject" -> binding.StatusNego.setTextColor(context.resources.getColor(R.color.Red))
            }
        }
    }
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ):ViewHolder {
        val binding = GetNegotiationAssignLeadBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return ViewHolder(binding)
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val negotiationItem = data[position]
        holder.bind(negotiationItem)
    }
    override fun getItemCount(): Int {
        return data.size
    }
}