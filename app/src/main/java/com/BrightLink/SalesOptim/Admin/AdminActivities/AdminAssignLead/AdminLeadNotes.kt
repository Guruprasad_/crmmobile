package com.BrightLink.SalesOptim.Admin.AdminActivities.AdminAssignLead

import android.content.Context
import android.graphics.PorterDuff
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.BrightLink.SalesOptim.AdminAdapters.NotesAdminAdapter
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.Constants.WrapContentLinearLayoutManager
import com.BrightLink.SalesOptim.ResponseModel.LeadNotesModelItem
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.AdminLeadNotesFragmentBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AdminLeadNotes : Fragment() {
    private var _binding: AdminLeadNotesFragmentBinding? = null
    private val binding get() = _binding!!
    private var leadStatus : String? = null
    private var leadId : Int? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = AdminLeadNotesFragmentBinding.inflate(inflater, container, false)
        val root: View = binding.root
        binding.actionBar.activityName.text = "Assign Lead"
        binding.actionBar.back.setOnClickListener{
            requireActivity().onBackPressed()
        }

        val sharedPreferences = requireContext().getSharedPreferences("AdminLead", Context.MODE_PRIVATE)
        leadId = sharedPreferences.getInt("leadId", -1)
        leadStatus = sharedPreferences.getString("leadStatus",leadStatus)
        if (leadStatus!= null){

            when(leadStatus)
            {
                "New Lead"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)

                }

                "Contacted"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Email Sent"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Not Contacted"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Qualified"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Not Qualified"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Proposal Sent"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Lead Won"->{
                    binding.actionBar.contactIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Lead Lost"->{
                    binding.actionBar.contactIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }
            }
        }

        binding.recyclerView.layoutManager = WrapContentLinearLayoutManager(requireContext())

        refreshNotesList(leadId!!)




        return root
    }
    private fun refreshNotesList(leadId: Int) {
        getToken()?.let { jwtToken ->
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            callApi(service, jwtToken, leadId)
        } ?: Constants.error(requireContext(), "Token is null. Please log in again.")

    }

    private fun callApi(service: ApiInterface, jwtToken: String, leadId: Int) {
        val call = service.adminNotesList("Bearer $jwtToken", leadId)
        call.enqueue(object : Callback<List<LeadNotesModelItem>> {
            override fun onResponse(
                call: Call<List<LeadNotesModelItem>>,
                response: Response<List<LeadNotesModelItem>>
            ) {
                if (isAdded) {
                    if (response.isSuccessful) {
                        response.body()?.let { data ->
                            if (data.isEmpty()){
                                binding.noNotesFound.visibility = View.VISIBLE
                            }
                            val adapter = NotesAdminAdapter(requireContext(), data)
                            binding.recyclerView.adapter = adapter
                        } ?: Constants.error(requireContext(), "Response body is null")
                    }
                }
            }
            override fun onFailure(call: Call<List<LeadNotesModelItem>>, t: Throwable) {
                Constants.error(requireContext(), "Error: ${t.message}")
            }

        })
    }


    private fun getToken(): String? {
        return requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
            .getString("jwtToken", null)
    }
}