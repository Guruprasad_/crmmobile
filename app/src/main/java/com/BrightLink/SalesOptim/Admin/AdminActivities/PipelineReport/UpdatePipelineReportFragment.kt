package com.BrightLink.SalesOptim.Admin.AdminActivities.PipelineReport

import android.app.Dialog
import android.app.TimePickerDialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminRequestModel.UpdatePipelineReportPutModel
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel.GetPipelineDetailsByIdModel
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel.UpdatePipelineReportResponseModel
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.ResponseModel.Admin.ShowEmployeeModelItem
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.AdminFragmentUpdatePipelineReportBinding
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.DateValidatorPointForward
import com.google.android.material.datepicker.MaterialDatePicker
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale

class UpdatePipelineReportFragment : DialogFragment() {

    interface PipelineRefreshListener {
        fun onPipelineRefresh()
    }


    private var _binding: AdminFragmentUpdatePipelineReportBinding? = null
    private val binding get() = _binding!!

    private var employeeName: String? = null
    private var employeeId: Int? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        _binding = AdminFragmentUpdatePipelineReportBinding.inflate(layoutInflater)
        val builder = AlertDialog.Builder(requireActivity())
        builder.setView(binding.root)

        binding.setTargetClose.setOnClickListener {
            dialog!!.dismiss()
        }

        val pid = arguments?.getInt("pid") ?: 0
        Log.d("pid", "pid: $pid")

        getPipelineDetailsById(pid)

        binding.startDatePipeline.setOnClickListener {
            showMaterialCalendar(binding.startDatePipeline)
        }

        binding.endDatePipeline.setOnClickListener {
            showMaterialCalendar(binding.endDatePipeline)
        }

        getToken()?.let { jwtToken ->
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            callEmployee(service, jwtToken)
        } ?: Constants.error(requireContext(), "Please try to Login Again")

        binding.saveTargetBtn.setOnClickListener {
            updateTarget(pid.toString())
        }

        return builder.create()
    }

    private fun callEmployee(service: ApiInterface, jwtToken: String) {
        val call = service.showsEmployeeAdmin("Bearer $jwtToken")
        call.enqueue(object : Callback<List<ShowEmployeeModelItem>> {
            override fun onResponse(
                call: Call<List<ShowEmployeeModelItem>>,
                response: Response<List<ShowEmployeeModelItem>>
            ) {
                if (response.isSuccessful){
                    response.body()?.let { employees ->
                        val matchingEmployee = employees.find { it.name == employeeName }
                        matchingEmployee?.let { employee ->
                            employeeId = employee.cId
                            Log.d("empId", "empId : $employeeId")
                        } ?: Constants.error(requireContext(), "Employee Id not found")
                    } ?: Constants.error(requireContext(), "Employee list is empty")
                }
                else {
                    Constants.error(requireContext(), "Something went wrong, Try again")
                }
            }

            override fun onFailure(call: Call<List<ShowEmployeeModelItem>>, t: Throwable) {
                Constants.error(requireContext(), "Failed to Fetch Employee List")
            }

        })
    }

    private fun getPipelineDetailsById(pid: Int) {
        getToken()?.let { jwtToken ->

            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.getPipelineDetailsByIdAdmin("Bearer $jwtToken", pid)
            call.enqueue(object : Callback<GetPipelineDetailsByIdModel> {
                override fun onResponse(
                    call: Call<GetPipelineDetailsByIdModel>,
                    response: Response<GetPipelineDetailsByIdModel>
                ) {
                    if (response.isSuccessful){
                        response.body()?.let { data ->

                            employeeName = data.empName

                            binding.employeeName.text = employeeName
                            binding.startDatePipeline.text = formatDate(data.startDate)
                            binding.endDatePipeline.text = formatDate(data.endDate)
                            binding.updateLeadTarget.setText(data.monthlyLeads.toString())
                            binding.revenueTarget.setText(data.monthlyRevenue.toString())
                            binding.winTarget.setText(data.winLeads.toString())

                        } ?: Constants.error(requireContext(), "Pipeline Report Details not Found")
                    }
                    else {
                        Constants.error(requireContext(), "Something went wrong, Try again")
                    }
                }

                override fun onFailure(call: Call<GetPipelineDetailsByIdModel>, t: Throwable) {
                    Constants.error(requireContext(), "Failed to Fetch Pipeline Report Details")
                }

            })

        } ?: Constants.error(requireContext(), "Please try to Login Again")
    }

    private fun formatDate(dateString: String): String {
        val inputFormat = SimpleDateFormat("dd-MM-yyyy HH:mm a", Locale.getDefault())
        val outputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm", Locale.getDefault())
        return try {
            val date = inputFormat.parse(dateString)
            outputFormat.format(date!!)
        } catch (e: ParseException) {
            Log.e("Parsing Error", "Error parsing date", e)
            ""
        }
    }

    private fun updateTarget(pId: String) {
        Log.d("pId", "pId : $pId")
        val empId = employeeId.toString()
        Log.d("empId", "empId : $empId")
        val startDate = binding.startDatePipeline.text.toString()
        val endDate = binding.endDatePipeline.text.toString()
        val monthlyLeads = binding.updateLeadTarget.text.toString()
        val monthlyRevenue = binding.revenueTarget.text.toString()
        val winLeads = binding.winTarget.text.toString()

        if (!isDateValid(startDate, endDate)){
            Constants.warning(requireContext(), "End Date must be $startDate or later")
            return
        }

        val reqBody = UpdatePipelineReportPutModel(empId, endDate, pId, monthlyLeads, monthlyRevenue, startDate, winLeads)
        Log.d("reqBody", "reqBody: $reqBody")

        getToken()?.let { jwtToken ->

            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.updateTargetAdmin("Bearer $jwtToken", reqBody)
            call.enqueue(object : Callback<UpdatePipelineReportResponseModel> {
                override fun onResponse(
                    call: Call<UpdatePipelineReportResponseModel>,
                    response: Response<UpdatePipelineReportResponseModel>
                ) {
                    if (response.isSuccessful){
                        val responseData = response.body()
                        if (responseData != null) {
                            Log.d("data", "data: $responseData")
                            val updatedEmployeeName = responseData.empName
                            if (updatedEmployeeName != null) {
                                Constants.success(requireContext(), "Target updated Successfully")
                                dialog!!.dismiss()
                                Log.d("data", "Employee Name: $updatedEmployeeName")
                            } else {
                                Constants.error(requireContext(), "Employee Name is null")
                            }
                        } else {
                            Constants.error(requireContext(), "Something went wrong, Try again")
                        }
                    }
                    else {
                        Constants.error(requireContext(), "Something went wrong, Try again")
                    }
                }

                override fun onFailure(call: Call<UpdatePipelineReportResponseModel>, t: Throwable) {
                    Constants.error(requireContext(), "Failed to Update Pipeline Target. Try again")
                }

            })

        } ?: Constants.error(requireContext(), "Please try to Login Again")
    }

    private fun getToken(): String? {
        return requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
            .getString("jwtToken", null)
    }

    private fun showMaterialCalendar(textView: TextView) {
        val today = Calendar.getInstance().timeInMillis
        val datePickerBuilder = MaterialDatePicker.Builder.datePicker()
        datePickerBuilder.setSelection(today)

        val constraintsBuilder = CalendarConstraints.Builder()
        constraintsBuilder.setValidator(DateValidatorPointForward.now())

        datePickerBuilder.setCalendarConstraints(constraintsBuilder.build())

        val datePicker = datePickerBuilder.build()

        datePicker.addOnPositiveButtonClickListener {selectedDate->
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = selectedDate

            // Launch TimePickerDialog
            val timePickerDialog = TimePickerDialog(
                requireContext(),
                { _, hourOfDay, minute ->
                    // Combine date and time
                    calendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
                    calendar.set(Calendar.MINUTE, minute)

                    // Format the final date-time
                    val formattedDateTime = SimpleDateFormat("yyyy-MM-dd'T'HH:mm", Locale.getDefault()).format(calendar.time)
                    textView.text = formattedDateTime
                },
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                true
            )

            timePickerDialog.show()
        }
        datePicker.show(parentFragmentManager, datePicker.toString())

    }

    private fun isDateValid(startDate: String, endDate: String): Boolean {
        val calendarFrom = Calendar.getInstance().apply {
            timeInMillis = parseDateString(startDate)
        }
        val calendarTo = Calendar.getInstance().apply {
            timeInMillis = parseDateString(endDate)
        }
        return calendarTo.after(calendarFrom)
    }

    private fun parseDateString(dateString: String): Long {
        val pattern = "yyyy-MM-dd'T'HH:mm"
        val dateFormat = SimpleDateFormat(pattern, Locale.getDefault())
        return try {
            dateFormat.parse(dateString)?.time ?: 0
        } catch (e: ParseException) {
            Log.e("Parsing Error", "Error parsing date", e)
            0
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}