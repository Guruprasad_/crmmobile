package com.BrightLink.SalesOptim.Admin.AdminActivities.AdminAssignLead

import android.content.Context
import android.graphics.PorterDuff
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.BrightLink.SalesOptim.AdminAdapters.ActivityAdapters.ContactActivityAdapter
import com.BrightLink.SalesOptim.AdminAdapters.ActivityAdapters.NegotiationActivityAdapter
import com.BrightLink.SalesOptim.AdminAdapters.ActivityAdapters.ProposalActivityAdapter
import com.BrightLink.SalesOptim.AdminAdapters.ActivityAdapters.QualifyActivityAdapter
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.Constants.WrapContentLinearLayoutManager
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.ResponseModel.ActivityStatus.ContactStatus
import com.BrightLink.SalesOptim.ResponseModel.ActivityStatus.NegotiationStatus
import com.BrightLink.SalesOptim.ResponseModel.ActivityStatus.ProposalStatus
import com.BrightLink.SalesOptim.ResponseModel.ActivityStatus.QualifyStatus
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.AdminLeadActivityFragmentBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AdminLeadActivity : Fragment() {
    private var _binding: AdminLeadActivityFragmentBinding? = null
    private val binding get() = _binding!!
    private var leadId : Int =  0
    private var leadStatus : String? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = AdminLeadActivityFragmentBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val sharedPreferences = requireContext().getSharedPreferences("AdminLead", Context.MODE_PRIVATE)
        leadId = sharedPreferences.getInt("leadId", -1)
        leadStatus = sharedPreferences.getString("leadStatus",leadStatus)
        if (leadStatus!= null){

            when(leadStatus)
            {
                "New Lead"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)

                }

                "Contacted"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Email Sent"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Not Contacted"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Qualified"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Not Qualified"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Proposal Sent"->{
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(255, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Lead Won"->{
                    binding.actionBar.contactIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }

                "Lead Lost"->{
                    binding.actionBar.contactIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.qualifyIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.proposalIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                    binding.actionBar.negotiationIcon.setColorFilter(android.graphics.Color.argb(50, 255, 255, 255), PorterDuff.Mode.MULTIPLY)
                }
            }
        }

        binding.contactBtn.setBackgroundColor(resources.getColor(R.color.white))

        binding.actionBar.activityName.text = "Assign Lead"
        binding.actionBar.back.setOnClickListener{
            requireActivity().onBackPressed()
        }

        binding.contactBtn.setOnClickListener {
            binding.contactBtn.setBackgroundColor(resources.getColor(R.color.white))
            binding.proposalBtn.setBackgroundColor(resources.getColor(R.color.LightGrey))
            binding.negotiationBtn.setBackgroundColor(resources.getColor(R.color.LightGrey))
            binding.qualifyBtn.setBackgroundColor(resources.getColor(R.color.LightGrey))

            val baseContainer = view?.findViewById<LinearLayout>(R.id.activityContainer)
            baseContainer?.removeAllViews()
            val inflater = LayoutInflater.from(requireContext())
            val layout = inflater.inflate(R.layout.activity_contact_lead, baseContainer, false)
            baseContainer?.addView(layout)
            layout?.let {
                val recyclerViewCnt = it.findViewById<RecyclerView>(R.id.ContactInfoRec)
                recyclerViewCnt?.layoutManager = WrapContentLinearLayoutManager(requireContext())

                val jwttoken = getToken()
                if (jwttoken != null) {
                    val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
                    getAllContactStatus(service, jwttoken, recyclerViewCnt)
                } else {
                    Constants.error(requireContext(), "Token is null Login Again")
                }
            }
        }

        binding.qualifyBtn.setOnClickListener {
            binding.qualifyBtn.setBackgroundColor(resources.getColor(R.color.white))
            binding.proposalBtn.setBackgroundColor(resources.getColor(R.color.LightGrey))
            binding.contactBtn.setBackgroundColor(resources.getColor(R.color.LightGrey))
            binding.negotiationBtn.setBackgroundColor(resources.getColor(R.color.LightGrey))


            val baseContainer = view?.findViewById<LinearLayout>(R.id.activityContainer)
            baseContainer?.removeAllViews()
            val inflater = LayoutInflater.from(requireContext())
            val layout = inflater.inflate(R.layout.qualify_assign_lead, baseContainer, false)
            baseContainer?.addView(layout)
            layout?.let {
                val recyclerViewQualify = it.findViewById<RecyclerView>(R.id.qualifyRecycle)
                recyclerViewQualify?.layoutManager = WrapContentLinearLayoutManager(requireContext())

                val jwttoken = getToken()
                if (jwttoken != null) {
                    val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
                    getQualifyStatus(service, jwttoken, recyclerViewQualify)
                } else {
                    Constants.error(requireContext(), "Token is null Login Again")
                }
            }
        }

        binding.proposalBtn.setOnClickListener {
            binding.proposalBtn.setBackgroundColor(resources.getColor(R.color.white))
            binding.negotiationBtn.setBackgroundColor(resources.getColor(R.color.LightGrey))
            binding.contactBtn.setBackgroundColor(resources.getColor(R.color.LightGrey))
            binding.qualifyBtn.setBackgroundColor(resources.getColor(R.color.LightGrey))


            val baseContainer = view?.findViewById<LinearLayout>(R.id.activityContainer)
            baseContainer?.removeAllViews()
            val inflater = LayoutInflater.from(requireContext())
            val layout = inflater.inflate(R.layout.proposal_assign_lead, baseContainer, false)
            baseContainer?.addView(layout)
            layout?.let {
                val recyclerViewProposal= it.findViewById<RecyclerView>(R.id.proposalRecycler)
                recyclerViewProposal?.layoutManager = WrapContentLinearLayoutManager(requireContext())

                val jwttoken = getToken()
                if (jwttoken != null) {
                    val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
                    getProposalStatus(service, jwttoken, recyclerViewProposal)
                } else {
                    Constants.error(requireContext(), "Token is null Login Again")
                }
            }

        }

        binding.negotiationBtn.setOnClickListener {
            binding.negotiationBtn.setBackgroundColor(resources.getColor(R.color.white))
            binding.proposalBtn.setBackgroundColor(resources.getColor(R.color.LightGrey))
            binding.contactBtn.setBackgroundColor(resources.getColor(R.color.LightGrey))
            binding.qualifyBtn.setBackgroundColor(resources.getColor(R.color.LightGrey))

            val baseContainer = view?.findViewById<LinearLayout>(R.id.activityContainer)
            baseContainer?.removeAllViews()
            val inflater = LayoutInflater.from(requireContext())
            val layout = inflater.inflate(R.layout.negotiation_assign_lead, baseContainer, false)
            baseContainer?.addView(layout)
            layout?.let {
                val recyclerViewNegotiation= it.findViewById<RecyclerView>(R.id.negoRecycler)
                recyclerViewNegotiation?.layoutManager = WrapContentLinearLayoutManager(requireContext())

                val jwttoken = getToken()
                if (jwttoken != null) {
                    val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
                    getNegotiationStatus(service, jwttoken, recyclerViewNegotiation)
                } else {
                    Constants.error(requireContext(), "Token is null Login Again")
                }
            }
        }



        return  root
    }
    private fun getNegotiationStatus(
        service: ApiInterface,
        jwtToken: String,
        recyclerViewNegotiation: RecyclerView
    ) {
        val call = service.adminNegotiationStatus("Bearer $jwtToken", leadId)
        call.enqueue(object : Callback<NegotiationStatus> {
            override fun onResponse(
                call: Call<NegotiationStatus>,
                response: Response<NegotiationStatus>
            ) {
                if (isAdded) {
                    if (response.isSuccessful) {
                        response.body()?.let { data ->
                            val adapter = NegotiationActivityAdapter(requireContext(), data)
                            recyclerViewNegotiation.adapter = adapter
                        } ?: Constants.error(requireContext(), "Response body is null")
                    } else {
                        if (response.body().isNullOrEmpty()){
                            val layout = view?.findViewById<LinearLayout>(R.id.noNegotiationStatus)
                            layout!!.visibility = View.VISIBLE
                        }else {
                            Constants.error(requireContext(), "Unsuccessful: ${response.code()}")
                        }
                    }
                }
            }
            override fun onFailure(call: Call<NegotiationStatus>, t: Throwable) {
                Constants.error(requireContext(), "Error: ${t.message}")
                Log.d("omar","${t.message}")
            }

        })
    }

    private fun getProposalStatus(
        service: ApiInterface,
        jwtToken: String,
        recyclerViewProposal: RecyclerView
    ) {
        val call = service.adminProposalStatus("Bearer $jwtToken", leadId)
        call.enqueue(object : Callback<ProposalStatus> {
            override fun onResponse(
                call: Call<ProposalStatus>,
                response: Response<ProposalStatus>
            ) {
                if (isAdded) {
                    if (response.isSuccessful) {
                        response.body()?.let { data ->
                            val adapter = ProposalActivityAdapter(requireContext(), data)
                            recyclerViewProposal.adapter = adapter
                        } ?: Constants.error(requireContext(), "Response body is null")
                    } else {
                        if (response.body().isNullOrEmpty()){
                            val layout = view?.findViewById<LinearLayout>(R.id.noProposalStatus)
                            layout!!.visibility = View.VISIBLE
                        }
                    }
                }
            }
            override fun onFailure(call: Call<ProposalStatus>, t: Throwable) {
                Constants.error(requireContext(), "Error: ${t.message}")
            }

        })
    }

    private fun getQualifyStatus(
        service: ApiInterface,
        jwtToken: String,
        recyclerViewQualify: RecyclerView
    ) {
        val call = service.adminQualifyStatus("Bearer $jwtToken",leadId)
        call.enqueue(object : Callback<QualifyStatus> {
            override fun onResponse(
                call: Call<QualifyStatus>,
                response: Response<QualifyStatus>
            ) {
                if (isAdded) {
                    if (response.isSuccessful) {
                        response.body()?.let { data ->
                            val adapter = QualifyActivityAdapter(requireContext(), data)
                            recyclerViewQualify.adapter = adapter
                        } ?: Constants.error(requireContext(), "Response body is null")
                    } else {
                        if (response.body().isNullOrEmpty()){
                            val layout = view?.findViewById<LinearLayout>(R.id.noQualifyStatus)
                            layout!!.visibility = View.VISIBLE
                        }
                    }
                }
            }
            override fun onFailure(call: Call<QualifyStatus>, t: Throwable) {
                Constants.error(requireContext(), "Error: ${t.message}")
            }
        })
    }
    private fun getAllContactStatus(
        service: ApiInterface,
        jwtToken: String,
        recyclerViewCnt: RecyclerView
    ) {
        val call = service.adminContactStatus("Bearer $jwtToken", leadId)
        call.enqueue(object : Callback<ContactStatus> {
            override fun onResponse(
                call: Call<ContactStatus>,
                response: Response<ContactStatus>
            ) {
                if (isAdded) {
                    if (response.isSuccessful) {
                        response.body()?.let { data ->
                            val adapter = ContactActivityAdapter(requireContext(), data)
                            recyclerViewCnt.adapter = adapter
                        } ?: Constants.error(requireContext(), "Response body is null")
                    } else {
                        //Constants.error(requireContext(), "Unsuccessful response: ${response.code()}")
                        if (response.body().isNullOrEmpty()){
                            val layout = view?.findViewById<LinearLayout>(R.id.noContactStatus)
                            layout!!.visibility = View.VISIBLE
                        }
                    }
                }
            }
            override fun onFailure(call: Call<ContactStatus>, t: Throwable) {
                Constants.error(requireContext(), "Error: ${t.message}")
            }

        })
    }

    private fun getToken(): String? {
        return requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
            .getString("jwtToken", null)
    }
    override fun onStart() {
        super.onStart()
        val baseContainer = view?.findViewById<LinearLayout>(R.id.activityContainer)
        baseContainer?.removeAllViews()
        val inflater = LayoutInflater.from(requireContext())
        val layout = inflater.inflate(R.layout.activity_contact_lead, baseContainer, false)
        baseContainer?.addView(layout)
        layout?.let {
            val recyclerViewCnt = it.findViewById<RecyclerView>(R.id.ContactInfoRec)
            recyclerViewCnt?.layoutManager = WrapContentLinearLayoutManager(requireContext())

            val jwttoken = getToken()
            if (jwttoken != null) {
                val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
                getAllContactStatus(service, jwttoken, recyclerViewCnt)
            } else {
                Constants.error(requireContext(), "Token is null Login Again")
            }
        }
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}