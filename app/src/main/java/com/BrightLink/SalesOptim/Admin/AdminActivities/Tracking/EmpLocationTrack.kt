package com.BrightLink.SalesOptim.Admin.AdminActivities.Tracking

import android.content.Context
import androidx.fragment.app.Fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.ViewModel.MainViewModel
import com.BrightLink.SalesOptim.databinding.FragmentEmpLocationTrackBinding

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions

class EmpLocationTrack : Fragment(), OnMapReadyCallback  {
    private lateinit var binding : FragmentEmpLocationTrackBinding
    private var googleMap: GoogleMap? = null
    private lateinit var viewModel: MainViewModel

    private val handler = android.os.Handler()
    private val refreshRunnable = object : Runnable {
        override fun run() {
            refreshDataAndAddMarker()
            handler.postDelayed(this, 5000) // Refresh every 5 seconds
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
       binding = FragmentEmpLocationTrackBinding.inflate(layoutInflater)
        viewModel = ViewModelProvider(requireActivity()).get(MainViewModel::class.java)

        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        val bundle = arguments
        val empId = bundle!!.getInt("empId")

        val token = getToken()
        Log.d("GURU",token.toString())
        if (token != null) {
            viewModel.EmpTrack(token,empId)
        } else {
            Constants.error(requireContext(), "Token is null")
        }

        viewModel.ObserveErrorMessage().observe(viewLifecycleOwner){
            Constants.error(requireContext(),it)
        }

        viewModel.ObserveEmpTrack().observe(viewLifecycleOwner){empTrackData->
            if (empTrackData != null) {

                val latitude = empTrackData.latitud
                val longitude = empTrackData.longitude
                val name = empTrackData.name
                val employeeId = empTrackData.emplyoeeId

                binding.name.text = name
                binding.latitude.text = latitude.toString()
                binding.longitude.text = longitude.toString()
                binding.empid.text = empId.toString()

                val latLng = LatLng(latitude, longitude)

                // Add a marker to the map
                googleMap?.addMarker(
                    MarkerOptions()
                        .position(latLng)
                        .title(name)
                        .snippet("Employee ID: $employeeId")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.user_location))
                )
                googleMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 18f))
            }
        }



        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        startRefreshHandler()
    }


    override fun onDestroyView() {
        super.onDestroyView()
        stopRefreshHandler()
    }



    override fun onMapReady(map: GoogleMap) {
        googleMap = map

        googleMap?.mapType = GoogleMap.MAP_TYPE_SATELLITE

        val defaultLocation = LatLng(18.48096, 73.9512395) // Example location
        googleMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(defaultLocation, 18f))

        googleMap!!.setOnMarkerClickListener(object : OnMarkerClickListener{
            override fun onMarkerClick(marker: Marker): Boolean {
                googleMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.position, 18f))
                return true
            }
        })
    }

    private fun startRefreshHandler() {
        handler.postDelayed(refreshRunnable, 5000) // Start refreshing after 5 seconds
    }

    private fun stopRefreshHandler() {
        handler.removeCallbacks(refreshRunnable) // Stop refreshing
    }


    private fun refreshDataAndAddMarker() {
        val bundle = arguments
        val empId = bundle!!.getInt("empId")

        val token = getToken()
        Log.d("GURU", token.toString())
        if (token != null) {
            viewModel.EmpTrack(token, empId)
        } else {
            Constants.error(requireContext(), "Token is null")
        }

        viewModel.ObserveErrorMessage().observe(viewLifecycleOwner) {
            Constants.error(requireContext(), it)
        }

        viewModel.ObserveEmpTrack().observe(viewLifecycleOwner) { empTrackData ->
            if (empTrackData != null) {
                val latitude = empTrackData.latitud
                val longitude = empTrackData.longitude
                val name = empTrackData.name
                val employeeId = empTrackData.emplyoeeId

                binding.name.text = name
                binding.latitude.text = latitude.toString()
                binding.longitude.text = longitude.toString()
                binding.empid.text = empId.toString()

                val latLng = LatLng(latitude, longitude)

                googleMap?.clear()

                googleMap?.addMarker(
                    MarkerOptions()
                        .position(latLng)
                        .title(name)
                        .snippet("Employee ID: $employeeId")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.user_location))
                )
            }
        }
    }
    private fun getToken(): String? {
        return requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
            .getString("jwtToken", null)
    }
}