package com.BrightLink.SalesOptim.Admin.AdminActivities.AdminRequestModel

data class AddPipelinePostModel(
    val employeeId: String,
    val endDate: String,
    val monthlyLeads: String,
    val monthlyRevenue: String,
    val startDate: String,
    val winLeads: String
)