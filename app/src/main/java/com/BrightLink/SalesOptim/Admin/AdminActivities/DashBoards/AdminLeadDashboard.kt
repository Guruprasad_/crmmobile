package com.BrightLink.SalesOptim.Admin.AdminActivities.DashBoards

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel.AdminLeadBarChartResponseModel
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel.AdminLeadLineChartResponseModel
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.Constants.CustomDialog
import com.BrightLink.SalesOptim.ResponseModel.LeadDashBoardResponseModel
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.FragmentAdminLeadDashboardBinding
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.math.roundToInt

class AdminLeadDashboard : Fragment() {

    private lateinit var binding : FragmentAdminLeadDashboardBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAdminLeadDashboardBinding.inflate(layoutInflater)

        binding.actionBar.activityName.text = "Lead Dashboard"
        binding.actionBar.back.setOnClickListener {
            requireActivity().onBackPressed()
        }

        binding.actionBar.option.visibility = View.INVISIBLE

        val jwtToken = gettoken(context)
        if (jwtToken!=null)
        {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            CallApi(service , jwtToken , context)
            CallDashBoardApi(service , jwtToken , context)
            CallBarChartApi(service , jwtToken , context)
        }
        else{
            Constants.error(context,"Token is null Login Again")
        }

        return binding.root
    }

    private fun CallBarChartApi(service: ApiInterface, jwtToken: String, context: Context?) {
        val loading = CustomDialog(requireContext())
        loading.show()
        val call = service.getAdminLeadBarChart("Bearer $jwtToken")
        call.enqueue(object : Callback<AdminLeadBarChartResponseModel> {
            override fun onResponse(
                call: Call<AdminLeadBarChartResponseModel>,
                response: Response<AdminLeadBarChartResponseModel>
            ) {
                if (isAdded)
                {
                    if (response.isSuccessful)
                    {
                        val data  = response.body()
                        if (data!=null)
                        {
                            loading.dismiss()
                            callbarChart(data)
                        }
                        else{
                            Constants.error(context, "Response body is null")
                            loading.dismiss()
                        }
                    }
                    else
                    {
                        Constants.error(context,"Response is not successful")
                        loading.dismiss()
                    }
                }

            }

            override fun onFailure(call: Call<AdminLeadBarChartResponseModel>, t: Throwable) {
                Constants.error(context,"Error barchart : ${t.message}")
                loading.dismiss()
            }

        })
    }

    private fun callbarChart(data: AdminLeadBarChartResponseModel) {
        val lastIndex = data.size - 1
        if (lastIndex >= 0) {
            val lost: Float = data[lastIndex].lostLeadsPercentage.toFloat()
            val proposal: Float = data[lastIndex].proposalLeadsPercentage.toFloat()
            val qualified: Float = data[lastIndex].qualifiedLeadsPercentage.toFloat()
            val won: Float = data[lastIndex].wonLeadsPercentage.toFloat()



            val barDataSets = arrayListOf<BarDataSet>()

            val entriesWon = arrayListOf(BarEntry(0f, won))
            val entriesQualified = arrayListOf(BarEntry(1f, qualified))
            val entriesProposal = arrayListOf(BarEntry(2f, proposal))
            val entriesLost = arrayListOf(BarEntry(3f, lost))

            val colors = intArrayOf(Color.rgb(0,200,192), Color.rgb(255,150,0), Color.rgb(96,99,255), Color.rgb(255,43,82))

            val dataSetWon = BarDataSet(entriesWon, "Won")
            dataSetWon.color = colors[0]
            dataSetWon.valueTextColor = Color.BLACK
            barDataSets.add(dataSetWon)

            val dataSetQualified = BarDataSet(entriesQualified, "Qualified")
            dataSetQualified.color = colors[1]
            dataSetQualified.valueTextColor = Color.BLACK
            barDataSets.add(dataSetQualified)

            val dataSetProposal = BarDataSet(entriesProposal, "Proposal")
            dataSetProposal.color = colors[2]
            dataSetProposal.valueTextColor = Color.BLACK
            barDataSets.add(dataSetProposal)

            val dataSetLost = BarDataSet(entriesLost, "Lost")
            dataSetLost.color = colors[3]
            dataSetLost.valueTextColor = Color.BLACK
            barDataSets.add(dataSetLost)

            val data = BarData(barDataSets as List<IBarDataSet>?)
            binding.barchart.data = data

            // Customize x-axis
            val xAxis = binding.barchart.xAxis
            xAxis.position = XAxis.XAxisPosition.BOTTOM
            xAxis.setDrawGridLines(true)
            xAxis.axisMinimum = 0f
            xAxis.granularity = 1f
            xAxis.valueFormatter = IndexAxisValueFormatter(arrayOf("Won", "Qualified", "Proposal", "Lost"))

            // Customize y-axis
            val yAxisLeft = binding.barchart.axisLeft
            yAxisLeft.setDrawGridLines(false)
            yAxisLeft.valueFormatter = object : ValueFormatter() {
                override fun getFormattedValue(value: Float): String {
                    return "${value.toInt()}%" // Display value with percentage symbol
                }
            }

            binding.barchart.axisRight.isEnabled = false
            binding.barchart.description.isEnabled = false
            binding.barchart.animateY(1000)
            binding.barchart.invalidate()

        } else {
            Log.e("LeadDashboardFragment", "Data list is empty")
        }
    }



    private fun CallDashBoardApi(service: ApiInterface, jwtToken: String, context: Context?) {
        val loadingProgressBar = CustomDialog(requireContext())
        loadingProgressBar.show()
        val call = service.getAdminLeadDashboard("Bearer $jwtToken")
        call.enqueue(object : Callback<LeadDashBoardResponseModel> {
            override fun onResponse(
                call: Call<LeadDashBoardResponseModel>,
                response: Response<LeadDashBoardResponseModel>
            ) {
                if (isAdded)
                {
                    if (response.isSuccessful)
                    {
                        val data  = response.body()
                        if (data!=null)
                        {
                            loadingProgressBar.dismiss()
                            binding.tvLeadCount.text = data.totalLeadCount.toString()
                            binding.tvWonCount.text = data.wonLeadCount.toString()
                            binding.tvContactedCount.text = data.contactedLeads.toString()
                            binding.tvQualifiedCount.text = data.qualifiedLeadCount.toString()
                            binding.tvProposalCount.text = data.proposalLeadCount.toString()
                            binding.tvLostCount.text = data.lostLeadCount.toString()
                        }
                        else{
                            Constants.error(context, "Response body is null")
                            loadingProgressBar.dismiss()
                        }
                    }
                    else
                    {
                        Constants.error(context,"Response is not successful")
                        loadingProgressBar.dismiss()
                    }
                }

            }

            override fun onFailure(call: Call<LeadDashBoardResponseModel>, t: Throwable) {
                Constants.error(context,"Error : ${t.message}")
                loadingProgressBar.dismiss()
            }

        })
    }

    private fun CallApi(apiservice : ApiInterface , jwtToken : String , context: Context? ) {
        context?:return
        val loadingProgressBar = CustomDialog(requireContext())
        loadingProgressBar.show()
        val call = apiservice.getAdminLeadLineChart("Bearer $jwtToken")
        call.enqueue(object : Callback<AdminLeadLineChartResponseModel> {
            override fun onResponse(
                call: Call<AdminLeadLineChartResponseModel>,
                response: Response<AdminLeadLineChartResponseModel>
            ) {
                if (isAdded)
                {
                    if (response.isSuccessful)
                    {
                        val data  = response.body()
                        if (data!=null)
                        {
                            loadingProgressBar.dismiss()
                            callLineChart(data)
                        }
                        else{
                            Constants.error(context, "Response body is null")
                            loadingProgressBar.dismiss()
                        }
                    }
                    else
                    {
                        Constants.error(context,"Response is not successful")
                        loadingProgressBar.dismiss()
                    }
                }

            }

            override fun onFailure(call: Call<AdminLeadLineChartResponseModel>, t: Throwable) {
                Constants.error(context,"Error : ${t.message}")
                loadingProgressBar.dismiss()
            }

        })

    }

    private fun callLineChart(data: AdminLeadLineChartResponseModel) {
        val resultListEntries = ArrayList<Entry>()
        val totalLeadsEntries = ArrayList<Entry>()

        data.resultList.forEach { entry ->
            resultListEntries.add(Entry(entry.month.toFloat(), entry.totalTask.toFloat()))
        }

        // Populate totalLeadsEntries from totalLeadList in API response
        data.totalLeadLists.forEach { entry ->
            totalLeadsEntries.add(Entry(entry.month.toFloat(), entry.totalTask.toFloat()))
        }

        val resultListDataSet = LineDataSet(resultListEntries, "Result List")
        resultListDataSet.color = Color.BLUE
        resultListDataSet.setCircleColor(Color.BLUE)

        val totalLeadsDataSet = LineDataSet(totalLeadsEntries, "Total Leads")
        totalLeadsDataSet.color = Color.RED
        totalLeadsDataSet.setCircleColor(Color.RED)

        val dataSets = ArrayList<ILineDataSet>()
        dataSets.add(resultListDataSet)
        dataSets.add(totalLeadsDataSet)

        val xAxis: XAxis = binding.lineChart.xAxis
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.granularity = 1f // Set granularity to 1 to ensure all months are displayed
        xAxis.isGranularityEnabled = true
        xAxis.valueFormatter = object : ValueFormatter() {
            override fun getFormattedValue(value: Float): String {
                val monthNumber = value.toInt()

                // Ensure monthNumber is within bounds
                return if (monthNumber in 1..12) {
                    // Return the corresponding month name using the month number
                    getMonthName(monthNumber)
                } else {
                    "" // Return empty string for out-of-bounds month numbers
                }
            }
        }


        val yAxisLeft = binding.lineChart.axisLeft
        yAxisLeft.valueFormatter = object : ValueFormatter() {
            override fun getFormattedValue(value: Float): String {
                // Format the y-axis values as integers
                return value.roundToInt().toString()
            }
        }

        val yAxisRight = binding.lineChart.axisRight
        yAxisRight.isEnabled = false

        val lineData = LineData(dataSets)
        binding.lineChart.data = lineData

        val description = Description()
        description.text = "Monthly Sales"
        binding.lineChart.description = description
        binding.lineChart.animateX(1500)

        binding.lineChart.notifyDataSetChanged()
        binding.lineChart.invalidate()
    }

    private fun gettoken(context: Context?): String? {
        val sharedPreferences = requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
        return sharedPreferences.getString("jwtToken", null)
    }

    private fun getMonthName(monthNumber: Int): String {
        // Array of month names
        val months = arrayOf("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec")
        // Return the corresponding month name
        return months[monthNumber - 1]
    }

}