package com.BrightLink.SalesOptim.Admin.AdminAdapters

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.ResponseModel.Admin.ShowEmployeeModelItem
import com.BrightLink.SalesOptim.databinding.AdminGetEmployeesBinding
import java.util.Locale

class EmployeeAdapter(val context: Context, private var dataList: List<ShowEmployeeModelItem>): RecyclerView.Adapter<EmployeeAdapter.ViewHolder>() {

    private var filteredList: List<ShowEmployeeModelItem> = dataList
    class ViewHolder(val binding: AdminGetEmployeesBinding) : RecyclerView.ViewHolder(binding.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = AdminGetEmployeesBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val currentItem = filteredList[position]

        val bitmap: Bitmap? = decodeBase64ToBitmap(currentItem.images)
        if (!currentItem.images.isNullOrEmpty()) {
            Glide.with(context)
                .asBitmap()
                .load(bitmap)
                .transition(BitmapTransitionOptions.withCrossFade())
                .into(holder.binding.image)
        }

        holder.binding.name.text = currentItem.name
        holder.binding.phone.text = currentItem.phone
        holder.binding.shiftTime.text = currentItem.shiftTime
        holder.binding.department.text = currentItem.department

        val cId = currentItem.cId

        holder.binding.layout.setOnClickListener {
            val bundle = Bundle()
            bundle.putInt("cId", cId)
            Navigation.findNavController(it).navigate(R.id.adminUpdateEmployee, bundle)
        }
    }

    private fun decodeBase64ToBitmap(base64String: String?): Bitmap? {
        if (base64String != null) {
            val decodedBytes: ByteArray = Base64.decode(base64String, Base64.DEFAULT)
            return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.size)
        } else {
            // Handle the case where base64String is null
            return null
        }
    }

    fun filter(text: String) {
        val searchText = text.lowercase(Locale.getDefault())
        filteredList = if (searchText.isEmpty()) {
            dataList
        } else {
            dataList.filter { item ->
                item.name.lowercase(Locale.getDefault()).contains(searchText)
            }
        }
        notifyDataSetChanged()
    }
}