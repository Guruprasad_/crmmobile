package com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel

data class AdminMeetingPostRes(
    val assignTo: String,
    val clientEmail: String,
    val contactId: Int,
    val contactName: Any,
    val leadId: Int,
    val locationType: String,
    val meetingFrom: String,
    val meetingId: Int,
    val meetingLinkOrLocation: String,
    val meetingTo: String,
    val proposal: Any,
    val title: String
)