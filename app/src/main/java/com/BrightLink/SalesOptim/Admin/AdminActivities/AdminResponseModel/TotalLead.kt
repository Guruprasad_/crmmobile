package com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel

data class TotalLead(
    val month: Int,
    val targetCount: Int,
    val year: Int
)