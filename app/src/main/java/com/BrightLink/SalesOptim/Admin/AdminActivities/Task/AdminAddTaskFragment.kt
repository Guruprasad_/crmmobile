package com.BrightLink.SalesOptim.Admin.AdminActivities.Task

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Spinner
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminReqModel.AdminAddTaskReqModel
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.ResponseModel.AddTaskResModel
import com.BrightLink.SalesOptim.ResponseModel.ShowAccountsModelItem
import com.BrightLink.SalesOptim.ResponseModel.ShowContactModelItem
import com.BrightLink.SalesOptim.ResponseModel.ShowLeadsModelItem
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.AdminActivityAddtaskBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.Date

class AdminAddTaskFragment : Fragment(){

    private var _binding: AdminActivityAddtaskBinding? = null
    private val binding get() = _binding!!

    //Storing Emp Id
    private var taskOwnerId: Int? = null
    //Storing Lead List
    private var leadLists: ArrayList<String> = ArrayList()
    //Setting adapter of lead
    private lateinit var leadAdapter: ArrayAdapter<String>

    //Storing Account List
    private var accountNames: ArrayList<String> = ArrayList()
    //Setting Adapter of Accounts
    private lateinit var accountAdapter: ArrayAdapter<String>

    //Storing Contact List
    private var contactLists: ArrayList<String> = ArrayList()
    //Setting Adapter of Contact
    private lateinit var contactAdapter: ArrayAdapter<String>

    //Static Spinners declarations
    private lateinit var taskStatusSpinner: Spinner
    private lateinit var taskPrioritySpinner: Spinner

    //Storing List of Id's fetched
    private val leadIds: ArrayList<String> = ArrayList()
    private val accountIds: ArrayList<String> = ArrayList()
    private val contactIds: ArrayList<String> = ArrayList()


    //Calendars Declaration
    private lateinit var dueDateEditText: EditText
    private lateinit var reminderDateEditText: EditText
    //Due-Rem Dates Initialization
    private var dueDate: Date? = null
    private var reminderDate: Date? = null

    override fun onResume() {
        super.onResume()
        setupSpinners()
    }

    private fun setupSpinners() {
        val taskStatus = resources.getStringArray(R.array.Task_Status)
        val arrayAdapter = ArrayAdapter(requireContext(), R.layout.dropdown_items, taskStatus)
        binding.taskStatusList.setAdapter(arrayAdapter)

        val taskPriority = resources.getStringArray(R.array.Task_Priority)
        val arrayAdapter1 = ArrayAdapter(requireContext(), R.layout.dropdown_items, taskPriority)
        binding.taskPriorityList.setAdapter(arrayAdapter1)

        val taskTypeList = resources.getStringArray(R.array.Task_type)
        val arrayAdapter2 = ArrayAdapter(requireContext(), R.layout.dropdown_items, taskTypeList)
        binding.taskTypeList.setAdapter(arrayAdapter2)

        binding.taskTypeList.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val selectedType = parent?.getItemAtPosition(position).toString()
                when (selectedType) {
                    "Lead" -> {
                        fetchLeadList()
                        binding.taskTypeListElements.onItemSelectedListener = object : AdapterView.OnItemSelectedListener
                        {
                            override fun onItemSelected(parent: AdapterView<*>?, view: View?, pos: Int, id: Long) {
                                val selectedLeadName = parent?.getItemAtPosition(pos).toString()
                                val selectedLeadId = leadIds[pos]
                                // Do something with the selected lead ID
                            }
                            override fun onNothingSelected(parent: AdapterView<*>?) {}
                        }
                    }

                    "Account" -> {
                        fetchAccountNames()
                        binding.taskTypeListElements.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(parent: AdapterView<*>?, view: View?, pos: Int, id: Long) {
                                val selectedAccountName = parent?.getItemAtPosition(pos).toString()
                                val selectedAccountId = accountIds[pos]
                                // Do something with the selected account ID
                            }
                            override fun onNothingSelected(parent: AdapterView<*>?) {}
                        }
                    }
                    "Contact" -> {
                        fetchContactsList()
                        binding.taskTypeListElements.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(parent: AdapterView<*>?, view: View?, pos: Int, id: Long) {
                                val selectedContactName = parent?.getItemAtPosition(pos).toString()
                                val selectedContactId = contactIds[pos]
                                // Do something with the selected contact ID
                            }
                            override fun onNothingSelected(parent: AdapterView<*>?) {}
                        }
                    }
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                // Handle case when nothing is selected
            }
        })
    }
    private fun fetchLeadList() {
        val jwtToken = AdminTaskUtils.gettoken(requireContext())
        jwtToken?.let { token ->
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val callLeads = service.showleadsAdmin("Bearer $token")

            callLeads.enqueue(object : Callback<List<ShowLeadsModelItem>?> {
                override fun onResponse(
                    call: Call<List<ShowLeadsModelItem>?>,
                    response: Response<List<ShowLeadsModelItem>?>
                ) {
                    if (response.isSuccessful) {
                        response.body()?.let { data ->
                            leadLists.clear()
                            data.forEach { item ->
                                item.leadId?.let {
                                    leadIds.add(it.toString())
                                    leadLists.add(item.customerName?: "")
                                }
                            }
                            leadAdapter.notifyDataSetChanged()
                            binding.taskTypeListElements.adapter = leadAdapter
                        } ?: Constants.error(requireContext(), "Response body is null")
                    } else {
                        Constants.error(
                            requireContext(),
                            "Unsuccessful response: ${response.code()}"
                        )
                    }
                }

                override fun onFailure(call: Call<List<ShowLeadsModelItem>?>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }
            })
        }
    }

    private fun fetchAccountNames() {
        val jwtToken = AdminTaskUtils.gettoken(requireContext())
        jwtToken?.let { token ->
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val callAccounts = service.showaccountsAdmin("Bearer $token")

            callAccounts.enqueue(object : Callback<ArrayList<ShowAccountsModelItem>> {
                override fun onResponse(
                    call: Call<ArrayList<ShowAccountsModelItem>>,
                    response: Response<ArrayList<ShowAccountsModelItem>>
                ) {
                    if (response.isSuccessful) {
                        response.body()?.let { data ->
                            accountNames.clear()
                            data.forEach { item ->
                                item.accountId?.let {
                                    accountIds.add(it.toString())
                                    accountNames.add(item.accountName.toString()?:"")
                                }
                            }
                            accountAdapter.notifyDataSetChanged()
                            binding.taskTypeListElements.adapter = accountAdapter
                            Log.d("TAG", "Fetch accounts successful: $data")
                        } ?: Constants.error(requireContext(), "Response body is null")
                    } else {
                        Constants.error(
                            requireContext(),
                            "Unsuccessful response: ${response.code()} - ${response.message()}"
                        )
                        Log.e("TAG", "Fetch accounts error: ${response.code()} - ${response.message()}")
                    }
                }

                override fun onFailure(call: Call<ArrayList<ShowAccountsModelItem>>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                    Log.e("TAG", "Fetch accounts error: ${t.message}")
                }
            })
        }
    }
    private fun fetchContactsList() {
        val jwtToken = AdminTaskUtils.gettoken(requireContext())
        jwtToken?.let { token ->
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val contacts = service.showcontactAdmin("Bearer $token")

            contacts.enqueue(object : Callback<List<ShowContactModelItem>?> {
                override fun onResponse(
                    call: Call<List<ShowContactModelItem>?>,
                    response: Response<List<ShowContactModelItem>?>
                ) {
                    if (response.isSuccessful) {
                        response.body()?.let { data ->
                            contactLists.clear()
                            data.forEach { item ->
                                item.contactid?.toString()?.let {
                                    contactIds.add(it.toString())
                                    contactLists.add(item.fullName.toString()?:"")
                                }
                            }
                            contactAdapter.notifyDataSetChanged()
                            binding.taskTypeListElements.adapter = contactAdapter
                        } ?: Constants.error(requireContext(), "Response body is null")
                    } else {
                        Constants.error(
                            requireContext(),
                            "Unsuccessful response: ${response.code()}"
                        )
                    }
                }
                override fun onFailure(call: Call<List<ShowContactModelItem>?>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }
            })
        }
    }



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = AdminActivityAddtaskBinding.inflate(inflater, container, false)
        val root:View = binding.root

        binding.actionBar.activityName.text = "Add Task"
        binding.actionBar.back.setOnClickListener {
            requireActivity().onBackPressed()
        }

        //Static Spinners Definition
        taskStatusSpinner = binding.taskStatusList
        taskPrioritySpinner = binding.taskPriorityList

        //Calendars Definition
        dueDateEditText = binding.taskDueDate
        reminderDateEditText = binding.reminderDate

        //Due Date Calendar operation
        binding.taskDueDate.setOnClickListener {
            AdminTaskUtils.showDatePickerDialog(
                requireContext(),
                dueDateEditText,
                reminderDate,
                dueDate,
                isDueDate = true,
                onDateSelected = { selectedDate ->
                    dueDate = selectedDate // Update the due date
                }
            )
        }
        //Reminder Date Calendar operation
        binding.reminderDate.setOnClickListener {
            AdminTaskUtils.showDatePickerDialog(
                requireContext(),
                reminderDateEditText,
                reminderDate,
                dueDate,
                isDueDate = false,
                onDateSelected = { selectedDate ->
                    reminderDate = selectedDate // Update the reminder date
                }
            )
        }
        //Fetching Task owner name
        AdminTaskUtils.fetchTaskOwnerName(
            requireContext(),
            onSuccess = { (taskOwnerName, ownerId) ->
                binding.addTaskOwnerName.setText(taskOwnerName)
                taskOwnerId = ownerId
            },
            onError = { errorMessage ->
                Constants.error(requireContext(), errorMessage)
            }
        )


        //Lead,Account,Contact Adapters Initialization
        accountAdapter = ArrayAdapter(requireContext(), R.layout.dropdown_items, accountNames)
        leadAdapter = ArrayAdapter(requireContext(), R.layout.dropdown_items, leadLists)
        contactAdapter = ArrayAdapter(requireContext(), R.layout.dropdown_items, contactLists)

        binding.addTaskButton.setOnClickListener {
            performAddTask()
        }
        binding.resetTaskInputs.setOnClickListener {
            resetFields()
        }

        return root
    }
    private fun performAddTask() {

        //Fetch EmployeeId and VendorId based on the selected Items
        val jwtToken = AdminTaskUtils.gettoken(requireContext())
        if (jwtToken != null) {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)

            //Spinner value picker will comes here......
            val taskStatus = binding.taskStatusList.selectedItem.toString()
            val taskPriority = binding.taskPriorityList.selectedItem.toString()
            //Custom Input of Lead
//        val leadIdOrAccountId = binding.taskTypeListElements.selectedItem.toString()
            val leadOrAccountType = binding.taskTypeList.selectedItem.toString()

            // Retrieve selected IDs based on the task type
            val selectedId: String? = when (binding.taskTypeList.selectedItem.toString()) {
                "Lead" -> leadIds.getOrNull(binding.taskTypeListElements.selectedItemPosition)
                "Account" -> accountIds.getOrNull(binding.taskTypeListElements.selectedItemPosition)
                "Contact" -> contactIds.getOrNull(binding.taskTypeListElements.selectedItemPosition)
                else -> null
            }

            Constants.info(requireContext(),"Selected Id is : ${selectedId}")

            // Handle case when selected ID is null
            if (selectedId == null) {
                Constants.error(requireContext(), "Selected ID is null")
                return
            }

            val duedate = requireNotNull(dueDate.toString()) { "Due date must be selected" }
            val reminderDate =
                requireNotNull(reminderDate.toString()) { "Reminder date must be selected" }

            val taskOwnerName = binding.addTaskOwnerName.text.toString()
            val subject = binding.addTaskNameSubjectEditText.text.toString()
            val description = binding.addTaskDesEditText.text.toString()

            val requestBody = AdminAddTaskReqModel(
                description, duedate,taskOwnerId.toString(), selectedId, leadOrAccountType,
                taskPriority, reminderDate, taskStatus, subject,taskOwnerName
            )

            if (taskOwnerName.isEmpty()) {
                binding.addTaskOwnerName.error = "Task Owner Name Field can't be blank"
                return
            }


            //Api Integration
            val addTaskApiCall = service.addTaskAdmin("Bearer $jwtToken", requestBody)
            addTaskApiCall.enqueue(object : Callback<AddTaskResModel> {
                override fun onResponse(
                    call: Call<AddTaskResModel>,
                    response: Response<AddTaskResModel>
                ) {
                    if (response.isSuccessful) {
                        val addTaskResModel = response.body()
                        if (addTaskResModel != null) {
                            Constants.success(requireContext(),"Task is added successfully")
                            findNavController().popBackStack()
                        }
                    } else {
                        Constants.error(requireContext(),"Unsuccessful response ${response.code()}")
                    }
                }

                override fun onFailure(call: Call<AddTaskResModel>, t: Throwable) {
                    Constants.error(requireContext(),"error ${t.message}")
                }
            })
        }else {
            Constants.error(requireContext(), "Token is null. Please log in again.")
        }
    }

    private fun resetFields() {
        listOf(
            binding.addTaskNameSubjectEditText,
            binding.taskDueDate,
            binding.reminderDate,
            binding.addTaskDesEditText
        ).forEach { it.text = null }

        listOf(
            binding.taskPriorityList,
            binding.taskStatusList,
            binding.taskTypeList,
            binding.taskTypeListElements
        ).forEach { it.setSelection(0) }
    }
}