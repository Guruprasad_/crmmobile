package com.BrightLink.SalesOptim.Admin.AdminActivities.AdminContact

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.RequestModel.AdminAddContactPostModel
import com.BrightLink.SalesOptim.ResponseModel.AddContactResponseModel
import com.BrightLink.SalesOptim.ResponseModel.ShowAccountsModelItem
import com.BrightLink.SalesOptim.ResponseModel.ShowEmployeelistModelItem
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.AdminCreateContactBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AdminAddContactsFragment : Fragment() {
    private var _binding: AdminCreateContactBinding? = null
    private val binding get() = _binding!!
    private lateinit var accountSpinner: Spinner
    private lateinit var vendorSpinner: Spinner
    private lateinit var accountAdapter: ArrayAdapter<String>
    private lateinit var vendorAdapter: ArrayAdapter<String>
    private var accountNames: ArrayList<String> = ArrayList()
    private var vendorNames: ArrayList<String> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = AdminCreateContactBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.actionBar.activityName.text = "Add Contact"
        binding.actionBar.back.setOnClickListener {
            requireActivity().onBackPressed()
        }
        binding.actionBar.option.visibility = View.INVISIBLE

        accountSpinner = binding.accountName

        accountAdapter = ArrayAdapter(requireContext(), R.layout.spinner_item_layout_currency, accountNames)
        accountAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        accountSpinner.adapter = accountAdapter

        binding.create.setOnClickListener {
            performAddContact()
        }
        binding.reset.setOnClickListener {
            // Call the method to perform the POST API call
            resetFields()
        }

        fetchAccountAndEmployeeNames()

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // Set up spinner with custom adapter
        setupSpinner(binding.leadSource)
    }


    private fun resetFields() {
        listOf(
            binding.fullName, binding.title,
            binding.phone, binding.alternatePhone, binding.department,
            binding.email, binding.assistant, binding.assistantPhone,
            binding.fax, binding.mailingStreet, binding.otherStreet,
            binding.mailingCity, binding.otherCity, binding.mailingCountry,
            binding.otherCountry,binding.mailingCode,binding.otherZipCode,binding.description,binding.twitter, binding.skypeId

            ).forEach { it.text = null }

        listOf(
            binding.leadSource
        ).forEach { it.setSelection(0) }
    }


    private fun setupSpinner(spinner: Spinner) {
        val adapter = ArrayAdapter.createFromResource(
            requireContext(),
            R.array.Account_Type,
            R.layout.spinner_item_layout_currency
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = adapter
    }


    private fun fetchAccountAndEmployeeNames() {
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null) {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val callAccounts = service.adminshowaccounts("Bearer $jwtToken")
            val callEmployeelist = service.adminemployeelist("Bearer $jwtToken")

            callAccounts.enqueue(object : Callback<ArrayList<ShowAccountsModelItem>> {
                override fun onResponse(
                    call: Call<ArrayList<ShowAccountsModelItem>>,
                    response: Response<ArrayList<ShowAccountsModelItem>>
                ) {
                    if (response.isSuccessful) {
                        val data = response.body()
                        if (data != null) {
                            accountNames.clear()
                            for (item in data) {
                                item.accountName?.let { accountNames.add(it) }
                            }
                            accountAdapter.notifyDataSetChanged()
                        } else {
                            Constants.error(requireContext(), "Response body is null")
                        }
                    } else {
                        Constants.error(requireContext(), "Unsuccessful response: ${response.code()}")
                    }
                }

                override fun onFailure(call: Call<ArrayList<ShowAccountsModelItem>>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }
            })

            callEmployeelist.enqueue(object : Callback<ArrayList<ShowEmployeelistModelItem>> {
                override fun onResponse(
                    call: Call<ArrayList<ShowEmployeelistModelItem>>,
                    response: Response<ArrayList<ShowEmployeelistModelItem>>
                ) {
                    if (response.isSuccessful) {
                        val employeelist = response.body()
                        if (employeelist != null && employeelist.isNotEmpty()) {
                            // For simplicity, let's just take the first employee from the list
                            val employee = employeelist[0]
                            // Set the employee name to the TextView
                            binding.leadOwnerName.text = employee.name
                        } else {
                            Constants.error(requireContext(), "Employee list is null or empty")
                        }
                    } else {
                        Constants.error(requireContext(), "Unsuccessful response: ${response.code()}")
                    }
                }

                override fun onFailure(call: Call<ArrayList<ShowEmployeelistModelItem>>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }
            })
        } else {
            Constants.error(requireContext(), "Token is null. Please log in again.")
        }
    }

    private fun performAddContact() {
        val selectedAccountName = accountSpinner.selectedItem.toString()

        // Fetch accountId based on the selected account name
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null) {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val callAccounts = service.adminshowaccounts("Bearer $jwtToken")
            callAccounts.enqueue(object : Callback<ArrayList<ShowAccountsModelItem>> {
                override fun onResponse(
                    call: Call<ArrayList<ShowAccountsModelItem>>,
                    response: Response<ArrayList<ShowAccountsModelItem>>
                ) {
                    if (response.isSuccessful) {
                        val accountData = response.body()
                        if (accountData != null) {
                            val selectedAccount = accountData.find { it.accountName == selectedAccountName }
                            val accountId = selectedAccount?.accountId ?: 0

                            // Now you have the accountId, proceed with adding the contact
                            addContactWithIds(accountId)
                        } else {
                            Constants.error(requireContext(), "Response body is null")
                        }
                    } else {
                        Constants.error(requireContext(), "Unsuccessful response: ${response.code()}")
                    }
                }

                override fun onFailure(call: Call<ArrayList<ShowAccountsModelItem>>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }
            })
        } else {
            Constants.error(requireContext(), "Token is null. Please log in again.")
        }
    }



    private fun addContactWithIds(accountId: Int) {
        // Now you have both accountId and vendorId, continue with adding the contact
        val leadOwnerName = binding.leadOwnerName.text.toString()
        val fullName = binding.fullName.text.toString()
        val accountName = accountSpinner.selectedItem.toString()
        val phone = binding.phone.text.toString()
        val department = binding.department.text.toString()
        val twitter = binding.twitter.text.toString()
        val assistant = binding.assistant.text.toString()
        val fax = binding.fax.text.toString()
        val leadSource = binding.leadSource.selectedItem.toString()
        val title = binding.title.text.toString()
        val alternatePhone = binding.alternatePhone.text.toString()
        val email = binding.email.text.toString()
        val skypeId = binding.skypeId.text.toString()
        val assistantPhone = binding.assistantPhone.text.toString()
        val mailingStreet = binding.mailingStreet.text.toString()
        val mailingCity = binding.mailingCity.text.toString()
        val mailingState = binding.mailingState.text.toString()
        val mailingCountry = binding.mailingCountry.text.toString()
        val zipCode = binding.mailingCode.text.toString()
        val otherStreet = binding.otherStreet.text.toString()
        val otherCity = binding.otherCity.text.toString()
        val othereState = binding.othereState.text.toString()
        val otherCountry = binding.otherCountry.text.toString()
        val otherZipCode = binding.otherZipCode.text.toString()
        val description = binding.description.text.toString()

        if (leadOwnerName.isEmpty() ) {
            binding.leadOwnerName.error  = "Please enter lead owner name"
            return
        }

        if (fullName.isEmpty() ) {
           binding.fullName.error  = "Please enter full name"
            return
        }

        // Create the request body with accountId and vendorId
        val requestBody = AdminAddContactPostModel(
            accountId,  fullName, leadSource, department, accountName, email, phone,
            alternatePhone, assistant, title, fax, assistantPhone, skypeId, twitter, mailingStreet, mailingCity, mailingState, mailingCountry,
            zipCode, otherStreet, otherCity, othereState, otherCountry, otherZipCode, description, leadOwnerName
        )

        val jwtToken = gettoken(requireContext())
        if (jwtToken != null) {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.adminaddcontact("Bearer $jwtToken", requestBody)
            call.enqueue(object : Callback<AddContactResponseModel> {
                override fun onResponse(
                    call: Call<AddContactResponseModel>,
                    response: Response<AddContactResponseModel>
                ) {
                    if (response.isSuccessful) {
                        Constants.success(
                            requireContext(),
                            "Created Successfully"
                        )
                        // Handle successful response
                        findNavController().popBackStack() // Go back to the previous fragment
                    } else {
                        Constants.error(
                            requireContext(),
                            "Unsuccessful response: ${response.code()}"
                        )
                    }
                }

                override fun onFailure(call: Call<AddContactResponseModel>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }
            })
        } else {
            Constants.error(requireContext(), "Token is null. Please log in again.")
        }
    }

    private fun gettoken(context: Context?): String? {
        val sharedPreferences = requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
        return sharedPreferences.getString("jwtToken", null)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
