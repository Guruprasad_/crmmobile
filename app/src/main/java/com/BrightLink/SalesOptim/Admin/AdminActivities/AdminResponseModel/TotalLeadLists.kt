package com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel

data class TotalLeadLists(
    val month: Int,
    val totalTask: Int,
    val year: Int
)