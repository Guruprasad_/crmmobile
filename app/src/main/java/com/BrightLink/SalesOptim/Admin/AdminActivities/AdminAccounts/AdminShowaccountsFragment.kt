package com.BrightLink.SalesOptim.Admin.AdminActivities.AdminAccounts

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.BrightLink.SalesOptim.Adapters.AccountAdapter
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.Constants.WrapContentLinearLayoutManager
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.ResponseModel.ShowAccountsModelItem
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.ActivityAccountBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AdminShowaccountsFragment : Fragment() {

    private var _binding: ActivityAccountBinding? = null
    private lateinit var adapter : AccountAdapter
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ActivityAccountBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.actionBar.activityName.text ="Account"
        binding.actionBar.back.setOnClickListener{
            requireActivity().onBackPressed()
        }

        binding.actionBar.option.visibility = View.INVISIBLE


        binding.recyclerView.layoutManager = WrapContentLinearLayoutManager(context)

        val jwtToken = gettoken(context)
        if (jwtToken!=null)
        {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            CallApi(service , jwtToken)
        }
        else{
            Constants.error(requireContext(),"Token is null Login Again")
        }

        binding.addaccount.setOnClickListener{
            findNavController().navigate(R.id.admin_addaccount)
        }
        // Assuming you have the adapter instantiated and attached to your RecyclerView

        return root
    }

    private fun CallApi(apiservice: ApiInterface, jwtToken: String) {
        val call = apiservice.adminshowaccounts("Bearer $jwtToken")
        call.enqueue(object : Callback<ArrayList<ShowAccountsModelItem>> {
            override fun onResponse(
                call: Call<ArrayList<ShowAccountsModelItem>>,
                response: Response<ArrayList<ShowAccountsModelItem>>
            ) {
                if (isAdded) { // Check if the fragment is added to the activity
                    if (response.isSuccessful) {
                        val data = response.body()
                        if (data != null) {

                             adapter = AccountAdapter(requireContext(), data)
                            adapter.setOnItemClickListener(object : AccountAdapter.onItemClickListener {
                                override fun onItemClick(position: Int) {
                                    val selectedItem = data[position] // Get the selected item
                                    val bundle = Bundle().apply {
                                        putSerializable("selectedItem", selectedItem) // Pass the selected item as a Serializable
                                    }
                                    findNavController().navigate(R.id.admin_updateaccount, bundle)
                                }
                            })
                            binding.recyclerView.adapter = adapter
                        } else {
                            Constants.error(requireContext(), "Response body is null")
                        }
                    } else {
                        Constants.error(requireContext(), "Unsuccessful response: ${response.code()}")
                    }
                }
            }

            override fun onFailure(call: Call<ArrayList<ShowAccountsModelItem>>, t: Throwable) {
                if (isAdded) { // Check if the fragment is added to the activity
                    Constants.error(requireContext(), "Error: ${t.message}")
                }
            }
        })
    }

    private fun gettoken(context: Context?): String? {
        val sharedPreferences = requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
        return sharedPreferences.getString("jwtToken", null)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
