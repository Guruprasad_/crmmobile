package com.BrightLink.SalesOptim.Admin.AdminActivities.Tracking

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.Constants.WrapContentLinearLayoutManager
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.ResponseModel.GetMapResponseModel
import com.BrightLink.SalesOptim.ResponseModel.Location
import com.BrightLink.SalesOptim.ViewModel.MainViewModel
import com.BrightLink.SalesOptim.databinding.EmpListMapBinding
import com.BrightLink.SalesOptim.databinding.FragmentMapsBinding
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class MapsFragment : Fragment(), OnMapReadyCallback {

    private lateinit var viewModel: MainViewModel
    private lateinit var binding: FragmentMapsBinding
    private var googleMap: GoogleMap? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMapsBinding.inflate(layoutInflater)
        viewModel = ViewModelProvider(requireActivity()).get(MainViewModel::class.java)

        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        val token = getToken()
        Log.d("GURU",token.toString())
        if (token != null) {
            viewModel.getCoordinates(token)
        } else {
            Constants.error(requireContext(), "Token is null")
        }

        observeCorrdinates()
        viewModel.ObserveErrorMessage().observe(viewLifecycleOwner){
            Constants.error(requireContext(),it)
        }

        return binding.root
    }
    override fun onMapReady(map: GoogleMap) {
        googleMap = map

        googleMap?.mapType = GoogleMap.MAP_TYPE_SATELLITE

        val defaultLocation = LatLng(18.48096, 73.9512395) // Example location
        googleMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(defaultLocation, 10f))
    }

    private fun observeCorrdinates() {
        viewModel.ObserveCoordinates().observe(viewLifecycleOwner) { data ->

            Log.d("GURU", "Received data: $data")
            binding.activeCount.text = data.activeCount.toString()
            binding.totalCount.text = data.totalCount.toString()
            binding.inactiveCount.text = data.inactiveCount.toString()

            binding.empList.setOnClickListener {
                empDialog(data)
            }

            Log.d("GURU", "Number of locations: ${data.locations.size}")

            googleMap?.clear()

            data.locations.let { locations ->
                var offset = 0.00002
                for (location in locations) {
                    val latitude = location.latitude + (offset * (locations.indexOf(location) % 2)) // Use index-based offset
                    val longitude = location.longitude + (offset * (locations.indexOf(location) % 2)) // Use index-based offset
                    val name = location.name
                    val employeeId = location.employeeId
                    val locationTime = location.timestamp.toDateTimeString()
                    val bitmapDescriptor = BitmapDescriptorFactory.fromResource(R.drawable.user_location)


                    val latLng = LatLng(latitude, longitude)

                    Log.d("GURU", "Adding marker at: $latLng with title: $name")

                    val markerOptions = MarkerOptions()
                        .position(latLng)
                        .title(name)
                        .snippet("Employee ID: $employeeId")
                        .icon(bitmapDescriptor)

                    googleMap?.addMarker(markerOptions)
                }

                if (locations.isNotEmpty()) {
                    val boundsBuilder = LatLngBounds.builder()
                    for (location in locations) {
                        boundsBuilder.include(LatLng(location.latitude, location.longitude))
                    }
                    val bounds = boundsBuilder.build()
                    val padding = 50 // Padding in pixels
                    val cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, padding)
                    googleMap?.moveCamera(cameraUpdate)
                }
            }
        }
    }

    private fun empDialog(data: GetMapResponseModel?) {
        val dialogView = layoutInflater.inflate(R.layout.emp_map_dialog, null)
        val search = dialogView.findViewById<EditText>(R.id.searchView)
        val recyclerView = dialogView.findViewById<RecyclerView>(R.id.recyclerView)

        val dialog = AlertDialog.Builder(requireContext())
            .setView(dialogView)
            .create()

        val adapter = MapEmpAdapter(requireContext(),data!!.locations,findNavController(),dialog) // Replace with your adapter
        recyclerView.adapter = adapter
        recyclerView.layoutManager = WrapContentLinearLayoutManager(requireContext())

        search.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                adapter.filter(s.toString())
            }
        })

        dialog.show()
    }

    private fun getToken(): String? {
        return requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
            .getString("jwtToken", null)
    }

    fun Long.toDateTimeString(): String {
        val date = Date(this)
        val format = SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.getDefault())
        return format.format(date)
    }

    class MapEmpAdapter(private val context: Context , private val dataList : List<Location>,private val navController: NavController , private val dailog : AlertDialog) :
        RecyclerView.Adapter<MapEmpAdapter.onviewHolder>() {

        class onviewHolder(val binding : EmpListMapBinding) : RecyclerView.ViewHolder(binding.root)

        private var filterList : List<Location> = dataList


        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): onviewHolder {
            val view  = EmpListMapBinding.inflate(LayoutInflater.from(parent.context),parent , false)
            return onviewHolder(view)
        }

        override fun getItemCount(): Int {
            return filterList.size
        }

        override fun onBindViewHolder(holder: onviewHolder, position: Int) {
            with(holder.binding){
                val currentItem = filterList[position]
                name.text = currentItem.name
                empId.text = currentItem.employeeId.toString()
                time.text = currentItem.timestamp.toDateTimeString()
                val bundle = Bundle()
                bundle.putInt("empId",currentItem.employeeId)
                track.setOnClickListener{
                   navController.navigate(R.id.empTracking,bundle)
                    dailog.dismiss()
                }
            }
        }

        fun filter(text: String) {
            val searchText = text.lowercase(Locale.getDefault())
            filterList= if (searchText.isEmpty()) {
                dataList
            } else {
                dataList.filter { item ->
                    item.employeeId.toString().lowercase(Locale.getDefault()).contains(searchText) ||
                            item.name.lowercase(Locale.getDefault()).contains(searchText)

                }
            }
            notifyDataSetChanged()
        }

        fun Long.toDateTimeString(): String {
            val date = Date(this)
            val format = SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.getDefault())
            return format.format(date)
        }
    }

}
