package com.BrightLink.SalesOptim.Admin.AdminActivities.Employee

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.OpenableColumns
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.MimeTypeMap
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminRequestModel.AddEmployeePostModel
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel.AddEmployeeAdminResponseModel
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.AdminFragmentAddEmployeeBinding
import com.google.android.material.button.MaterialButton
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.MaterialDatePicker
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale

class AddEmployeeFragment : Fragment() {

    object FileTypeHelper {

        fun getMediaTypeFromFileExtension(extension: String): MediaType? {
            val mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
            return mimeType?.let { it.toMediaTypeOrNull() }
        }
    }

    private var _binding: AdminFragmentAddEmployeeBinding? = null
    private val binding get() = _binding!!

    private var filePath: String? = null
    private var path : String = ""
    private val PICKFILE_RESULT_CODE = 1

    private lateinit var departmentSpinner: Spinner

    private val shiftTimeMap: MutableList<String> = mutableListOf()
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = AdminFragmentAddEmployeeBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.updateVendorActionBar.activityName.text = "Add Employee"
        binding.updateVendorActionBar.back.setOnClickListener {
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }

        departmentSpinner = binding.department

        val departmentAdapter = ArrayAdapter.createFromResource(requireContext(), R.array.Department, R.layout.spinner_layout)
        departmentAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        departmentSpinner.adapter = departmentAdapter

        binding.addShift.setOnClickListener {
            val dialogView = layoutInflater.inflate(R.layout.admin_employee_add_shift, null)
            val dialogBuilder = AlertDialog.Builder(requireContext())
                .setView(dialogView)

            val dialog = dialogBuilder.create()
            dialog.show()

            val from = dialogView.findViewById<TextView>(R.id.fromShift)
            val to = dialogView.findViewById<TextView>(R.id.toShift)
            val submit = dialogView.findViewById<MaterialButton>(R.id.submitBtn)

            from.setOnClickListener {
                showTimePickerDialog(from)
            }

            to.setOnClickListener {
                showTimePickerDialog(to)
            }

            submit.setOnClickListener {

                val fromTime = from.text.toString()
                val toTime = to.text.toString()

                if (fromTime.isNotBlank() && toTime.isNotBlank()) {
                    val shiftTime = "$fromTime to $toTime"
                    binding.shiftTime.text = shiftTime
                    dialog.dismiss()
                } else {
                    Constants.warning(requireContext(), "Select Time")
                }
            }
        }

        binding.hiredTime.setOnClickListener {
            showMaterialCalendar(binding.hiredTime)
        }

        binding.chooseFile.setOnClickListener {
            showFileChooser()
        }

        binding.saveBtn.setOnClickListener{
            addEmployee()
        }

        return root
    }

    fun showTimePickerDialog(textView: TextView) {
        val calendar = Calendar.getInstance()
        val timePickerDialog = TimePickerDialog(
            requireContext(),
            { _, hourOfDay, minute ->
                // Combine date and time
                calendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
                calendar.set(Calendar.MINUTE, minute)

                // Format the final date-time with AM/PM indicator
                val formattedDateTime = SimpleDateFormat("hh:mm a", Locale.getDefault()).format(calendar.time)
                textView.text = formattedDateTime.uppercase(Locale.getDefault())
            },
            calendar.get(Calendar.HOUR_OF_DAY),
            calendar.get(Calendar.MINUTE),
            false // Set to false for 12-hour format with AM/PM
        )

        timePickerDialog.show()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun addEmployee() {
        val name = binding.name.text.toString()
        val nikName = binding.nikName.text.toString()
        val work = binding.work.text.toString()
        val email = binding.email.text.toString()
        val phone = binding.phone.text.toString()
        val department = binding.department.selectedItem.toString()
        val shiftTime = binding.shiftTime.text.toString()
        val hiredTime = binding.hiredTime.text.toString()
        val password = binding.password.text.toString()
        val gender = getLocationType()
        val description = binding.description.text.toString()
        val timeZone = "Asia/Kolkata"

        val attach = AddEmployeePostModel(gender, hiredTime, name, department, description, email, password, phone, nikName, shiftTime, timeZone, work)

        Log.d("attach", "attach: $attach")

        if (path.isNullOrEmpty()){
            Constants.warning(requireContext(), "Choose the profile photo please")
        }
        else {
            val file = File(path)
            val content: ByteArray = file.readBytes()

            val fileExtension = MimeTypeMap.getFileExtensionFromUrl(file.absolutePath)
            val mediaType = FileTypeHelper.getMediaTypeFromFileExtension(fileExtension)

            if (mediaType != null){
                val requestBody = RequestBody.create(mediaType, content)
                val body = MultipartBody.Part.createFormData("file", file.name, requestBody)
                Log.d("file", "body: $body")
                performAddEmployee(attach, body)
            } else {
                Constants.error(requireContext(), "Unsupported file type")
            }
        }

    }

    private fun performAddEmployee(attach: AddEmployeePostModel, fileBody: MultipartBody.Part?) {
        val jwtToken = gettoken(requireContext())
        if (jwtToken != null) {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.addEmployeeAdmin("Bearer $jwtToken", fileBody, attach)
            call.enqueue(object : Callback<AddEmployeeAdminResponseModel> {
                override fun onResponse(
                    call: Call<AddEmployeeAdminResponseModel>,
                    response: Response<AddEmployeeAdminResponseModel>
                ) {
                    if (isAdded)
                    {
                        if (response.isSuccessful) {
                            val responseBody = response.body()
                            Log.d("ResponseBody", "$responseBody")
                            Constants.success(requireContext(),"Employee Added Successfully")
                            requireActivity().supportFragmentManager.popBackStack()
                        }else{
                            Log.d("ResponseBody", "${response.body()}")
                            Constants.error(requireContext(),"Something went wrong, Try again")
                        }
                    }
                }
                override fun onFailure(call: Call<AddEmployeeAdminResponseModel>, t: Throwable) {
                    Constants.error(requireContext(), "Failed to add employee. Try again")

                }
            })
        } else {
            Constants.error(requireContext(), "Please try to Login Again")
        }
    }

    private fun getLocationType(): String {
        return when (binding.gender.checkedRadioButtonId) {
            R.id.male -> "male"
            R.id.female -> "female"
            else -> ""
        }
    }

    private fun showFileChooser() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
            intent.type = "*/*"
            startActivityForResult(intent, PICKFILE_RESULT_CODE)
        } else {
            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.type = "*/*"
            intent.addCategory(Intent.CATEGORY_OPENABLE)
            startActivityForResult(
                Intent.createChooser(intent, "Select File"), PICKFILE_RESULT_CODE
            )
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == PICKFILE_RESULT_CODE && resultCode == Activity.RESULT_OK && data != null && data.data != null) {
            val uri: Uri = data.data!!
            path = getPathFromUri(requireContext(), uri)!!
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                updateTextViewWithFileName(uri)
            } else {
                // Handle for lower versions
            }
        }
    }

    @SuppressLint("Range")
    private fun getPathFromUri(context: Context, uri: Uri): String? {
        var path: String? = null
        if ("content".equals(uri.scheme, ignoreCase = true)) {
            // Using ACTION_OPEN_DOCUMENT
            val cursor = context.contentResolver.query(uri, null, null, null, null)
            cursor?.use {
                if (it.moveToFirst()) {
                    val displayName = it.getString(it.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                    val type = it.getString(it.getColumnIndex("mime_type"))
                    val file = File(context.cacheDir, displayName)
                    try {
                        val inputStream = context.contentResolver.openInputStream(uri)
                        inputStream?.use { input ->
                            FileOutputStream(file).use { output ->
                                input.copyTo(output)
                            }
                        }
                        path = file.absolutePath
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
            }
        } else if ("file".equals(uri.scheme, ignoreCase = true)) {
            path = uri.path
        }
        return path
    }

    @SuppressLint("SetTextI18n")
    private fun updateTextViewWithFileName(uri: Uri) {
        val fileName = getFileNameFromUri(uri)
        Log.d("FileName", "File Name: $fileName")
        filePath = uri.toString()
        if (fileName != null) {
            binding.orderFilePath.text = Html.fromHtml("<b><font color='#7FFF00'>$fileName</font></b>")
        }
    }

    private fun getFileNameFromUri(uri: Uri): String? {
        var fileName: String? = null
        val cursor =requireContext().contentResolver.query(uri, null, null, null, null)
        if (cursor!= null){
            if (cursor.moveToFirst()) {
                val displayNameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
                if (displayNameIndex != -1) {
                    fileName = cursor.getString(displayNameIndex)
                    Log.d("FileName", "File Name: $fileName")
                }
            }
            cursor.close()
        }
        return fileName
    }

    private fun showMaterialCalendar(textView: TextView) {
        val today = Calendar.getInstance().timeInMillis
        val datePickerBuilder = MaterialDatePicker.Builder.datePicker()
        datePickerBuilder.setSelection(today)

        val constraintsBuilder = CalendarConstraints.Builder()

        datePickerBuilder.setCalendarConstraints(constraintsBuilder.build())

        val datePicker = datePickerBuilder.build()

        datePicker.addOnPositiveButtonClickListener {selectedDate->
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = selectedDate

            // Launch TimePickerDialog
            val timePickerDialog = TimePickerDialog(
                requireContext(),
                { _, hourOfDay, minute ->
                    // Combine date and time
                    calendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
                    calendar.set(Calendar.MINUTE, minute)

                    // Format the final date-time
                    val formattedDateTime = SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.getDefault()).format(calendar.time)
                    textView.text = formattedDateTime.uppercase(Locale.getDefault())
                },
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                false
            )

            timePickerDialog.show()
        }
        datePicker.show(parentFragmentManager, datePicker.toString())

    }

    private fun gettoken(context: Context?): String? {
        val sharedPreferences = requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
        return sharedPreferences.getString("jwtToken", null)
    }

}