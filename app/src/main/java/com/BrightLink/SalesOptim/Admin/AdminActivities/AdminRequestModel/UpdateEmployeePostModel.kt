package com.BrightLink.SalesOptim.Admin.AdminActivities.AdminRequestModel

data class UpdateEmployeePostModel(
    val Gender: String,
    val Hiredate: String,
    val Name: String,
    val cId: Int,
    val department: String,
    val description: String,
    val email: String,
    val phone: String,
    val secondName: String,
    val shiftTime: String,
    val timeZone: String,
    val work: String
)