package com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel

data class AdminAcheiveLeadResponseModelItem(
    val month: Int,
    val wonLeads: Int,
    val year: Int
)