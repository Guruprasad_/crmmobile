package com.BrightLink.SalesOptim.Admin.AdminActivities.AdminReqModel

data class AdminAddTaskReqModel(
    val description: String,
    val duedate: String,
    val employeeId: String,
    val leadIdOrAccountId: String,
    val leadOrAccountType: String,
    val priority: String,
    val reminderDate: String,
    val status: String,
    val subject: String,
    val taskOwner: String
)