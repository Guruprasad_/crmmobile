package com.BrightLink.SalesOptim.Admin.AdminActivities.DashBoards

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel.AdminEmployeeDashboardResponseModel
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel.BottomEmployee
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel.Employee
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel.TopEmployee
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.Constants.CustomDialog
import com.BrightLink.SalesOptim.Constants.WrapContentLinearLayoutManager
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.AdminEmployeeDashboardAllemployeeBinding
import com.BrightLink.SalesOptim.databinding.AdminEmployeeDashboardBinding
import com.BrightLink.SalesOptim.databinding.AdminEmployeeHighPerformerBinding
import com.BrightLink.SalesOptim.databinding.AdminEmployeeLowPerformerBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AdminEmployeeDashboard : Fragment() {

    private lateinit var binding : AdminEmployeeDashboardBinding
    private lateinit var highEmployeeAdapter : EmployeeAdapter
    private lateinit var lowEmployeeAdapter : LowEmployeeAdapter
    private lateinit var allEmployeeAdapter : AllEmployeeAdapter


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = AdminEmployeeDashboardBinding.inflate(layoutInflater)


        val jwtToken = gettoken(context)
        if (jwtToken!=null)
        {
        val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
        CallApi(service,jwtToken,requireContext())
        }
        else{
            Constants.error(context,"Token is null Login Again")
        }

        binding.recyclerView1.layoutManager = WrapContentLinearLayoutManager(requireContext())
        binding.recyclerView2.layoutManager = WrapContentLinearLayoutManager(requireContext())
        binding.recyclerView3.layoutManager = WrapContentLinearLayoutManager(requireContext())


        return binding.root
    }

    private fun gettoken(context: Context?): String? {
        val sharedPreferences = requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
        return sharedPreferences.getString("jwtToken", null)
    }

    private fun CallApi(apiservice : ApiInterface, jwtToken : String, context: Context? ) {
        context?:return
        val loadingProgressBar = CustomDialog(requireContext())
        loadingProgressBar.show()
        val call = apiservice.getAdminEmployeeDashboard("Bearer $jwtToken")
        call.enqueue(object : Callback<AdminEmployeeDashboardResponseModel> {
            override fun onResponse(
                call: Call<AdminEmployeeDashboardResponseModel>,
                response: Response<AdminEmployeeDashboardResponseModel>
            ) {
                if (isAdded)
                {
                    if (response.isSuccessful)
                    {
                        val data  = response.body()
                        if (data!=null)
                        {
                            loadingProgressBar.dismiss()
                            highEmployeeAdapter = EmployeeAdapter(data.topEmployee)
                            lowEmployeeAdapter = LowEmployeeAdapter(data.bottomEmployee)
                            allEmployeeAdapter =AllEmployeeAdapter(data.employee)
                            binding.recyclerView1.adapter = highEmployeeAdapter
                            binding.recyclerView2.adapter = lowEmployeeAdapter
                            binding.recyclerView3.adapter = allEmployeeAdapter

                        }
                        else{
                            Constants.error(context, "Response body is null")
                            loadingProgressBar.dismiss()
                        }
                    }
                    else
                    {
                        Constants.error(context,"Response is not successful")
                        loadingProgressBar.dismiss()
                    }
                }

            }

            override fun onFailure(call: Call<AdminEmployeeDashboardResponseModel>, t: Throwable) {
                Constants.error(context,"Error : ${t.message}")
                loadingProgressBar.dismiss()
            }

        })

    }

    private inner class EmployeeAdapter(private val datalist : List<TopEmployee>) :
        RecyclerView.Adapter<EmployeeAdapter.onViewHolder>()
    {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): onViewHolder {
            val view = AdminEmployeeHighPerformerBinding.inflate(LayoutInflater.from(parent.context),parent,false)
            return onViewHolder(view)
        }

        override fun onBindViewHolder(holder: onViewHolder, position: Int) {
            with(holder.binding){

                empId.text = datalist.get(position).emplyoeeId.toString()
                empName.text = datalist.get(position).assignedTo.toString()
                totalTask.text = datalist.get(position).totalCount.toString()
                completedTask.text = datalist.get(position).completeCount.toString()
                task.text = datalist.get(position).topResult.toString()
            }
        }

        override fun getItemCount(): Int {
           return datalist.size
        }

        inner class onViewHolder( val binding : AdminEmployeeHighPerformerBinding) : RecyclerView.ViewHolder(binding.root)

    }


    private inner class LowEmployeeAdapter(private val datalist : List<BottomEmployee>) :
        RecyclerView.Adapter<LowEmployeeAdapter.onViewHolder>()
    {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): onViewHolder {
            val view = AdminEmployeeLowPerformerBinding.inflate(LayoutInflater.from(parent.context),parent,false)
            return onViewHolder(view)
        }

        override fun onBindViewHolder(holder: onViewHolder, position: Int) {
            with(holder.binding){

                empId.text = datalist.get(position).emplyoeeId.toString()
                empName.text = datalist.get(position).assignedTo.toString()
                totalTask.text = datalist.get(position).totalCount.toString()
                completedTask.text = datalist.get(position).completeCount.toString()
                task.text = datalist.get(position).topResult.toString()
            }
        }

        override fun getItemCount(): Int {
            return datalist.size
        }

        inner class onViewHolder( val binding : AdminEmployeeLowPerformerBinding) : RecyclerView.ViewHolder(binding.root)

    }

    private inner class AllEmployeeAdapter(private val datalist : List<Employee>) :
        RecyclerView.Adapter<AllEmployeeAdapter.onViewHolder>()
    {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): onViewHolder {
            val view = AdminEmployeeDashboardAllemployeeBinding.inflate(LayoutInflater.from(parent.context),parent,false)
            return onViewHolder(view)
        }

        override fun onBindViewHolder(holder: onViewHolder, position: Int) {
            with(holder.binding){

                empId.text = datalist.get(position).emplyoeeId.toString()
                empName.text = datalist.get(position).assignedTo.toString()
                totalTask.text = datalist.get(position).totalCount.toString()
                completedTask.text = datalist.get(position).completeCount.toString()
                inprocessTask.text = datalist.get(position).inProcessCount.toString()
                pendingTask.text = datalist.get(position).pendingCount.toString()
                cancelTask.text = datalist.get(position).cancelCount.toString()
            }
        }

        override fun getItemCount(): Int {
            return datalist.size
        }

        inner class onViewHolder( val binding : AdminEmployeeDashboardAllemployeeBinding) : RecyclerView.ViewHolder(binding.root)

    }
}