package com.BrightLink.SalesOptim.AdminAdapters

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel.PipelineReportModelItem
import com.BrightLink.SalesOptim.Admin.AdminActivities.PipelineReport.UpdatePipelineReportFragment
import com.BrightLink.SalesOptim.databinding.GetPipelineReportBinding
import java.text.DecimalFormat

class PipelineReportAdapter(val context : Context, private var datalist : List<PipelineReportModelItem>, private val fragmentManager: FragmentManager) : RecyclerView.Adapter<PipelineReportAdapter.ViewHolder>() {
    class ViewHolder(val binding : GetPipelineReportBinding) : RecyclerView.ViewHolder(binding.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = GetPipelineReportBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return datalist.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentItem = datalist[position]
        holder.binding.empName.text = currentItem.empName
        holder.binding.startDate.text = currentItem.startDate
        holder.binding.endDate.text = currentItem.endDate
        holder.binding.targetLead.text = currentItem.monthlyLeads.toString()
        holder.binding.targetWonLead.text = currentItem.winLeads.toString()

        val targetRevenue = currentItem.monthlyRevenue.toString()
        val revenueTargetInLakhs = convertToLakhs(targetRevenue).toString()
        holder.binding.targetRevenue.text = revenueTargetInLakhs

        holder.binding.achieveLead.text = currentItem.actualLeads.toString()
        holder.binding.achieveWonLead.text = currentItem.actualWonLeads.toString()
        holder.binding.actualRevenue.text = currentItem.actualRevenue?.toString()

        val pid =  currentItem.id
        holder.binding.updateTarget.setOnClickListener {
            val bundle = Bundle()
            bundle.putInt("pid", pid)
            val fragment = UpdatePipelineReportFragment()
            fragment.arguments = bundle
            fragment.show(fragmentManager, "update_pipeline_dialog")
        }
    }

    private fun convertToLakhs(value: String): String {
        // Assuming value is a string representation of a numeric value
        val numericValue = value.toDoubleOrNull() ?: return "0.00" // Convert the string to a numeric value
        val valueInLakhs = numericValue / 100000.0 // Divide the value by 100,000 to convert to lakhs
        val df = DecimalFormat("#.####################")
        return df.format(valueInLakhs)
    }
}