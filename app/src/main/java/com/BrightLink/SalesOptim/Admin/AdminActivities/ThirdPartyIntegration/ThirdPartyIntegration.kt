package com.BrightLink.SalesOptim.Admin.AdminActivities.ThirdPartyIntegration

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageButton
import android.widget.Spinner
import androidx.fragment.app.Fragment
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminAdapters.IntegrationAdapter
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.Constants.WrapContentLinearLayoutManager
import com.BrightLink.SalesOptim.R
import com.BrightLink.SalesOptim.RequestModel.ThirdPartyIntegrationPostModel
import com.BrightLink.SalesOptim.ResponseModel.ThirdPartyIntegrationResModel
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.AdminAddThirdPartyBinding
import com.BrightLink.SalesOptim.databinding.AdminThirdPartyIntegrationBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ThirdPartyIntegration : Fragment() {

    private var _binding: AdminThirdPartyIntegrationBinding? = null
    private val binding get() = _binding!!
    private var popupWindow: AdminAddThirdPartyBinding? = null
    private var selectedItem: ThirdPartyIntegrationResModel? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = AdminThirdPartyIntegrationBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupActionBar()
        setupRecyclerView()
        setupAddMeetingButton()
        popupWindow?.let { setupSpinner(it.platform, R.array.platform) }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setupActionBar() {
        with(binding.actionBar) {
            activityName.text = "Third Party Integration"
            back.setOnClickListener {
                requireActivity().onBackPressed()
            }
            option.visibility = View.INVISIBLE
        }
    }

    private fun setupRecyclerView() {
        binding.recyclerView.layoutManager = WrapContentLinearLayoutManager(context)
        refreshIntegrationList()
    }

    private fun setupAddMeetingButton() {
        binding.connectAccount.setOnClickListener {
            showAddPopup()
        }
    }

    private fun refreshIntegrationList() {
        getToken()?.let { jwtToken ->
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            callApi(service, jwtToken)
        } ?: Constants.error(requireContext(), "Token is null. Please log in again.")
    }

//    private fun showAddPopup() {
//        val dialog = Dialog(requireContext())
//        val popupBinding = AdminAddThirdPartyBinding.inflate(layoutInflater)
//        popupWindow = popupBinding
//        dialog.setContentView(popupBinding.root)
//        dialog.setCancelable(false)
//        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//        dialog.window?.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
//
//        // Fill the popup fields with selected item data
//        selectedItem?.let { setUpUIWithData(it, popupBinding) }
//
//        popupBinding.addToAccount.setOnClickListener {
//            add(popupBinding)
//            dialog.dismiss()
//            selectedItem?.id?.let { id ->
//                performUpdateIntegration(id, popupBinding)
//            }
//            dialog.dismiss()
//        }
//        dialog.findViewById<ImageButton>(R.id.closeButton)?.setOnClickListener {
//            dialog.dismiss()
//        }
//        dialog.show()
//    }

    private fun showAddPopup() {
        val dialog = Dialog(requireContext())
        val popupBinding = AdminAddThirdPartyBinding.inflate(layoutInflater)
        popupWindow = popupBinding
        dialog.setContentView(popupBinding.root)
        dialog.setCancelable(false)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window?.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)

        // Fill the popup fields with selected item data if available
        selectedItem?.let { setUpUIWithData(it, popupBinding) }

        popupBinding.addToAccount.setOnClickListener {
            if (selectedItem != null) {
                // Update existing data
                selectedItem?.id?.let { id ->
                    performUpdateIntegration(id, popupBinding)
                }
            } else {
                // Add new data
                add(popupBinding)
            }
            dialog.dismiss()
            // Clear selectedItem after operation
            selectedItem = null
        }
        dialog.findViewById<ImageButton>(R.id.closeButton)?.setOnClickListener {
            dialog.dismiss()
            // Clear selectedItem if the popup is dismissed without adding/updating
            selectedItem = null
        }
        dialog.show()
    }


    private fun getIndex(spinner: Spinner, myString: String): Int {
        val adapter = spinner.adapter as? ArrayAdapter<String>
        adapter?.let {
            for (i in 0 until it.count) {
                if (it.getItem(i) == myString) {
                    return i
                }
            }
        }
        return 0 // Default to 0 if not found
    }
    private fun setUpUIWithData(selectedItem: ThirdPartyIntegrationResModel, binding: AdminAddThirdPartyBinding) {
        // Set UI fields with data from the selected item
        binding.adsId.setText(selectedItem.adsId ?: "")
        binding.platform.setSelection(getIndex(binding.platform, selectedItem.platform ?: ""))
        binding.appId.setText(selectedItem.appId ?: "")
        binding.aapSecret.setText(selectedItem.appSecret ?: "")
        binding.tokenorurl.setText(selectedItem.tokenOrUrl ?: "")
    }


    private fun add(binding: AdminAddThirdPartyBinding) {
        val platform = binding.platform.selectedItem.toString()
        val appId = binding.appId.text.toString()
        val appSecret = binding.aapSecret.text.toString()
        val tokenOrUrl  = binding.tokenorurl.text.toString()
        val  adsId = binding.adsId.text.toString()

        val requestBody = ThirdPartyIntegrationPostModel(
            adsId, appId, appSecret, platform, tokenOrUrl
        )

        getToken()?.let { jwtToken ->
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.addintegration("Bearer $jwtToken", requestBody)
            call.enqueue(object : Callback<ThirdPartyIntegrationResModel> {
                override fun onResponse(
                    call: Call<ThirdPartyIntegrationResModel>,
                    response: Response<ThirdPartyIntegrationResModel>
                ) {
                    if (response.isSuccessful) {
                        Constants.success(requireContext(), "Integration added successfully")
                        refreshIntegrationListDelayed()
                    } else {
                        Constants.error(requireContext(), "Unsuccessful response: ${response.code()}")
                    }
                }

                override fun onFailure(call: Call<ThirdPartyIntegrationResModel>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }
            })
        } ?: Constants.error(requireContext(), "Token is null. Please log in again.")
    }

    private fun setupSpinner(spinner: Spinner, arrayResourceId: Int) {
        val adapter = ArrayAdapter.createFromResource(
            requireContext(),
            arrayResourceId,
            R.layout.simple_spinner_item_layout
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = adapter
    }

    private fun callApi(apiservice: ApiInterface, jwtToken: String) {
        val call = apiservice.integrationlist("Bearer $jwtToken")
        call.enqueue(object : Callback<List<ThirdPartyIntegrationResModel>> {
            override fun onResponse(
                call: Call<List<ThirdPartyIntegrationResModel>>,
                response: Response<List<ThirdPartyIntegrationResModel>>
            ) {
                if (response.isSuccessful) {
                    response.body()?.let { data ->
                        val adapter = IntegrationAdapter(requireContext(), data)
                        adapter.setOnItemClickListener(object : IntegrationAdapter.onItemClickListener {
                            override fun onItemClick(position: Int) {
                                selectedItem = data[position] // Get the selected item
                                showAddPopup()
                            }
                        })
                        binding.recyclerView.adapter = adapter
                    } ?: Constants.error(requireContext(), "Response body is null")
                } else {
                    Constants.error(requireContext(), "Unsuccessful response: ${response.code()}")
                }
            }

            override fun onFailure(call: Call<List<ThirdPartyIntegrationResModel>>, t: Throwable) {
                if (isAdded) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }
            }
        })
    }

    private fun refreshIntegrationListDelayed() {
        Handler().postDelayed({
            refreshIntegrationList()
        }, 3000)
    }

    private fun getToken(): String? {
        return requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
            .getString("jwtToken", null)
    }

    private fun performUpdateIntegration(id: Int, binding: AdminAddThirdPartyBinding) {
        val adsId = binding.adsId.text?.toString()
        val appId = binding.appId.text?.toString()
        val appSecret = binding.aapSecret.text?.toString()
        val platform = binding.platform.selectedItem?.toString()
        val tokenOrUrl = binding.tokenorurl.text?.toString()

        val requestBody = ThirdPartyIntegrationResModel(
            adsId, appId, appSecret, platform, tokenOrUrl, id
        )

        getToken()?.let { jwtToken ->
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            val call = service.adminupdateintegration("Bearer $jwtToken", requestBody)
            call.enqueue(object : Callback<ThirdPartyIntegrationResModel> {
                override fun onResponse(
                    call: Call<ThirdPartyIntegrationResModel>,
                    response: Response<ThirdPartyIntegrationResModel>
                ) {
                    if (response.isSuccessful) {
                        Constants.success(requireContext(), "Integration updated successfully")
                        refreshIntegrationListDelayed() // Refresh the integration list
                    } else {
                        Constants.error(requireContext(), "Unsuccessful response: ${response.code()}")
                    }
                }

                override fun onFailure(call: Call<ThirdPartyIntegrationResModel>, t: Throwable) {
                    Constants.error(requireContext(), "Error: ${t.message}")
                }
            })
        } ?: Constants.error(requireContext(), "Token is null. Please log in again.")
    }

}