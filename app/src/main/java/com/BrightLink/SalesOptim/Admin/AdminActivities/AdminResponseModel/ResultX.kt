package com.BrightLink.SalesOptim.Admin.AdminActivities.AdminResponseModel

data class ResultX(
    val actualCounts: Int,
    val month: Int,
    val year: Int
)