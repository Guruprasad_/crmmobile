package com.BrightLink.SalesOptim.Constants

import android.content.Context
import android.content.SharedPreferences
import com.BrightLink.SalesOptim.Model.ProductModel
import com.BrightLink.SalesOptim.Model.SalesOrderInvoiceProductModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

object SharedPreferecneManager {

    private val PREF_NAME = "ProductPrefs"
    private val SALES_PREF_NAME = "SalesPrefs"

    fun removeProduct(context: Context, product: ProductModel) {
        val prefs = context.getSharedPreferences("ProductPrefs", Context.MODE_PRIVATE)
        val editor = prefs.edit()
        val gson = Gson()

        // Get the current list of products from SharedPreferences
        val productListJson = prefs.getString("Product", null)
        val productList = gson.fromJson(productListJson, Array<ProductModel>::class.java)?.toMutableList()
        productList?.remove(product)
        val updatedListJson = gson.toJson(productList)
        editor.putString("Product", updatedListJson)
        editor.apply()
    }


    fun saveItems(context: Context, items: List<ProductModel>) {
        val prefs: SharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        val editor = prefs.edit()
        val gson = Gson()
        val json = gson.toJson(items)
        editor.putString("Product", json)
        editor.apply()
    }

    fun getItems(context: Context): List<ProductModel> {
        val prefs: SharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        val gson = Gson()
        val json = prefs.getString("Product", null)
        val type = object : TypeToken<List<ProductModel>>() {}.type
        return gson.fromJson(json, type) ?: emptyList()
    }


    fun saveSalesOrderInvoiceItems(requireContext: Context, items: MutableList<SalesOrderInvoiceProductModel>) {
        val prefs: SharedPreferences = requireContext.getSharedPreferences(SALES_PREF_NAME, Context.MODE_PRIVATE)
        val editor = prefs.edit()
        val gson = Gson()
        val json = gson.toJson(items)
        editor.putString("SalesItems", json)
        editor.apply()
    }

    fun removeSalesOrderInvoiceProduct(context: Context, lastItem: SalesOrderInvoiceProductModel) {
        val prefs = context.getSharedPreferences("SalesPrefs", Context.MODE_PRIVATE)
        val editor = prefs.edit()
        val gson = Gson()

        val productListJson = prefs.getString("SalesItems", null)
        val productList = gson.fromJson(productListJson, Array<SalesOrderInvoiceProductModel>::class.java)?.toMutableList()
        productList?.remove<Any>(lastItem)
        val updatedListJson = gson.toJson(productList)
        editor.putString("SalesItems", updatedListJson)
        editor.apply()
    }

    fun getSalesItems(context: Context): List<SalesOrderInvoiceProductModel> {
        val prefs: SharedPreferences = context.getSharedPreferences(SALES_PREF_NAME, Context.MODE_PRIVATE)
        val gson = Gson()
        val json = prefs.getString("SalesItems", null)
        val type = object : TypeToken<List<SalesOrderInvoiceProductModel>>() {}.type
        return gson.fromJson(json, type) ?: emptyList()
    }

}