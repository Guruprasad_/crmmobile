package com.BrightLink.SalesOptim.Authenthication

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.BrightLink.SalesOptim.Activities.HomeActivity
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.RequestModel.LoginPostModel
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.permissionx.guolindev.PermissionX
import com.BrightLink.SalesOptim.databinding.ActivityLoginBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.Manifest
import com.BrightLink.SalesOptim.Admin.AdminActivities.AdminHomeActivity
import com.BrightLink.SalesOptim.Constants.CustomDialog
import com.BrightLink.SalesOptim.ResponseModel.LoginModel


class LoginActivity : AppCompatActivity() {

    private lateinit var binding : ActivityLoginBinding
    private lateinit var sharedPreferences: SharedPreferences

    @SuppressLint("InlinedApi")
    @RequiresApi(Build.VERSION_CODES.R)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)



        requestPermissions()

        sharedPreferences = getSharedPreferences("myPrefs",Context.MODE_PRIVATE)
        val savedUsername = sharedPreferences.getString("username", "")
        val savedPassword = sharedPreferences.getString("password", "")

        binding.email.setText(savedUsername)
        binding.password.setText(savedPassword)

        binding.remPassword.isChecked = savedUsername!!.isNotEmpty() && savedPassword!!.isNotEmpty()

        binding.remPassword.setOnCheckedChangeListener{ buttonView , ischecked->

            if (ischecked)
            {
                val username = binding.email.text.toString() // Get username from EditText
                val password = binding.password.text.toString() // Get password from EditText
                saveCredentials(username, password)
            }
            else
            {
                clearCredentials()
            }

        }

        val apiservice = ApiUtilities.getinstance().create(ApiInterface::class.java)
        binding.login.setOnClickListener{
            val loading = CustomDialog(this@LoginActivity)


            val username = binding.email.text.toString()
            val password = binding.password.text.toString()

            if (Validate(username,password))
            {
                loading.show()
                CallAPI(apiservice , username, password , loading)
            }
        }
    }

    private fun saveCredentials(username: String, password: String) {
        val editor = sharedPreferences.edit()
        editor.putString("username", username)
        editor.putString("password", password)
        editor.apply()
    }

    private fun clearCredentials() {
        val editor = sharedPreferences.edit()
        editor.remove("username")
        editor.remove("password")
        editor.apply()
    }

    private fun Validate(username: String, password: String) :Boolean {
        if (username.isEmpty())
        {
            Constants.error(this@LoginActivity,"UserName Required")
            return false
        }
        if (password.isEmpty())
        {
            Constants.error(this@LoginActivity,"Password is empty")
            return false
        }

        return  true
    }

    private fun CallAPI(apiservice: ApiInterface , username : String , password : String , loading : CustomDialog) {

        val model = LoginPostModel(password,username)

        val call = apiservice.login(model)
        call.enqueue(object : Callback<LoginModel>{
            override fun onResponse(call: Call<LoginModel>, response: Response<LoginModel>) {
                if (response.isSuccessful)
                {
                    loading.dismiss()
                    val loginResponse = response.body()
                    Constants.success(this@LoginActivity,"Login Successful")
                    saveToken(loginResponse!!.jwtToken)
                    saveUserId(loginResponse.id)
                    saveUserName(loginResponse.name)
                    saveDetail(loginResponse.email,loginResponse.role,loginResponse.enabled,loginResponse.emailAccess,loginResponse.integrationAccess,
                        loginResponse.trackingAccess,loginResponse.leadDashboardAccess,loginResponse.taskDashboardAccess,loginResponse.pipelineAccess,
                        loginResponse.planName,loginResponse.timeZone,loginResponse.expiryDate,loginResponse.empStatus,loginResponse.empID
                    ,loginResponse.adminId,loginResponse.companyName,loginResponse.companyPhone,loginResponse.companyAddress,loginResponse.companyEmail,
                        loginResponse.gstin)

                    if (loginResponse.role == "ROLE_ADMIN")
                    {
                        startActivity(Intent(this@LoginActivity,AdminHomeActivity::class.java))
                        finish()
                    }
                    else
                    {
                        startActivity(Intent(this@LoginActivity,HomeActivity::class.java))
                        finish()
                    }
                    Log.d("GURUPRASAD",loginResponse.jwtToken)
                }
                else
                {
                    Constants.error(this@LoginActivity,"Failed to Login the user")
                    loading.dismiss()
                }
            }

            override fun onFailure(call: Call<LoginModel>, t: Throwable) {
                Constants.error(this@LoginActivity,"Failed to Login ${t.message}")
                Log.d("GURUPRASAD",t.message.toString())
                loading.dismiss()
            }

        })

    }

    private fun saveDetail(
        email: String,
        role: String,
        enabled: Boolean,
        emailAccess: Int,
        integrationAccess: Int,
        trackingAccess: Int,
        leadDashboardAccess: Int,
        taskDashboardAccess: Int,
        pipelineAccess: Int,
        planName: String,
        timeZone: String,
        expiryDate: String,
        empStatus: String,
        empID: Int,
        adminId: Int,
        companyName: String,
        companyPhone: String,
        companyAddress: String,
        companyEmail: String,
        gstin: String
    ) {

        val sharedPreferences = getSharedPreferences("UserDetails", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("email", email)
        editor.putString("role", role)
        editor.putBoolean("enabled", enabled)
        editor.putInt("emailAccess", emailAccess)
        editor.putInt("integrationAccess", integrationAccess)
        editor.putInt("trackingAccess", trackingAccess)
        editor.putInt("leadDashboardAccess", leadDashboardAccess)
        editor.putInt("taskDashboardAccess", taskDashboardAccess)
        editor.putInt("pipelineAccess", pipelineAccess)
        editor.putString("planName", planName)
        editor.putString("timeZone", timeZone)
        editor.putString("expiryDate", expiryDate)
        editor.putString("empStatus", empStatus)
        editor.putInt("empID", empID)
        editor.putInt("adminId", adminId)
        editor.putString("companyName", companyName)
        editor.putString("companyPhone", companyPhone)
        editor.putString("companyAddress", companyAddress)
        editor.putString("companyEmail", companyEmail)
        editor.putString("gstin", gstin)
        editor.apply()

    }

    private fun saveToken(token: String) {
        val sharedPreferences = getSharedPreferences("Token", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("jwtToken", token)
        editor.apply()
    }

    private fun saveUserId(id: Int) {
        val sharedPreferences = getSharedPreferences("User", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putInt("id", id)
        editor.apply()
    }

    private fun saveUserName(userName: String) {
        val sharedPreferences = getSharedPreferences("UserName", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("name", userName)
        editor.apply()
    }




     fun requestPermissions() {
        PermissionX.init(this)
            .permissions(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.CALL_PHONE,
                Manifest.permission.ACCESS_BACKGROUND_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.SEND_SMS,
                Manifest.permission.POST_NOTIFICATIONS,
                Manifest.permission.READ_CALL_LOG,
                Manifest.permission.WRITE_CALL_LOG,
                Manifest.permission.FOREGROUND_SERVICE,
                Manifest.permission.FOREGROUND_SERVICE_LOCATION
            )
            .onExplainRequestReason { scope, deniedList ->
                // Explain why the permissions are needed
                scope.showRequestReasonDialog(
                    deniedList,
                    "Permissions are needed to access GPS, phone, and storage.",
                    "OK",
                    "Cancel"
                )
            }
            .onForwardToSettings { scope, deniedList ->
                // Forward to app settings page if necessary
                scope.showForwardToSettingsDialog(
                    deniedList,
                    "You need to allow access to these permissions.",
                    "OK",
                    "Cancel"
                )
            }
            .request { allGranted, grantedList, deniedList ->
                // Handle permission request result
               // if (allGranted) {
                    // All permissions are granted
                    // Perform your desired actions here
               // } else {

                    // Some permissions are denied
                    // Handle the denied permissions appropriately
              //  }
            }
    }

}