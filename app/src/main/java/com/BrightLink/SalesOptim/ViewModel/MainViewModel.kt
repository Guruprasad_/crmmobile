package com.BrightLink.SalesOptim.ViewModel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.BrightLink.SalesOptim.ResponseModel.EmpTrackResponseModel
import com.BrightLink.SalesOptim.ResponseModel.GetMapResponseModel
import com.BrightLink.SalesOptim.ResponseModel.Location
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainViewModel : ViewModel() {

    private var LocationLiveData = MutableLiveData<GetMapResponseModel>()
    private var ErrorMessage = MutableLiveData<String>()
    private var EmpLocationTrack = MutableLiveData<EmpTrackResponseModel>()

    fun getCoordinates(token: String) {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            service.getMap("Bearer $token").enqueue(object : Callback<GetMapResponseModel> {
                override fun onResponse(
                    call: Call<GetMapResponseModel>,
                    response: Response<GetMapResponseModel>
                ) {
                    if (response.isSuccessful) {
                        val data = response.body()
                        if (data != null) {
                            LocationLiveData.postValue(data)
                        } else {
                            Log.e("GURU", "Response body is null")
                            ErrorMessage.postValue("Response body is null")
                        }
                    } else {
                        Log.e("GURU", "API Error: ${response.code()} - ${response.message()}")
                        ErrorMessage.postValue("API Error: ${response.code()} - ${response.message()}")
                    }
                }

                override fun onFailure(call: Call<GetMapResponseModel>, t: Throwable) {
                    Log.e("GURU", "Network Error: ${t.message}", t)
                    ErrorMessage.postValue("Network Error: ${t.message}")
                }
            })
    }

    fun EmpTrack(token : String , EmpId : Int) {
        val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
        service.getEmpLocation("Bearer $token" , EmpId).enqueue(object : Callback<EmpTrackResponseModel> {
            override fun onResponse(
                call: Call<EmpTrackResponseModel>,
                response: Response<EmpTrackResponseModel>
            ) {
                if (response.isSuccessful) {
                    val data = response.body()
                    if (data != null) {
                        EmpLocationTrack.postValue(data)
                    } else {
                        Log.e("GURU", "Response body is null")
                        ErrorMessage.postValue("Response body is null")
                    }
                } else {
                    Log.e("GURU", "API Error: ${response.code()} - ${response.message()}")
                    ErrorMessage.postValue("API Error: ${response.code()} - ${response.message()}")
                }
            }

            override fun onFailure(call: Call<EmpTrackResponseModel>, t: Throwable) {
                Log.e("GURU", "Network Error: ${t.message}", t)
                ErrorMessage.postValue("Network Error: ${t.message}")
            }
        })
    }

    fun ObserveCoordinates() : LiveData<GetMapResponseModel>{
        return LocationLiveData
    }

    fun ObserveErrorMessage() : LiveData<String>{
        return ErrorMessage
    }

    fun ObserveEmpTrack() : LiveData<EmpTrackResponseModel>{
        return EmpLocationTrack
    }
}