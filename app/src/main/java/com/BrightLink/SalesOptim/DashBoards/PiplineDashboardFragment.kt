package com.BrightLink.SalesOptim.DashBoards

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.db.funnel_meterchartview.FunnelChartData
import com.ekn.gruzer.gaugelibrary.Range
import com.BrightLink.SalesOptim.Adapters.PiplineTableAdapter
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.Constants.CustomDialog
import com.BrightLink.SalesOptim.Constants.WrapContentLinearLayoutManager
import com.BrightLink.SalesOptim.ResponseModel.PipliineDashboardResponseModel
import com.BrightLink.SalesOptim.ResponseModel.PiplineGuageResponseModelItem
import com.BrightLink.SalesOptim.ResponseModel.PiplineResponseModelItem
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.FragmentPiplineDashboardBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class PiplineDashboardFragment : Fragment() {

    private lateinit var binding:FragmentPiplineDashboardBinding
    private lateinit var adapter : PiplineTableAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentPiplineDashboardBinding.inflate(inflater, container, false);
        val view = binding.root


        binding.actionBar.activityName.text = "Pipeline Dashboard"
        binding.actionBar.back.setOnClickListener{
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }

        binding.actionBar.option.visibility = View.INVISIBLE

        val jwtToken = gettoken(context)
        if (jwtToken!=null)
        {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            CallPiplineChart(service , jwtToken , context)
            CallGuageChart(service , jwtToken , context)
            CallTargetDetails(service , jwtToken , context)
            CallCustomerTable(service , jwtToken , context)
        }
        else{
            Constants.error(context,"Token is null Login Again")
        }

        binding.recyclerView.layoutManager = WrapContentLinearLayoutManager(context)

        return view
    }

    private fun CallCustomerTable(service: ApiInterface, jwtToken: String, context: Context?) {
        val loading = CustomDialog(requireContext())
        loading.show()
        val call = service.getPiplineTable("Bearer $jwtToken")
        call.enqueue(object : Callback<PipliineDashboardResponseModel> {
            override fun onResponse(
                call: Call<PipliineDashboardResponseModel>,
                response: Response<PipliineDashboardResponseModel>
            ) {
                if (isAdded) {
                    if (response.isSuccessful) {
                        val data = response.body()
                        if (data != null) {
                            loading.dismiss()
                            adapter = PiplineTableAdapter(requireContext(),data.pipetable)
                            binding.recyclerView.adapter = adapter

                        } else {
                            Constants.error(context, "Response body is null or empty")
                            loading.dismiss()
                        }
                    } else {
                        Constants.error(context, "Response is not successful")
                        loading.dismiss()
                    }
                }
            }

            override fun onFailure(call: Call<PipliineDashboardResponseModel>, t: Throwable) {
                Constants.error(context, "Error : ${t.message}")
                Log.d("GURU", t.message.toString())
                loading.dismiss()
            }
        })
    }

    private fun CallTargetDetails(service: ApiInterface, jwtToken: String, context: Context?) {
        val loading = CustomDialog(requireContext())
        loading.show()
        val call = service.getTargetDetails("Bearer $jwtToken")
        call.enqueue(object : Callback<PipliineDashboardResponseModel> {
            override fun onResponse(
                call: Call<PipliineDashboardResponseModel>,
                response: Response<PipliineDashboardResponseModel>
            ) {
                if (isAdded) {
                    if (response.isSuccessful) {
                        val data = response.body()
                        if (data != null) {
                            loading.dismiss()
                            binding.leadsTarget.setText(data.targetDetails.get(data.targetDetails.size-1).monthlyLeads.toString())
                            binding.revenueTarget.setText(data.targetDetails.get(data.targetDetails.size-1).monthlyRevenue.toString())
                            binding.winLeadsTarget.setText(data.targetDetails.get(data.targetDetails.size-1).winLeads.toString())
                            binding.startDate.setText(data.targetDetails.get(data.targetDetails.size-1).startDate.toString())
                            binding.endDate.setText(data.targetDetails.get(data.targetDetails.size-1).endDate.toString())

                        } else {
                            Constants.error(context, "Response body is null or empty")
                            loading.dismiss()
                        }
                    } else {
                        Constants.error(context, "Response is not successful")
                        loading.dismiss()
                    }
                }
            }

            override fun onFailure(call: Call<PipliineDashboardResponseModel>, t: Throwable) {
                Constants.error(context, "Error : ${t.message}")
                Log.d("GURU", t.message.toString())
                loading.dismiss()
            }
        })
    }

    private fun CallGuageChart(service: ApiInterface, jwtToken: String, context: Context?) {
        val loading = CustomDialog(requireContext())
        loading.show()
        val call = service.getGuageChart("Bearer $jwtToken")
        call.enqueue(object : Callback<List<PiplineGuageResponseModelItem>> {
            override fun onResponse(
                call: Call<List<PiplineGuageResponseModelItem>>,
                response: Response<List<PiplineGuageResponseModelItem>>
            ) {
                if (isAdded) {
                    if (response.isSuccessful) {
                        val data = response.body()
                        if (data != null) {
                            loading.dismiss()
                            guage(data)
                        } else {
                            loading.dismiss()
                            Constants.error(context, "Response body is null or empty")
                        }
                    } else {
                        loading.dismiss()
                        Constants.error(context, "Response is not successful")
                    }
                }
            }

            override fun onFailure(call: Call<List<PiplineGuageResponseModelItem>>, t: Throwable) {
                Constants.error(context, "Error : ${t.message}")
                Log.d("GURU", t.message.toString())
                loading.dismiss()
            }
        })
    }

    private fun CallPiplineChart(service: ApiInterface, jwtToken: Any, context: Context?) {
        val loading = CustomDialog(requireContext())
        loading.show()
        val call = service.getPiplineChart("Bearer $jwtToken")
        call.enqueue(object : Callback<List<PiplineResponseModelItem>> {
            override fun onResponse(
                call: Call<List<PiplineResponseModelItem>>,
                response: Response<List<PiplineResponseModelItem>>
            ) {
                if (isAdded) {
                    if (response.isSuccessful) {
                        val data = response.body()
                        if (data != null) {
                            loading.dismiss()
                            updateFunnelChartData(data)
                        } else {
                            loading.dismiss()
                            Constants.error(context, "Response body is null or empty")
                        }
                    } else {
                        loading.dismiss()
                        Constants.error(context, "Response is not successful")
                    }
                }
            }

            override fun onFailure(call: Call<List<PiplineResponseModelItem>>, t: Throwable) {
                Constants.error(context, "Error : ${t.message}")
                Log.d("GURU", t.message.toString())
                loading.dismiss()
            }
        })
    }


    private fun guage(data : List<PiplineGuageResponseModelItem>) {
        if (data.isNotEmpty()) {
            val size = data.size - 1
            if (size >= 0) {
                val range = Range()
                range.color = Color.parseColor("#ce0000")
                range.from = 0.0
                range.to = 50.0

                val range2 = Range()
                range2.color = Color.parseColor("#E3E500")
                range2.from = 50.0
                range2.to = 100.0

                val range3 = Range()
                range3.color = Color.parseColor("#00b20b")
                range3.from = 100.0
                range3.to = 150.0

                binding.monthlyLeads.addRange(range)
                binding.monthlyLeads.addRange(range2)
                binding.monthlyLeads.addRange(range3)

                binding.monthlyRevenue.addRange(range)
                binding.monthlyRevenue.addRange(range2)
                binding.monthlyRevenue.addRange(range3)

                binding.WonLeads.addRange(range)
                binding.WonLeads.addRange(range2)
                binding.WonLeads.addRange(range3)


                binding.monthlyLeads.minValue = 0.0
                binding.monthlyLeads.maxValue = data.get(size).monthlyLeads.toDouble()
                binding.monthlyLeads.value = data.get(size).actualLeads.toDouble()
                binding.actualLeads.text = "Logged ${data.get(size).actualLeads} Leads"
                binding.totalLeads.text = "Out of ${data.get(size).monthlyLeads}"

                binding.monthlyRevenue.minValue = 0.0
                binding.monthlyRevenue.maxValue = data.get(size).monthlyRevenue.toDouble()
                binding.monthlyRevenue.value = data.get(size).actualRevenue.toDouble()
                binding.actualRevenue.text = "Got ${data.get(size).actualRevenue}L Revenue"
                binding.totalRevenue.text = "Out of ${data.get(size).monthlyRevenue}L"

                binding.WonLeads.minValue = 0.0
                binding.WonLeads.maxValue = data.get(size).winLeads.toDouble()
                binding.WonLeads.value = data.get(size).actualWonLeads.toDouble()
                binding.actualWonLeads.text = "Won ${data.get(size).actualWonLeads} Deals"
                binding.totalWonLeads.text = "Out of ${data.get(size).winLeads}"

            } else {
                Log.e("GURU", "Empty data list")
            }
        } else {
            Log.e("GURU", "Empty data list")
        }


    }

    private fun updateFunnelChartData(data : List<PiplineResponseModelItem>) {
        val mDataSet = ArrayList<FunnelChartData>()
        val size = data.size-1
        mDataSet.add(FunnelChartData("#4472C4", data.get(size).New_Lead.toString()))
        mDataSet.add(FunnelChartData("#5B9BD5", data.get(size).Contact_Count.toString()))
        mDataSet.add(FunnelChartData("#ED7D31", data.get(size).Qualify_Count.toString()))
        mDataSet.add(FunnelChartData("#FFC000", data.get(size).Proposal_Count.toString()))
        mDataSet.add(FunnelChartData("#CEE4BE", data.get(size).Negociation_Count.toString()))

        binding.funnelChart.setmDataSet(mDataSet)
    }

    private fun gettoken(context: Context?): String? {
        val sharedPreferences = requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
        return sharedPreferences.getString("jwtToken", null)
    }

}