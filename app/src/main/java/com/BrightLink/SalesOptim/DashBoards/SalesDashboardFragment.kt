package com.BrightLink.SalesOptim.DashBoards

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.anychart.AnyChart
import com.anychart.chart.common.dataentry.DataEntry
import com.anychart.chart.common.dataentry.ValueDataEntry
import com.anychart.enums.Align
import com.anychart.enums.LegendLayout
import com.BrightLink.SalesOptim.Constants.Constants
import com.BrightLink.SalesOptim.Constants.CustomDialog
import com.BrightLink.SalesOptim.ResponseModel.SalesDashBoardRevenueBarChartResponseModelItem
import com.BrightLink.SalesOptim.ResponseModel.SalesDashboardResp
import com.BrightLink.SalesOptim.ResponseModel.SalesDashboardRevenueByLeadResponseModelItem
import com.BrightLink.SalesOptim.ResponseModel.SalesDashboardWinRateReponseModel
import com.BrightLink.SalesOptim.Retrofit.ApiInterface
import com.BrightLink.SalesOptim.Retrofit.ApiUtilities
import com.BrightLink.SalesOptim.databinding.FragmentSalesDashboardBinding
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.LegendEntry
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.ValueFormatter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale

class SalesDashboardFragment : Fragment() {

    private lateinit var binding:FragmentSalesDashboardBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentSalesDashboardBinding.inflate(inflater, container, false);
        val view = binding.root

        binding.actionBar.activityName.text = "Sales Dashboard"
        binding.actionBar.back.setOnClickListener{
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }

        val jwtToken = gettoken(context)
        if (jwtToken!=null)
        {
            val service = ApiUtilities.getinstance().create(ApiInterface::class.java)
            CallRevenueBarChartApi(service , jwtToken , context)
            CallWinRateChartApi(service , jwtToken , context)
            CallForecastChartApi(service , jwtToken , context)
            CallRevenueByLeadPieChart(service , jwtToken , context)
        }
        else{
            Constants.error(context,"Token is null Login Again")
        }

        binding.actionBar.option.visibility = View.INVISIBLE
        return view
    }

    private fun CallRevenueByLeadPieChart(service: ApiInterface, jwtToken: String, context: Context?) {
        val loading = CustomDialog(requireContext())
        loading.show()
        val call = service.getSalesDashboardRevenueByLead("Bearer $jwtToken")
        call.enqueue(object : Callback<List<SalesDashboardRevenueByLeadResponseModelItem>> {
            override fun onResponse(
                call: Call<List<SalesDashboardRevenueByLeadResponseModelItem>>,
                response: Response<List<SalesDashboardRevenueByLeadResponseModelItem>>
            ) {
                if (response.isSuccessful) {
                    val data = response.body()
                    if (data != null) {
                        loading.dismiss()
                        setupPieChart(data)
                    } else {
                        Constants.error(context, "Response body is null or empty")
                        loading.dismiss()
                    }
                } else {
                    Constants.error(context, "Response is not successful")
                    loading.dismiss()
                }
            }

            override fun onFailure(call: Call<List<SalesDashboardRevenueByLeadResponseModelItem>>, t: Throwable) {
                Constants.error(context, "Error: ${t.message}")
                Log.d("GURU", t.message.toString())
                loading.dismiss()
            }
        })


    }

    private fun setupPieChart(data: List<SalesDashboardRevenueByLeadResponseModelItem>) {
        val colors = listOf(
            Color.GREEN,
            Color.rgb(120,93,208),
            Color.rgb(255,69,96),
            Color.rgb(254,176,24),
            Color.rgb(0,226,150),
            Color.rgb(161,2,196),
            Color.rgb(1,143,251)
        )

        val entries = mutableListOf<PieEntry>()
        data.forEach { item ->
            entries.add(PieEntry(item.coldCallCount.toFloat(), "Cold Call"))
            entries.add(PieEntry(item.linkedinCount.toFloat(), "LinkedIn"))
            entries.add(PieEntry(item.referenceCount.toFloat(), "Reference"))
            entries.add(PieEntry(item.otherCount.toFloat(), "Other"))
            entries.add(PieEntry(item.facebookCount.toFloat(), "Facebook"))
            entries.add(PieEntry(item.instagramCount.toFloat(), "Instagram"))
            entries.add(PieEntry(item.emailCount.toFloat(), "Email"))
        }
        val dataSet = PieDataSet(entries, "Revenue By Lead")
        dataSet.colors = colors

        val legendEntries = mutableListOf<LegendEntry>()
        entries.forEachIndexed { index, entry ->
            legendEntries.add(LegendEntry(entry.label, Legend.LegendForm.DEFAULT, 10f, 2f, null, colors[index]))
        }

        val legend = binding.Revenuepiechart.legend
        legend.isWordWrapEnabled = true
        legend.setCustom(legendEntries)

        val data = PieData(dataSet)
        binding.Revenuepiechart.data = data
        binding.Revenuepiechart.description.isEnabled = false
        binding.Revenuepiechart.setUsePercentValues(true)
        binding.Revenuepiechart.setDrawEntryLabels(false)
        binding.Revenuepiechart.invalidate()
    }





    private fun CallForecastChartApi(service: ApiInterface, jwtToken: String, context: Context?) {
        val loading = CustomDialog(requireContext())
        loading.show()
        val call = service.getSalesDashboardForecast("Bearer $jwtToken")
        call.enqueue(object : Callback<SalesDashboardResp> {
            override fun onResponse(
                call: Call<SalesDashboardResp>,
                response: Response<SalesDashboardResp>
            ) {
                if (response.isSuccessful) {
                    val data = response.body()
                    if (data != null) {
                        loading.dismiss()
                        forecastBarChart(data)
                    } else {
                        loading.dismiss()
                        Constants.error(context, "Response body is null or empty")
                    }
                } else {
                    Constants.error(context, "Response is not successful")
                    loading.dismiss()
                }
            }

            override fun onFailure(call: Call<SalesDashboardResp>, t: Throwable) {
                Constants.error(context, "Error: ${t.message}")
                Log.d("GURU", t.message.toString())
                loading.dismiss()
            }
        })
    }


    private fun forecastBarChart(data: SalesDashboardResp?) {
        if (data?.resultList != null) {

            val resultList = ArrayList<BarEntry>()
            val achievementList = ArrayList<BarEntry>()
            val labels = ArrayList<String>()

            for (item in data.resultList) {
                val monthYear =
                    (item.year * 12) + item.month // Convert year-month to months since some initial reference point
                resultList.add(BarEntry(monthYear.toFloat(), item.opportunityCount.toFloat()))
                achievementList.add(BarEntry(monthYear.toFloat(), item.achievementCount.toFloat()))
                val calendar = Calendar.getInstance()
                calendar.set(Calendar.MONTH, item.month - 1) // Adjust month index
                calendar.set(Calendar.YEAR, item.year)
                val formattedDate = SimpleDateFormat("MMM", Locale.getDefault()).format(calendar.time)
                labels.add(formattedDate)
            }




            val resultDataSet = BarDataSet(resultList, "Opportunity Count")
            resultDataSet.color = Color.rgb(1, 124, 217)
            resultDataSet.valueTextColor = Color.BLACK

            val achievementDataSet = BarDataSet(achievementList, "Achievement Count")
            achievementDataSet.color = Color.rgb(124, 1, 217)
            achievementDataSet.valueTextColor = Color.BLACK

            val barData = BarData(resultDataSet, achievementDataSet)

            // Initialize chart properly
            val chart = binding.multiplebarchart
            chart.data = barData

            // Configure chart

            val xAxis: XAxis = binding.salesBarChart.xAxis
            xAxis.position = XAxis.XAxisPosition.BOTTOM
            xAxis.valueFormatter = object : ValueFormatter() {
                override fun getFormattedValue(value: Float): String {
                    // Ensure value is within the labels array bounds
                    val index = value.toInt()
                    return if (index >= 0 && index < labels.size) {
                        labels[index]
                    } else {
                        ""
                    }
                }
            }

            val yAxisLeft = chart.axisLeft
            yAxisLeft.setDrawGridLines(false)
            yAxisLeft.valueFormatter = object : ValueFormatter() {
                override fun getFormattedValue(value: Float): String {
                    return value.toInt().toString()
                }
            }

            chart.axisRight.isEnabled = false
            chart.description.isEnabled = false
            chart.animateY(1000)
        }else
        {
            Log.e("forecastBarChart", "Data or resultList is null")
        }
    }


    private fun CallWinRateChartApi(service: ApiInterface, jwtToken: String, context: Context?) {
        val loading = CustomDialog(requireContext())
        loading.show()
        val call = service.getSalesDashboardWinRate("Bearer $jwtToken")
        call.enqueue(object : Callback<SalesDashboardWinRateReponseModel> {
            override fun onResponse(
                call: Call<SalesDashboardWinRateReponseModel>,
                response: Response<SalesDashboardWinRateReponseModel>
            ) {
                if (isAdded)
                {
                    if (response.isSuccessful)
                    {
                        val data  = response.body()
                        Log.d("RESPONSE",response.body().toString())
                        if (data != null)
                        {
                            loading.dismiss()
                            WinRatePieChart(data)
                        }
                        else{
                            Constants.error(context, "Response body is null or empty")
                            loading.dismiss()
                        }
                    }
                    else
                    {
                        Constants.error(context,"Response is not successful")
                        loading.dismiss()
                    }
                }

            }

            override fun onFailure(call: Call<SalesDashboardWinRateReponseModel>, t: Throwable) {
                Constants.error(context,"Error : ${t.message}")
                Log.d("GURU",t.message.toString())
                loading.dismiss()
            }

        })
    }

    private fun WinRatePieChart(data: SalesDashboardWinRateReponseModel) {
        val wonLeadPercentage = data.wonLeadPercentage.toFloat()

        val pie = AnyChart.pie()
        val dataEntries = arrayListOf(
            ValueDataEntry("Won Leads", wonLeadPercentage),
            ValueDataEntry("Lost Leads", 100 - wonLeadPercentage)
        )
        pie.data(dataEntries as List<DataEntry>?)

        // Customize the chart
        pie.title("Win Rate Percentage")
        pie.labels().position("outside")
        pie.legend().position("right")
        pie.legend().itemsLayout(LegendLayout.VERTICAL)
        pie.legend().align(Align.CENTER)
        pie.palette(arrayOf("#4CAF50", "#E57373"))

        binding.pieChart.setChart(pie)
    }
    private fun CallRevenueBarChartApi(service: ApiInterface, jwtToken: String, context: Context?) {

        val loading = CustomDialog(requireContext())
        loading.show()

        val call = service.getSalesDashboardRevenue("Bearer $jwtToken")
        call.enqueue(object : Callback<List<SalesDashBoardRevenueBarChartResponseModelItem>> {
            override fun onResponse(
                call: Call<List<SalesDashBoardRevenueBarChartResponseModelItem>>,
                response: Response<List<SalesDashBoardRevenueBarChartResponseModelItem>>
            ) {
                if (isAdded)
                {
                    if (response.isSuccessful)
                    {
                        val data  = response.body()
                        if (data != null && data.isNotEmpty())
                        {
                            loading.dismiss()
                            RevenueBarChart(data)
                        }
                        else{
                            loading.dismiss()
                            Constants.error(context, "Response body is null or empty")
                        }
                    }
                    else
                    {
                        loading.dismiss()
                        Constants.error(context,"Response is not successful")
                    }
                }

            }

            override fun onFailure(call: Call<List<SalesDashBoardRevenueBarChartResponseModelItem>>, t: Throwable) {
                Constants.error(context,"Error : ${t.message}")
                Log.d("GURU",t.message.toString())
                loading.dismiss()
            }

        })

    }


    private fun RevenueBarChart(data: List<SalesDashBoardRevenueBarChartResponseModelItem>) {
        if (data.isNotEmpty()) {
            val entries = ArrayList<BarEntry>()
            val labels = ArrayList<String>()

            data.forEachIndexed { index, item ->
                val revenue = item.revenue1.toFloat()
                entries.add(BarEntry(index.toFloat(), revenue))


                // Format the month and year
                val calendar = Calendar.getInstance()
                calendar.set(Calendar.MONTH, item.month - 1) // Adjust month index
                calendar.set(Calendar.YEAR, item.year)
                val formattedDate = SimpleDateFormat("MMM", Locale.getDefault()).format(calendar.time)
                labels.add(formattedDate)
            }

            val dataSet = BarDataSet(entries, "")
            dataSet.color = Color.rgb(1, 124, 217)
            dataSet.valueTextColor = Color.BLACK

            val barData = BarData(dataSet)
            binding.salesBarChart.data = barData

            val xAxis: XAxis = binding.salesBarChart.xAxis
            xAxis.position = XAxis.XAxisPosition.BOTTOM
            xAxis.valueFormatter = object : ValueFormatter() {
                override fun getFormattedValue(value: Float): String {
                    // Ensure value is within the labels array bounds
                    val index = value.toInt()
                    return if (index >= 0 && index < labels.size) {
                        labels[index]
                    } else {
                        ""
                    }
                }
            }
            val yAxisLeft = binding.salesBarChart.axisLeft
            yAxisLeft.setDrawGridLines(false)
            yAxisLeft.valueFormatter = object : ValueFormatter() {
                override fun getFormattedValue(value: Float): String {
                    return "${value.toInt()}K" // Format value as currency
                }
            }

            binding.salesBarChart.axisRight.isEnabled = false
            binding.salesBarChart.description.isEnabled = false
            binding.salesBarChart.animateY(1000)
        } else {
            Log.e("SalesDashboardFragment", "Data list is empty")
        }
    }





    private fun gettoken(context: Context?): String? {
        val sharedPreferences = requireContext().getSharedPreferences("Token", Context.MODE_PRIVATE)
        return sharedPreferences.getString("jwtToken", null)
    }

}